# CoevRJ

CoevRJ is a new Bayesian framework to analyze a nucleotide alignment and jointly estimate:

1. How many and which pairs of sites co-evolve thus differentiating from a background model of independent evolution. 
2. The parameters of independent and dependent substitution models.
3. The underlying phylogenetic tree describing the relationships between sequences.  

This method captures the reciprocal effects of the phylogeny, the pairs of coevolving sites and the independent sites while accounting for parameter uncertainties.

## Tutorial

### Basic
* [Installing CoevRJ](documentation/installation.md)
* [Using CoevRJ (command line)](documentation/execution.md)
* [CoevRJ outputs and post-processing](documentation/outputs.md)

### Advanced
* [Using CoevRJ XML configuration files](documentation/configXML.md)
* [Obtaining a consensus tree](documentation/consensus.md)
* [Using restart files](documentation/restart.md)

## Data
### Simulated datasets
Example of simulated datasets can be found in the `data/simulation` folder.
### 16S ribosomal RNA
The alignment, consensus trees obtained with CoevRJ and `restart` files can be found in the `data/16SRNA` folder.
This alignment is stored here for convenience and was published in [Yeang et al., 2007](https://doi.org/10.1093/molbev/msm142).

### Protein-coding gene from eukyarotes
The alignments can be found in the `data/Eukaryotes` folder.
These alignments have been collected from [Ensembl! Release 67](http://may2012.archive.ensembl.org/index.html) and [Selectome](https://selectome.unil.ch). The alignment archived for convenience on this GIT repositiory have been subject to a filtering step where all the fully gapped positions (position with gap accross all species) were filtered out of the alignments.


## How to cite

Meyer X., Dib L., Silvestro D. and Salamin N. Simultaneous Bayesian inference of phylogeny and molecular coevolution. 2018.