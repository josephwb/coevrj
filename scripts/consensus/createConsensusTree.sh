#/bin/bash

TREE_ORIGINAL=$1

TREE_FILE_HOGAN=$2
TREE_FILE_MRBAYES=$3

CONSENSUS_HOGAN=$4
CONSENSUS_MRBAYES=$5

MCCT_HOGAN=$6
MCCT_MRBAYES=$7

OUTPUT_FILE=$8

TMP_TREE_FILES=tempTreeFilesSim.txt

N_TREE_DRAW=1000




###########################################
### HOGAN
###########################################

# Draw trees
python /home/meyerx/Projet/CoevRJMCMC/LindaScripts/AnalyzeRJMCMC/AnalyzeRJMCMC/ScriptXavier/ConsensusTrees/drawTreesForConsensus.py $TREE_FILE_HOGAN $TMP_TREE_FILES $N_TREE_DRAW

# Summarize tree
rm ${CONSENSUS_HOGAN}
rm ${MCCT_HOGAN}
sumtrees.py --burnin 200 ${TMP_TREE_FILES} -o ${CONSENSUS_HOGAN}
sumtrees.py --burnin 200 -s mcct ${TMP_TREE_FILES} -o ${MCCT_HOGAN}

###########################################
### MrBayes
###########################################

rm ${CONSENSUS_MRBAYES}
rm ${MCCT_MRBAYES}
sumtrees.py --burnin 200 ${TREE_FILE_MRBAYES} -o ${CONSENSUS_MRBAYES} 
sumtrees.py --burnin 200 -s mcct ${TREE_FILE_MRBAYES} -o ${MCCT_MRBAYES}

###########################################
### PLOT
###########################################

# plot tree
Rscript /home/meyerx/Projet/CoevRJMCMC/LindaScripts/AnalyzeRJMCMC/AnalyzeRJMCMC/ScriptXavier/ConsensusTrees/plotTrees.R ${TREE_ORIGINAL} ${CONSENSUS_HOGAN} ${CONSENSUS_MRBAYES} ${OUTPUT_FILE}_cons

Rscript /home/meyerx/Projet/CoevRJMCMC/LindaScripts/AnalyzeRJMCMC/AnalyzeRJMCMC/ScriptXavier/ConsensusTrees/plotTrees.R ${TREE_ORIGINAL} ${MCCT_HOGAN} ${MCCT_MRBAYES} ${OUTPUT_FILE}_mcct


