#!/usr/bin/python

import sys
import random
import numpy as np
from subprocess import PIPE,Popen

#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)

BASE_NAME = sys.argv[1]

# file to read
logFile = BASE_NAME + "_0.log"
logCoevFile = BASE_NAME + "_0.logCoev"

# get last line
proc = Popen(['head', '-1', logFile], stdout=PIPE)
firstLineLog = proc.communicate()[0]

proc = Popen(['tail', '-1', logFile], stdout=PIPE)
lastLineLog = proc.communicate()[0]

proc = Popen(['tail', '-1', logCoevFile], stdout=PIPE)
lastLineLogCoev = proc.communicate()[0]

# Header
headers = firstLineLog.split('\t')
iStart = headers.index("StFreq (A)")
iEnd = len(headers)

# Get params
nParams = (iEnd-1)-iStart
params = []

values = lastLineLog.split()

for iV in range(iStart, iEnd-1):
	params.append(values[iV])

# Get Coev params
coevParams = lastLineLogCoev.split()
nCoev = int(coevParams[1])
coevClusters = []

for iC in range(0, nCoev):
	cluster = []
	offset = 2 + iC*8

	positions = coevParams[offset].split('_')
	cluster.append(int(positions[0])) # Position P0
	cluster.append(int(positions[1])) # Position P1

	# Profile, r1, r2, s, d
	cluster.append(coevParams[offset+3])

	coevClusters.append(cluster)

# restart file
restartFile = BASE_NAME + ".restart"
outFile = open(restartFile, 'w')

# header
outFile.write(repr(nParams))
outFile.write("\t")
outFile.write(repr(nCoev))
outFile.write("\n")

# params
for p in params:
	outFile.write(repr(float(p)))
	outFile.write("\t")
outFile.write("\n")

# coev clusters
for cluster in coevClusters:
	cluster =  map(str, cluster)
	line = "\t".join(cluster)
	outFile.write(line)
	outFile.write("\n")
