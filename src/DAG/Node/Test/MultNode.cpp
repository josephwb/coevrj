//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MultNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "MultNode.h"

#include <stddef.h>

namespace DAG {

MultNode::MultNode() : BaseNode() {
	res = 1;
}

MultNode::~MultNode() {
}

double MultNode::getRes() const {
	return res;
}

void MultNode::doProcessing() {
	res = 1;
	for(uint i=0;i<values.size(); ++i){
		res *= values[i];
	}
	//cout << "MultNode [" << getId() << "] processing -> res=" << res << endl;
}

bool MultNode::processSignal(BaseNode* aChild) {
	bool isProcessed = false;

	if(values.empty()) {
		values.assign(BaseNode::getNChildren(), 0.);
	}
	size_t rowId = BaseNode::getChildrenRow(aChild);
	const type_info &childtype = typeid(*aChild);

	if(childtype == typeid(LoadNode)){
		LoadNode *ln = static_cast<LoadNode*>(aChild);
		values[rowId] = ln->getValue();
		//cout << "MultNode [" << getId() << "] process signal from LoadNode [" << aChild->getId() << "] with rowID=" << rowId << " and value val=" << values[rowId] << endl;
		isProcessed = true;
	}

	return isProcessed;
}

} /* namespace DAG */
