//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseScheduler.cpp
 *
 * @date Dec 9, 2014
 * @author meyerx
 * @brief
 */
#include "BaseScheduler.h"

#include <assert.h>
#include <stddef.h>
#include <boost/dynamic_bitset.hpp>

#include "DAG/Node/Base/BaseNode.h"

namespace DAG {
namespace Scheduler {

BaseScheduler::BaseScheduler(BaseNode* const aRoot) : root(aRoot) {
	initDAG();
	initTopoList();
}

BaseScheduler::~BaseScheduler() {

}

void BaseScheduler::initDAG() {
	set<size_t> exploredNodes;
	bool resetNodeState = true;
	findLeaves(root, exploredNodes, resetNodeState);

	readyNodes.clear();
	readyNodes.insert(readyNodes.begin(), leaves.begin(), leaves.end());

	listBNode_t::iterator it;
	for(it=readyNodes.begin(); it != readyNodes.end(); ++it) {
		(*it)->scheduled();
	}
}

bool BaseScheduler::resetPartial() {
	readyNodes.clear();
	findReady(root);
	/*std::cout << "RdyNodes : " << std::endl;
	for(listBNode_t::iterator it=readyNodes.begin(); it != readyNodes.end(); ++it) {
		std::cout << (*it)->toString() << std::endl;
	}
	std::cout << std::endl;
	getchar();*/

	return !readyNodes.empty();
}

bool BaseScheduler::resetPartial(const std::set<BaseNode*> &updatedNodes) {
	readyNodes.clear();

	std::set<BaseNode*>::const_iterator it;
	for(it=updatedNodes.begin(); it != updatedNodes.end(); ++it) {
		BaseNode *curNode = *it;
		if(curNode->isUpdated()) {
			// Set him to scheduled
			curNode->scheduled();
			// Update its parents
			updateParentsState(curNode);
		} else { // The node is already pending, no need to signal updated
			assert(curNode->isPending());
		}
	}

	// We only add scheduled node :
	// This is is done there so we don't register updated node that have an updated child
	for(it=updatedNodes.begin(); it != updatedNodes.end(); ++it) {
		BaseNode *curNode = *it;
		if(curNode->isScheduled()) {
			readyNodes.push_back(curNode);
		}
	}

	/*std::cout << "************* RESET PARTIAL ***************" << std::endl; // DEBUG SCHED COEVRJ

	std::cout << "UpdNodes : " << std::endl;  // FIXME DEBUG REM
	for(std::set<BaseNode*>::const_iterator it=updatedNodes.begin(); it != updatedNodes.end(); ++it) {
		std::cout << (*it)->toString() << std::endl;
	}
	std::cout << std::endl;

	std::cout << "RdyNodes : " << std::endl;
	for(listBNode_t::iterator it=readyNodes.begin(); it != readyNodes.end(); ++it) {
		std::cout << (*it)->toString() << std::endl;
	}
	std::cout << std::endl;
	std::cout << "-------------------------------------------" << std::endl; // DEBUG SCHED COEVRJ
	std::cout << root->subtreeToString() << std::endl; // DEBUG SCHED COEVRJ
	std::cout << "********************************************" << std::endl; // DEBUG SCHED COEVRJ
	//getchar(); // FIXME DEBUG REM*/

	return !readyNodes.empty();
}

void BaseScheduler::process(bool doFakeComputation) {
	if(readyNodes.empty()) return;

	doProcessing(doFakeComputation);
}

void BaseScheduler::resetDAG() {
	readyNodes.clear();
	resetNode(root);
	readyNodes.insert(readyNodes.begin(), leaves.begin(), leaves.end());

	listBNode_t::iterator it;
	for(it=readyNodes.begin(); it != readyNodes.end(); ++it) {
		(*it)->scheduled();
	}

}

void BaseScheduler::resetNode(BaseNode *node) {
	node->resetState();
	for(size_t i=0; i<node->getNChildren(); ++i) {
		resetNode(node->getChild(i));
	}
}

bool BaseScheduler::findReady(BaseNode *node) {
	bool reqProc = false;
	for(size_t i=0; i<node->getNChildren(); ++i) {
		bool isChildUpdated = findReady(node->getChild(i));
		reqProc = reqProc || isChildUpdated;

		if(isChildUpdated) {
			node->childUpdated(i);
		}
	}

	// Node is updated and child are untouched (and not already in nodeRdy)
	if(node->isUpdated() && !reqProc && (std::find(readyNodes.begin(), readyNodes.end(), node) == readyNodes.end())) {
		//node->ready();
		readyNodes.push_back(node);
		node->scheduled();
		return true;
	} else if(reqProc){ // Child have been updated
		return true;
	} else if(node->isScheduled()){ // node has already been visited and is sheduled
		return true;
	} else {// else remain unchanged (no update and no required processing from children)
		return false;
	}
}

void BaseScheduler::updateParentsState(BaseNode *node) {
	// For each parent node
	for(size_t i=0; i<node->getNParent(); ++i) {
		BaseNode* pNode = node->getParent(i);
		// Keep track of parent state
		BaseNode::state_t pState = pNode->getState();

		// Signal my parent that I'm updated
		pNode->childUpdated(node);
		// Parent is now in pending state

		// If the parent was not already pending/scheduled before...
		if(pState != BaseNode::PENDING && pState != BaseNode::SCHEDULED) {
			// It was either DONE or UPDATED
			// And we have thus to signal that it now requires to be computed
			updateParentsState(pNode);
		} // Else the node is pending and thus wait already to be computed
	}
}


bool BaseScheduler::findNodeToProcess(BaseNode *node, std::vector<BaseNode*> &mustBeProcessed) {
	bool reqProc = false;
	for(size_t i=0; i<node->getNChildren(); ++i) {
		bool isChildUpdated = findNodeToProcess(node->getChild(i), mustBeProcessed);
		reqProc = reqProc || isChildUpdated;
	}

	if(node->isUpdated() || reqProc ) { // Node is updated or child is updated
		// If ot already in the mustBeProc list
		if(std::find(mustBeProcessed.begin(), mustBeProcessed.end(), node) == mustBeProcessed.end()) {
			mustBeProcessed.push_back(node); // Insert it in the list
		}
		return true; // This node has to be proc
	} else {// else remain unchanged (no update and no required processing from children)
		return false;
	}
}

void BaseScheduler::findLeaves(BaseNode *node, std::set<size_t> &exploredNodes, bool resetNodeState) {

	//std::cout << "Exploring : " << node->toString() << std::endl;

	// The second element return of the pair return TRUE when the element was successfully inserted and FALSE otherwise
	std::pair<std::set<size_t>::iterator, bool> result = exploredNodes.insert(node->getId());
	if(!result.second) {
		cerr << "Error in : BaseScheduler::findLeaves(BaseNode *node, set<size_t> &exploredNodes)" << endl;
		cerr << "There is a cycle in the DAG at node [" << node->getId() << "]" << endl;
		abort();
	}

	if(resetNodeState) {
		node->resetState();
	}

	if(node->isLeaf() && (std::find(leaves.begin(), leaves.end(), node) == leaves.end())) {
		leaves.push_back(node);
	} else {
		for(size_t i=0; i<node->getNChildren(); ++i) {
			findLeaves(node->getChild(i), exploredNodes, resetNodeState);
		}
	}

	exploredNodes.erase(node->getId());
}

/// WARNING : This is not safe if there are cycle in the DAG
void BaseScheduler::findLeavesFast(BaseNode *node, size_t depth, const size_t MAX_DEPTH) {

	depth++;

	assert(depth < MAX_DEPTH && "There is a cycle in the DAG [DAG depth seems greater than the number of nodes].");

	if(node->isLeaf() && (std::find(leaves.begin(), leaves.end(), node) == leaves.end())) {
		leaves.push_back(node);
	} else {
		for(size_t i=0; i<node->getNChildren(); ++i) {
			findLeavesFast(node->getChild(i), depth, MAX_DEPTH);
		}
	}
}

std::vector<double> BaseScheduler::getTimeStatistics() const {

	typedef std::map<size_t, size_t> mapSizeT_t;
	mapSizeT_t inDegree;
	typedef std::map<size_t, double> mapDbl_t;
	mapDbl_t distance;
	typedef std::list<const BaseNode*> list_t;
	typedef list_t::iterator itList_t;
	list_t queue;

	std::vector<double> stats(2, 0.);

	queue.insert(queue.begin(), leaves.begin(), leaves.end());
	for(itList_t it=queue.begin(); it!=queue.end(); ++it) {
		inDegree[(*it)->getId()] = (*it)->getNChildren();
		distance[(*it)->getId()] = (*it)->getAverageTime();
		stats[0] += (*it)->getAverageTime();
	}

	while(!queue.empty()) {
		// Take first element of queue
		const BaseNode *node = queue.front();
		queue.pop_front();

		double myDist = distance[node->getId()];

		// For each parents :
		for(size_t iP=0; iP<node->getNParent(); ++iP) {
			const BaseNode *parent = node->getParent(iP);
			double edgeCost = parent->getAverageTime();
			// If never seen, init. its in inDegree and distance
			if(inDegree.find(parent->getId()) == inDegree.end()){
				inDegree[parent->getId()] = parent->getNChildren();
				distance[parent->getId()] = 0.;
				stats[0] += parent->getAverageTime();
			}

			distance[parent->getId()] = std::max(distance[parent->getId()], myDist+edgeCost);
	        inDegree[parent->getId()] -= 1;
	        if(inDegree[parent->getId()] == 0) {
	        	queue.push_back(parent);
	        }
		}
	}

	stats[1] = distance[root->getId()];

	return stats;
}

const std::list<BaseNode*>& BaseScheduler::getTopologicalList() const {
	return topoList;
}


void BaseScheduler::initTopoList() {
	if(!topoList.empty()) return;

	typedef std::map<size_t, size_t> mapSizeT_t;
	mapSizeT_t inDegree;

	typedef std::list<BaseNode*> list_t;
	typedef list_t::iterator itList_t;
	list_t queue;
	queue.insert(queue.begin(), leaves.begin(), leaves.end());

	for(itList_t it=queue.begin(); it!=queue.end(); ++it) {
		inDegree[(*it)->getId()] = (*it)->getNChildren();
		(*it)->setLevel(1);
	}

	while(!queue.empty()) {
		// Take first element of queue
		BaseNode *node = queue.front();
		queue.pop_front();

		// Add it to topological list
		topoList.push_back(node);

		// For each parents :
		for(size_t iP=0; iP<node->getNParent(); ++iP) {
			BaseNode *parent = node->getParent(iP);
			// If never seen, init. its in inDegree
			if(inDegree.find(parent->getId()) == inDegree.end()){
				inDegree[parent->getId()] = parent->getNChildren();
			}
			// Update in degree
	        inDegree[parent->getId()] -= 1; // Count the number of visits

	        // Update level
	        if(parent->getLevel() < node->getLevel() + 1) {
	        	parent->setLevel(node->getLevel() + 1);
	        }

	        // Add to queue if all children have signaled (in degree equals 0)
	        if(inDegree[parent->getId()] == 0) {
	        	queue.push_back(parent);
	        }
		}
	}
}

void BaseScheduler::defaultReinitialize(reinitStrategy_t reinitStrat, bool reinitTopo) {

	if(reinitStrat == HARD_REINIT) {
		// Reinit all
		leaves.clear();
		readyNodes.clear();
		initDAG();

		topoList.clear();
		initTopoList();
	} else if(reinitStrat == PARTIAL_REINIT) {
		// Reinit leaves without touching node states
		leaves.clear();

		// All false by default
		//boost::dynamic_bitset<> exploredNodes(BaseNode::getBiggestBaseNodeID());
		//findLeavesFastSafe(root, exploredNodes);

		size_t depth = 0;
		// The max depth can be defined by the biggest possible Node ID
		// In linear graph the max depth would be equal to this ID + 2
		// We use +100 to make sure.
		const size_t MAX_DEPTH = BaseNode::getBiggestBaseNodeID() + 100;
		findLeavesFast(root, depth, MAX_DEPTH);

		if(reinitTopo) { // Reinit topo list if requested
			topoList.clear();
			initTopoList();
		}
	} else if(reinitStrat == SOFT_REINIT) {
		if(reinitTopo) { // Reinit topo list if requested
			topoList.clear();
			initTopoList();
		}
	} else {
		assert(false && "Strategy not implemented");
	}
}

} /* namespace Scheduler */
} /* namespace DAG */
