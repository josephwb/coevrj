//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//============================================================================
// Name        : main.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#define DEBUG_MC3 1


#include <vector>
#include <iostream>
#include <fstream>

#include "Model/Parameter/Parameters.h"
#include "Model/Prior/Priors.h"
#include "Model/Model.h"
#include "Sampler/Samples/Samples.h"
#include "Sampler/IncSamplers.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Likelihood/LikelihoodFactory.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "Utils/XML/ReaderXML.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Logger/LogAdapter.h"

#include "Sampler/LightSampler.h"
#include "Sampler/Proposal/Proposals.h"
#include "Sampler/Proposal/ProposalPFAMCMC.h"
#include "Sampler/TraceWriter/TextWriter.h"

using namespace Sampler::Strategies;
using namespace StatisticalModel;
using namespace StatisticalModel::Likelihood;


std::string createRunXML(int nIteration, std::string &fastaFileName, std::string &outputFile) {

	int nP = Parallel::mpiMgr().getNProc();

	std::stringstream ss;
	ss << "<config method=\"MCMC\">" << std::endl;

	ss << "    <topology>" << std::endl;
	ss << "        <nChain>" << nP << "</nChain>" << std::endl;
	ss << "    </topology>" << std::endl;

	ss << "    <seed>1234</seed>" << std::endl;

	ss << "    <nIterations>" << nIteration << "</nIterations>" << std::endl;

	ss << "    <likelihood name=\"CoevReversibleJump\">" << std::endl;
	ss << "        <nGamma>4</nGamma>" << std::endl;
	ss << "        <sampleTreeTopology>1</sampleTreeTopology> <!-- 0 for no, 1 for yes-->" << std::endl;
	ss << "        <alignFile>" << fastaFileName << "</alignFile>" << std::endl;
	ss << "		<useScoreCaching>true</useScoreCaching>" << std::endl;
	ss << "    </likelihood>" << std::endl;

	ss << "    <sampler name=\"RJMCMCSampler\">" << std::endl;

	ss << "        <blocks name=\"default\"> " << std::endl;
	ss << "            <blockSize>8</blockSize> <!-- Number of parameters per blocks -->" << std::endl;
	ss << "            <adaptiveType>PCA_Adaptive</adaptiveType>" << std::endl;
	ss << "        </blocks>" << std::endl;

	ss << "         <proposal name=\"PFAMCMC\"> " << std::endl;
	ss << "            <treeMoveProportion>0.10</treeMoveProportion>" << std::endl;
	ss << "            <frequency>0.6</frequency>" << std::endl;
	ss << "        </proposal>" << std::endl;

	ss << "       <proposal name=\"ReversibleJumpMCMC\">" << std::endl;
	ss << "			<burnin>0</burnin> <!-- Number of Reversible jump to skip. Enable fast convergence of GTR parameters. -->" << std::endl;
	ss << "            <frequency>0.</frequency> <!-- Frequency of this type of moves compared to PFAMCMC moves -->" << std::endl;

	ss << "        </proposal>" << std::endl;
	if(nP > 1) {
		ss << "		<!-- A MC3 move will be proposed each xth iterations using the period parameter for x -->" << std::endl;
		ss << "        <proposal name=\"MC3\">" << std::endl;
		ss << "            <period>100</period> " << std::endl;
		ss << "        </proposal>	" << std::endl;
	}

	ss << "       <checkpoint>" << std::endl;
	ss << "                <keepNCheckpoints>1</keepNCheckpoints>" << std::endl;
	ss << "                <iterationFrequency>" << int(nIteration/4) << "</iterationFrequency>" << std::endl;
	ss << "        </checkpoint>" << std::endl;

	ss << "        <output>" << std::endl;
	ss << "            <filename>" << outputFile << "</filename>" << std::endl;
	ss << "            <verbose>1</verbose>" << std::endl;
	ss << "            <stdOutputFrequency>1000</stdOutputFrequency>" << std::endl;
	ss << "            <nThinning>100</nThinning>" << std::endl;
	ss << "        </output>" << std::endl;

	ss << "    </sampler>" << std::endl;
	ss << "</config>" << std::endl;

	return ss.str();
}

typedef enum {TREE, COEV} analysis_type_t;

std::string createAnalyzeLogXML(analysis_type_t analysisType, std::string &burnin, std::string &outputFile) {

	std::stringstream ss;
	ss << "<config method=\"LogAnalysis\"> " << std::endl;
	ss << "    <topology>" << std::endl;
	ss << "        <nChain>1</nChain>" << std::endl;
	ss << "    </topology>" << std::endl;
	ss << "    <seed>1332</seed>" << std::endl;

	if(analysisType == TREE) {
		ss << "    <analysis name=\"AnalyzeTreeLog\">" << std::endl;
	} else if(analysisType == COEV) {
		ss << "    <analysis name=\"AnalyzeCoevLog\">" << std::endl;
	}

	ss << "         <filename>" << outputFile << "</filename>" << std::endl;
	ss << "			<burninIteration>" << burnin << "</burninIteration>" << std::endl;
	ss << "			<withCoevTrace>false</withCoevTrace>" << std::endl;
	ss << "			<withTreeSplitFreq>false</withTreeSplitFreq>" << std::endl;
	ss << "    </analysis>" << std::endl;
	ss << "</config>" << std::endl;

	return ss.str();

}

int main(int argc, char** argv) {

	std::stringstream ss;
	ss << "------------------------------------------------------------------------------------------" << std::endl;
	ss << "Estimating the posterior probability with CoevRJ, given a fasta alignment, can be done by:" << std::endl;
	ss << "1) Default execution: mpirun -np <P> ./CoevRJ -r <nIterations> <myAlignment.fasta> <outputBaseName>" << std::endl;
	ss << "-- where <P> is the number of MC3 parallel chains you want (1 = no MC3)." << std::endl;
	ss << "2) Advanced execution: mpirun -np <P> ./CoevRJ -x <myConfig.xml>" << std::endl;
	ss << "-- where <P> is the number of MC3 parallel chains you want (1 = no MC3) and <myConfig.xml> is the path to your XML configuration file. See ... for more detail." << std::endl;
	ss << "------------------------------------------------------------------------------------------" << std::endl;
	ss << "Post-processing the samples (log files) can done by:" << std::endl;
	ss << "1) Analyze the tree distribution : mpirun -np 1 ./CoevRJ -t <burnin> <outputBaseName>" << std::endl;
	ss << "-- where <burnin> is the number of samples to discard for the burnin phase." << std::endl;
	ss << "2) Analyze the coevolving pairs distribution : mpirun -np 1 ./CoevRJ -c  <burnin> <outputBaseName>" << std::endl;
	ss << "-- where <burnin> is the number of samples to discard for the burnin phase." << std::endl;
	ss << "3) Advanced execution: mpirun -np 1 ./CoevRJ -x <myConfig.xml>" << std::endl;
	ss << "-- where <myConfig.xml> is the path to your XML configuration file. See ... for more detail." << std::endl;


	// Init the MPI environnement
	Parallel::mpiMgr().init(&argc, &argv);


	if (argc == 5 && strlen(argv[1]) == 2 && strcmp(argv[1], "-r") == 0 ) {

		int nIteration = atoi(argv[2]);
		std::string inputFile(argv[3]);
		std::string outputFile(argv[4]);

		std::string noName;
		std::string xmlString(createRunXML(nIteration, inputFile, outputFile));

		XML::ReaderXML readerXML(noName, xmlString);
		readerXML.run();

	} else if(argc == 3 && strlen(argv[1]) == 2 && strcmp(argv[1], "-x") == 0 ) {

		XML::ReaderXML readerXML(argv[2]);
		readerXML.run();

	} else if (argc == 4 && strlen(argv[1]) == 2 && strcmp(argv[1], "-t") == 0 ) {

		std::string burnin(argv[2]);
		std::string outputFile(argv[3]);

		std::string noName;
		std::string xmlString(createAnalyzeLogXML(TREE, burnin, outputFile));

		XML::ReaderXML readerXML(noName, xmlString);
		readerXML.run();

	} else if (argc == 4 && strlen(argv[1]) == 2 && strcmp(argv[1], "-c") == 0 ) {

		std::string burnin(argv[2]);
		std::string outputFile(argv[3]);

		std::string noName;
		std::string xmlString(createAnalyzeLogXML(COEV, burnin, outputFile));

		XML::ReaderXML readerXML(noName, xmlString);
		readerXML.run();

	} else if (argc == 2 && strlen(argv[1]) == 2 && strcmp(argv[1],"-h") == 0 ) {

		if(Parallel::mpiMgr().isMainProcessor()) std::cout << ss.str();

	} else {
		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "CoevRJ was unable to execute with these parameters." << std::endl;
			std::cout << ss.str();
		}
	}
	return 0;
}
