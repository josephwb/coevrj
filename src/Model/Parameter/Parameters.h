//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Parameters.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include <math.h>
#include <stddef.h>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <vector>

#include "Model/Parameter/ParameterType.h"
#include "ParamDblDef.h"
#include "ParamIntDef.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/Code/IDManagement/IDManager.h"

namespace StatisticalModel { class ParamDblDef; }
namespace StatisticalModel { class ParamIntDef; }

using namespace std;

namespace StatisticalModel {

class Parameters : private Uncopyable {
public:
	Parameters();
	~Parameters();

	void addBaseParameter(ParamIntDef aParam);
	void addBaseParameter(ParamDblDef aParam);

	size_t addDynamicParameter(ParamIntDef aParam);
	size_t addDynamicParameter(ParamDblDef aParam);
	void restoreDynamicParameter(size_t iParam, ParamIntDef aParam);
	void restoreDynamicParameter(size_t iParam, ParamDblDef aParam);

	void removeDynamicParameter(size_t aInd);

	string getName(size_t aInd) const;
	std::vector<string> getNames(bool onlyBaseParameters=true) const;
	double getParameterFreq(size_t aInd) const;
	parameterType_t getType(size_t aInd) const;

	size_t getNBaseParameters() const;
	std::vector<size_t> findBaseParametersOfType(parameterType_t pType) const;
	Sampler::Sample generateBaseRandomSample() const;

	double processPriorValue(const Sampler::Sample &sample) const;
	double processLogPriorValue(const Sampler::Sample &sample) const;

	void signalReflections(size_t aInd, int cnt) const;
	bool isOverflowed(size_t aInd) const;
	void disableReflection(size_t aInd);

	bool isInt(size_t aInd) const;
	bool isDbl(size_t aInd) const;

	bool isInBound(size_t aInd, const double value) const;
	bool requireReflection(size_t aInd) const;
	double getBoundMin(size_t aInd) const;
	double getBoundMax(size_t aInd) const;

	void setBoundMin(size_t aInd, const double value);
	void setBoundMax(size_t aInd, const double value);

private:

	static Utils::IDManagement::IDManager idManager;

	typedef std::vector<ParamIntDef>::iterator pIntIt_t;
	typedef std::vector<ParamDblDef>::iterator pDblIt_t;

	typedef std::vector<ParamIntDef>::const_iterator pIntCstIt_t;
	typedef std::vector<ParamDblDef>::const_iterator pDblCstIt_t;

	std::vector<ParamIntDef> intParams;
	std::vector<ParamDblDef> dblParams;

	typedef std::vector<pair<size_t, PriorInterface*> >::iterator priorIt_t;
	typedef std::vector<pair<size_t, PriorInterface*> >::const_iterator priorCstIt_t;

	std::vector<pair<size_t, PriorInterface*> > intPrior, dblPrior;

	std::list<ParamIntDef> intDynamicParams;
	std::list<ParamDblDef> dblDynamicParams;

	typedef std::list<pair<size_t, PriorInterface*> > listPrior_t;
	typedef listPrior_t::iterator listPriorIt_t;
	typedef listPrior_t::const_iterator listPriorCstIt_t;
	listPrior_t dynamicPrior;

	enum listType_t {INT_LIST, DBL_LIST};
	struct ParameterLocator {
		listType_t listType;
		std::list<ParamIntDef>::iterator itIList;
		std::list<ParamDblDef>::iterator itDList;
	};

	std::map<size_t, ParameterLocator> mapDynamicParams;


	void registerDynamicParameter(size_t iParam, ParamIntDef &aParam);
	void registerDynamicParameter(size_t iParam, ParamDblDef &aParam);


};

} // namespace StatisticalModel


#endif /* PARAMETERS_H_ */
