//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevRJHlp.cpp
 *
 * @date Apr 10, 2017
 * @author meyerx
 * @brief
 */
#include "CoevRJHlp.h"

#include <assert.h>
#include <cmath>
#include <stdlib.h>
#include <sys/types.h>
#include <algorithm>

#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/CombinedProposalCRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/DefineConstantsCoev.h"
#include "Model/Likelihood/CoevRJ/Restart/RestartFileReader.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/AddMoveCoevRJ.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/RemoveMoveCoevRJ.h"

#include "Model/Parameter/Parameters.h"
#include "Model/Prior/PriorInterface.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/Manager/MpiManager.h"
#include "Parallel/RNG/RNG.h"

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "Sampler/ReversibleJump/Proposals/RJMoveProposal.h"
#include "Sampler/Samples/ModelSpecific/ModelSpecificSample.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"

namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

const double CoevRJHlp::DEFAULT_P = 0.75;

CoevRJHlp::CoevRJHlp(CoevRJ::Base *aPtrLik) : ptrLik(aPtrLik) {
	restartReader = NULL;
}

CoevRJHlp::~CoevRJHlp() {
	if(restartReader != NULL) delete restartReader;
}

void CoevRJHlp::defineParameters(Parameters &params) const {

	double treeFreq = 0.3;
	double branchFreq = 0.68;
	double rateFreq = 0.02;

	const bool USE_ST_FREQ = ptrLik->isSamplingStationaryFreqGTR();

	const bool reflect = true;//ptrLik->getTreeUpdateStrategy() != ptrLik->TREE_LV_STRATEGY;
	RNG *myRNG = Parallel::mpiMgr().getPRNG();

	size_t iParam = 0;

	// Tree parameter
	ParamIntDef pTree(CoevRJ::Base::NAME_TREE_PARAMETER, PriorInterface::createNoPrior(), reflect);
	pTree.setFreq(treeFreq/2.);
	pTree.setType(StatisticalModel::TREE_PARAM);
	params.addBaseParameter(pTree);
	++iParam;

	size_t nRate = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;
	{ // GTR

		for(size_t iT=0; iT<3; iT++) {
			std::stringstream ssName;
			ssName << "StFreq (" << MolecularEvolution::Definition::Nucleotides::NUCL_BASE[iT] << ")";
			//ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(myRNG, 0., 100.), reflect);
			ParamDblDef pStFreq(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pStFreq.setFreq(rateFreq/(double)(nRate+3));
			if(USE_ST_FREQ) {
				pStFreq.setMin(0.);
				pStFreq.setMax(500.);
			} else {
				pStFreq.setMin(1.);
				pStFreq.setMax(1.);
			}
			pStFreq.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pStFreq);
			++iParam;
		}

		for(size_t iT=0; iT<nRate; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT << " (" << MolecularEvolution::MatrixUtils::GTR_MF::PARAMETERS_NAME[iT] << ")";
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setFreq(rateFreq/(double)(nRate+1));
			pTheta.setMin(0.);
			if(nRate == 6) {
				pTheta.setMax(1.);
			} else {
				pTheta.setMax(500.);
			}
			pTheta.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pTheta);
			++iParam;
		}
	}

	{ // Gamma - alpha params
		std::string alphaName("alpha");
		ParamDblDef pAlpha(alphaName, PriorInterface::createExponentialBoost(myRNG, CoevRJ::Priors::GAMMA_RATE_ALPHA_EXPRATE), reflect);
		pAlpha.setFreq(rateFreq/(double)(nRate+1));
		if(ptrLik->getNGamma() > 1) {
			pAlpha.setMin(0.);
			pAlpha.setMax(500.);
		} else {
			pAlpha.setMin(1.);
			pAlpha.setMax(1.);
		}
		pAlpha.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pAlpha);
		++iParam;
	}


	{ // Coev scaling
		assert(iParam == CoevRJ::Parameters::COEV_SCALING_PARAMETER_OFFSET);
		std::string coevScalingName("coevScaling");
		ParamDblDef pCoevScaling(coevScalingName, PriorInterface::createNoPrior(), reflect);
		pCoevScaling.setFreq(rateFreq/(double)(2.));
		pCoevScaling.setMin(0.);
		pCoevScaling.setMax(5000.);
		pCoevScaling.setType(StatisticalModel::BRANCH_LENGTH);

		params.addBaseParameter(pCoevScaling);
		++iParam;
	}


	{ // Lambda for poisson distribution
		std::string lambdaPoissonName("lambdaPoisson");
		ParamDblDef pLambdaPoisson(lambdaPoissonName, PriorInterface::createNoPrior(), reflect);
		pLambdaPoisson.setFreq(rateFreq/(double)(2.));
		if(ptrLik->isUsingFixedPairsStrategy()) { // only if using CoevScaling
			pLambdaPoisson.setMin(1.);
			pLambdaPoisson.setMax(1.);
		} else { // Only if using Coev Scaling
			pLambdaPoisson.setMin(0.);
			pLambdaPoisson.setMax(5000.);
		}
		pLambdaPoisson.setType(StatisticalModel::BRANCH_LENGTH);
		params.addBaseParameter(pLambdaPoisson);
		++iParam;
	}

	{ // Hyperpriors
		assert(iParam == CoevRJ::Parameters::PRIOR_PARAMETERS_OFFSET);
		for(size_t iP=0; iP<CoevRJ::Parameters::SINGLE_NB_PARAMS; ++iP) {
			ParamDblDef pParams(CoevRJ::Parameters::SINGLE_PARAM_NAMES[iP], PriorInterface::createNoPrior(), reflect);
			pParams.setFreq(branchFreq/(double)CoevRJ::Parameters::SINGLE_NB_PARAMS);
			if(CoevRJ::Parameters::SINGLE_PARAM_NAMES[iP] == "S") {
				pParams.setMin(-0.);
				pParams.setMax(0.);
			} else {
				pParams.setMin(-100.);
				pParams.setMax(100.);
			}
			pParams.setType(StatisticalModel::BRANCH_LENGTH);
			params.addBaseParameter(pParams);

			++iParam;
		}
	}

	// Branches
	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		ParamDblDef pBL(ssBranch.str(), PriorInterface::createExponentialBoost(myRNG, 10.), reflect);
		pBL.setFreq(branchFreq/(double)ptrLik->getNBranch());
		pBL.setMin(0.);
		pBL.setMax(1000.);
		pBL.setType(StatisticalModel::BRANCH_LENGTH);
		params.addBaseParameter(pBL);
		++iParam;
	}

	// Class specific parameters
	cLikInfo_t& coevInformations = ptrLik->getActivePermCoevLikelihoods();
	std::vector<std::string> coevParamNames = CoevRJ::UtilsCoevRJ::PARAM_NAMES;

	for(itCLikInfo_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {
		std::vector<size_t> pInd;

		std::stringstream ssProfName;
		ssProfName << (*it)->getUIDSubSpace()->getLabelUID() << "_";
		ssProfName << "Prof";
		ParamIntDef pProfile(ssProfName.str(), PriorInterface::createDiscreteUniform(myRNG, (*it)->getProfilesId().size()), true);
		pProfile.setFreq(0.1);
		pProfile.setType(StatisticalModel::COEV_PARAM);
		pInd.push_back(params.addDynamicParameter(pProfile));


		// Gamma parametrization shape=alpha, scale=1/beta
		std::vector<PriorInterface::sharedPtr_t> priorsCoev(coevParamNames.size());
		priorsCoev[0] = PriorInterface::createNoPrior();
		priorsCoev[1] = PriorInterface::createNoPrior();
		priorsCoev[2] = PriorInterface::createNoPrior();
		priorsCoev[3] = PriorInterface::createNoPrior();

		// Create parameters
		for(size_t iP=0; iP<coevParamNames.size(); ++iP) {
			std::stringstream ssName;
			ssName << (*it)->getUIDSubSpace()->getLabelUID() << "_";
			ssName << coevParamNames[iP];

			ParamDblDef pCoev(ssName.str(), priorsCoev[iP], reflect);
			pCoev.setFreq(rateFreq/(double)(nRate+1));
			pCoev.setMin(-100.);
			pCoev.setMax(100.);

			pCoev.setType(StatisticalModel::COEV_PARAM);
			pInd.push_back(params.addDynamicParameter(pCoev));
			//std::cout << pInd[iP] << " -- " << params.getName(pInd[iP]) << std::endl;// FIXME DEBUG REM
		}

		// Save the pInd for later
		(*it)->setPInd(pInd);
	}
}


void CoevRJHlp::defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType,
		                     const std::vector<size_t> &blockSize, Model &aModel, Blocks &blocks) const {
	size_t iParam = 0;
	size_t nBLPerB = blockSize[0];
	div_t divRes = div((int)ptrLik->getNBranch(), (int)nBLPerB);

	size_t nB_BL = divRes.quot;
	if(divRes.rem > (int)((double)nBLPerB*4./5.)) nB_BL++;

	size_t nElemBRate = 3+MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;  // GTR Parameters
	//std::cout << "Params size = " << aModel.getParams().size() << " :: nBranch = " << ptrLik->getNBranch() << " :: nElem = " << nElemBRate << std::endl;


	{ /** TREE MOVES **/
		if(ptrLik->getTreeUpdateStrategy() == StatisticalModel::Likelihood::CoevRJ::Base::OPTIMIZED_STRATEGY) {
			Block::sharedPtr_t bTreeSTNNI(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
			bTreeSTNNI->setName("TreeSTNNI");
			bTreeSTNNI->addParameter(iParam,
					BlockModifier::createSTNNIBM(ptrLik->getTreeManager(), aModel),
					aModel.getParams().getParameterFreq(iParam));

			blocks.addBaseBlock(bTreeSTNNI);

			Block::sharedPtr_t bTreeESPR(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
			bTreeESPR->setName("TreeESPR");
			bTreeESPR->addParameter(iParam,
					BlockModifier::createESPRBM(DEFAULT_P, ptrLik->getTreeManager(), aModel),
					aModel.getParams().getParameterFreq(iParam));

			blocks.addBaseBlock(bTreeESPR);
		}

		iParam++;
	} /** END TREE MOVES **/

	{ /** GTR rates + GAMMA **/
		Block::sharedPtr_t bRate(new Block(adaptiveType));
		bRate->setName("BlockRate");
		// Add kappa or thethas, Px and Wx
		for(size_t i=0; i<nElemBRate+1; ++i) { // # GTR + # GAMMA (1=> alpha)
			if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
				bRate->addParameter(iParam,
						BlockModifier::createGaussianWindow(0.03, aModel),
						aModel.getParams().getParameterFreq(iParam));
			}
			iParam++;
		}
		blocks.addBaseBlock(bRate);
	} /** END GTR + GAMMA rates **/

	/** GAMMA + COEV SCALING **/
	if(ptrLik->isUsingFixedPairsStrategy()) {
		iParam--; // We want alpha of Gamma rate again
		Block::sharedPtr_t bCoevGammaScaling(new Block(adaptiveType));
		bCoevGammaScaling->setName("Gamma and CoevScaling");
		for(size_t iP=0; iP<2; ++iP) { // Adding a block for Gamma and Coev Scaling.
			if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
				bCoevGammaScaling->addParameter(iParam,
						BlockModifier::createGaussianWindow(0.03, aModel),
						aModel.getParams().getParameterFreq(iParam));
			}
			iParam++;
		}
		blocks.addBaseBlock(bCoevGammaScaling);
	} else {
		iParam++;
	}
	/** END COEV SCALING + GAMMA **/

	{ /** GIBBS FOR LAMBDA POISSON **/
		Block::sharedPtr_t bCoevLambdaPoisson(new Block(ParameterBlock::Block::NOT_ADAPTIVE));
		bCoevLambdaPoisson->setName("LambdaPoisson");
		if(aModel.getParams().getBoundMin(iParam) < aModel.getParams().getBoundMax(iParam)) {
			bCoevLambdaPoisson->addParameter(iParam,
					BlockModifier::createCoevPoissonConjugateBM(
							CoevRJ::HyperPriors::GAMMA_POISSON_ALPHA,
							CoevRJ::HyperPriors::GAMMA_POISSON_BETA, aModel),
					aModel.getParams().getParameterFreq(iParam));
			blocks.addBaseBlock(bCoevLambdaPoisson);
		}
		iParam++;
	} /** END GIBBS FOR LAMBDA POISSON  **/

	{ /** SINGLE PARAMS! **/
		if(ptrLik->isUsingFixedPairsStrategy() && ptrLik->getNActiveCoevLikelihood() > 1) {
			std::vector<size_t> pInd = boost::assign::list_of(CoevRJ::Parameters::PRIOR_PARAMETERS_OFFSET)
															 (CoevRJ::Parameters::PRIOR_PARAMETERS_OFFSET+1)
															 (CoevRJ::Parameters::PRIOR_PARAMETERS_OFFSET+2);

			Block::sharedPtr_t bRateCoev(new Block(ParameterBlock::Block::PCA_DOUBLE_VARIABLES));
			std::stringstream ssNameRateCoev;
			ssNameRateCoev << "CoevRates";
			bRateCoev->setName(ssNameRateCoev.str());
			for(size_t iP=0; iP<pInd.size(); ++iP) {
				BlockModifier::sharedPtr_t ptrBlockMod;
				ptrBlockMod = BlockModifier::createGaussianWindow(CoevRJ::Proposals::GAMMA_NORMAL_GAUSSIAN_BM_STD_SMALL, aModel);

				if(aModel.getParams().getBoundMin(pInd[iP]) < aModel.getParams().getBoundMax(pInd[iP])) {
						bRateCoev->addParameter(pInd[iP],
						ptrBlockMod,
						aModel.getParams().getParameterFreq(pInd[iP]));
				}
				iParam++;
			}
			blocks.addBaseBlock(bRateCoev);
		} else {
			iParam += CoevRJ::Parameters::SINGLE_NB_PARAMS;
		}
	} /** END SINGLE PARAMS! **/

	{ /** BRANCH LENGTH **/
		Block::sharedPtr_t bBL[nB_BL];
		for(size_t iB=0; iB < nB_BL; ++iB){
			bBL[iB].reset(new Block(adaptiveType));
			stringstream ss;
			ss << "BranchLength_" << iB;
			bBL[iB]->setName(ss.str());
		}

		std::vector<size_t> activeParamsIdx;
		for(size_t iBL=iParam; iBL<iParam+ptrLik->getNBranch(); ++iBL){
			activeParamsIdx.push_back(iBL);
		}
		//std::random_shuffle(activeParamsIdx.begin(), activeParamsIdx.end());

		size_t iP = 0;
		for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
			uint idx = iBL/nBLPerB;
			if(idx >= nB_BL) idx = iBL % nB_BL;
			iParam = activeParamsIdx[iP];
			const double l = ptrLik->getOptiL(Parallel::mcmcMgr().getNProposal(), 1, 0.002*0.002)/pow(nBLPerB, 0.75);
			bBL[idx]->addParameter(iParam,
					BlockModifier::createGaussianWindow(l, aModel),
					aModel.getParams().getParameterFreq(iParam));
			++iP;
		}

		for(size_t iB=0; iB < nB_BL; ++iB){
			blocks.addBaseBlock(bBL[iB]);
		}
	} /** END BRANCH LENGTH **/

	if(ptrLik->isUsingFixedPairsStrategy()) {
		assert(false && "Implement define block for single parameters as in CoevUtils.");
	}
}

Sample CoevRJHlp::defineInitSample(const Parameters &params) const {
	const RNG* rng = Parallel::mpiMgr().getPRNG();
	std::vector<std::string> coevParamNames = CoevRJ::UtilsCoevRJ::PARAM_NAMES;

	Sampler::Sample sample;
	sample.getIntValues().push_back(ptrLik->getCurrentTree()->getHashKey());

	if(restartReader != NULL) {
		sample.getDblValues() = restartReader->getParametersValue();

		// Load newick branch length
		if(!ptrLik->getInitialBranchLenghts().empty()) {
			size_t offset = sample.getDblValues().size() - ptrLik->getNBranch();
			for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
				sample.getDblValues()[offset + iBL] = ptrLik->getInitialBranchLenghts()[iBL];
			}
		}

	} else {


		const bool USE_ST_FREQ = ptrLik->isSamplingStationaryFreqGTR();

		if(USE_ST_FREQ) {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.-rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
			}
		} else {
			for(size_t iT=0; iT<3; ++iT) {
				sample.getDblValues().push_back(1.0);
			}
		}

		size_t nParamsGTR = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;
		for(size_t iT=0; iT<nParamsGTR; ++iT) {
			sample.getDblValues().push_back(rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
		}

		if(ptrLik->getNGamma() > 1) {
			sample.getDblValues().push_back(0.5+rng->genUniformDbl()); // Random alpha for Gamma rates
		} else {
			sample.getDblValues().push_back(1.0);
		}

		if(ptrLik->isUsingFixedPairsStrategy()) {
			sample.getDblValues().push_back(2.+3*rng->genUniformDbl()); // Random for scaling
		} else {
			sample.getDblValues().push_back(0.0); // Scaling is not sampled
		}

		if(ptrLik->isUsingFixedPairsStrategy()) { // Lambda poisson
			sample.getDblValues().push_back(1.);
		} else {
			sample.getDblValues().push_back(0.5+rng->genUniformDbl());
		}

		// Values
		for(size_t iP=0; iP<CoevRJ::Parameters::SINGLE_NB_PARAMS; ++iP) {
			double val = 0.; // Only has a value if it as an impact
			sample.getDblValues().push_back(val);
		}

		if(!ptrLik->getInitialBranchLenghts().empty()) {
			for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
				sample.getDblValues().push_back(ptrLik->getInitialBranchLenghts()[iBL]);
			}
		} else {
			for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
				sample.getDblValues().push_back(rng->genExponential(65.)); // Branches between 0.5 and 25.
			}
		}

		/** MODEL SPECIFIC : COEV **/
		cLikInfo_t& coevInformations = ptrLik->getActivePermCoevLikelihoods();
		for(itCLikInfo_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {

			size_t nInt = 1;
			const std::vector<size_t> &pInd = (*it)->getPInd();
			Sampler::ModelSpecificSample mss((*it)->getUIDSubSpace(), nInt, pInd);

			// Draw a random value between [0..#nb of profile]
			int idRand = rng->genUniformInt(0, (*it)->getProfilesId().size()-1);
			mss.setIntParameter(0, (*it)->getProfilesId()[idRand]);

			assert(false && "Not implemented for fixed coev.");

			sample.registerMSS(mss);
		}
	}

	return sample;
}

void CoevRJHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const {
	const std::vector<double> &values = sample.getDblValues();

	size_t nNuclParams = MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF;;
	std::cout << "Thetas :\t";
	for(size_t iT=0; iT<nNuclParams; iT++) {
		std::cout << values[iT] << "\t";
	}
	std::cout << std::endl;
	size_t iParam = nNuclParams;

	std::cout << "Alpha :\t" << values[iParam] << "\t";
	iParam++;

	std::cout << "CoevScaling :\t" << values[iParam] << std::endl;
	iParam++;

	std::cout << "PoissonLambda :\t" << values[iParam] << std::endl;
	iParam++;

	std::cout << "Single parameters :\t";
	for(size_t iP=0; iP<CoevRJ::Parameters::SINGLE_NB_PARAMS; ++iP) {
		 std::cout << values[iParam] << "\t";
		 iParam++;
	}
	std::cout << std::endl;


	std::cout << "Branch lengths :\t";
	for(size_t i=iParam; i<values.size(); ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;

	std::cout << "Topology :\t";
	std::cout << ptrLik->getCurrentTree()->getIdString();
	std::cout << endl;

	cLikInfo_t& coevInformations = ptrLik->getActivePermCoevLikelihoods();
	for(itCLikInfo_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {

		const Sampler::ModelSpecificSample &mss = sample.getMSS((*it)->getUIDSubSpace());

		std::pair<size_t, size_t> pairPos = (*it)->getPositions();
		std::cout << "Coev ==> " << (*it)->getUIDSubSpace()->getLabelUID() << std::endl;

		std::cout << "iProf : " << mss.getIntValues()[0] << "\t";

		std::vector<std::string> coevParamNames = CoevRJ::UtilsCoevRJ::PARAM_NAMES;
		for(size_t iN=0; iN<coevParamNames.size(); ++iN) {
			std::cout << coevParamNames[iN] << " : " << mss.getDblValues()[iN] << "\t";
		}
		std::cout << std::endl;
	}
}

Sampler::RJMCMC::RJMoveManager::sharedPtr_t CoevRJHlp::createRJMoveManager(size_t aSeed, size_t iType, StatisticalModel::Model &model) const {
	using Sampler::RJMCMC::RJMoveManager;
	using StatisticalModel::Likelihood::CoevRJ::CombinedProposalCRJ;

	RJMoveManager::sharedPtr_t moveMgrRJ(new RJMoveManager(aSeed));

	Sampler::RJMCMC::RJMoveProposal::sharedPtr_t propRJ;
	if(iType == 0) {
		assert(false);
		//propRJ.reset(new BaseProposalCoevRJ(model));
	} else if(iType == 1) {
		propRJ.reset(new CombinedProposalCRJ(CombinedProposalCRJ::CONSTANT_CT, model));
	} else if(iType == 2) {
		propRJ.reset(new CombinedProposalCRJ(CombinedProposalCRJ::PROPORTIONAL_TO_K_CT, model));
	} else {
		assert(false && "Unknown proposal type for CoevRJ.");
	}
	moveMgrRJ->addProposal(propRJ);

	return moveMgrRJ;
}

std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> CoevRJHlp::createRJInitMoves(StatisticalModel::Model &model) const {

	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> empty;

	if(restartReader != NULL) {
		return restartReader->getInitMoves();
	} else if(!ptrLik->getRestartFileName().empty()) {
		restartReader = new CoevRJ::RestartFileReader(ptrLik);
		return restartReader->getInitMoves();
	} else {
		restartReader = NULL;
		return empty;
	}

}

std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> CoevRJHlp::createRJUpdateMoves(Sampler::Sample &sample, StatisticalModel::Model &model) const {
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> moves;

	/// Collecting remote labels
	std::vector<std::string> remoteLabels;
	std::vector<Sampler::ModelSpecificSample> allMSS(sample.getAllMSS());
	for(size_t iM=0; iM<allMSS.size(); ++iM) {
		std::string label(allMSS[iM].getLabelSubSpaceUID());
		if(label != "PARAMS_COEV" && label != "SCALING_COEV" && label != "HYPERPRIOR_COEV") {
			remoteLabels.push_back(label);
		}
	}
	std::sort(remoteLabels.begin(), remoteLabels.end());
	//std::set<std::string> setRemoteLabels(remoteLabels.begin(), remoteLabels.end());

	/// Collection local labels
	std::vector<std::string> localLabels;
	const std::list<CoevRJ::PermCoevLikelihoodInfo*>& permLik = ptrLik->getActivePermCoevLikelihoods();
	typedef std::list<CoevRJ::PermCoevLikelihoodInfo*>::const_iterator constIt_t;
	for(constIt_t it = permLik.begin(); it != permLik.end(); ++it) {
		localLabels.push_back((*it)->getUIDSubSpace()->getLabelUID());
	}
	std::sort(localLabels.begin(), localLabels.end());
	//std::set<std::string> setLocalLabels(localLabels.begin(), localLabels.end());

	/// Getting differences
	std::vector<std::string>::iterator itStr;
	std::vector<std::string> toAdd(remoteLabels.size()+localLabels.size()), toRemove(remoteLabels.size()+localLabels.size());
	// To add -> Remote \ Local
	itStr = std::set_difference(remoteLabels.begin(), remoteLabels.end(), localLabels.begin(), localLabels.end(), toAdd.begin());
	toAdd.resize(itStr-toAdd.begin());
	// To remove -> Local \ Remote
	itStr = std::set_difference(localLabels.begin(), localLabels.end(), remoteLabels.begin(), remoteLabels.end(), toRemove.begin());
	toRemove.resize(itStr-toRemove.begin());

	/// Creating moves
	for(size_t iR=0; iR<toRemove.size(); ++iR) {
		//std::cout << "[CoevRJHlp] To remove : " << toRemove[iR] << std::endl; // FIXME
		Sampler::RJMCMC::RJMove::sharedPtr_t ptr(new CoevRJ::RemoveMoveCoevRJ(toRemove[iR], sample, ptrLik));
		moves.push_back(ptr);
	}
	for(size_t iA=0; iA<toAdd.size(); ++iA) {
		//std::cout << "[CoevRJHlp] To remove : " << toAdd[iA] << std::endl; // FIXME
		Sampler::RJMCMC::RJMove::sharedPtr_t ptr(new CoevRJ::AddMoveCoevRJ(toAdd[iA], sample, ptrLik));
		moves.push_back(ptr);
	}

	return moves;
}


} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
