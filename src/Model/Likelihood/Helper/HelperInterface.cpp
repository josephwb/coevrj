//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * HelperInterface.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "HelperInterface.h"

#include <assert.h>
#include <stdlib.h>

#include "Helpers.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

HelperInterface::HelperInterface() {
}

HelperInterface::~HelperInterface() {
}

HelperInterface::sharedPtr_t HelperInterface::createHelper(LikelihoodInterface *likPtr) {
	if(CoevRJ::Base *ptr = dynamic_cast<CoevRJ::Base *>(likPtr)) {
		return sharedPtr_t(new CoevRJHlp(ptr));
	} else {
		std::cerr << "Helper::sharedPtr_t Helper::createHelper(LikelihoodInterface *likPtr);" << std::endl;
		std::cerr << "No Likelihood::Helper available for likelihood '" << likPtr->getName() << "'" << std::endl;
		abort();
	}
	return sharedPtr_t();
}

void HelperInterface::defineOptiBlocks(
		Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize,
		Model &model, Blocks &blocks) const {
	std::cout << "[WARNING] Optimized blocks creation is not define for this Likelihood." << std::endl;
	std::cout << "[WARNING] ... default block creation will be used." << std::endl;
	defineBlocks(adaptiveType, blockSize, model, blocks);
}

void HelperInterface::defineRandomBlocks(
		Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize,
		Model &model, Blocks &blocks) const {
	std::cout << "[WARNING] Randomized blocks creation is not define for this Likelihood." << std::endl;
	std::cout << "[WARNING] ... default block creation will be used." << std::endl;
	defineBlocks(adaptiveType, blockSize, model, blocks);
}

void HelperInterface::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	if(sep == '\n') {
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << params.getName(iP) << " =\t";
			std::cout << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << std::endl;
		}
	} else {
		// Print params names
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << params.getName(iP) << sep;
		}
		std::cout << std::endl;
		// Print sample values
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << sep;
		}
		std::cout << std::endl;
	}
}

std::string HelperInterface::sampleToString(const Parameters &params, const Sample &sample, const char sep) const {
	std::stringstream ss;
	if(sep == '\n') {
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << params.getName(iP) << " =\t";
			ss << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << std::endl;
		}
	} else {
		// Print params names
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << params.getName(iP) << sep;
		}
		ss << std::endl;
		// Print sample values
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << sep;
		}
		ss << std::endl;
	}
	return ss.str();
}

Sampler::RJMCMC::RJMoveManager::sharedPtr_t HelperInterface::createRJMoveManager(size_t aSeed, size_t iType, StatisticalModel::Model &model) const {
	std::cerr << "Sampler::RJMCMC::sharedPtrRJMoveManager_t getRJMoveManager() not defined." << endl;
	std::cerr << "This probably means that this likelihood does not support RJMCMC type moves." << endl;
	assert(false);
	return Sampler::RJMCMC::RJMoveManager::sharedPtr_t();
}

std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> HelperInterface::createRJInitMoves(StatisticalModel::Model &model) const {
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> empty;
	std::cerr << "std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJInitMoves(...) not defined." << endl;
	std::cerr << "This probably means that this likelihood does not support RJMCMC type moves." << endl;
	assert(false);
	return empty;
}

std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> HelperInterface::createRJUpdateMoves(Sampler::Sample &sample, StatisticalModel::Model &model) const {
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> empty;
	std::cerr << "std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJUpdateMoves(...) not defined." << endl;
	std::cerr << "This probably means that this likelihood does not support RJMCMC type moves." << endl;
	assert(false);
	return empty;
}


} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
