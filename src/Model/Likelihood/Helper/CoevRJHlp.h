//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevRJHlp.h
 *
 * @date Apr 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef COEVRJHLP_H_
#define COEVRJHLP_H_

#include <stddef.h>

#include "../Likelihoods.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "HelperInterface.h"
#include "ParameterBlock/Block.h"
#include "Sampler/ReversibleJump/RJMoveManager.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Model; }
namespace StatisticalModel { class Parameters; }
namespace StatisticalModel { namespace Likelihood { namespace CoevRJ { class Base; } } }
namespace StatisticalModel { namespace Likelihood { namespace CoevRJ { class RestartFileReader; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

class CoevRJHlp: public HelperInterface {
public:
	CoevRJHlp(CoevRJ::Base *aPtrLik);
	~CoevRJHlp();

	void defineParameters(Parameters &params) const;
	void defineBlocks(ParameterBlock::Block::adaptiveType_t adaptiveType, const std::vector<size_t> &blockSize, Model &aModel, Blocks &blocks) const;
	Sample defineInitSample(const Parameters &params) const;
	void printSample(const Parameters &params, const Sample &sample, const char sep) const;

	Sampler::RJMCMC::RJMoveManager::sharedPtr_t createRJMoveManager(size_t aSeed, size_t iType, StatisticalModel::Model &model) const;
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJInitMoves(StatisticalModel::Model &model) const;
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> createRJUpdateMoves(Sampler::Sample &sample, StatisticalModel::Model &model) const;

private:

	typedef std::list<StatisticalModel::Likelihood::CoevRJ::PermCoevLikelihoodInfo*> cLikInfo_t;
	typedef cLikInfo_t::iterator itCLikInfo_t;

	static const double DEFAULT_P;

	CoevRJ::Base *ptrLik;
	mutable CoevRJ::RestartFileReader *restartReader;

};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COEVRJHLP_H_ */
