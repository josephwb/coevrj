//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 *
 */
#ifndef BASE_COEVRJ_H_
#define BASE_COEVRJ_H_

#include "Eigen/Core"
#include "Eigen/Dense"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/dynamic_bitset.hpp>
#include <stddef.h>
#include <fstream>
#include <iostream>

#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"

#include "Model/Likelihood/LikelihoodInterface.h"

#include "Utils/Statistics/Dirichlet/DirichletPDF.h"
#include "Utils/Statistics/Dirichlet/DirichletHyperPrior.h"
#include "Utils/Statistics/ConjugatePrior/GammaNormalConjugatePrior.h"
#include "Utils/Statistics/ConjugatePrior/InverseGammaConjugatePrior.h"

#include "TreeNode.h"
#include "BaseSampleWrapper.h"
#include "CoevAlignementManager.h"
#include "CoevLikelihoodInfo.h"
#include "PermCoevLikelihoodInfo.h"
#include "PermutedCoevLikelihood.h"
#include "Nodes/IncNodes.h"
#include "Writer/CoevTraceWriter.h"
#include "UID/SubSpaceUID.h"

#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/NucleotideFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotideModels/GTRMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/NucleotidePairModels/COEVMF.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"




namespace DAG { class BaseNode; }
namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace MolecularEvolution { namespace DataLoader { class FastaReader; } }
namespace MolecularEvolution { namespace DataLoader { class MSA; } }
namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }
namespace Sampler { class Sample; }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class BaseSampleWrapper;
class BranchMatrixCoevNode;
class CoevLikelihoodInfo;
class DiscreteGammaNode;
class FinalResultNode;
class MatrixGTRNode;
class TreeNode;

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;
namespace TR = MolecularEvolution::TreeReconstruction;

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;

class Base : public LikelihoodInterface {
public:
	static const std::string NAME_TREE_PARAMETER;
	enum coevStrategy_t {FIXED_POSITIONS_STRATEGY=0, RJ_STRATEGY=1};
	enum treeUpdateStrategy_t {FIXED_TREE_STRATEGY=0, OPTIMIZED_STRATEGY=1, TREE_LV_STRATEGY=2};
public:
	Base(size_t aNGamma,  const size_t aNThread, bool aUseStationaryFrequencyGTR, bool aCachePairProfileInfo,
		 DL::NewickParser *aNP, const std::string &aFileAlign, treeUpdateStrategy_t aStrat);
	Base(size_t aNGamma,  const size_t aNThread, bool aUseStationaryFrequencyGTR, bool aCachePairProfileInfo,
		 DL::NewickParser *aNP, const std::string &aFileAlign,
	     const std::string &aDataFileName, treeUpdateStrategy_t aStrat);
	~Base();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	size_t getNGamma() const;
	size_t getNParamPrior() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	double processLikelihoodSpecificPrior(const Sampler::Sample &sample);

	void saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff);
	void loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff);

	void initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff>& offsets);
	std::vector<std::streamoff> getCustomizedLogSize() const;
	void writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples);

	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	size_t getNThread() const;
	std::string getAlignFile() const;

	TR::Tree::sharedPtr_t getCurrentTree() const;
	TR::TreeManager::sharedPtr_t getTreeManager() const;

	treeUpdateStrategy_t getTreeUpdateStrategy() const;
	void printDAG() const;

	bool isUsingCoevScaling() const;
	coevScalingType_t getCoevScalingType() const;
	coevPriorScalingType_t getCoevScalingPriorType() const;

	CoevTraceWriter& getCoevTraceWriter();
	CoevAlignementManager::sharedPtr_t getCoevAlignManager() const;

	const std::set<size_t>& getSitesGTR() const;
	const std::set<size_t>& getSegregatingSitesGTR() const;
	const boost::dynamic_bitset<>& getSitesAvailability() const;

	bool isUsingFixedPairsStrategy() const;
	bool isSamplingStationaryFreqGTR() const;

	size_t getNActiveCoevLikelihood() const;
	size_t getMaxActiveCoevLikelihood() const;

	CoevLikelihoodInfo* createTempCoevLikelihood(SubSpaceUID::sharedPtr_t ptrUID);
	CoevLikelihoodInfo* getActiveTempCoevLikelihood(const std::string &labelUID);
	CoevLikelihoodInfo* getInactiveTempCoevLikelihood(const std::string &labelUID);
	//std::list<CoevLikelihoodInfo*>& getActiveTempCoevLikelihoods();
	void removeTempCoevLikelihood(const std::string &labelUID);

	void registerCoevDAG(CoevLikelihoodInfo* coevLikInfo, bool refreshScheduler);
	void unregisterCoevDAG(CoevLikelihoodInfo* coevLikInfo, bool refreshScheduler);

	PermCoevLikelihoodInfo* createPermutedCoevPair(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace, size_t idProfile);
	std::list<PermCoevLikelihoodInfo*>& getActivePermCoevLikelihoods();
	PermCoevLikelihoodInfo* getActivePermutedCoevLikelihood(const std::string &labelUID);
	PermCoevLikelihoodInfo* getInactivePermuedCoevLikelihood(const std::string &labelUID);
	void removePermutedCoevPair(const std::string &labelUID);

	void enablePermutedCoevPair(PermCoevLikelihoodInfo* permLikInfo);
	void disablePermutedCoevPair(PermCoevLikelihoodInfo* permLikInfo);

	void setRestartFileName(std::string aFileName);
	std::string getRestartFileName() const;

	const std::vector<double>& getInitialBranchLenghts() const;

	//std::vector< std::pair<position_t, position_t> > drawMostProbablePairs() const;

private:

	enum markersGTR_t {DISABLE_GTR_MARKERS, ENABLE_GTR_MARKERS};
	static const markersGTR_t STATE_GTR_MARKERS;
	enum debug_t {DISABLE_DEBUG=0, GLOBAL_DEBUG=1, TREE_DEBUG=2, UPDATE_DEBUG=3};
	static const debug_t doDebug;
	std::ofstream dbgFile;

	static const size_t MAX_PENDING_COEV_DAG, MIN_SUBSTITUTION_PER_SITE;
	static const double MAXIMUM_GAP_RATIO;

	/* Input data */
	const size_t N_GAMMA;
	bool first, isTreeExternalyImported, requireSchedulerPartialReset;
	size_t nThread, nSite, nSegregatingSite, nPriorParams;
	const bool useStationaryFrequencyGTR, cachePairProfileInfo;
	std::string fileAlign, fileRestart;

	/* Internal data */
	const coevStrategy_t COEV_STRATEGY;
	const treeUpdateStrategy_t TREE_UPDATE_STRATEGY;
	TreeNode *rootTree;
	long int curHash;
	TR::Tree::sharedPtr_t curTree;
	TR::TreeManager::sharedPtr_t treeManager;
	std::vector<TreeNode*> branchPtr;
	CoevTraceWriter coevTraceWriter;

	typedef std::map<size_t, TreeNode*> branchMap_t;
	branchMap_t branchMap;
	DL::Utils::Frequencies freqGTR, freqCoev;
	std::vector<size_t> labelBranches;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	std::vector<double> initialBranchLenghts;

	/* Coev pairs and DAG data */
	std::set<size_t> sitesGTRG, segregatingSites;
	boost::dynamic_bitset<> sitesAvailability;

	listCLI_t activeTempCoevInfo, inactiveTempCoevInfo, pendingDAGs;
	listPCLI_t activePermCoevInfo, inactivePermCoevInfo;
	CoevAlignementManager::sharedPtr_t coevAlignManager;

	std::set<DAG::BaseNode*> pendingUpdatedNodes;

	/* GTR+Gamma DAG data */
	std::vector<CombineSiteGTRGNode*> cmbNodes;
	MatrixGTRNode *ptrMatrixGTR;
	DiscreteGammaNode *ptrDiscreteGamma;

	/* DAG ROOT */
	FinalResultNode *rootDAG;

	/* DAG Scheduler */
	size_t cntErrMsg;
	DAG::Scheduler::BaseScheduler *scheduler;

	/* CLI for fast Coev */
	std::vector<PermutedCoevLikelihood*> permutedCoev;

	// Prior
	std::vector<double> precomputedLogFactorial;
	std::vector<double> precomputedPriorPairs;
	Utils::Statistics::DirichletPDF::sharedPtr_t ptrGTRPr, ptrGTRFreqPr;
	Utils::Statistics::DirichletPDF::sharedPtr_t ptrCoevPr;
	typedef boost::shared_ptr< boost::math::normal_distribution<> > ptrNormalDistr_t;

	// Hyperpriors
	typedef boost::shared_ptr< boost::math::gamma_distribution<> > ptrGammaDistr_t;
	ptrGammaDistr_t ptrDistrAlphaRGammaHP, ptrDistrBetaRGammaHP;
	ptrGammaDistr_t ptrDistrAlphaDGammaHP, ptrDistrBetaDGammaHP;
	ptrGammaDistr_t ptrDistrBetaRGamma2HP, ptrDistrBetaDGamma2HP;

	Utils::Statistics::DirichletHyperPrior::sharedPtr_t ptrCoevDirichletHP;

	Utils::Statistics::InverseGammaConjugatePrior::sharedPtr_t ptrInverseGammaS;
	Utils::Statistics::GammaNormalConjugatePrior::sharedPtr_t ptrGammaNormalR, ptrGammaNormalD; //ptrGammaNormalS,

	//! Called by constructor to init everything
	void init(DL::NewickParser *aNP, const std::string &aDataFileName);
	void initPriors();

	//! Read coev clusters
	void loadCoevInformations(const std::string &dataFileName);

	//! Labelize the internal branches
	void labelizeBranch(const DL::TreeNode &newickNode);

	//! Create recursively the internal tree based on the newick one
	void createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node);

	//! Clean the tree and the whole DAG (starting from root)
	void cleanTreeAndDAG();

	// BASE Parameters -- GTR+Gamma DAG -- related
	//! Create the DiscreteGamma, MatrixGTR nodes (and more)
	void prepareBaseDAG();
	//! Create the tree topology as well as the GTR+Gamma DAG
	void createTreeAndBaseDAG(DL::FastaReader &fr, DL::MSA &msa);
	//! Create the DAGs associated with the permuted Coev Likelihoods
	void createPermutedDAG();

	//! Create the BranchMatrixNodes for the GTR+Gamma DAG
	void addBranchMatrixGTRGNodes(TreeNode *node);
	//! Recursive function that create the DAG according to the TreeNode topology
	void createBaseSubDAG(size_t idGamma, const std::vector<size_t> &subSites,
			TreeNode *node, DAG::BaseNode *nodeDAG, DL::MSA &msa);

	// Setters for the base samples
	void setBaseSample(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setTree(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setAlpha(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setThetas(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);
	void setBranchLength(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);

	void setCoevSample(const Sampler::Sample &sample, const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes);

	// Additional Parameters -- Coev DAG -- related
	void createCoevDAG(CoevLikelihoodInfo *coevLikInfo);
	bool tryToReuseCoevDAG(CoevLikelihoodInfo *coevLikInfo);
	void createCoevSubDAG(CoevLikelihoodInfo *coevLikInfo,
						  TreeNode *node, DAG::BaseNode *nodeDAG);
	BranchMatrixCoevNode* addBranchMatrixCoevNodes(CoevLikelihoodInfo *coevLikInfo, TreeNode *node);
	std::pair<usintBitset_t, usintBitset_t> getPairNuclCode(const std::pair<size_t, size_t> &pairPos,
															const std::string &specieName);

	void deleteCoevDAG(CoevLikelihoodInfo *coevLikInfo);
	void unregisterFromTreeNodes(CoevLikelihoodInfo *coevLikInfo, TreeNode *node, DAG::BaseNode *nodeDAG);


	// Permuted coevolution
	void createPermutedCoevSubDAG(PermutedCoevLikelihood *coevLikInfo,
						  TreeNode *node, DAG::BaseNode *nodeDAG);
	PermBranchMatrixCoevNode* addBranchMatrixPermutedCoevNodes(PermutedCoevLikelihood *coevLikInfo, TreeNode *node);
	void deletePermutedCoevDAG(PermutedCoevLikelihood *coevLikInfo);
	void createPermCoevDAG(PermutedCoevLikelihood *coevLikInfo);

	void updateSitesAvailabilityToCoev(std::pair<size_t, size_t> positions);
	void updateSitesAvailabilityToGTR(std::pair<size_t, size_t> positions);

	void writeCoevClustersInfo(const Sample &sample);

	// Utilitarian functions for the tree moves
	void addEdge(TreeNode *parent, TreeNode *child);
	void removeEdge(TreeNode *parent, TreeNode *child);
	void orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR);

	// Functions used to replicate tree topology move on the DAG
	void getDifference(const TR::vecTN_t &childrenTR, const treeNodeTI_children &children,
			std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove);
	void memorizeUpdatedParentNodes(TreeNode *node, std::set<DAG::BaseNode*> &updatedNodes);
	void applyChangesToTreeAndDAG(std::vector<TR::TreeNode *> &updTreeNodes);
	void signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
			std::set<DAG::BaseNode*> &updatedNodes);
	void updateTreeAndDAG(std::set<DAG::BaseNode*> &updatedNodes);
	bool fullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR,
			TreeNode *node,	std::set<DAG::BaseNode*> &updatedNodes);

	// Return the total branch length
	double getTotalTreeLength(const BaseSampleWrapper &sw);

	// Write the markers
	void writeMarkers(const Sampler::Sample &sample, const BaseSampleWrapper &sw);

	// Compute various priors
	double computeCoevPrior(const Sampler::Sample &sample, const BaseSampleWrapper &sw);
	double computeCoevHyperPrior(const Sampler::Sample &sample, const BaseSampleWrapper &sw);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BASE_COEVRJ_H_ */
