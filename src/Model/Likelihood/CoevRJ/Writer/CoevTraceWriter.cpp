//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevTraceWriter.cpp
 *
 * @date Apr 19, 2017
 * @author meyerx
 * @brief
 */
#include "CoevTraceWriter.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep
#include "Utils/Code/Algorithm.h" // IWYU pragma: keep

#include "Model/Likelihood/CoevRJ/CoevLikelihoodInfo.h"
#include "Parallel/Manager/MCMCManager.h"

#include "../ReversibleJump/DefineConstantsCoev.h"

#include <assert.h>

namespace Sampler { class ModelSpecificSample; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

CoevTraceWriter::CoevTraceWriter(bool areCoevPositionsFixed) :
		COEV_POS_FIXED(areCoevPositionsFixed), N_SITE(0) {

	profileTrace = CATEGORICAL;
}

CoevTraceWriter::~CoevTraceWriter() {
}

void CoevTraceWriter::initFile(const std::string &fnPrefix, const std::string &fnSuffix, std::streamoff offset) {
	std::stringstream ssFName;
	ssFName << fnPrefix << "_" << Parallel::mpiMgr().getRank() << fnSuffix << ".logCoev";
	coevStringFN = ssFName.str();
	oFile.reset(new Utils::Checkpoint::File(coevStringFN, offset));

	prefixFN = fnPrefix;
	suffixFN = fnSuffix;
}

void CoevTraceWriter::initCoev(listPCLI_t& coevInformations) {
	if(COEV_POS_FIXED) {
		initCoevFixed(coevInformations);
	} else {
		initCoevRJ();
	}
}

void CoevTraceWriter::initCoevFixed(listPCLI_t& coevInformations) {
	if(Parallel::mcmcMgr().isOutputManager()) {
		assert(oFile != NULL);
		if(oFile->getSize() == 0) { // write headers
			const char sep = '\t';

			// Prepare
			const std::vector<std::string>& pNames= MolecularEvolution::MatrixUtils::COEV_MF::PARAMETERS_NAME;

			 // Write an header listing column that are of the "categorical" type (profile)
			if(profileTrace == CATEGORICAL) {
				oFile->getOStream() << "#categorical";
				for(itListPCLI_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {

					std::string subSpaceLabel((*it)->getUIDSubSpace()->getLabelUID());
					std::string profileName(subSpaceLabel);
					profileName.append("idProfile");

					oFile->getOStream() << sep << profileName;
				}
				oFile->getOStream() << std::endl;
			}

			// Write the default header
			// First value
			oFile->getOStream() << "Iteration";

			for(itListPCLI_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {
				std::string subSpaceLabel((*it)->getUIDSubSpace()->getLabelUID());

				// TODO ADD MARKERS INFORMATION ???
				std::string subLikName(subSpaceLabel);
				subLikName.append("Lik");
				oFile->getOStream() << sep << subLikName;

				std::string sdRatioName(subSpaceLabel);
				sdRatioName.append("D/S");
				oFile->getOStream() << sep << sdRatioName;

				std::string profileName(subSpaceLabel);
				profileName.append("idProfile");
				oFile->getOStream() << sep << profileName;

				for(size_t iP=0; iP<pNames.size(); ++iP) {
					std::string labeledName(subSpaceLabel);
					labeledName.append(pNames[iP]);
					oFile->getOStream() << sep << labeledName;
				}

			}
			oFile->getOStream() << std::endl;
		}
	}
}

void CoevTraceWriter::initCoevRJ() {

	if(Parallel::mcmcMgr().isOutputManager()) {
		assert(oFile != NULL);
		if(oFile->getSize() == 0) { // write headers
			const char sep = '\t';

			// Prepare
			const std::vector<std::string>& pNames= MolecularEvolution::MatrixUtils::COEV_MF::PARAMETERS_NAME;

			// Write the default header
			// First value
			oFile->getOStream() << "Iteration";
			oFile->getOStream() << sep << "NbCoevLik";
			oFile->getOStream() << sep << "x";
			oFile->getOStream() << sep << "[";
			oFile->getOStream() << sep << "LabelUID";
			oFile->getOStream() << sep << "Lik";
			oFile->getOStream() << sep << "D/S";
			oFile->getOStream() << sep << "idProfile";

			for(size_t iP=0; iP<pNames.size(); ++iP) {
				oFile->getOStream() << sep << pNames[iP];
			}

			oFile->getOStream() << sep << "]";
			oFile->getOStream() << sep << "with";
			oFile->getOStream() << sep << "alphaHP=" << HyperPriors::GAMMA_POISSON_ALPHA;
			oFile->getOStream() << sep << "betaHP=" << HyperPriors::GAMMA_POISSON_BETA;
			oFile->getOStream() << sep << "NPosCoev=" << N_SITE;

			oFile->getOStream() << std::endl;
		}
	}
}

std::streamoff CoevTraceWriter::getFileLength() const {
	return oFile->getSize();
}

void CoevTraceWriter::writeSamples(listPCLI_t& coevInformations,
		      					   const Sampler::SampleUtils::vecPairItSample_t &iterSamples) {

	if(COEV_POS_FIXED) {
		writeSamplesCoevFixed(coevInformations, iterSamples);
	} else {
		writeSamplesCoevRJ(coevInformations, iterSamples);
	}

}

void CoevTraceWriter::setNSite(size_t aNSite) {
	N_SITE = aNSite;
}

void CoevTraceWriter::writeSamplesCoevFixed(listPCLI_t& coevInformations,
		      					   const Sampler::SampleUtils::vecPairItSample_t &iterSamples) {
	const char sep = '\t';
	using namespace ::MolecularEvolution::Definition;

	assert(oFile->getOStream().is_open());

	if(profileTrace == NUMERICAL) {
		for(size_t iS=0; iS<iterSamples.size(); ++iS) {
			oFile->getOStream() << iterSamples[iS].first << sep; // iteration number
			for(itListPCLI_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {
				// Get the MSS
				const Sampler::ModelSpecificSample &mss = iterSamples[iS].second.getMSS((*it)->getUIDSubSpace());
				mss.write(oFile->getOStream(), sep);
			}
			oFile->getOStream() << std::endl;
		}
	} else if(profileTrace == CATEGORICAL) {
		for(size_t iS=0; iS<iterSamples.size(); ++iS) {
			oFile->getOStream() << iterSamples[iS].first << sep; // iteration number
			for(itListPCLI_t it=coevInformations.begin(); it != coevInformations.end(); ++it) {
				// Get the MSS
				const Sampler::ModelSpecificSample &mss = iterSamples[iS].second.getMSS((*it)->getUIDSubSpace());

				// Customized write
				for(size_t i=0; i<mss.getMarkers().size(); ++i){
					oFile->getOStream() << sep << mss.getMarkers()[i];
				}

				for(size_t i=0; i<mss.getIntValues().size(); ++i){
					size_t iProfile = mss.getIntValues()[i];
					profile_t profile = getNucleotidesPairProfiles()->idToProfile(iProfile);
					oFile->getOStream() << sep << getNucleotidesPairProfiles()->profileToString(profile);
				}

				assert(mss.getDblValues().size() == 4);
				for(size_t i=0; i<mss.getDblValues().size(); ++i){
					oFile->getOStream() << sep << mss.getDblValues()[i];
				}

			}
			oFile->getOStream() << std::endl;// iteration number
		}
	}
}

void CoevTraceWriter::writeSamplesCoevRJ(listPCLI_t& coevInformations,
				    				 const Sampler::SampleUtils::vecPairItSample_t &iterSamples) {

	/*for(size_t iS=0; iS<iterSamples.size(); ++iS) {
		std::cout << iterSamples[iS].first << " :: " << << std::endl;
	}*/

	const char sep = '\t';
	using namespace ::MolecularEvolution::Definition;

	assert(oFile->getOStream().is_open());

	for(size_t iS=0; iS<iterSamples.size(); ++iS) {
		oFile->getOStream() << iterSamples[iS].first; // iteration number
		std::vector<Sampler::ModelSpecificSample> allMSS(iterSamples[iS].second.getAllMSS());
		oFile->getOStream() << sep << allMSS.size(); // Number of coev lik

		for(size_t iM=0; iM < allMSS.size(); ++iM) { // For each coev lik
			// Write the LabelUID
			oFile->getOStream() << sep << allMSS[iM].getLabelSubSpaceUID();

			// Customized write
			for(size_t i=0; i<allMSS[iM].getMarkers().size(); ++i){
				oFile->getOStream() << sep << allMSS[iM].getMarkers()[i];
			}

			for(size_t i=0; i<allMSS[iM].getIntValues().size(); ++i){
				size_t iProfile = allMSS[iM].getIntValues()[i];
				profile_t profile = getNucleotidesPairProfiles()->idToProfile(iProfile);
				oFile->getOStream() << sep << getNucleotidesPairProfiles()->profileToString(profile);
			}

			assert(allMSS[iM].getDblValues().size() == 4);
			for(size_t i=0; i<allMSS[iM].getDblValues().size(); ++i){
				oFile->getOStream() << sep << allMSS[iM].getDblValues()[i];
			}

		}
		oFile->getOStream() << std::endl;
	}

}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
