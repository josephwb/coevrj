//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevTraceAnalyzer.h
 *
 * @date May 13, 2017
 * @author meyerx
 * @brief
 */
#ifndef COEVTRACEANALYSER_H_
#define COEVTRACEANALYSER_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class CoevTraceAnalyser {
public:
	CoevTraceAnalyser(const std::string &aFnPrefix, const std::string &aFnSuffix);
	~CoevTraceAnalyser();

	void setBurninIteration(size_t aBurninIteration);
	void setBurninRatio(double aBurninRatio);

	bool summarizeRJLog(bool aWithCoevTraces=false);

	const std::string& getCoevStringFN() const;
	const std::string& getCoevStatStringFN() const;

private:

	static const double REPORT_PERCENT_THRESHOLD, REPORT_COEVOLUTION_THRESHOLD;

	const std::string fnPrefix, fnSuffix;
	std::string coevStringFN, coevStatStringFN;

	bool withCoevTraces, withBF;
	size_t burninIteration;
	double burninRatio;

	size_t nPosCoev;
	double alphaHP, betaHP, logPriorPairProb;

	class CoevPairStats {
	public:
		CoevPairStats() : count(0), varAccumulators(6) {};
		~CoevPairStats() {};
	public:
		size_t count;

		std::ofstream oFile;

		// mean + variance accumulators for double params
		typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > varianceAccumulator_t;
		std::vector<varianceAccumulator_t> varAccumulators;

		// Accumulator for the profile
		std::map< std::string, size_t > profAccumulator;
	};

	typedef std::map<std::string, CoevPairStats*> mapCoevPairStats_t;

	bool analizeRJLog(size_t &nSample, std::vector<size_t> &cntPos, mapCoevPairStats_t &mapCoevPairStats, std::map<size_t, size_t> &posteriorDistK);
	void writeSummaryRJLog(size_t &nSample, std::vector<size_t> &cntPos, mapCoevPairStats_t &mapCoevPairStats, double significanceThresh);
	double computeLogPriorPairProbability(std::map<size_t, size_t> &posteriorDistK);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COEVTRACEANALYSER_H_ */
