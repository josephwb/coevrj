//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevTraceAnalyser.cpp
 *
 * @date May 13, 2017
 * @author meyerx
 * @brief
 */

#include "CoevTraceAnalyser.h"
#include "Parallel/Manager/MpiManager.h"
#include "Utils/Code/Algorithm.h"
#include "Utils/Math/Functions.h"

#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/math/special_functions/gamma.hpp>

#include <fstream>
#include <iomanip>
#include <sstream>

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

const double CoevTraceAnalyser::REPORT_PERCENT_THRESHOLD = 1e-2;
const double CoevTraceAnalyser::REPORT_COEVOLUTION_THRESHOLD = 100./4.;

CoevTraceAnalyser::CoevTraceAnalyser(const std::string &aFnPrefix, const std::string &aFnSuffix) :
	fnPrefix(aFnPrefix), fnSuffix(aFnSuffix) {

	burninIteration = 0;
	burninRatio = 0.;
	withCoevTraces = false;

	withBF = false;
	nPosCoev = 0;
	alphaHP = 0.;
	betaHP = 0.;
	logPriorPairProb = 0.;

	std::stringstream ssFName;
	ssFName << fnPrefix << "_" << Parallel::mpiMgr().getRank() << fnSuffix << ".logCoev";
	coevStringFN = ssFName.str();

	std::stringstream ssStatFName;
	ssStatFName << fnPrefix << "_" << Parallel::mpiMgr().getRank() << fnSuffix << ".CoevStat";
	coevStatStringFN = ssStatFName.str();
}

CoevTraceAnalyser::~CoevTraceAnalyser() {
}

bool CoevTraceAnalyser::summarizeRJLog(bool aWithCoevTraces) {
	withCoevTraces = aWithCoevTraces;

	size_t nSample = 0;
	std::vector<size_t> cntPos;
	mapCoevPairStats_t mapCoevPairStats;
	std::map<size_t, size_t> posteriorDistK;
	bool fileExists = analizeRJLog(nSample, cntPos, mapCoevPairStats, posteriorDistK);

	// Compute the log prior probability for a pair to be sampled
	double logPriorPairProb = computeLogPriorPairProbability(posteriorDistK);
	//std::cout << "Alpha = " << alphaHP << "\tBeta = " << betaHP << "\tnPosCoev = " << nPosCoev << std::endl; //FIXME DEBUG
	//std::cout << "prior([A,B]) = " << exp(logPriorPairProb) << "\t log( prior([A,B]) ) = " << logPriorPairProb << std::endl; //FIXME DEBUG

	// Compute the threshold of singificance as 10. = 2 x log(BF)
	double priorPairProb = exp(logPriorPairProb);
    double Z = exp(10./2.)*priorPairProb/(1.-priorPairProb);
    double significanceThresh = Z/(1.-Z);
    // std::cout << "Significance threshold " << significanceThresh << std::endl; //FIXME DEBUG

	if(fileExists) {
		writeSummaryRJLog(nSample, cntPos, mapCoevPairStats, significanceThresh);
		for(mapCoevPairStats_t::iterator it = mapCoevPairStats.begin(); it != mapCoevPairStats.end(); ++it) {
			delete it->second;
		}
		return true;
	} else {
		return false;
	}
}

const std::string& CoevTraceAnalyser::getCoevStringFN() const {
	return coevStringFN;
}

const std::string& CoevTraceAnalyser::getCoevStatStringFN() const {
	return coevStatStringFN;
}

void CoevTraceAnalyser::setBurninIteration(size_t aBurninIteration) {
	burninIteration = aBurninIteration;
}

void CoevTraceAnalyser::setBurninRatio(double aBurninRatio) {
	burninRatio = aBurninRatio;
}


bool CoevTraceAnalyser::analizeRJLog(size_t &nSample, std::vector<size_t> &cntPos,
		                           mapCoevPairStats_t &mapCoevPairStats,
		                           std::map<size_t, size_t> &posteriorDistK) {
	// Open file.
	std::string line;
	std::ifstream iFile(coevStringFN.c_str());

	if(!iFile.is_open()) return false;

	// Counting number of line in file for the burn-in
	std::getline(iFile, line); // Trash headers

	// For each line: count nLine and define the k distrib
	size_t nLine = 0;
	while(std::getline(iFile, line)) {
		nLine++;
	}

	// Go back to the start of the file
	iFile.clear();
	iFile.seekg(0, ios::beg);

	// First line is header (but we know it)
	std::getline(iFile, line); // Trash headers
	{
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t "));

		if(words.size() < 17 || words[16].empty()) {
			withBF = false;
			if(Parallel::mpiMgr().isMainProcessor()) {
				std::cout << "[CoevTraceAnalyser] Warning: these logs have been created by an older version of CoevRJMCMC.";
				std::cout << "The analysis wont contain the Bayes Factors for pairs prediction significance." << std::endl;
			}
		} else {
			assert(words.size() == 17);
			withBF = true;

			assert(words[14].size() > 8);
			alphaHP = atof(words[14].substr(8).c_str());
			assert(words[15].size() > 7);
			betaHP = atof(words[15].substr(7).c_str());
			assert(words[16].size() > 9);
			nPosCoev = atol(words[16].substr(9).c_str());
		}
	}

	// Define burnin ratio
	size_t burninRatioToIter = burninRatio*nLine;
	const size_t BURN_IN_OFFSET =  std::max(burninIteration, burninRatioToIter);

	// For each line
	size_t iLine = 0;
	while(std::getline(iFile, line)) {
		if(iLine < BURN_IN_OFFSET) {
			iLine++;
			continue; // Skip burn-in
		}

		// Split the line
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t "));

		// [iteration] [ncluster] x ([LabelUID]        [Lik]     [D/S]     [idProfile]       [R1]      [R2]      [S]       [D])
		size_t iteration = atol(words[0].c_str());
		size_t nCoevPair = atol(words[1].c_str());

		if(posteriorDistK.count(nCoevPair) == 0) {
			posteriorDistK.insert(std::make_pair(nCoevPair, 1.));
		} else {
			posteriorDistK[nCoevPair] += 1;
		}

		for(size_t iC=0; iC < nCoevPair; ++iC) {
			size_t offset = 2+iC*8;

			std::string label(words[offset].c_str());

			// Count pos occurence
			std::vector<std::string> pos;
			boost::split(pos, label, boost::is_any_of("_"));
			assert(pos.size() >= 2);

			// Get the positions
			size_t iPos1 = atol(pos[0].c_str());
			size_t iPos2 = atol(pos[1].c_str());

			// Check if the cntPos vector is big enough
			size_t largestPos = std::max(iPos1, iPos2);
			if(largestPos+1 > cntPos.size()) {
				cntPos.resize(largestPos+1, 0);
			}

			// Count occurences
			cntPos[iPos1]++;
			cntPos[iPos2]++;

			// Get the stats object related to this pair
			mapCoevPairStats_t::iterator it = mapCoevPairStats.find(label);
			if(it == mapCoevPairStats.end()) { // If not existing, create it
				std::pair<std::string, CoevPairStats*> newPair;
				newPair.first = label;
				newPair.second = new CoevPairStats();
				mapCoevPairStats.insert(newPair);
				it = mapCoevPairStats.find(label);
				if(withCoevTraces) { // If coev traces are enable create the trace file
					std::stringstream ssStatFName;
					ssStatFName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << "_" << label << "pair.log";
					it->second->oFile.open(ssStatFName.str().c_str());
					it->second->oFile << "#categorical" << "\t" << "idProfile" << std::endl;
					it->second->oFile << "Iteration" << "\t" << "Lik" << "\t" << "ratioDS";
					it->second->oFile << "\t" << "idProfile" << "\t" << "r1" << "\t" << "r2" << "\t" << "s" << "\t" << "d" << std::endl;

					it->second->oFile.precision(8);
					it->second->oFile << std::scientific;
				}
			}

			// Write fake iteration
			if(withCoevTraces) it->second->oFile << it->second->count << "\t";
			it->second->count++;

			// Process each separate sampled values
			size_t iAcc=0;
			double lik = atof(words[offset+1].c_str());
			if(withCoevTraces) it->second->oFile << lik << "\t";
			it->second->varAccumulators[iAcc](lik); iAcc++;

			/*
			 * This track the overal average DS. We are not intersted by that.
			//double ratioDS = atof(words[offset+2].c_str());
			//if(withCoevTraces) it->second->oFile << ratioDS << "\t";
			//it->second->varAccumulators[iAcc](ratioDS); iAcc++;
			*/

			// Compute true parameters values
			double r1 = atof(words[offset+4].c_str());
			double expR1 = exp(r1);
			double r2 = atof(words[offset+5].c_str());
			double expR2 = exp(r2);
			double s = atof(words[offset+6].c_str());
			double expS = exp(s);
			double d = atof(words[offset+7].c_str());
			double expD = exp(d);

			double expSum = expR1 + expR2 + expS + expD;


			// Record ds ratio
			double ratioDS = expD/expS;
			if(withCoevTraces) it->second->oFile << ratioDS << "\t";
			it->second->varAccumulators[iAcc](ratioDS); iAcc++;

			// Record profiles type
			std::string profile(words[offset+3].c_str());
			if(withCoevTraces) it->second->oFile << profile << "\t";
			if(it->second->profAccumulator.count(profile) > 0) {
				it->second->profAccumulator[profile] += 1;
			} else {
				it->second->profAccumulator[profile] = 1;
			}

			// Record rate parameters
			if(withCoevTraces) it->second->oFile << r1 << "\t";
			it->second->varAccumulators[iAcc](expR1/expSum); iAcc++;


			if(withCoevTraces) it->second->oFile << r2 << "\t";
			it->second->varAccumulators[iAcc](expR2/expSum); iAcc++;


			if(withCoevTraces) it->second->oFile << s << "\t";
			it->second->varAccumulators[iAcc](expS/expSum); iAcc++;


			if(withCoevTraces) it->second->oFile << d << std::endl;
			it->second->varAccumulators[iAcc](expD/expSum); iAcc++;
		}

		iLine++;
		nSample++;
	}
	return true;
}

void CoevTraceAnalyser::writeSummaryRJLog(size_t &nSample, std::vector<size_t> &cntPos, mapCoevPairStats_t &mapCoevPairStats, double significanceThresh) {
	// Open file.
	std::ofstream oFile(coevStatStringFN.c_str());

	{ // Writing coev frequency for each position ordered by most frequent to least frequent
		oFile << "Ordered sites by coevolution frequency  [ pos : perc ] : " << std::endl;
		oFile << std::fixed << std::setprecision(2);
		std::vector<size_t> frequentlyCoevPosition;
		std::vector<size_t> orderingPos = Utils::Algorithm::getOrderedIndices(cntPos, false);
		for(size_t iO=0; iO < orderingPos.size(); ++iO) {
			double perc = 100.*(double)cntPos[orderingPos[iO]] / (double)nSample;

			if(perc > REPORT_COEVOLUTION_THRESHOLD) {	// We keep these in memory for a subsequent report
				frequentlyCoevPosition.push_back(orderingPos[iO]);
			} else if(perc < REPORT_PERCENT_THRESHOLD) { // To low to report
				oFile << std::endl;
				break;
			}

			oFile << std::setw(5) << orderingPos[iO] << " : " << std::setw(6) << perc << "\t";
			if(((iO+1) % 10 == 0) || iO == orderingPos.size()-1){
				oFile << std::endl;
			}
		}
		oFile << std::endl;

		std::sort(frequentlyCoevPosition.begin(), frequentlyCoevPosition.end());
		oFile << "Frequently coevolving position ( > " << REPORT_COEVOLUTION_THRESHOLD << "% ) -> " << frequentlyCoevPosition.size() << " positions : " << std::endl;
		for(size_t iF=0; iF < frequentlyCoevPosition.size(); ++iF) {
			oFile << frequentlyCoevPosition[iF];
			if(iF < frequentlyCoevPosition.size()-1) oFile << ", ";
		}
		oFile << std::endl;
		oFile << "--------------------------------------------------" << std::endl;
	}

	{ // Preparing and writting the ordered coev pairs by freq
		std::vector<mapCoevPairStats_t::iterator> itPairs;
		std::vector<size_t> cntPairs;
		for(mapCoevPairStats_t::iterator it = mapCoevPairStats.begin(); it != mapCoevPairStats.end(); ++it) {
			itPairs.push_back(it);
			cntPairs.push_back(it->second->count);
		}

		oFile << std::scientific << std::setprecision(2);
		oFile << "Significantly coevolving pairs ordered by posterior probability." << std::endl;
		oFile << "> Significance threshold defined as p>" << significanceThresh << " using Kass and Raftery 1995 guidelines for strong significance (2 x ln(BF) > 10)." << std::endl << std::endl;
		oFile << std::setw(8) << "Posterior prob." << "\t" << std::setw(8) <<"Label" << "\t" << std::setw(20) << "Most Prob. Prof" << "\t";
		oFile << std::setw(8) << "Prof prob." << "\t" << std::setw(11) << "Lik" << "\t";
		oFile << std::setw(22) << "ratioDS" << "\t" << std::setw(21) << "r1" << "\t" << std::setw(21) << "r2" << "\t";
		oFile << std::setw(21) << "s" << "\t" << std::setw(21) << "d" << std::endl;
		std::vector<size_t> orderingPos = Utils::Algorithm::getOrderedIndices(cntPairs, false);

		size_t iO = 0;
		while(iO < orderingPos.size()) {
			std::vector<size_t> positions, firstNb;
			{
				size_t idPair = orderingPos[iO];
				double firstFreq = (double)cntPairs[idPair] / (double)nSample;
				while(positions.empty() || (iO < orderingPos.size() &&
					  fabs(firstFreq - ((double)cntPairs[orderingPos[iO]] / (double)nSample)) < 1e-8)) {

					size_t idPair = orderingPos[iO];
					std::vector<std::string> words;
					boost::split(words, itPairs[idPair]->first, boost::is_any_of("_"));
					positions.push_back(idPair);
					firstNb.push_back(atol(words[0].c_str()));
					iO++;
				}
			}

			std::vector<size_t> orderingNames = Utils::Algorithm::getOrderedIndices(firstNb, true);

			for(size_t iN=0; iN < orderingNames.size(); ++iN) {
				size_t idPair = positions[orderingNames[iN]];

				if((double)cntPairs[idPair] / (double)nSample < significanceThresh) {
					break;
				}

				oFile << std::setw(8) << (double)cntPairs[idPair] / (double)nSample << "\t"; // Pair frequency
				oFile << std::setw(8) << itPairs[idPair]->first << "\t"; // Pair label

				// Finding the most probable profile
				std::string mostFreqProf;
				size_t mostFreqProfCnt = 0;
				typedef std::map< std::string, size_t >::iterator itPA_t;
				for(itPA_t itPA = itPairs[idPair]->second->profAccumulator.begin(); itPA != itPairs[idPair]->second->profAccumulator.end(); ++itPA) {
					if(itPA->second > mostFreqProfCnt) {
						mostFreqProf =  itPA->first;
						mostFreqProfCnt = itPA->second;
					}
				}
				oFile << std::setw(18) << mostFreqProf << "\t" << std::setw(8) << (double)mostFreqProfCnt / (double)itPairs[idPair]->second->count << "\t"; // profile

				// Summary from accumulators
				for(size_t iA=0; iA<itPairs[idPair]->second->varAccumulators.size(); ++iA) {
					oFile << std::setw(9) << boost::accumulators::mean(itPairs[idPair]->second->varAccumulators[iA]);
					oFile << " +/- " << std::setw(8) << sqrt((double)boost::accumulators::variance(itPairs[idPair]->second->varAccumulators[iA])) << "\t";
				}

				oFile << std::endl;
			}
		}
	}
}

double CoevTraceAnalyser::computeLogPriorPairProbability(std::map<size_t, size_t> &posteriorDistK) {

	const size_t K_MAX = std::floor(nPosCoev/2.); // Max number of pairs

	double logProb = 0.;
	double normConstant = 0.; // Normalizing constant due to truncated poisson distribution

	size_t kCount=0;
	for(std::map<size_t, size_t>::iterator it=posteriorDistK.begin(); it != posteriorDistK.end(); ++it) {
		kCount += it->second;
	}

	// Integrating over possible k : Sum [ P(k)*P([A,B] | k) ]
	for(size_t k=0; k<K_MAX; ++k) { // Integrate over k
		// Compute the prior probability of a pair given k ==> P([A,B] | k)
		double logPairProbGivenK = 0.;
		logPairProbGivenK = log(2.*k/(nPosCoev*(nPosCoev-1.)));

		// Compute the prior probability of k ==> P(k)
		// This probability is function of:
		// - the poisson prior (lambda) : k~p(lambda)
		// - the gamma hyperprior (alpha, beta) : lambda~p(alpha, beta)

		typedef enum {CASE_HYPERPRIOR, CASE_EMPIRIC_LAMBDA, CASE_EMPIRIC_P_OF_K} myCases_t;
		myCases_t myCase = CASE_EMPIRIC_P_OF_K;

		double logNbPairProb = 0.;
		if(myCase == CASE_HYPERPRIOR) {
			// Special case (default parameters) // POISSON x GAMMA
			if(alphaHP == 1.0 && betaHP == 10.0) {
				logNbPairProb = log(10.)-(k+1.)*log(11.);
			} else { // Any other cases
				logNbPairProb = alphaHP*log(betaHP) + (-alphaHP-k)*log(betaHP+1.) + boost::math::lgamma((double)(alphaHP+k));
				logNbPairProb -= (boost::math::lgamma(k+1.) - boost::math::lgamma((double) alphaHP));
			}
		} else if(myCase == CASE_EMPIRIC_LAMBDA) {
			double lambda = 36.5;
			logNbPairProb = k*log(lambda)-lambda-boost::math::lgamma(k+1.); // Poisson with estimated lambda
		} else if(myCase == CASE_EMPIRIC_P_OF_K) {
			if(posteriorDistK.count(k) > 0 ) {
				//std::cout << k << "\t" << posteriorDistK[k] << "\t" << kCount << std::endl;
				logNbPairProb = log((double)posteriorDistK[k]/(double)kCount);
			} else {
				logNbPairProb = log(0.);
			}
		}

		// logProb is equal to P(k)*P([A,B] | k) for k known
		double logProbGivenK = logPairProbGivenK + logNbPairProb;

		// Summing log values (Sum [ P(k)*P([A,B] | k) ])
		if(k==1) {
			logProb = logProbGivenK;
		} else if(k>1 && logNbPairProb != -std::numeric_limits<double>::infinity()) {
			logProb = Utils::Math::Functions::addLogValues(logProb, logProbGivenK);
		}
		//std::cout << k << "\tlog P([A,B]|k) =" << logPairProbGivenK << "\tlog P(k) = " << logNbPairProb << "\t SumLP =" << logProb << std::endl; //FIXME DEBUG

		// Keeping track of P(k) => Sum [ P(k) ]
		if(k==0) {
			normConstant = logNbPairProb;
		} else if (logNbPairProb != -std::numeric_limits<double>::infinity()) {
			normConstant = Utils::Math::Functions::addLogValues(normConstant, logNbPairProb);
		}

	}
	// Applying normalizing constant for truncated poisson
	//std::cout << "Final :\tlogProb = " << logProb << "\tnrmCst = " << normConstant << "\tlogProb-nrmCst = " << logProb-normConstant << std::endl; //FIXME DEBUG
	logProb -= normConstant;


	return logProb;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
