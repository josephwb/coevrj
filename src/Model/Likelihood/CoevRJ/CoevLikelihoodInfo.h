//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevLikelihoodInfo.h
 *
 * @date Apr 5, 2017
 * @author meyerx
 * @brief
 */
#ifndef COEVLIKELIHOODINFO_H_
#define COEVLIKELIHOODINFO_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <string>
#include <vector>

#include "Nodes/IncNodes.h"
#include "UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;
class RootCoevNode;

namespace MD = ::MolecularEvolution::Definition;

// Utils
class CoevLikelihoodInfo;

typedef std::list<CoevLikelihoodInfo*> listCLI_t;
typedef listCLI_t::iterator itListCLI_t;
typedef listCLI_t::const_iterator constItListCLI_t;

class CoevLikelihoodInfo {
public:
	CoevLikelihoodInfo(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace, const std::vector<MD::idProfile_t> &aVecProfilesId);
	~CoevLikelihoodInfo();

	std::pair<size_t, size_t> getPositions() const;
	const std::vector<MD::idProfile_t>& getProfilesId() const;

	SubSpaceUID::sharedPtr_t getUIDSubSpace() const;

	void setRootDAG(RootCoevNode *aRootDAG);
	RootCoevNode* getRootDAG();

	void setMatrixCoev(MatrixCoevNode *aMatrixCoev);
	MatrixCoevNode* getMatrixCoev();

	void setLeafNodes(std::vector<LeafCoevNode*>& aLeafNodes);
	std::vector<LeafCoevNode*>& getLeafNodes();

	void setPInd(const std::vector<size_t>& aPind);
	const std::vector<size_t>& getPInd() const;

	void setTreeHash(long int aTreeHash);
	long int getTreeHash() const;

	bool operator== (const CoevLikelihoodInfo &aPLS) const;

	std::string toString() const;

private:

	SubSpaceUID::sharedPtr_t ptrUIDSubSpace;
	size_t pos1, pos2;
	long int treeHash;
	std::vector<size_t> pInd;
	std::vector<MD::idProfile_t> vecProfilesId;

	RootCoevNode *rootDAG;
	MatrixCoevNode* ptrMatrixCoev;
	std::vector<LeafCoevNode*> leafNodes;

	friend class Base;
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COEVLIKELIHOODINFO_H_ */
