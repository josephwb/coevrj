//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "TreeNode.h"

#include "Utils/MolecularEvolution/TreeReconstruction/TreeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class BranchMatrixCoevNode;
class BranchMatrixGTRGNode;
class EdgeCoevNode;
class EdgeGTRGNode;

TreeNode::TreeNode(const size_t aNGamma, const std::string aName, const TR::TreeNode *aTreeNodeTR) :
		N_GAMMA(aNGamma) {
	id = aTreeNodeTR->getId();
	name = aName;
	branchLength = 0.;

	leaf = false;

	requireReinitDAG = false;
}

TreeNode::~TreeNode() {
}


void TreeNode::addChild(TreeNode *child) {
	children.push_back(child);
	//sortChildren();
}

bool TreeNode::removeChild(TreeNode *child) {
	std::vector<TreeNode*>::iterator position = std::find(children.begin(), children.end(), child);
	if (position != children.end()) {
		children.erase(position);
		//sortChildren();
		return true;
	}
	return false;
}

bool TreeNode::isParent(const TreeNode *aChild) const {
	std::vector<TreeNode*>::const_iterator position = std::find(children.begin(), children.end(), aChild);
	return position != children.end();
}

bool TreeNode::isChild(const TreeNode *aParent) const {
	return aParent->isParent(this);
}

TreeNode* TreeNode::getChild(size_t id) {
	return children[id];
}


treeNodeTI_children& TreeNode::getChildren() {
	return children;
}

const treeNodeTI_children& TreeNode::getChildren() const {
	return children;
}

const std::string& TreeNode::getName() const {
	return name;
}

size_t TreeNode::getId() const {
	return id;
}

void TreeNode::addCoevEdgeNode(EdgeCoevNode *node) {
	//std::cout << "[TreeNode] id= " << getId() << "\t-> Adding EdgeCoevNode [" << node->getId() << "]" << std::endl; // FIXME
	treeStructCoev.push_back(node);
}

std::vector<EdgeCoevNode *>& TreeNode::getCoevEdgeNodes() {
	return treeStructCoev;
}

bool TreeNode::removeCoevEdgeNode(EdgeCoevNode *node) {
	size_t pos = treeStructCoev.size();
	for(size_t iN=0; iN<treeStructCoev.size(); ++iN) {
		if(treeStructCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}

	if(pos == treeStructCoev.size()) {
		return false;
	} else {
		//std::cout << "[TreeNode] id= " << getId() << "\t-> Removing EdgeCoevNode [" << node->getId() << "]" << std::endl; // FIXME
		treeStructCoev.erase(treeStructCoev.begin()+pos);
		return true;
	}
}

bool TreeNode::hasCoevEdgeNode(DAG::BaseNode *node) {
	size_t pos = treeStructCoev.size();
	for(size_t iN=0; iN<treeStructCoev.size(); ++iN) {
		if(treeStructCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}
	if(pos == treeStructCoev.size()) {
		return false;
	} else {
		return true;
	}
}

void TreeNode::addCoevBranchMatrixNode(BranchMatrixCoevNode *node) {
	branchMCoev.push_back(node);
	//std::cout << "[TreeNode] id= " << getId() << "\t-> Adding branchMCoev [" << node->getId() << "]" << std::endl; // FIXME
}

std::vector<BranchMatrixCoevNode *>& TreeNode::getCoevBranchMatrixNodes() {
	return branchMCoev;
}

bool TreeNode::removeCoevBranchMatrixNode(BranchMatrixCoevNode *node) {
	size_t pos = branchMCoev.size();
	for(size_t iN=0; iN<branchMCoev.size(); ++iN) {
		if(branchMCoev[iN]->getId() == node->getId()) {
			pos = iN;
			break;
		}
	}

	if(pos == branchMCoev.size()) {
		return false;
	} else {
		//std::cout << "[TreeNode] id= " << getId() << "\t-> Removing branchMCoev [" << node->getId() << "]" << std::endl; // FIXME
		branchMCoev.erase(branchMCoev.begin()+pos);
		return true;
	}
}

void TreeNode::addPermCoevEdgeNode(PermEdgeCoevNode *node) {
	treeStructPermCoev.push_back(node);
}

std::vector<PermEdgeCoevNode *>& TreeNode::getPermCoevEdgeNodes() {
	return treeStructPermCoev;
}

void TreeNode::addPermCoevBranchMatrixNode(PermBranchMatrixCoevNode *node) {
	branchMPermCoev.push_back(node);
}

std::vector<PermBranchMatrixCoevNode *>& TreeNode::getPermCoevBranchMatrixNodes() {
	return branchMPermCoev;
}

void TreeNode::addGTRGEdgeNode(EdgeGTRGNode *node) {
	treeStructGTRG.push_back(node);
}

std::vector<EdgeGTRGNode *>& TreeNode::getGTRGEdgeNodes() {
	return treeStructGTRG;
}

void TreeNode::addGTRGBranchMatrixNode(BranchMatrixGTRGNode *node) {
	branchMGTRG.push_back(node);
}

std::vector<BranchMatrixGTRGNode *>& TreeNode::getGTRGBranchMatrixNodes() {
	return branchMGTRG;
}

void TreeNode::setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes) {
	// FIXME
	//std::stringstream ss;
	//ss << "[" << Parallel::mcmcMgr().getRankProposal() << "] upd : " << branchLength << " -> " << aBL << " (" << branchMDAG.size() << " -> " ;
	branchLength = aBL;

	for(size_t i=0; i<branchMGTRG.size(); ++i){
		bool isUpdated = branchMGTRG[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMGTRG[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}
	//ss << std::endl;

	for(size_t i=0; i<branchMCoev.size(); ++i){
		bool isUpdated = branchMCoev[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}

	for(size_t i=0; i<branchMPermCoev.size(); ++i){
		bool isUpdated = branchMPermCoev[i]->setBranchLength(branchLength);
		if(isUpdated) updatedNodes.insert(branchMPermCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}

	//ss << std::endl;
	//std::cout << ss.str();
}

void TreeNode::setCoevScaling(const double aCoevScaling, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t i=0; i<branchMCoev.size(); ++i){
		bool isUpdated = branchMCoev[i]->setCoevScaling(aCoevScaling);
		if(isUpdated) updatedNodes.insert(branchMCoev[i]);
	}

	for(size_t i=0; i<branchMPermCoev.size(); ++i){
		bool isUpdated = branchMPermCoev[i]->setCoevScaling(aCoevScaling);
		if(isUpdated) updatedNodes.insert(branchMPermCoev[i]);
		//ss << branchMDAG[i]->getId() << ", ";
	}
	//std::cout << "Update coevScaling = " << aCoevScaling << std::endl; // FIXME DEBUG
}

double TreeNode::getBranchLength() const {
	return branchLength;
}

void TreeNode::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNode::isLeaf() const {
	return leaf;
}

void TreeNode::setRequireReinitDAG(const bool aRequire) {
	requireReinitDAG = aRequire;
}

bool TreeNode::doRequireReinitDAG() const {
	return requireReinitDAG;
}


const std::string TreeNode::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << branchLength;
	if(leaf) ss << " - isLeaf";
	ss << std::endl;

	ss << "Edge Coev : ";
	for(size_t i=0; i<treeStructCoev.size(); ++i) ss << treeStructCoev[i]->getId() << "\t";
	ss << std::endl << "BM Coev : ";
	for(size_t i=0; i<branchMCoev.size(); ++i) ss << branchMCoev[i]->getId() << "\t";



	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]" << children[i]->name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNode::deleteChildren() {
	while(!children.empty()) {
		TreeNode *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string TreeNode::buildNewick() const {
	std::string nwkStr;
	addNodeToNewick(nwkStr);
	return nwkStr;
}

void TreeNode::addNodeToNewick(std::string &nwk) const {
	if(!children.empty()) {
		nwk.push_back('(');
		children[0]->addNodeToNewick(nwk);

		for(size_t iC=1; iC<children.size(); ++iC) {
			nwk.push_back(',');
			nwk.push_back(' ');
			children[iC]->addNodeToNewick(nwk);
		}
		nwk.push_back(')');
	}

	nwk.append(name);
	nwk.push_back(':');
	std::stringstream ss;
	ss << branchLength;
	nwk.append(ss.str());
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
