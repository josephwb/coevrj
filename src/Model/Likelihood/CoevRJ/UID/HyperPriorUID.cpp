//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HyperPriorUID.cpp
 *
 * @date May 25, 2017
 * @author meyerx
 * @brief
 */
#include "HyperPriorUID.h"

#include <sstream>
#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(StatisticalModel::Likelihood::CoevRJ::HyperPriorUID, "Model::Likelihood::CoevRJ::HyperPriorUID")

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

HyperPriorUID::HyperPriorUID() : labelUID("HYPERPRIOR_COEV") {
}

HyperPriorUID::~HyperPriorUID() {
}

bool HyperPriorUID::operator==(const Sampler::RJMCMC::RJSubSpaceUID &rhs) const {
	const HyperPriorUID *castedRHS = dynamic_cast<const HyperPriorUID*>(&rhs);
	if (castedRHS == NULL) {
		return false;
	}

	return (labelUID == castedRHS->labelUID);
}

std::string HyperPriorUID::getLabelUID() const {
	return labelUID;
}

template<class Archive>
void HyperPriorUID::save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar << BOOST_SERIALIZATION_NVP( labelUID );
}

template<class Archive>
void HyperPriorUID::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Sampler::RJMCMC::RJSubSpaceUID);

	ar >> BOOST_SERIALIZATION_NVP( labelUID );
}

template void HyperPriorUID::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void HyperPriorUID::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void HyperPriorUID::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void HyperPriorUID::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
