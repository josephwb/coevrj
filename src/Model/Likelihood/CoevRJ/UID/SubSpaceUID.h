//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SubSpaceUID.h
 *
 * @date Apr 18, 2017
 * @author meyerx
 * @brief
 */
#ifndef SUBSPACEUID_H_
#define SUBSPACEUID_H_

#include <stddef.h>
#include <list>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class SubSpaceUID;

class SubSpaceUID: public Sampler::RJMCMC::RJSubSpaceUID {
public:
	typedef boost::shared_ptr<SubSpaceUID> sharedPtr_t;

public:
	SubSpaceUID(size_t aPos1, size_t aPos2);
	~SubSpaceUID();

	bool operator==(const Sampler::RJMCMC::RJSubSpaceUID &rhs) const;

	std::string getLabelUID() const;

	size_t getPos1() const;
	size_t getPos2() const;

	std::pair<size_t, size_t> getPositions() const;

private:

	SubSpaceUID();

	size_t pos1, pos2;
	std::string labelUID;

	void defineLabelUID();

	friend class CoevLikelihoodInfo;
	friend class PermCoevLikelihoodInfo;

	// Boost serialization
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SUBSPACEUID_H_ */
