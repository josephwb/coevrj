//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef TREENODE_COEVRJ_H_
#define TREENODE_COEVRJ_H_

#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

#include <stddef.h>
#include <algorithm>
#include <sstream>
#include <vector>

#include "Nodes/IncNodes.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/TmpNode.h"

namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class BranchMatrixCoevNode;
class BranchMatrixGTRGNode;
class EdgeCoevNode;
class EdgeGTRGNode;

namespace TR = MolecularEvolution::TreeReconstruction;

class TreeNode;

typedef std::vector<TreeNode*> treeNodeTI_children;

class TreeNode {
public:
	TreeNode(const size_t aNGamma, const std::string aName, const TR::TreeNode *aTreeNodeTR);
	~TreeNode();

	void addChild(TreeNode *child);
	bool removeChild(TreeNode *child);

	bool isParent(const TreeNode *aChild) const;
	bool isChild(const TreeNode *aParent) const;

	TreeNode* getChild(size_t id);
	treeNodeTI_children& getChildren();
	const treeNodeTI_children& getChildren() const;
	const std::string& getName() const;
	size_t getId() const;

	// Coev nodes
	void addCoevEdgeNode(EdgeCoevNode *node);
	std::vector<EdgeCoevNode *>& getCoevEdgeNodes();
	bool removeCoevEdgeNode(EdgeCoevNode *node);
	bool hasCoevEdgeNode(DAG::BaseNode  *node);

	void addCoevBranchMatrixNode(BranchMatrixCoevNode *node);
	std::vector<BranchMatrixCoevNode *>& getCoevBranchMatrixNodes();
	bool removeCoevBranchMatrixNode(BranchMatrixCoevNode *node);

	// Perm coev nodes
	// Coev nodes
	void addPermCoevEdgeNode(PermEdgeCoevNode *node);
	std::vector<PermEdgeCoevNode *>& getPermCoevEdgeNodes();

	void addPermCoevBranchMatrixNode(PermBranchMatrixCoevNode *node);
	std::vector<PermBranchMatrixCoevNode *>& getPermCoevBranchMatrixNodes();

	// GTRG nodes
	void addGTRGEdgeNode(EdgeGTRGNode *node);
	std::vector<EdgeGTRGNode *>& getGTRGEdgeNodes();

	void addGTRGBranchMatrixNode(BranchMatrixGTRGNode *node);
	std::vector<BranchMatrixGTRGNode *>& getGTRGBranchMatrixNodes();

	// Other
	void setBranchLength(const double aBL, std::set<DAG::BaseNode*> &updatedNodes);
	void setCoevScaling(const double aCoevScaling, std::set<DAG::BaseNode*> &updatedNodes);
	void setLeaf(const bool aLeaf);

	double getBranchLength() const;
	bool isLeaf() const;

	void setRequireReinitDAG(const bool aRequire);
	bool doRequireReinitDAG() const;

	const std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

private:

	typedef std::vector<EdgeCoevNode *> treeStructCoev_t;
	typedef std::vector<BranchMatrixCoevNode *> branchMCoev_t;

	typedef std::vector<PermEdgeCoevNode *> treeStructPermCoev_t;
	typedef std::vector<PermBranchMatrixCoevNode *> branchMPermCoev_t;

	typedef std::vector<EdgeGTRGNode *> treeStructGTRG_t;
	typedef std::vector<BranchMatrixGTRGNode *> branchMGTRG_t;

	/* Default data */
	size_t id;
	const size_t N_GAMMA;
	std::string name;
	double branchLength;

	/* state */
	bool leaf, requireReinitDAG;
	treeNodeTI_children children;

	/* Linkage with DAG elements */
	treeStructCoev_t treeStructCoev;
	branchMCoev_t branchMCoev;

	treeStructPermCoev_t treeStructPermCoev;
	branchMPermCoev_t branchMPermCoev;

	treeStructGTRG_t treeStructGTRG;
	branchMGTRG_t branchMGTRG;

};


struct SortTreeNodePtrById
{
    bool operator()( const TreeNode* n1, const TreeNode* n2 ) const {
    	return n1->getId() < n2->getId();
    }
};


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODE_COEVRJ_H_ */
