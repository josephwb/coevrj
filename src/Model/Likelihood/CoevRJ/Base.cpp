//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Base.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "Base.h"

#include <iosfwd>
#include <assert.h>
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include "mpi.h"

#include "Model/Likelihood/CoevRJ/ReversibleJump/DefineConstantsCoev.h"
#include "Model/Likelihood/CoevRJ/ReversibleJump/UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/BaseSampleWrapper.h"
#include "Model/Likelihood/CoevRJ/CoevAlignementManager.h"
#include "Model/Likelihood/CoevRJ/CoevLikelihoodInfo.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/EdgeCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/RootCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/FinalResultNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/DiscreteGammaNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Model/Likelihood/CoevRJ/TreeNode.h"

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Scheduler/BaseScheduler.h"
#include "Sampler/Samples/Sample.h"

#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"

using boost::accumulators::tag::accumulator;
using boost::accumulators::tag::variance;

namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }
namespace MolecularEvolution { namespace TreeReconstruction { class TreeNode; } }
namespace Sampler { class ModelSpecificSample; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class BranchMatrixCoevNode;
class BranchMatrixGTRGNode;
class CPVCoevNode;
class CPVGTRGNode;
class CoevTraceWriter;
class CombineSiteGTRGNode;
class EdgeGTRGNode;
class LeafCoevNode;
class LeafGTRGNode;
class MatrixCoevNode;
class RootGTRGNode;

const std::string Base::NAME_TREE_PARAMETER = "TreeHash";

const Base::markersGTR_t Base::STATE_GTR_MARKERS = Base::DISABLE_GTR_MARKERS;

const Base::debug_t Base::doDebug = DISABLE_DEBUG;

const size_t Base::MAX_PENDING_COEV_DAG = 3;
const size_t Base::MIN_SUBSTITUTION_PER_SITE = 2;
const double Base::MAXIMUM_GAP_RATIO = 0.90;
//const coevScalingType_t Base::COEV_SCALING_TYPE = WITH_SCALING_COEV;
//const coevPriorScalingType_t Base::PRIOR_SCALING_TYPE = GAMMA_PR;

Base::Base(size_t aNGamma, const size_t aNThread,  bool aUseStationaryFrequencyGTR, bool aCachePairProfileInfo,
		   DL::NewickParser *aNP, const std::string &aFileAlign, treeUpdateStrategy_t aStrat) :
	    		   N_GAMMA(aNGamma), nThread(aNThread), useStationaryFrequencyGTR(aUseStationaryFrequencyGTR),
	    		   cachePairProfileInfo(aCachePairProfileInfo), fileAlign(aFileAlign),
	    		   COEV_STRATEGY(RJ_STRATEGY), TREE_UPDATE_STRATEGY(aStrat),
	    		   coevTraceWriter(COEV_STRATEGY==FIXED_POSITIONS_STRATEGY),
	    		   freqGTR(MD::Nucleotides::NB_BASE_NUCL),
	    		   freqCoev(MD::Nucleotides::NB_BASE_NUCL*MD::Nucleotides::NB_BASE_NUCL) {

	if(doDebug != DISABLE_DEBUG) {
		std::stringstream ss;
		ss << "dbgFile_" << Parallel::mcmcMgr().getRankProposal() << "_" << Parallel::mcmcMgr().getNProposal() << "P.txt";
		dbgFile.open(ss.str().c_str());
	}

	std::string dummyStr;
	init(aNP, dummyStr);
	MPI_Barrier(MPI_COMM_WORLD);
}

Base::Base(size_t aNGamma, const size_t aNThread, bool aUseStationaryFrequencyGTR, bool aCachePairProfileInfo,
		   DL::NewickParser *aNP, const std::string &aFileAlign,
		   const std::string &aDataFileName, treeUpdateStrategy_t aStrat) :
	    		   N_GAMMA(aNGamma), nThread(aNThread), useStationaryFrequencyGTR(aUseStationaryFrequencyGTR),
	    		   cachePairProfileInfo(aCachePairProfileInfo), fileAlign(aFileAlign),
	    		   COEV_STRATEGY(FIXED_POSITIONS_STRATEGY), TREE_UPDATE_STRATEGY(aStrat),
	    		   coevTraceWriter(COEV_STRATEGY==FIXED_POSITIONS_STRATEGY),
	    		   freqGTR(MD::Nucleotides::NB_BASE_NUCL),
	    		   freqCoev(MD::Nucleotides::NB_BASE_NUCL*MD::Nucleotides::NB_BASE_NUCL) {

	if(doDebug != DISABLE_DEBUG) {
		std::stringstream ss;
		ss << "dbgFile_" << Parallel::mcmcMgr().getRankProposal() << "_" << Parallel::mcmcMgr().getNProposal() << "P.txt";
		dbgFile.open(ss.str().c_str());
	}

	init(aNP, aDataFileName);

	MPI_Barrier(MPI_COMM_WORLD);
}

Base::~Base() {
	//rootDAG->deleteChildren();
	//delete rootDAG;

	while(!activeTempCoevInfo.empty()) {
		CoevLikelihoodInfo* ptr = activeTempCoevInfo.front();
		activeTempCoevInfo.pop_front();
		// deleteCoevDAG(ptr); <-- No need, its connected to the DAG
		delete ptr;
	}

	while(!inactiveTempCoevInfo.empty()) {
		CoevLikelihoodInfo* ptr = inactiveTempCoevInfo.front();
		deleteCoevDAG(ptr);
		inactiveTempCoevInfo.pop_front();
		delete ptr;
	}

	while(!activePermCoevInfo.empty()) {
		PermCoevLikelihoodInfo* ptr = activePermCoevInfo.front();
		activePermCoevInfo.pop_front();
		delete ptr;
	}

	while(!inactivePermCoevInfo.empty()) {
		PermCoevLikelihoodInfo* ptr = inactivePermCoevInfo.front();
		inactiveTempCoevInfo.pop_front();
		delete ptr;
	}

	while(!pendingDAGs.empty()) {
		CoevLikelihoodInfo* ptr = pendingDAGs.front();
		pendingDAGs.pop_front();
		deleteCoevDAG(ptr);
		delete ptr;
	}

	for(size_t iP=0; iP<permutedCoev.size(); ++iP) {
		PermutedCoevLikelihood* ptr = permutedCoev[iP];
		deletePermutedCoevDAG(ptr);
	}


	cleanTreeAndDAG();
	delete scheduler;
}

void Base::init(DL::NewickParser *aNP, const std::string &aDataFileName) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	isTreeExternalyImported = false;
	requireSchedulerPartialReset = false;

	DL::FastaReader fr(fileAlign);
	DL::MSA msa(DL::MSA::NUCLEOTIDE, fr.getAlignments(), false);

	// If the PairProfileInfos have to be cached (saved or loaded from a file)
	if(cachePairProfileInfo) {
		std::stringstream ssCacheFile; // Create the file name
		ssCacheFile << fileAlign << ".scoresCache";
		std::string cacheFile(ssCacheFile.str());
		coevAlignManager.reset(new CoevAlignementManager(msa, cacheFile));
	} else { // No cache
		coevAlignManager.reset(new CoevAlignementManager(msa));
	}

	//drawMostProbablePairs();
	// Frequencies are built as uniform by default:
	//freqGTR.set(DL::NucleotideFrequencies(msa).getEmpiric()); // FIXME
	//std::cout << freqGTR.getEigen() << std::endl;
	// >> freqGTR.set(DL::NucleotideFrequencies(msa).getUniform());
	// >> freqCoev.set(...);

	std::vector<std::string> orderedTaxaNames = msa.getOrderedTaxaNames();//getOrderedTaxaNames(msa);
	treeManager.reset(new TR::TreeManager(orderedTaxaNames));

	if(aNP != NULL) {
		curTree.reset(new TR::Tree(aNP->getRoot(), treeManager->getNameMapping()));
	} else {
		curTree.reset(new TR::Tree(true, msa, treeManager->getNameMapping()));
	}

	treeManager->setTree(curTree);
	treeManager->memorizeTree();
	curHash = curTree->getHashKey();

	//nSite = msa.getNValidSite();
	nSite = msa.getNSite();

	// By default all site are GTR+Gamma
	size_t nConserved = 0, nGapped = 0, nNonInformative = 0, nPseudoConserved = 0;
	sitesAvailability.resize(nSite, false);
	for(size_t iS=0; iS<nSite; ++iS) {
		sitesGTRG.insert(iS);

		bool isConserved = msa.isConservedSite(iS);
		bool isFullGap = msa.isAllGapSite(iS);
		bool isNonInformative = msa.computeGapRatioAtSite(iS) > MAXIMUM_GAP_RATIO;
		bool isPseudoConserved = !isConserved && !isFullGap && !isNonInformative && msa.isPseudoConvserved(iS, MIN_SUBSTITUTION_PER_SITE);

		if(!isConserved && !isFullGap && !isNonInformative && !isPseudoConserved) {
			segregatingSites.insert(iS);
			sitesAvailability[iS] = true;
		} else {
			nConserved += isConserved ? 1 : 0;
			nGapped += isFullGap ? 1 : 0;
			nNonInformative += isNonInformative ? 1 : 0;
			nPseudoConserved += isPseudoConserved ? 1 : 0;
		}
	}
	nSegregatingSite = segregatingSites.size();
	coevTraceWriter.setNSite(nSegregatingSite);

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "Number of sites : " << nSite;
		std::cout << "\t|\t Number available for Coevolution : " << nSegregatingSite << std::endl;
		std::cout << "--> Number of conserved sites : " << nConserved;
		std::cout << "\t|\t Number of pseudo-conserved (differences <= ";
		std::cout << MIN_SUBSTITUTION_PER_SITE << ") : " << nPseudoConserved << std::endl;
		std::cout << "--> Number of gapped sites : " << nGapped << "\t|\t Number of non-informative sites (gap > ";
		std::cout << MAXIMUM_GAP_RATIO << " ) : " << nNonInformative << std::endl;
	}
	assert((nSegregatingSite + nConserved + nGapped + nNonInformative + nPseudoConserved) == nSite);

	prepareBaseDAG();
	createTreeAndBaseDAG(fr, msa);
	createPermutedDAG();

	// Recover initial branch length if existing
	if(aNP != NULL) {
		const std::map<size_t, size_t> &mapId = curTree->getNodeIdtoNewickMapping();
		assert(!mapId.empty());
		const std::map<size_t, double> &newickBL = aNP->getInitialBLs();
		initialBranchLenghts.resize(branchPtr.size());

		for(size_t iBL=0; iBL<branchPtr.size(); ++iBL) {
			std::map<size_t, size_t>::const_iterator itNewickId = mapId.find(branchPtr[iBL]->getId());
			assert(itNewickId != mapId.end());
			size_t newickId = itNewickId->second;
			std::map<size_t, double>::const_iterator itFind = newickBL.find(newickId);
			assert(itFind != newickBL.end());
			double initBL = itFind->second;
			initialBranchLenghts[iBL] = initBL;
		}
	}

	if(COEV_STRATEGY == FIXED_POSITIONS_STRATEGY) {
		loadCoevInformations(aDataFileName);
	}

	cntErrMsg = 0;
	if(nThread > 1) {
		Eigen::initParallel();
		//scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	initPriors();

	// Priors [HAVE TO BE FIRST 5 or(7)!]
	if(!isUsingFixedPairsStrategy()) {
		markerNames.push_back("PriorK");
		markerNames.push_back("HyerPriorK");
	}
	markerNames.push_back("PriorPairs");
	markerNames.push_back("PriorGTR");
	markerNames.push_back("PriorFreqGTR");
	markerNames.push_back("PriorCoev");
	markerNames.push_back("PriorScaling");

	// GTR normalized
	for(size_t iT=0; iT<4; ++iT) {
		std::stringstream ss;
		ss << "Norm. " << MD::Nucleotides::NUCL_BASE[iT];// "NormTheta_" << iT;
		markerNames.push_back(ss.str());
	}

	for(size_t iT=0; iT<6; ++iT) {
		std::stringstream ss;
		ss << "Norm. " << MolecularEvolution::MatrixUtils::GTR_MF::PARAMETERS_NAME[iT];
		//ss << "NormTheta_" << iT;
		markerNames.push_back(ss.str());
	}

	markerNames.push_back("avg R1");
	markerNames.push_back("avg R2");
	markerNames.push_back("avg S");
	markerNames.push_back("avg D");
	markerNames.push_back("avg D/S");

	// Branch length
	markerNames.push_back("Total_BL");
	markerNames.push_back("GTR_Lik");
	markerNames.push_back("N_PairCoev");
	markerNames.push_back("COEV_Lik");

	if(STATE_GTR_MARKERS == ENABLE_GTR_MARKERS) {
		for(size_t iM=0; iM<nSite; ++iM) {
			std::stringstream ss;
			ss << "LikForGTR_" << iM;
			markerNames.push_back(ss.str());
		}
	}
	markers.resize(markerNames.size());
}

void Base::initPriors() {
	nPriorParams = 0;
	nPriorParams = Parameters::SINGLE_NB_PARAMS;

	// Set the prior for GTR
	ptrGTRPr.reset(new Utils::Statistics::DirichletPDF(MolecularEvolution::MatrixUtils::GTR_MF::GTR_NB_COEFF+1, 5.));
	ptrGTRFreqPr.reset(new Utils::Statistics::DirichletPDF(4, 5.));

	// Precompute the prior pairs
	precomputedLogFactorial.resize(1+getMaxActiveCoevLikelihood(), 0.);
	precomputedPriorPairs.resize(1+getMaxActiveCoevLikelihood(), 0.);

	// Prior on the pair currently sampled
	const double valLog2 = log(2.);
	double priorPairs = 0.;
	for(size_t iP=0; iP<getMaxActiveCoevLikelihood(); ++iP) {
		priorPairs += valLog2 - log((double)((nSegregatingSite-2*iP)*(nSegregatingSite-(2*iP+1))));
		priorPairs += log(iP+1.);
		// Probability of unordered pairs
		precomputedPriorPairs[iP+1] += priorPairs;
		// Log factorial
		precomputedLogFactorial[iP+1] += precomputedLogFactorial[iP] + log(iP+1.);
	}
}

void Base::loadCoevInformations(const std::string &dataFileName) {

	ifstream iFile(dataFileName.c_str());
	std::string line;

	while(std::getline(iFile, line)) {
		// Break string to words
		std::vector<std::string> words;
		boost::split(words, line, boost::is_any_of("\t "), boost::token_compress_on);

		if(words.empty()) continue;

		size_t pos1, pos2;
		std::istringstream issPos1(words[0]), issPos2(words[1]);
		issPos1 >> pos1;
		issPos2 >> pos2;
		assert(!issPos1.fail() && !issPos2.fail());

		SubSpaceUID::sharedPtr_t ssUID(new SubSpaceUID(pos1, pos2));
		std::cout << "Created fixed Coev Pair : " << ssUID->getLabelUID() << std::endl;
		CoevLikelihoodInfo *CLI = createTempCoevLikelihood(ssUID);
		registerCoevDAG(CLI, false);
	}
}

double Base::processLikelihood(const Sampler::Sample &sample) {

	// Update the values
	std::set<DAG::BaseNode*> updatedNodes;
	BaseSampleWrapper sw(getNParamPrior(), getNBranch(), sample.getIntValues(), sample.getDblValues());
	setBaseSample(sw, updatedNodes);
	setCoevSample(sample, sw, updatedNodes);

	// Add pending updated node that could be existing from Reversible Jumps
	updatedNodes.insert(pendingUpdatedNodes.begin(), pendingUpdatedNodes.end());
	pendingUpdatedNodes.clear();

	// Reset the DAG (could use partial result instead
	if(requireSchedulerPartialReset) {
		scheduler->reinitialize(scheduler->PARTIAL_REINIT);
		requireSchedulerPartialReset = false;
	}
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	/*std::cout << rootDAG->getLikelihood() << std::endl;
	getchar();*/

	writeMarkers(sample, sw);

	// Compute likelihiood
	double lik = rootDAG->getLikelihood();

	curTree->clearUpdatedNodes();

	// return the likelihood
	return lik;//priorGTR + priorPairs + lik;
}

std::string Base::getName(char sep) const {
	return std::string("TreeInference");
}

size_t Base::getNBranch() const {
	return branchPtr.size();
}

size_t Base::getNGamma() const {
	return N_GAMMA;
}

size_t Base::getNParamPrior() const {
	return nPriorParams;
}


size_t Base::stateSize() const {
	size_t size = 0;
	return size;
}

void Base::setStateLH(const char* aState) {
}

void Base::getStateLH(char* aState) const {
}

double Base::update(const vector<size_t>& pIndices, const Sample& sample) {

	//std::stringstream ss; // FIXME
	//ss << " NB COEV PAIR = " << getNActiveCoevLikelihood(); // FIXME
	//Parallel::mpiMgr().print(ss.str()); // FIXME

	volatile double sSample, eSample, sDAG, eDAG, sProc, eProc;

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Indices : ";
		for(size_t i=0; i<pIndices.size(); ++i) {
			dbgFile << pIndices[i] << ", ";
		}
		dbgFile << std::endl;
		sSample = MPI_Wtime();
	}

	std::set<DAG::BaseNode*> updatedNodes;
	BaseSampleWrapper sw(getNParamPrior(), getNBranch(), sample.getIntValues(), sample.getDblValues());
	setBaseSample(sw, updatedNodes);
	setCoevSample(sample, sw, updatedNodes);

	// Add pending updated node that could be existing from Reversible Jumps
	updatedNodes.insert(pendingUpdatedNodes.begin(), pendingUpdatedNodes.end());
	pendingUpdatedNodes.clear();

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eSample = MPI_Wtime();
		sDAG = MPI_Wtime();
	}

	if(requireSchedulerPartialReset) {
		scheduler->reinitialize(scheduler->PARTIAL_REINIT);
		requireSchedulerPartialReset = false;
	}

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial(updatedNodes) && (!pIndices.empty()) &&
		  (pIndices.front() != 0)  && (pIndices.front() != Parameters::PRIOR_PARAMETERS_OFFSET) && (pIndices.front() != Parameters::LAMBDA_POISSON_PARAMETERS_OFFSET)) { // its possible that the tree is the same
			if(cntErrMsg < 1000) {
				cntErrMsg++;
				/*if(cntErrMsg == 1000) {
					std::cerr << "[" << Parallel::mcmcMgr().getMyChainMC3() << "][" << Parallel::mcmcMgr().getRankProposal();
					std::cerr <<  "] First pInd = " << pIndices.front();
					std::cerr << " || error with partial update in block having pInd=" << pIndices.front() << std::endl;
				}*/
			}
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eDAG = MPI_Wtime();
		sProc = MPI_Wtime();
	}

	// Process the tree
	scheduler->process();

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		eProc = MPI_Wtime();
	}

	if(doDebug == GLOBAL_DEBUG || doDebug == UPDATE_DEBUG) {
		dbgFile << "Update::setSample : " << eSample -sSample << std::endl;
		dbgFile << "Update::resetPartial : " << eDAG - sDAG << std::endl;
		dbgFile << "Update::process : " << eProc - sProc << std::endl;
		dbgFile << "----------------------------------------------" << std::endl;
	}

	writeMarkers(sample, sw);

	double lik = rootDAG->getLikelihood();

	curTree->clearUpdatedNodes();

	//double lik2 = processLikelihood(sample);
	//assert(lik==lik2);

	// return the likelihood
	return lik;
}

double Base::processLikelihoodSpecificPrior(const Sampler::Sample &sample) {

	double logPrior = 0;

	size_t iMarker = 0;
	BaseSampleWrapper sw(getNParamPrior(), getNBranch(), sample.getIntValues(), sample.getDblValues());

	/** Prior and hyperprior on the number of pairs **/
	if(!isUsingFixedPairsStrategy()) {
		double K = getNActiveCoevLikelihood();
		double priorK = K*log(sw.lambdaPoisson) - sw.lambdaPoisson - precomputedLogFactorial[K];
		markers[iMarker] = priorK;
		iMarker++;
		//std::cout << "PriorK = " << priorK << std::endl; // FIXME REMOVE

		boost::math::gamma_distribution<> gammaDistr(HyperPriors::GAMMA_POISSON_ALPHA, 1./HyperPriors::GAMMA_POISSON_BETA);
		double hyperpriorK = log(boost::math::pdf(gammaDistr, sw.lambdaPoisson));
		markers[iMarker] = hyperpriorK;
		iMarker++;
		//std::cout << "hyperpriorK = " << hyperpriorK << std::endl; // FIXME REMOVE

		logPrior += priorK + hyperpriorK;
	}

	/** Prior due to the amount of coev pairs **/
	double priorPairs = precomputedPriorPairs[getNActiveCoevLikelihood()];
	markers[iMarker] = priorPairs;
	iMarker++;
	logPrior += priorPairs;
	//std::cout << "priorPairs = " << priorPairs << std::endl; // FIXME REMOVE

	/** Prior for the GTR swap probabilities **/
	std::vector<double> nrmTheta(sw.thetas.size()+1);
	double sum = 1. ; // Non sampled theta paramater in GTR
	for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
		sum += sw.thetas[iT];
	}
	for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
		nrmTheta[iT] = sw.thetas[iT]/sum;
	}
	nrmTheta.back() = 1./sum;
	double priorGTR = log(ptrGTRPr->computePDF(nrmTheta));
	markers[iMarker] = priorGTR;
	iMarker++;
	logPrior += priorGTR;
	//std::cout << "priorGTR = " << priorGTR << std::endl; // FIXME REMOVE

	std::vector<double> stationaryFreq(sw.stFreq);
	stationaryFreq.push_back(1.);
	double tmpSum = 0.;
	for(size_t iF=0; iF<stationaryFreq.size(); ++iF) tmpSum += stationaryFreq[iF];
	for(size_t iF=0; iF<stationaryFreq.size(); ++iF) stationaryFreq[iF] /= tmpSum;
	double priorFreqGTR = log(ptrGTRFreqPr->computePDF(stationaryFreq));
	markers[iMarker] = priorFreqGTR;
	iMarker++;
	logPrior += priorFreqGTR;

	/** Prior for Coev parameters **/
	double priorCoev = computeCoevPrior(sample, sw);
	markers[iMarker] = priorCoev;

	iMarker++;
	logPrior += priorCoev;
	//std::cout << "priorCoev = " << priorCoev << std::endl; // FIXME REMOVE

	/** Scaling prior **/
	double priorScaling = 0.;
	if(getNActiveCoevLikelihood() > 0) {
		boost::math::gamma_distribution<> gammaPScaling(Priors::BL_SCALING_ALPHA, Priors::BL_SCALING_BETA);
		priorScaling = log(boost::math::pdf(gammaPScaling, sw.coevScaling));
	}
	markers[iMarker] = priorScaling;
	iMarker++;
	logPrior += priorScaling;
	//std::cout << "priorScaling = " << priorScaling << std::endl; // FIXME REMOVE

	return logPrior;
}

void Base::saveInternalState(const Sampler::Sample &sample, Utils::Serialize::buffer_t &buff) {
	 // TODO add Coev clusters

	// Serialize the tree to the other chain (get the good tree)
	TR::Tree::sharedPtr_t aTree = treeManager->getCurrentTree();
	if(sample.getIntParameter(0) != aTree->getHashKey()) {
		aTree = treeManager->getTree(sample.getIntParameter(0));
	}
	// Serialize it
	std::string aString = aTree->getIdString();
	std::copy(aString.begin(), aString.end(), std::back_inserter(buff));
}

void Base::loadInternalState(const Sampler::Sample &sample, const Utils::Serialize::buffer_t &buff) {
	 // TODO add Coev clusters

	// Get treeString
	std::string aString;
	std::copy(buff.begin(), buff.end(), std::back_inserter(aString));

	// Create tree
	TR::Tree::sharedPtr_t aTree(new TR::Tree(aString));

	// Add it to the pool
	treeManager->setTree(aTree);
	treeManager->memorizeTree();
	isTreeExternalyImported = true;
}


void Base::initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff>& offsets) {
	if(offsets.empty()) {
		treeManager->initTreeFile(fnPrefix, fnSuffix, 0);
		coevTraceWriter.initFile(fnPrefix, fnSuffix, 0);
		coevTraceWriter.initCoev(activePermCoevInfo);
	} else {
		assert(offsets.size() == 2);
		treeManager->initTreeFile(fnPrefix, fnSuffix, offsets[0]);
		coevTraceWriter.initFile(fnPrefix, fnSuffix, offsets[1]);
	}
}

std::vector<std::streamoff> Base::getCustomizedLogSize() const {
	std::vector<std::streamoff> filesOffset(2,0);
	filesOffset[0] = treeManager->getTreeFileLength();
	filesOffset[1] = coevTraceWriter.getFileLength();
	return filesOffset;
}

void Base::writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples) {
	std::vector<long int> treesHash;
	for(size_t iS=0; iS<iterSamples.size(); ++iS) {
		treesHash.push_back(iterSamples[iS].second.getIntParameter(0));
	}

	treeManager->writeTreeString(treesHash);

	coevTraceWriter.writeSamples(activePermCoevInfo, iterSamples);
}

std::vector<DAG::BaseNode*> Base::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	std::set<DAG::BaseNode*> updatedNodes;
	BaseSampleWrapper sw(getNParamPrior(), getNBranch(), sample.getIntValues(), sample.getDblValues());
	setBaseSample(sw, updatedNodes);
	setCoevSample(sample, sw, updatedNodes);

	if(requireSchedulerPartialReset) {
		scheduler->reinitialize(scheduler->PARTIAL_REINIT);
		requireSchedulerPartialReset = false;
	}

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

size_t Base::getNThread() const {
	return nThread;
}

std::string Base::getAlignFile() const {
	return fileAlign;
}

TR::Tree::sharedPtr_t Base::getCurrentTree() const {
	return curTree;
}

TR::TreeManager::sharedPtr_t Base::getTreeManager() const {
	return treeManager;
}

Base::treeUpdateStrategy_t Base::getTreeUpdateStrategy() const {
	return TREE_UPDATE_STRATEGY;
}

bool Base::isUsingCoevScaling() const {
	return true;
}

coevScalingType_t Base::getCoevScalingType() const {
	return WITH_SCALING_COEV;
}

coevPriorScalingType_t Base::getCoevScalingPriorType() const {
	return SINGLE_PARAM;
}

void Base::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

DAG::Scheduler::BaseScheduler* Base::getScheduler() const {
	return scheduler;
}

void Base::prepareBaseDAG() {

	ptrMatrixGTR = new MatrixGTRNode(freqGTR);
	ptrDiscreteGamma = new DiscreteGammaNode(N_GAMMA, DiscreteGammaNode::MEAN_GAMMA);

	// Root of the DAG
	rootDAG = new FinalResultNode();
}

void Base::createTreeAndBaseDAG(DL::FastaReader &fr, DL::MSA &msa) {

	// Root
	rootTree = new TreeNode(N_GAMMA, treeManager->getTreeNodeName(curTree->getRoot()), curTree->getRoot());
	branchMap.insert(std::make_pair(rootTree->getId(), rootTree));
	createTree(curTree->getRoot(), NULL,  rootTree);
	std::sort(branchPtr.begin(), branchPtr.end(), SortTreeNodePtrById());

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > (size_t)N_GAMMA){
		size_t tmpVal = ceil((double)nThread/N_GAMMA);
		// TODO OPTIMIZE THAT nProc = 2 with nSubDag = 2 -> nThread = 4 nSubDag = ? etc..
		nSubDAG = std::min(tmpVal, nSite);
	}
	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);

		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteGTRGNode* ptrCSGTRGN = new CombineSiteGTRGNode(N_GAMMA, subSites);
		cmbNodes.push_back(ptrCSGTRGN);

		// Create root(s) and subDAG for class iC
		for(size_t iG=0; iG<N_GAMMA; ++iG) {
			RootGTRGNode *ptrRoot = new RootGTRGNode(iG, subSites, freqGTR);
			createBaseSubDAG(iG, subSites, rootTree, ptrRoot, msa); // Create sub DAG for class iC
			rootTree->addGTRGEdgeNode(ptrRoot);

			// Add root to the CombineSiteNode
			ptrCSGTRGN->addChild(ptrRoot);

			/*std::cout << ptrRoot->toString() << std::endl;
			std::cout << "----------------------------" << std::endl;
			ptrRoot->init();
			std::cout << ptrRoot->toString() << std::endl;
			getchar();*/
		}

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSGTRGN);
	}
}

void Base::createPermutedDAG() {
	for(size_t iPS=2; iPS<=4; ++iPS) {
		permutedCoev.push_back(new PermutedCoevLikelihood(iPS));
		createPermCoevDAG(permutedCoev.back());
	}
}


void Base::addBranchMatrixGTRGNodes(TreeNode *node) {
	// Add BranchMatrixNodes for each classes
	for(size_t iG=0; iG<N_GAMMA; ++iG) {
		BranchMatrixGTRGNode *ptrBMGTRGN;
		ptrBMGTRGN = new BranchMatrixGTRGNode(iG, LOCAL_SCALING, freqGTR);
		ptrBMGTRGN->addChild(ptrDiscreteGamma);
		ptrBMGTRGN->addChild(ptrMatrixGTR);
		node->addGTRGBranchMatrixNode(ptrBMGTRGN);
	}
}


/**
 * @param nodeTR a node in the TreeReconstruction tree
 * @param node a node in the TreeInference tree
 */
void Base::createTree(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node) {

	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);

	if(childrenTR.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick
		for(size_t iC=0; iC<childrenTR.size(); ++iC){
			TreeNode *childNode = new TreeNode(N_GAMMA, treeManager->getTreeNodeName(childrenTR[iC]), childrenTR[iC]);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update
			branchMap.insert(std::make_pair(childNode->getId(), childNode));

			createTree(childrenTR[iC], nodeTR, childNode);
			node->addChild(childNode);
			addBranchMatrixGTRGNodes(childNode); // Add BranchMatrixNode(s)
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

/**
 * This method create one sub-DAG for a set of sites for a given class.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param idGamma the gamma class
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 * @param msa Multi-sequence alignement
 */
void Base::createBaseSubDAG(size_t idGamma, const std::vector<size_t> &subSites, TreeNode *node,
		DAG::BaseNode *nodeDAG, DL::MSA &msa) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TreeNode *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafGTRGNode *newNode;
			newNode = new LeafGTRGNode(idGamma, subSites, childNode->getName(), msa, freqGTR);
			childNode->addGTRGEdgeNode(newNode);
			newNodeDAG = newNode;
		} else { // create CPV element
			CPVGTRGNode *newNode;
			newNode = new CPVGTRGNode(idGamma, subSites, freqGTR);
			childNode->addGTRGEdgeNode(newNode);
			newNodeDAG = newNode;

			// create sub-DAG
			createBaseSubDAG(idGamma, subSites, childNode, newNode, msa);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		assert(childNode->getGTRGBranchMatrixNodes().size() == N_GAMMA); // TODO Remove when secure
		BranchMatrixGTRGNode *ptrBMN = childNode->getGTRGBranchMatrixNodes()[idGamma];
		newNodeDAG->addChild(ptrBMN);

	}

}

void Base::setBaseSample(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	volatile double sTime, eTime;
	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setTree(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setTree : " << eTime-sTime << std::endl;
	}

	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setThetas(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setMatrix : " << eTime-sTime << std::endl;
	}

	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setAlpha(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setMatrix : " << eTime-sTime << std::endl;
	}


	if(doDebug == GLOBAL_DEBUG) sTime = MPI_Wtime();
	setBranchLength(sw, updatedNodes);
	if(doDebug == GLOBAL_DEBUG) {
		eTime = MPI_Wtime();
		dbgFile << "SetSample::setBranchLength : " << eTime-sTime << std::endl;
	}

	// Set coev scaling if needed
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setCoevScaling(sw.coevScaling, updatedNodes);
	}
}

void Base::setTree(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	volatile double sMPI, eMPI;

	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sMPI = MPI_Wtime();
	//treePool->updatePool();
	if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
		eMPI = MPI_Wtime();
		dbgFile << "SetTree::UpdatePools : " << eMPI - sMPI << std::endl;
	}

	// FIXME std::cout << curTree->getHashKey() << " -> " << sw.treeH << std::endl;

	if(curHash != sw.treeH || curTree->getHashKey() != sw.treeH) {
		if(TREE_UPDATE_STRATEGY != FIXED_TREE_STRATEGY) {
			volatile double sTree, eTree, sUpd, eUpd, sReinit, eReinit;

			// Find tree
			// FIXME std::cout << "Current tree : " << curTree->getString() << std::endl;
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sTree = MPI_Wtime();
			//std::cout << "Base tree : " << curTree->getIdString() << std::endl; // FIXME
			//std::cout << "[" << Parallel::mcmcMgr().getRankProposal() << "] curTree -> " << curTree->getHashKey() << "\t ";
			//std::cout << " sample -> " << sw.treeH << "\t curHash -> " << curHash << std::endl; // FIXME
			curTree = treeManager->getTree(sw.treeH);
			//std::cout << "New tree : " << curTree->getIdString() << std::endl; // FIXME
			if(curTree == NULL) {
				std::cerr << "[Error] " << sw.treeH << std::endl;
			}
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eTree = MPI_Wtime();
			// FIXME std::cout << "Next tree : " << curTree->getString() << std::endl;

			// Update tree + DAG
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sUpd = MPI_Wtime();
			if(!isTreeExternalyImported) {
				updateTreeAndDAG(updatedNodes);
			} else {
				fullUpdateTreeAndDAG(curTree->getRoot(), NULL,  rootTree, updatedNodes);
				isTreeExternalyImported = false;
			}
			curHash = curTree->getHashKey();
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eUpd = MPI_Wtime();

			// Update scheduler
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) sReinit = MPI_Wtime();
			scheduler->reinitialize(scheduler->SOFT_REINIT); // only a soft reset
			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) eReinit = MPI_Wtime();

			if(doDebug == TREE_DEBUG || doDebug == GLOBAL_DEBUG) {
				dbgFile << "SetTree::getTree : " << eTree - sTree << std::endl;
				dbgFile << "SetTree::UpdateDaG : " << eUpd - sUpd << std::endl;
				dbgFile << "SetTree::ReinitSched : " << eReinit - sReinit << std::endl;
				if(doDebug == TREE_DEBUG) {
					dbgFile << "----------------------------------------------" << std::endl;
				}
			}

			/// Update active coev likelihood tree hash
			for(itListCLI_t it = activeTempCoevInfo.begin(); it != activeTempCoevInfo.end(); ++it) {
				(*it)->setTreeHash(curHash);
			}

			// Inactive Coev Likelihood are still maped with the topology, and thus the likelihood nodes are also swapped.
			for(itListCLI_t it = inactiveTempCoevInfo.begin(); it != inactiveTempCoevInfo.end(); ++it) {
				(*it)->setTreeHash(curHash);
			}

			// Pending Coev Likelihood are still maped with the topology, and thus the likelihood nodes are also swapped.
			for(itListCLI_t it = pendingDAGs.begin(); it != pendingDAGs.end(); ++it) {
				(*it)->setTreeHash(curHash);
			}
		} else {
			assert(false && "[Error] Likelihood strategy is 'FIXED_TREE_STRATEGY' and topological moves are happening.");
		}
	}
}

void Base::setAlpha(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	bool isUpdated = ptrDiscreteGamma->setAlpha(sw.alpha);
	if(isUpdated) updatedNodes.insert(ptrDiscreteGamma);
}

void Base::setThetas(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	bool isUpdated = ptrMatrixGTR->setThetas(sw.thetas);

	bool isUpdatedFreq = ptrMatrixGTR->setFrequencies(sw.stFreq);
	if(isUpdatedFreq) {
		std::vector<double> stationaryFreq(sw.stFreq);
		stationaryFreq.push_back(1.);
		double tmpSum = 0.;
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) tmpSum += stationaryFreq[iF];
		for(size_t iF=0; iF<stationaryFreq.size(); ++iF) stationaryFreq[iF] /= tmpSum;

		freqGTR.set(stationaryFreq);
	}

	if(isUpdated || isUpdatedFreq) updatedNodes.insert(ptrMatrixGTR);
}

void Base::setBranchLength(const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setBranchLength(sw.branchLength[i], updatedNodes);
	}
}

void Base::setCoevSample(const Sampler::Sample &sample, const BaseSampleWrapper &sw, std::set<DAG::BaseNode*> &updatedNodes) {

	// TODO REWORK
	//assert(false && "setCoevSample must be reworked");

	/// Update temp likelihoods
	for(itListCLI_t it=activeTempCoevInfo.begin(); it != activeTempCoevInfo.end(); ++it) {
		// Getting the model specific sample
		const Sampler::ModelSpecificSample& mss = sample.getMSS((*it)->getUIDSubSpace());

		// Parameter is profile code
		assert(mss.getIntValues().size() == 1);
		long int idProfile = mss.getIntValues()[0];
		MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(idProfile);
		//std::cout << "[TEMP] >> " << (*it)->getUIDSubSpace()->getLabelUID() << " :: " << MD::getNucleotidesPairProfiles()->profileToString(profile) << std::endl; // FIXME REMOVE
		bool isUpdatedProfile = (*it)->getMatrixCoev()->setProfile(profile);

		// Parameters are r1, r2, s, d
		assert(mss.getDblValues().size() == 4);
		double r1 = mss.getDblValues()[0];
		double r2 = mss.getDblValues()[1];
		double s = mss.getDblValues()[2];
		double d = mss.getDblValues()[3];

		double sumExp = 2*exp(sw.priorParamValues[0])+exp(sw.priorParamValues[1])+exp(sw.priorParamValues[2]);
		r1 = exp(sw.priorParamValues[0])/sumExp;
		r2 = exp(sw.priorParamValues[0])/sumExp;
		s = exp(sw.priorParamValues[1])/sumExp;
		d = exp(sw.priorParamValues[2])/sumExp;

		// Update the coev matrix node
		bool isUpdatedParameters = (*it)->getMatrixCoev()->setParameters(r1, r2, s, d);
		if(isUpdatedProfile || isUpdatedParameters) updatedNodes.insert((*it)->getMatrixCoev());
	}

	/// Update permuted pairs profile
	for(itListPCLI_t it=activePermCoevInfo.begin(); it != activePermCoevInfo.end(); ++it) {
		// Getting the model specific sample
		const Sampler::ModelSpecificSample& mss = sample.getMSS((*it)->getUIDSubSpace());

		// Parameter is profile code
		assert(mss.getIntValues().size() == 1);
		unsigned long int idProfile = mss.getIntValues()[0];
		MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(idProfile);
		MD::profile_t profileTmp = MD::getNucleotidesPairProfiles()->idToProfile((*it)->getIdProfile());
		//std::cout << "[PERM] >> " << (*it)->getUIDSubSpace()->getLabelUID() << " :: From - " << MD::getNucleotidesPairProfiles()->profileToString(profileTmp);// FIXME REMOVE
		//std::cout << " :: To - " << MD::getNucleotidesPairProfiles()->profileToString(profile) << std::endl; // FIXME REMOVE
		//std::cout << (*it)->getUIDSubSpace()->getLabelUID() << " :: " << MD::getNucleotidesPairProfiles()->profileToString(profile) << std::endl; // FIXME REMOVE
		if(idProfile != (*it)->getIdProfile()) {
			// Same number of pair in profile, we just update the rotated vector
			if(profile.size() == (*it)->getProfileSize()) {
				// Register into the leaves
				std::vector<PermLeafCoevNode*>& leaves = permutedCoev[profile.size()-2]->getLeafNodes();
				for(size_t iL=0; iL<leaves.size(); ++iL) {
					leaves[iL]->replace((*it)->getPositions(),
							   getPairNuclCode((*it)->getPositions(), leaves[iL]->getSpecieName()),
							   profile);
					updatedNodes.insert(leaves[iL]);
				}
			} else { // Number of pairs is different, we have to swap to another permuted likelihood
				{ // Removes from initial leaves and root
					size_t initProfSize = (*it)->getProfileSize();
					permutedCoev[initProfSize-2]->getRootDAG()->remove((*it)->getPositions());
					updatedNodes.insert(permutedCoev[initProfSize-2]->getRootDAG());
					std::vector<PermLeafCoevNode*>& leaves = permutedCoev[initProfSize-2]->getLeafNodes();
					for(size_t iL=0; iL<leaves.size(); ++iL) {
						leaves[iL]->remove((*it)->getPositions());
						updatedNodes.insert(leaves[iL]);
					}
				}

				{ // Add to target leaves and root
					size_t newProfSize = profile.size();
					permutedCoev[newProfSize-2]->getRootDAG()->add((*it)->getPositions());
					updatedNodes.insert(permutedCoev[newProfSize-2]->getRootDAG());
					std::vector<PermLeafCoevNode*>& leaves = permutedCoev[newProfSize-2]->getLeafNodes();
					for(size_t iL=0; iL<leaves.size(); ++iL) {
						leaves[iL]->add((*it)->getPositions(),
								   getPairNuclCode((*it)->getPositions(), leaves[iL]->getSpecieName()),
								   profile);
						updatedNodes.insert(leaves[iL]);
					}
				}
			}
			(*it)->setIdProfile(idProfile);
			(*it)->setProfileSize(profile.size());
		}
	}

	/// update permuted likelihoods
	double r1 = sw.priorParamValues[0];
	double r2 = sw.priorParamValues[0];
	double s = sw.priorParamValues[1];
	double d = sw.priorParamValues[2];

	double sumExp = 2*exp(sw.priorParamValues[0])+exp(sw.priorParamValues[1])+exp(sw.priorParamValues[2]);
	r1 = exp(sw.priorParamValues[0])/sumExp;
	r2 = exp(sw.priorParamValues[0])/sumExp;
	s = exp(sw.priorParamValues[1])/sumExp;
	d = exp(sw.priorParamValues[2])/sumExp;

	for(size_t iP=0; iP<permutedCoev.size(); ++iP) {
		if(permutedCoev[iP]->getRootDAG()->isActive()) {
			bool isUpdatedParameters = permutedCoev[iP]->getMatrixCoev()->setParameters(r1, r2, s, d);
			if(isUpdatedParameters) {
				updatedNodes.insert(permutedCoev[iP]->getMatrixCoev());
			}
		}
	}
}

void Base::createCoevDAG(CoevLikelihoodInfo *coevLikInfo) {
	// Q Matrix
	MatrixCoevNode* ptrMatrixCoevNode = new MatrixCoevNode(freqCoev);
	ptrMatrixCoevNode->setProfile(MD::getNucleotidesPairProfiles()->ALL_POSSIBLE_PROFILES[0]);
	coevLikInfo->setMatrixCoev(ptrMatrixCoevNode);
	ptrMatrixCoevNode->updated(); // Make sure the coev node is computed at least once "init"
	pendingUpdatedNodes.insert(ptrMatrixCoevNode);

	// Create root node for Coev
	RootCoevNode *ptrRoot = new RootCoevNode(freqCoev);
	ptrRoot->setActive(false);
	static_cast<DAG::BaseNode*>(ptrRoot)->addChild(ptrMatrixCoevNode);

	createCoevSubDAG(coevLikInfo, rootTree, ptrRoot); // Create sub DAG for class iC
	rootTree->addCoevEdgeNode(ptrRoot);

	coevLikInfo->setRootDAG(ptrRoot);

	// Register the root
	rootDAG->addChild(coevLikInfo->getRootDAG());
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);

	coevLikInfo->setTreeHash(curHash);
	//std::cout << "DAG created with hash : " << coevLikInfo->getTreeHash() << " ( current = " << curHash << " )" << std::endl;
}

bool Base::tryToReuseCoevDAG(CoevLikelihoodInfo *coevLikInfo) {
	// First we clean the pending DAG that have been created under a different tree topology
	while(!pendingDAGs.empty() && pendingDAGs.front()->getTreeHash() != curHash) {
		CoevLikelihoodInfo *toRemoveCLI = pendingDAGs.front();
		pendingDAGs.pop_front();
		delete toRemoveCLI;
	}

	if(pendingDAGs.empty()) return false;
	assert(pendingDAGs.front()->getTreeHash() == curHash);

	// Get the last unused DAG
	CoevLikelihoodInfo *removedCLI = pendingDAGs.front();
	pendingDAGs.pop_front();

	// Q Matrix
	coevLikInfo->setMatrixCoev(removedCLI->getMatrixCoev());
	coevLikInfo->getMatrixCoev()->updated();
	pendingUpdatedNodes.insert(coevLikInfo->getMatrixCoev());

	// Root node for Coev
	coevLikInfo->setRootDAG(removedCLI->getRootDAG());

	// Copy leaves and reinit
	coevLikInfo->setLeafNodes(removedCLI->getLeafNodes());
	std::vector<LeafCoevNode*> &leafNodes = coevLikInfo->getLeafNodes();
	for(size_t iL=0; iL<leafNodes.size(); ++iL) {
		leafNodes[iL]->init(coevLikInfo->getPositions(),
							getPairNuclCode(coevLikInfo->getPositions(), leafNodes[iL]->getSpecieName()));
		leafNodes[iL]->updated();
		pendingUpdatedNodes.insert(leafNodes[iL]);
	}

	// Tree hash
	coevLikInfo->setTreeHash(removedCLI->getTreeHash());

	// delete oldCLI (now useless)
	delete removedCLI;

	return true;
}


std::pair<usintBitset_t, usintBitset_t> Base::getPairNuclCode(const std::pair<size_t, size_t> &pairPos,
															  const std::string &specieName) {

	std::pair<usintBitset_t, usintBitset_t> result = coevAlignManager->getPairSitesCode(specieName, pairPos.first, pairPos.second);
	return result;
}


/**
 * [COEV] This method create one sub-DAG for a pair of sites.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param pairPos the positionPair
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 * @param msa Multi-sequence alignement
 */
void Base::createCoevSubDAG(CoevLikelihoodInfo *coevLikInfo, TreeNode *node,
							DAG::BaseNode *nodeDAG) {
	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TreeNode *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafCoevNode *newNode;
			newNode = new LeafCoevNode(childNode->getName(), coevLikInfo->getPositions(),
					   getPairNuclCode(coevLikInfo->getPositions(), childNode->getName()),
					   freqCoev);
			coevLikInfo->getLeafNodes().push_back(newNode);
			childNode->addCoevEdgeNode(newNode);
			//std::cout << "Add edge : TreeNode ID = " << childNode->getId() << "\t DAG Node ID = " << newNode->getId() << std::endl; // TODO PRINT DEBUG
			newNodeDAG = newNode;
		} else { // create CPV element
			CPVCoevNode *newNode;
			newNode = new CPVCoevNode(freqCoev);
			childNode->addCoevEdgeNode(newNode);
			//std::cout << "Add edge : TreeNode ID = " << childNode->getId() << "\t DAG Node ID = " << newNode->getId() << std::endl; // TODO PRINT DEBUG
			newNodeDAG = newNode;

			// create sub-DAG
			createCoevSubDAG(coevLikInfo, childNode, newNodeDAG);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		BranchMatrixCoevNode *ptrBMCN = addBranchMatrixCoevNodes(coevLikInfo, childNode);
		newNodeDAG->addChild(ptrBMCN);
		//std::cout << "[Base] Add BCM : newNodeDAG ID = " << newNodeDAG->getId() << "\t BranchMatrixCoevNode Node ID = " << ptrBMCN->getId() << std::endl; // TODO PRINT DEBUG
	}
}

BranchMatrixCoevNode* Base::addBranchMatrixCoevNodes(CoevLikelihoodInfo *coevLikInfo, TreeNode *node) {
	// Add BranchMatrixNodes for each classes
	BranchMatrixCoevNode *ptrBMCN;
	ptrBMCN = new BranchMatrixCoevNode(freqCoev);
	ptrBMCN->addChild(coevLikInfo->getMatrixCoev());
	node->addCoevBranchMatrixNode(ptrBMCN);
	//std::cout << "[Base] Add BCM : TreeNode ID = " << node->getId() << "\t DAG Node ID = " << ptrBMCN->getId() << std::endl; // TODO PRINT DEBUG
	return ptrBMCN;
}

void Base::deleteCoevDAG(CoevLikelihoodInfo *coevLikInfo) {

	// Get the root of the DAG
	RootCoevNode *ptrRoot = coevLikInfo->getRootDAG();

	// Remove the root from the dag
	rootDAG->removeChild(ptrRoot);
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);

	// Delete the subdag from the tree topology (this does not delete the DAG node)
	unregisterFromTreeNodes(coevLikInfo, rootTree, ptrRoot);

	//std::cout << "DAG nodes are unregistered." << std::endl; // TODO PRINT DEBUG

	// Delete the Coev DAG itself
	assert(ptrRoot->getNParent() == 0); // We do not want to remove the whole DAG
	ptrRoot->deleteChildren();
	delete ptrRoot;
	//std::cout << "DAG is deleted" << std::endl; // TODO PRINT DEBUG

	// Remove
	coevLikInfo->setRootDAG(NULL);
	coevLikInfo->setMatrixCoev(NULL);
}

void Base::unregisterFromTreeNodes(CoevLikelihoodInfo *coevLikInfo, TreeNode *node,
								   DAG::BaseNode *nodeDAG) {
	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		TreeNode *childNode = node->getChild(i);
		DAG::BaseNode *childNodeDAG = NULL;
		for(size_t iC=0; iC<nodeDAG->getNChildren(); ++iC) {
			if(childNode->hasCoevEdgeNode(nodeDAG->getChild(iC))) {
				childNodeDAG = nodeDAG->getChild(iC);
				break;
			}
		}
		//assert(childNodeDAG == childNodeDAG2);
		assert(childNodeDAG && "CoevEdgeNode not found!");

		// Go to the bottom of the tree
		unregisterFromTreeNodes(coevLikInfo, childNode, childNodeDAG);
	}

	// Apply operation while going up
	EdgeCoevNode* edgeNode = dynamic_cast<EdgeCoevNode*>(nodeDAG);
	assert(edgeNode != NULL);
	// Delete the branch matrix node from the tree topo
	if(edgeNode->getBMCNode() != NULL) { // If we are not at the root node
		//std::cout << "[Base] Remove BCM : TreeNode ID = " << node->getId() << "\t DAG Node ID = " << edgeNode->getBMCNode()->getId() << std::endl; // TODO PRINT DEBUG
		bool removedBMCNode = node->removeCoevBranchMatrixNode(edgeNode->getBMCNode());
		if(!removedBMCNode) std::cout << node->toString() << std::endl;
		assert(removedBMCNode);
	}

	// Delete the edge node from the tree topo
	bool removedEdgeNode = node->removeCoevEdgeNode(edgeNode);
	//std::cout << "Remove edge : TreeNode ID = " << node->getId() << "\t DAG Node ID = " << edgeNode->getId() << std::endl; // TODO PRINT DEBUG
	assert(removedEdgeNode);
}


void Base::updateSitesAvailabilityToCoev(std::pair<size_t, size_t> positions) {
	//double sTimeRemove = MPI_Wtime(); //FIXME
	// Remove pair position from pool
	size_t nRemoved = 0;
	nRemoved += segregatingSites.erase(positions.first);
	nRemoved += segregatingSites.erase(positions.second);
	assert(nRemoved == 2);

	sitesAvailability[positions.first] = false;
	sitesAvailability[positions.second] = false;
}

void Base::updateSitesAvailabilityToGTR(std::pair<size_t, size_t> positions) {
	// Add pair position to pool
	typedef std::pair< std::set<size_t>::iterator, bool > insertResult_t;
	insertResult_t insRes;

	insRes = segregatingSites.insert(positions.first);
	assert(insRes.second);
	insRes = segregatingSites.insert(positions.second);
	assert(insRes.second);

	sitesAvailability[positions.first] = true;
	sitesAvailability[positions.second] = true;
}


void Base::registerCoevDAG(CoevLikelihoodInfo *coevLikInfo, bool refreshScheduler) {

	//std::cout << "Registering likelihood with UID : " << coevLikInfo->getUIDSubSpace()->getLabelUID() << std::endl; // FIXME
	assert(coevLikInfo->getTreeHash() == curHash);

	// Check that the CLI is inactive and swap it to active
	itListCLI_t itCLI = std::find(inactiveTempCoevInfo.begin(), inactiveTempCoevInfo.end(), coevLikInfo);
	assert(itCLI != inactiveTempCoevInfo.end() && "Registering an already active CLI ?");

	activeTempCoevInfo.push_back(coevLikInfo);
	inactiveTempCoevInfo.erase(itCLI);

	// Get the positions
	std::vector<size_t> sites(2);
	sites[0] = coevLikInfo->getPositions().first;
	sites[1] = coevLikInfo->getPositions().second;

	updateSitesAvailabilityToCoev(coevLikInfo->getPositions());
	//std::cout << "[BASE] Time remove : " << MPI_Wtime() - sTimeRemove << std::endl; // FIXME

	// Disable pair position in CombineSiteNodes
	//double sTimeNodes = MPI_Wtime(); //FIXME
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->disableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}

	// Add subDAG to terminal Node
	/*rootDAG->addChild(coevLikInfo->getRootDAG());
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);*/
	coevLikInfo->getRootDAG()->setActive(true);
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);

	//std::cout << "[BASE] Time node : " << MPI_Wtime() -  sTimeNodes << std::endl; // FIXME


	// Refresh scheduler
	//double sTimeScheduler = MPI_Wtime(); //FIXME
	requireSchedulerPartialReset = true;
	/*if(refreshScheduler){
		scheduler->reinitialize(scheduler->PARTIAL_REINIT);
	}*/
	//std::cout << "[BASE] Time sched : " <<  MPI_Wtime() - sTimeScheduler << std::endl; // FIXME

}

void Base::unregisterCoevDAG(CoevLikelihoodInfo *coevLikInfo, bool refreshScheduler) {

	// Check that the CLI is active and swap it to inactive
	itListCLI_t itCLI = std::find(activeTempCoevInfo.begin(), activeTempCoevInfo.end(), coevLikInfo);
	assert(itCLI != activeTempCoevInfo.end() && "unregistering an already inactive CLI ?");

	//std::cout << "Unregistering likelihood with UID : " << coevLikInfo->getUIDSubSpace()->getLabelUID() << std::endl; // FIXME

	inactiveTempCoevInfo.push_back(coevLikInfo);
	activeTempCoevInfo.erase(itCLI);

	// Get the positions
	std::vector<size_t> sites(2);
	sites[0] = coevLikInfo->getPositions().first;
	sites[1] = coevLikInfo->getPositions().second;

	updateSitesAvailabilityToGTR(coevLikInfo->getPositions());

	// Enable pair position in CombineSiteNodes
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->enableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}

	// Remove subDAG from terminal Node
	/*rootDAG->removeChild(coevLikInfo->getRootDAG());
	rootDAG->updated();*/
	coevLikInfo->getRootDAG()->setActive(false);
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);

	// Refresh scheduler
	requireSchedulerPartialReset = true;
	/*if(refreshScheduler) {
		scheduler->reinitialize(scheduler->PARTIAL_REINIT);
	}*/
}

PermCoevLikelihoodInfo* Base::createPermutedCoevPair(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace, size_t idProfile) {

	assert(segregatingSites.count(aPtrUIDSubSpace->getPos1()) == 1 && segregatingSites.count(aPtrUIDSubSpace->getPos2()) == 1);

	std::vector<MD::idProfile_t> vecProfilesId(coevAlignManager->getPossibleProfilesId(aPtrUIDSubSpace->getPos1(), aPtrUIDSubSpace->getPos2()));
	PermCoevLikelihoodInfo* newPair = new PermCoevLikelihoodInfo(aPtrUIDSubSpace, vecProfilesId);

	// Get information about profile
	MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(idProfile);
	size_t profileSize = profile.size();

	// Save information about profile
	newPair->setIdProfile(idProfile);
	newPair->setProfileSize(profileSize);
	//std::cout << "Add pair : " << aPtrUIDSubSpace->getLabelUID() << " to likelihood " << profileSize-2 << std::endl;

	// Get the proper likelihood
	PermutedCoevLikelihood *ptrPermCL = permutedCoev[profileSize-2];

	// Register into the leaves
	std::vector<PermLeafCoevNode*>& leaves = ptrPermCL->getLeafNodes();
	for(size_t iL=0; iL<leaves.size(); ++iL) {
		leaves[iL]->add(aPtrUIDSubSpace->getPositions(),
				   getPairNuclCode(aPtrUIDSubSpace->getPositions(), leaves[iL]->getSpecieName()),
				   profile);
		pendingUpdatedNodes.insert(leaves[iL]);
	}

	// Register into the root
	ptrPermCL->getRootDAG()->add(aPtrUIDSubSpace->getPositions());
	pendingUpdatedNodes.insert(ptrPermCL->getRootDAG());

	activePermCoevInfo.push_back(newPair);

	// Get the positions
	std::vector<size_t> sites(2);
	sites[0] = newPair->getPositions().first;
	sites[1] = newPair->getPositions().second;

	updateSitesAvailabilityToCoev(newPair->getPositions());
	//std::cout << "[BASE] Time remove : " << MPI_Wtime() - sTimeRemove << std::endl; // FIXME

	// Disable pair position in CombineSiteNodes
	//double sTimeNodes = MPI_Wtime(); //FIXME
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->disableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}

	return newPair;
}

std::list<PermCoevLikelihoodInfo*>& Base::getActivePermCoevLikelihoods() {
	return activePermCoevInfo;
}

PermCoevLikelihoodInfo* Base::getActivePermutedCoevLikelihood(const std::string &labelUID) {
	bool found = false;
	for(itListPCLI_t it=activePermCoevInfo.begin(); it != activePermCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			return (*it);
		}
	}

	assert(found && "Asking for an active permuted coev likelihood?");
	return NULL;
}

PermCoevLikelihoodInfo* Base::getInactivePermuedCoevLikelihood(const std::string &labelUID) {
	bool found = false;
	for(itListPCLI_t it=inactivePermCoevInfo.begin(); it != inactivePermCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			return (*it);
		}
	}

	assert(found && "Asking for an inactive permuted coev likelihood?");
	return NULL;
}

void Base::removePermutedCoevPair(const std::string &labelUID) {

	bool found = false;
	itListPCLI_t itCLI;
	for(itListPCLI_t it = inactivePermCoevInfo.begin(); it != inactivePermCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			itCLI = it;
			break;
		}
	}
	assert(found && "Deleting an active perm likelihood before unregistering?");

	PermCoevLikelihoodInfo *ptrPCLI = (*itCLI);
	//std::cout << "[REMOVE_PERMUTED_COEV_PAIR] " << ptrPCLI->getUIDSubSpace()->getLabelUID() << std::endl; //FIXME

	size_t profileSize = ptrPCLI->getProfileSize();
	PermutedCoevLikelihood *ptrPermCL = permutedCoev[profileSize-2];

	// Remove from the leaves
	std::vector<PermLeafCoevNode*>& leaves = ptrPermCL->getLeafNodes();
	for(size_t iL=0; iL<leaves.size(); ++iL) {
		leaves[iL]->remove(ptrPCLI->getPositions());
		pendingUpdatedNodes.insert(leaves[iL]);
	}

	// Remove from the root
	ptrPermCL->getRootDAG()->remove(ptrPCLI->getPositions());
	pendingUpdatedNodes.insert(ptrPermCL->getRootDAG());

	// Get the positions
	/*std::vector<size_t> sites(2);
	sites[0] = (*itCLI)->getPositions().first;
	sites[1] = (*itCLI)->getPositions().second;

	updateSitesAvailabilityToGTR((*itCLI)->getPositions());

	// Enable pair position in CombineSiteNodes
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->enableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}*/

	// Clean the PCLI
	inactivePermCoevInfo.erase(itCLI);
	delete ptrPCLI;
}


void Base::enablePermutedCoevPair(PermCoevLikelihoodInfo* permLikInfo) {

	MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(permLikInfo->getIdProfile());

	size_t profileSize = profile.size();
	PermutedCoevLikelihood *ptrPermCL = permutedCoev[profileSize-2];

	// Register into the root
	ptrPermCL->getRootDAG()->enable(permLikInfo->getPositions());
	pendingUpdatedNodes.insert(ptrPermCL->getRootDAG());

	// Check that the CLI is inactive and swap it to active
	itListPCLI_t itCLI = std::find(inactivePermCoevInfo.begin(), inactivePermCoevInfo.end(), permLikInfo);
	assert(itCLI != inactivePermCoevInfo.end() && "Enabling an already enabled CLI ?");

	activePermCoevInfo.push_back(permLikInfo);
	inactivePermCoevInfo.erase(itCLI);

	// Get the positions
	std::vector<size_t> sites(2);
	sites[0] = permLikInfo->getPositions().first;
	sites[1] = permLikInfo->getPositions().second;

	updateSitesAvailabilityToCoev(permLikInfo->getPositions());

	// Enable pair position in CombineSiteNodes
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->disableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}
}

void Base::disablePermutedCoevPair(PermCoevLikelihoodInfo* permLikInfo) {

	MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(permLikInfo->getIdProfile());

	size_t profileSize = profile.size();
	PermutedCoevLikelihood *ptrPermCL = permutedCoev[profileSize-2];

	// Register into the root
	ptrPermCL->getRootDAG()->disable(permLikInfo->getPositions());
	pendingUpdatedNodes.insert(ptrPermCL->getRootDAG());

	// Check that the CLI is inactive and swap it to active
	itListPCLI_t itCLI = std::find(activePermCoevInfo.begin(), activePermCoevInfo.end(), permLikInfo);
	assert(itCLI != activePermCoevInfo.end() && "Disabling an already disabled CLI ?");

	inactivePermCoevInfo.push_back(permLikInfo);
	activePermCoevInfo.erase(itCLI);

	// Get the positions
	std::vector<size_t> sites(2);
	sites[0] = permLikInfo->getPositions().first;
	sites[1] = permLikInfo->getPositions().second;

	updateSitesAvailabilityToGTR(permLikInfo->getPositions());

	// Enable pair position in CombineSiteNodes
	for(size_t iC=0; iC<cmbNodes.size(); ++iC) {
		bool updated = cmbNodes[iC]->enableSites(sites);
		if(updated) pendingUpdatedNodes.insert(cmbNodes[iC]);
	}
}


/**
 * [Permuted COEV] This method create one sub-DAG for a permuted Coev likelihood.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 */
void Base::createPermutedCoevSubDAG(PermutedCoevLikelihood *permutedCoevLik, TreeNode *node,
							DAG::BaseNode *nodeDAG) {
	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		DAG::BaseNode *newNodeDAG;
		TreeNode *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			PermLeafCoevNode *newNode;
			newNode = new PermLeafCoevNode(childNode->getName(), freqCoev);
			permutedCoevLik->getLeafNodes().push_back(newNode);
			childNode->addPermCoevEdgeNode(newNode);
			//std::cout << "Add edge : TreeNode ID = " << childNode->getId() << "\t DAG Node ID = " << newNode->getId() << std::endl; // TODO PRINT DEBUG
			newNodeDAG = newNode;
		} else { // create CPV element
			PermCPVCoevNode *newNode;
			newNode = new PermCPVCoevNode(freqCoev);
			childNode->addPermCoevEdgeNode(newNode);
			//std::cout << "Add edge : TreeNode ID = " << childNode->getId() << "\t DAG Node ID = " << newNode->getId() << std::endl; // TODO PRINT DEBUG
			newNodeDAG = newNode;

			// create sub-DAG
			createPermutedCoevSubDAG(permutedCoevLik, childNode, newNodeDAG);
		}

		// (2) We add the child node created to the current node (register to father).
		nodeDAG->addChild(newNodeDAG);

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		PermBranchMatrixCoevNode *ptrBMCN = addBranchMatrixPermutedCoevNodes(permutedCoevLik, childNode);
		newNodeDAG->addChild(ptrBMCN);
		//std::cout << "[Base] Add BCM : newNodeDAG ID = " << newNodeDAG->getId() << "\t BranchMatrixCoevNode Node ID = " << ptrBMCN->getId() << std::endl; // TODO PRINT DEBUG
	}
}

PermBranchMatrixCoevNode* Base::addBranchMatrixPermutedCoevNodes(PermutedCoevLikelihood *coevLikInfo, TreeNode *node) {
	// Add BranchMatrixNodes for each classes
	PermBranchMatrixCoevNode *ptrBMCN;
	ptrBMCN = new PermBranchMatrixCoevNode(freqCoev);
	ptrBMCN->addChild(coevLikInfo->getMatrixCoev());
	node->addPermCoevBranchMatrixNode(ptrBMCN);
	//std::cout << "[Base] Add BCM : TreeNode ID = " << node->getId() << "\t DAG Node ID = " << ptrBMCN->getId() << std::endl; // TODO PRINT DEBUG
	return ptrBMCN;
}

void Base::deletePermutedCoevDAG(PermutedCoevLikelihood *permutedCoevLik) {

	// Get the root of the DAG
	PermRootCoevNode *ptrRoot = permutedCoevLik->getRootDAG();

	// Remove the root from the dag
	rootDAG->removeChild(ptrRoot);
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);
	// Delete the Coev DAG itself
	assert(ptrRoot->getNParent() == 0); // We do not want to remove the whole DAG
	ptrRoot->deleteChildren();
	delete ptrRoot;
	//std::cout << "DAG is deleted" << std::endl; // TODO PRINT DEBUG

	delete permutedCoevLik;
}

void Base::createPermCoevDAG(PermutedCoevLikelihood *permCoevLik) {
	// Q Matrix
	PermMatrixCoevNode* ptrMatrixCoevNode = new PermMatrixCoevNode(permCoevLik->getProfileSize(), freqCoev);
	permCoevLik->setMatrixCoev(ptrMatrixCoevNode);
	ptrMatrixCoevNode->updated(); // Make sure the coev node is computed at least once "init"
	pendingUpdatedNodes.insert(ptrMatrixCoevNode);

	// Create root node for Coev
	PermRootCoevNode *ptrRoot = new PermRootCoevNode(freqCoev);
	static_cast<DAG::BaseNode*>(ptrRoot)->addChild(ptrMatrixCoevNode);

	createPermutedCoevSubDAG(permCoevLik, rootTree, ptrRoot); // Create sub DAG for class iC
	rootTree->addPermCoevEdgeNode(ptrRoot);

	permCoevLik->setRootDAG(ptrRoot);

	// Register the root
	rootDAG->addChild(permCoevLik->getRootDAG());
	rootDAG->updated();
	pendingUpdatedNodes.insert(rootDAG);

	permCoevLik->setTreeHash(curHash);
	//std::cout << "DAG created with hash : " << coevLikInfo->getTreeHash() << " ( current = " << curHash << " )" << std::endl;
}

void Base::setRestartFileName(std::string aFileName) {
	fileRestart = aFileName;
}

std::string Base::getRestartFileName() const {
	return fileRestart;
}

const std::vector<double>& Base::getInitialBranchLenghts() const {
	return initialBranchLenghts;
}

/*std::vector< std::pair<position_t, position_t> > Base::drawMostProbablePairs() const {
	std::vector< std::pair<position_t, position_t> > pairs;
	std::vector<PairProfileInfo> PPI;
	coevAlignManager->defineProbablePairs(pairs, PPI);

	return pairs;
}*/

void Base::writeCoevClustersInfo(const Sample &sample) {

	for(itListCLI_t it=activeTempCoevInfo.begin(); it != activeTempCoevInfo.end(); ++it) {
		std::cout << (*it)->toString() << std::endl;
		std::cout << "Params vals : ";
		for(size_t iP=0; iP < (*it)->getPInd().size(); ++iP) {
			size_t pId = (*it)->getPInd()[iP];
			std::cout << "[" << pId << "] = " << sample.getDoubleParameter(pId) << ", ";
		}
		std::cout << std::endl;
		//}
	}
}

CoevTraceWriter& Base::getCoevTraceWriter() {
	return coevTraceWriter;
}

CoevAlignementManager::sharedPtr_t Base::getCoevAlignManager() const {
	return coevAlignManager;
}

const std::set<size_t>& Base::getSitesGTR() const {
	return sitesGTRG;
}

const std::set<size_t>& Base::getSegregatingSitesGTR() const {
	return segregatingSites;
}

const boost::dynamic_bitset<>& Base::getSitesAvailability() const {
	return sitesAvailability;
}

bool Base::isUsingFixedPairsStrategy() const {
	return COEV_STRATEGY == FIXED_POSITIONS_STRATEGY;
}

bool Base::isSamplingStationaryFreqGTR() const {
	return useStationaryFrequencyGTR;
}

size_t Base::getNActiveCoevLikelihood() const {
	return activeTempCoevInfo.size()+activePermCoevInfo.size();
}

size_t Base::getMaxActiveCoevLikelihood() const {
	return std::floor((double)nSegregatingSite/2.0);
}

CoevLikelihoodInfo* Base::createTempCoevLikelihood(SubSpaceUID::sharedPtr_t ptrUID) {

	//std::cout << segregatingSites.count(ptrUID->getPos1()) << "\t" << segregatingSites.count(ptrUID->getPos2()) << std::endl;
	assert(segregatingSites.count(ptrUID->getPos1()) == 1 && segregatingSites.count(ptrUID->getPos2()) == 1);
	//double sProfileTime = MPI_Wtime(); // FIXME
	std::vector<MD::idProfile_t> vecProfilesId(coevAlignManager->getPossibleProfilesId(ptrUID->getPos1(), ptrUID->getPos2()));
	assert(!vecProfilesId.empty() && "No possible profile for the given pair of positions.");

	//std::cout << "[BASE] Time profile : " << MPI_Wtime() - sProfileTime << std::endl;
	//std::cout << "[BASE] define possible profiles." << std::endl; // TODO PRINT DEBUG
	inactiveTempCoevInfo.push_back(new CoevLikelihoodInfo(ptrUID, vecProfilesId));
	//std::cout << "[BASE] Add coev info." << std::endl; // TODO PRINT DEBUG
	//double sCreateLikTime = MPI_Wtime(); // FIXME

	bool hasSuccessfullyReused = tryToReuseCoevDAG(inactiveTempCoevInfo.back());
	if(!hasSuccessfullyReused) {
		//std::cout << "Was not able to reuse." << std::endl;
		//std::cout << "Trying to add : " << ptrUID->getLabelUID() << std::endl;
		createCoevDAG(inactiveTempCoevInfo.back());
	} //else {
		//std::cout << "Was able to reuse." << std::endl;
	//}

	//std::cout << "Likelihhood created with hash : " << inactiveCoevInfo.back()->getTreeHash() << " ( current = " << curHash << " )" << std::endl;
	//std::cout << "[BASE] Time create lik : " << MPI_Wtime() - sCreateLikTime << std::endl;

	//std::cout << "[BASE] create DAG." << std::endl; // TODO PRINT DEBUG
	return inactiveTempCoevInfo.back();
}

CoevLikelihoodInfo* Base::getActiveTempCoevLikelihood(const std::string &labelUID) {
	bool found = false;
	for(itListCLI_t it=activeTempCoevInfo.begin(); it != activeTempCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			return (*it);
		}
	}

	assert(found && "Asking for an inactive coev likelihood?");
	return NULL;
}

CoevLikelihoodInfo* Base::getInactiveTempCoevLikelihood(const std::string &labelUID) {
	bool found = false;
	for(itListCLI_t it=inactiveTempCoevInfo.begin(); it != inactiveTempCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			return (*it);
		}
	}

	assert(found && "Asking for an active coev likelihood?");
	return NULL;
}

/*std::list<CoevLikelihoodInfo*>& Base::getActiveTempCoevLikelihoods() {
	return activeTempCoevInfo;
}*/

void Base::removeTempCoevLikelihood(const std::string &labelUID) {
	bool found = false;
	itListCLI_t itCLI;
	for(itListCLI_t it = inactiveTempCoevInfo.begin(); it != inactiveTempCoevInfo.end(); ++it) {
		if((*it)->getUIDSubSpace()->getLabelUID() == labelUID) {
			found = true;
			itCLI = it;
			break;
		}
	}
	assert(found && "Deleting an active likelihood before unregistering?");

	if(pendingDAGs.size() < MAX_PENDING_COEV_DAG) { // Keep it aside
		pendingDAGs.push_back(*itCLI);
	} else {									 	// Delete it
		deleteCoevDAG(*itCLI);
		delete *itCLI;
	}

	inactiveTempCoevInfo.erase(itCLI);
}


void Base::addEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);

	// Add tree link
	parent->addChild(child);
	parent->setRequireReinitDAG(true);

	// Add DAG link
	{ // GTR Edge node : Add edge node according to TreeNode
		std::vector<EdgeGTRGNode*>& parentEN = parent->getGTRGEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<EdgeGTRGNode*>& childEN = child->getGTRGEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

	{ // Coev Edge node : Add edge node according to TreeNode
		std::vector<EdgeCoevNode*>& parentEN = parent->getCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<EdgeCoevNode*>& childEN = child->getCoevEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

	{ // Coev Edge node : Add edge node according to TreeNode
		std::vector<PermEdgeCoevNode*>& parentEN = parent->getPermCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			std::vector<PermEdgeCoevNode*>& childEN = child->getPermCoevEdgeNodes();
			parentEN[iE]->addChild(childEN[iE]);
		}
	}

}

void Base::removeEdge(TreeNode *parent, TreeNode *child) {
	assert(parent != NULL); assert(child != NULL);
	// Was only required for moves
	// orderNodes(parent, child);

	// Remove DAG link
	{ // Remove GTRG edge node
		std::vector<EdgeGTRGNode*>& parentEN = parent->getGTRGEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			EdgeGTRGNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<EdgeGTRGNode*>& childEN = child->getGTRGEdgeNodes();
			EdgeGTRGNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}

	{ // Remove Coev edge node
		std::vector<EdgeCoevNode*>& parentEN = parent->getCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			EdgeCoevNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<EdgeCoevNode*>& childEN = child->getCoevEdgeNodes();
			EdgeCoevNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}

	{ // Remove Coev edge node
		std::vector<PermEdgeCoevNode*>& parentEN = parent->getPermCoevEdgeNodes();
		for(size_t iE=0; iE<parentEN.size(); ++iE) {
			PermEdgeCoevNode *pNode = parentEN[iE];
			assert(pNode != NULL);

			std::vector<PermEdgeCoevNode*>& childEN = child->getPermCoevEdgeNodes();
			PermEdgeCoevNode *cNode = childEN[iE];
			assert(cNode != NULL);

			pNode->removeChild(cNode);
		}
	}

	// Remove tree link
	parent->removeChild(child);
}

void Base::orderChildren(TreeNode *nodeInt, TR::vecTN_t &childrenTR) {
	treeNodeTI_children &childrenInt = nodeInt->getChildren();
	assert(childrenTR.size() == childrenInt.size());

	// Sort childrenInt according to childrenTR
	for(size_t iC=0; iC<childrenTR.size()-1; ++iC) {
		if(childrenTR[iC]->getId() != childrenInt[iC]->getId()) {
			bool found = false;
			for(size_t jC=iC+1; jC<childrenInt.size(); ++jC) {
				found = childrenTR[iC]->getId() == childrenInt[jC]->getId();
				if(found) {
					std::swap(childrenInt[iC], childrenInt[jC]);
					break;
				}
			}
			assert(found);
		}
	}
}



void Base::getDifference(const TR::vecTN_t &childrenTR, const treeNodeTI_children &children,
		std::vector<TreeNode*> &toAdd, std::vector<TreeNode*> &toRemove) {

	// We check if childrenTR are in children
	for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
		bool found = false;
		for(size_t iInt=0; iInt<children.size(); ++iInt) { // for each INT child
			found = childrenTR[iTR]->getId() == children[iInt]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toAdd
			toAdd.push_back(branchMap[childrenTR[iTR]->getId()]);
		}
	}

	// We check if children are in childrenTR
	for(size_t iInt=0; iInt<children.size(); ++iInt) {  // for each INT child
		bool found = false;
		for(size_t iTR=0; iTR<childrenTR.size(); ++iTR) { // for each TR child
			found = children[iInt]->getId() == childrenTR[iTR]->getId();
			if(found) { // If found : ok
				break;
			}
		}
		if(!found) { // if not found : add to toRemove
			toRemove.push_back(children[iInt]);
		}
	}
}


void Base::applyChangesToTreeAndDAG(std::vector<TR::TreeNode *> &updTreeNodes) {
	std::vector<TR::TreeNode *> updTNodes = curTree->getUpdatedNodes();

	size_t iN=0;
	while(iN<updTNodes.size()) { // updTNodes.size() may change during the loop
		// Get TR children
		TR::TreeNode *nodeTR = updTNodes[iN];
		TR::TreeNode *parentNodeTR = nodeTR->getParent();
		TR::vecTN_t childrenTR = nodeTR->getChildren();

		// Get Internal children
		TreeNode *node = branchMap[nodeTR->getId()];
		const treeNodeTI_children &children = node->getChildren();

		if(parentNodeTR != NULL) { // If we are not at the root node
			TreeNode *parentNode = branchMap[parentNodeTR->getId()];
			// If the edge with the parent has changed
			if(!parentNode->isParent(node)) {
				// We must update the parent too
				updTNodes.push_back(parentNodeTR);
			}
		}

		// Get difference
		std::vector<TreeNode*> toAdd, toRemove;
		getDifference(childrenTR, children, toAdd, toRemove);

		if(!toRemove.empty() || !toAdd.empty()) { // If there are differences
			// Remove edges
			for(size_t iR=0; iR<toRemove.size(); ++iR) {
				removeEdge(node, toRemove[iR]);
			}

			// Add edges
			for(size_t iA=0; iA<toAdd.size(); ++iA) {
				addEdge(node, toAdd[iA]);
			}

			{
				std::vector<EdgeGTRGNode*>& edgeNodesGTRG = node->getGTRGEdgeNodes();
				for(size_t iE=0; iE<edgeNodesGTRG.size(); ++iE) {
					edgeNodesGTRG[iE]->setDone(); // A bit dirty but necessary
				}
			}

			{
				std::vector<EdgeCoevNode*>& edgeNodesCoev = node->getCoevEdgeNodes();
				for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
					edgeNodesCoev[iE]->setDone(); // A bit dirty but necessary
				}
			}

			{
				std::vector<PermEdgeCoevNode*>& edgeNodesPermCoev = node->getPermCoevEdgeNodes();
				for(size_t iE=0; iE<edgeNodesPermCoev.size(); ++iE) {
					edgeNodesPermCoev[iE]->setDone(); // A bit dirty but necessary
				}
			}

			// This node has been updated
			updTreeNodes.push_back(nodeTR);
		}
		iN++;
	}

}

void Base::memorizeUpdatedParentNodes(TreeNode *node, std::set<DAG::BaseNode*> &updatedNodes) {
	{ // GTR
		std::vector<EdgeGTRGNode*>& edgeNodesGTRG = node->getGTRGEdgeNodes();
		for(size_t iE=0; iE<edgeNodesGTRG.size(); ++iE) {
			ParentGTRGNode *pNode = dynamic_cast<ParentGTRGNode*>(edgeNodesGTRG[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}

	{ // COEV
		std::vector<EdgeCoevNode*>& edgeNodesCoev = node->getCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
			ParentCoevNode *pNode = dynamic_cast<ParentCoevNode*>(edgeNodesCoev[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}

	{ // COEV
		std::vector<PermEdgeCoevNode*>& edgeNodesCoev = node->getPermCoevEdgeNodes();
		for(size_t iE=0; iE<edgeNodesCoev.size(); ++iE) {
			PermParentCoevNode *pNode = dynamic_cast<PermParentCoevNode*>(edgeNodesCoev[iE]);
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}
	}
}

void Base::signalUpdatedTreeAndNodes(const std::vector<TR::TreeNode *> &updTreeNodes,
		std::set<DAG::BaseNode*> &updatedNodes) {
	std::set<size_t> markerNodes;
	for(size_t iN=0; iN<updTreeNodes.size(); ++iN) {
		// TR children
		TR::TreeNode *nodeTR = updTreeNodes[iN];
		TR::vecTN_t childrenTR;
		// Internal node
		TreeNode *node;

		while (nodeTR != NULL) { // While we are not at the root

			// Get children and node
			childrenTR = nodeTR->getChildren();
			node = branchMap[nodeTR->getId()];
			// If visited, we are done
			if(markerNodes.count(node->getId()) > 0) {
				break;
			}

			// Order nodes
			orderChildren(node, childrenTR);

			// Signal node updated
			memorizeUpdatedParentNodes(node, updatedNodes);

			// Mark node
			markerNodes.insert(node->getId());
			// Next node
			nodeTR = nodeTR->getParent();
		}
	}
}

void Base::updateTreeAndDAG(std::set<DAG::BaseNode*> &updatedNodes) {
	// Vector to keep track of changed tree nodes
	std::vector<TR::TreeNode *> updTreeNodes;
	// Change TreeNode and dag Topology
	applyChangesToTreeAndDAG(updTreeNodes);
	// Signal which node must be updated in DAG and order the ndoes
	signalUpdatedTreeAndNodes(updTreeNodes, updatedNodes);
	// At this point both tree are cleared : no need to keep track of TR::Tree updated nodes
	curTree->clearUpdatedNodes();
}


bool Base::fullUpdateTreeAndDAG(const TR::TreeNode *nodeTR, const TR::TreeNode *parentNodeTR, TreeNode *node,
		std::set<DAG::BaseNode*> &updatedNodes) {
	bool requireReinit = false;

	// Get TR children
	TR::vecTN_t childrenTR = nodeTR->getChildren(parentNodeTR);
	// Get Internal children
	const treeNodeTI_children &children = node->getChildren();

	// Get difference
	std::vector<TreeNode*> toAdd, toRemove;
	getDifference(childrenTR, children, toAdd, toRemove);

	// Remove edges
	for(size_t iR=0; iR<toRemove.size(); ++iR) {
		removeEdge(node, toRemove[iR]);
	}

	// Add edges
	for(size_t iA=0; iA<toAdd.size(); ++iA) {
		addEdge(node, toAdd[iA]);
	}

	// If the node is a leaf, we don't have to reinit it
	if(node->isLeaf()) return false;

	// Order nodes
	orderChildren(node, childrenTR);

	// Apply the same for all children
	for(size_t iC=0; iC<childrenTR.size(); ++iC) {
		bool childRequireInit = fullUpdateTreeAndDAG(childrenTR[iC], nodeTR, node->getChild(iC), updatedNodes);
		requireReinit = requireReinit || childRequireInit;
	}
	// From there my subtree is correct
	// If one of my children was reinit'd or if I require to be reinit'd
	if(requireReinit || node->doRequireReinitDAG()) {
		// reinit each internal DAG nodes
		memorizeUpdatedParentNodes(node, updatedNodes);
		/* OLD
		for(size_t iE=0; iE<node->getNumberEdgeNode(); ++iE){
			ParentNode *pNode = dynamic_cast<ParentNode*>(node->getEdgeNode(iE));
			if(pNode) {
				pNode->updated();
				updatedNodes.insert(pNode);
			}
		}*/
		node->setRequireReinitDAG(false); // I did it
		return true; // But by dad as to do it
	} else {
		return false;
	}
}




void Base::cleanTreeAndDAG() {
	rootTree->deleteChildren();
	delete rootTree;
	rootDAG->deleteChildren();
	delete rootDAG;

	branchPtr.clear();
	branchMap.clear();
}

double Base::getTotalTreeLength(const BaseSampleWrapper &sw){
	double sum = 0.;
	for(size_t i=0; i<sw.N_BL; ++i) {
		sum += sw.branchLength[i];
	}
	return sum;
}

void Base::writeMarkers(const Sampler::Sample &sample, const BaseSampleWrapper &sw) {
	// Markers
	size_t iMarker;
	if(isUsingFixedPairsStrategy()) {
		iMarker = 5; // No prior and hyperprior on K and
	} else {
		iMarker = 7; // Prior and hyperprior on K and
	}

	// Compute normalized stationary freq
	double stFreqSum = 1.;
	for(size_t iF=0; iF<3; ++iF) {
		stFreqSum += sw.stFreq[iF];
	}
	for(size_t iT=0; iT<sw.stFreq.size(); ++iT) {
		markers[iMarker] = sw.stFreq[iT]/stFreqSum;
		iMarker++;
	}
	markers[iMarker] = 1./stFreqSum;
	iMarker++;

	// Compute normalized theta
	double sum = 1. ; // Non sampled theta parameter in GTR
	for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
		sum += sw.thetas[iT];
	}
	for(size_t iT=0; iT<sw.thetas.size(); ++iT) {
		markers[iMarker] = sw.thetas[iT]/sum;
		iMarker++;
	}
	markers[iMarker] = 1./sum;
	iMarker++;

	/// COEV SUMMARY + MSS
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > meanAccumulator_t;
	std::vector< meanAccumulator_t > coevSummary(5);
	// 1) MSS
	vecSSMarkers.clear();
	for(itListPCLI_t it=activePermCoevInfo.begin(); it != activePermCoevInfo.end(); ++it) {
		// Getting the model specific sample
		//std::cout << "ACTIVE PERM COEV - WRITE - " << (*it)->getUIDSubSpace()->getLabelUID() << std::endl; // FIXME
		const Sampler::ModelSpecificSample& mss = sample.getMSS((*it)->getUIDSubSpace());

		// Parameters are r1, r2, s, d
		assert(mss.getDblValues().size() == 4);
		double r1 = mss.getDblValues()[0];
		double r2 = mss.getDblValues()[1];
		double s = mss.getDblValues()[2];
		double d = mss.getDblValues()[3];

		double sumExp = 2*exp(sw.priorParamValues[0])+exp(sw.priorParamValues[1])+exp(sw.priorParamValues[2]);
		r1 = exp(sw.priorParamValues[0])/sumExp;
		r2 = exp(sw.priorParamValues[0])/sumExp;
		s = exp(sw.priorParamValues[1])/sumExp;
		d = exp(sw.priorParamValues[2])/sumExp;

		Sampler::SampleUtils::subspaceMarkers_t ssMarkers;
		ssMarkers.first = (*it)->getUIDSubSpace();
		//ssMarkers.second.push_back((*it)->getRootDAG()->getLogLikelihood());
		double pairLogLik = permutedCoev[(*it)->getProfileSize()-2]->getRootDAG()->getPairLogLikelihood((*it)->getPositions());
		ssMarkers.second.push_back(pairLogLik); // FIXME
		ssMarkers.second.push_back(d/s);
		vecSSMarkers.push_back(ssMarkers);

		// Store statistics
		coevSummary[0]( r1 ); // R1
		coevSummary[1]( r2 ); // R2
		coevSummary[2]( s ); // S
		coevSummary[3]( d ); // D
		coevSummary[4](d/s); // D/S
	}

	for(itListCLI_t it=activeTempCoevInfo.begin(); it != activeTempCoevInfo.end(); ++it) {
		// Getting the model specific sample
		//std::cout << "ACTIVE TEMP COEV - WRITE - " << (*it)->getUIDSubSpace()->getLabelUID() << std::endl; //FIXME
		const Sampler::ModelSpecificSample& mss = sample.getMSS((*it)->getUIDSubSpace());

		// Parameters are r1, r2, s, d
		assert(mss.getDblValues().size() == 4);
		double r1 = mss.getDblValues()[0];
		double r2 = mss.getDblValues()[1];
		double s = mss.getDblValues()[2];
		double d = mss.getDblValues()[3];

		double sumExp = 2*exp(sw.priorParamValues[0])+exp(sw.priorParamValues[1])+exp(sw.priorParamValues[2]);
		r1 = exp(sw.priorParamValues[0])/sumExp;
		r2 = exp(sw.priorParamValues[0])/sumExp;
		s = exp(sw.priorParamValues[1])/sumExp;
		d = exp(sw.priorParamValues[2])/sumExp;

		Sampler::SampleUtils::subspaceMarkers_t ssMarkers;
		ssMarkers.first = (*it)->getUIDSubSpace();
		//ssMarkers.second.push_back((*it)->getRootDAG()->getLogLikelihood());
		double pairLogLik = (*it)->getRootDAG()->getLogLikelihood();
		ssMarkers.second.push_back(pairLogLik); // FIXME
		ssMarkers.second.push_back(d/s);
		vecSSMarkers.push_back(ssMarkers);

		// Store statistics
		coevSummary[0]( r1 ); // R1
		coevSummary[1]( r2 ); // R2
		coevSummary[2]( s ); // S
		coevSummary[3]( d ); // D
		coevSummary[4](d/s); // D/S
	}


	if(activePermCoevInfo.empty()) {
		for(size_t iV=0; iV<5; ++iV) {
			markers[iMarker] = 0.;
			iMarker++;
		}
	} else {
		for(size_t iS=0; iS<5; ++iS) {
			markers[iMarker] = boost::accumulators::mean(coevSummary[iS]);
			iMarker++;
		}
	}

		// Total BL
	markers[iMarker] = getTotalTreeLength(sw);
	iMarker++;

	//GTR Lik
	double likGTR=0;
	for(size_t iN=0; iN<cmbNodes.size(); ++iN) {
		likGTR += cmbNodes[iN]->getLikelihood();
	}

	markers[iMarker] = likGTR;
	iMarker++;

	//Coev Lik
	double likCoev=0;
	for(size_t iP=0; iP<permutedCoev.size(); ++iP) {
		likCoev += permutedCoev[iP]->getRootDAG()->getLogLikelihood();
	}

	markers[iMarker] = getNActiveCoevLikelihood();
	iMarker++;

	markers[iMarker] = likCoev;
	iMarker++;

	if(STATE_GTR_MARKERS == ENABLE_GTR_MARKERS) {
		assert(cmbNodes.size() == 1);
		const TI_EigenVector_t& logLikGTR = cmbNodes.front()->getVectorLogLikGTR();
		assert((size_t)logLikGTR.size() == nSite);
		for(size_t iM=0; iM<nSite; ++iM) {
			markers[iMarker] = logLikGTR[iM];
			iMarker++;
		}
	}
}

/**
 * For PRIOR_SCALING_TYPE == GAMMA_PR : everything is done by the implicit prior mechanism of HOGAN
 * For the other - everything is done here
 *
 * @param sample the current sample
 * @param sw the sample wrapper that enable an easy acces to values
 * @return the prior values
 */
double Base::computeCoevPrior(const Sampler::Sample &sample, const BaseSampleWrapper &sw) {
	double priorCoev = 0.;

	// If there is no coev lik, we quit
	if(getNActiveCoevLikelihood() == 0) return priorCoev;

	// Else we check if there is some Coev scaling, the prior are managed by HOGAN implicit prior (see CoevRJHlp)
	ptrNormalDistr_t ptrDistrR, ptrDistrS, ptrDistrD;
	if(getNActiveCoevLikelihood() > 0) {
		ptrDistrR.reset(new boost::math::normal_distribution<>(Priors::GAMMA_NORMAL_MU_R, Priors::GAMMA_NORMAL_STD_R));
		ptrDistrS.reset(new boost::math::normal_distribution<>(Priors::GAMMA_NORMAL_MU_S, Priors::GAMMA_NORMAL_STD_S));
		ptrDistrD.reset(new boost::math::normal_distribution<>(Priors::GAMMA_NORMAL_MU_D, Priors::GAMMA_NORMAL_STD_D));

		std::vector<double> values = sw.priorParamValues;
		assert(values.size() == Parameters::SINGLE_NB_PARAMS);
		priorCoev += log(boost::math::pdf(*ptrDistrR, values[0]));
		//priorCoev += log(boost::math::pdf(*ptrDistrR, values[0]));
		//priorCoev += log(boost::math::pdf(*ptrDistrS, values[1]));
		priorCoev += log(boost::math::pdf(*ptrDistrD, values[2]));
	}

	return priorCoev;
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
