//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermCoevLikelihoodInfo.h
 *
 * @date Aug 15, 2017
 * @author meyerx
 * @brief
 */
#ifndef PERM_COEVLIKELIHOODINFO_H_
#define PERM_COEVLIKELIHOODINFO_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <string>
#include <vector>

#include "Nodes/IncNodes.h"
#include "UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;
class RootCoevNode;

namespace MD = ::MolecularEvolution::Definition;

// Utils
class PermCoevLikelihoodInfo;

typedef std::list<PermCoevLikelihoodInfo*> listPCLI_t;
typedef listPCLI_t::iterator itListPCLI_t;
typedef listPCLI_t::const_iterator constItListPCLI_t;

class PermCoevLikelihoodInfo {
public:
	PermCoevLikelihoodInfo(SubSpaceUID::sharedPtr_t aPtrUIDSubSpace, const std::vector<MD::idProfile_t> &aVecProfilesId);
	~PermCoevLikelihoodInfo();

	std::pair<size_t, size_t> getPositions() const;
	const std::vector<MD::idProfile_t>& getProfilesId() const;

	SubSpaceUID::sharedPtr_t getUIDSubSpace() const;

	void setPInd(const std::vector<size_t>& aPind);
	const std::vector<size_t>& getPInd() const;

	void setIdProfile(size_t aIdProfile);
	size_t getIdProfile() const;

	void setProfileSize(size_t aProfileSize);
	size_t getProfileSize() const;

	bool operator== (const PermCoevLikelihoodInfo &aPLS) const;

	std::string toString() const;

private:

	SubSpaceUID::sharedPtr_t ptrUIDSubSpace;
	size_t pos1, pos2;
	size_t currentIdProfile, profileSize;
	std::vector<size_t> pInd;
	std::vector<MD::idProfile_t> vecProfilesId;

	friend class Base;
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PERM_COEVLIKELIHOODINFO_H_ */
