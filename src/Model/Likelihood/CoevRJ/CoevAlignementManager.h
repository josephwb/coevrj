//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevAlignementManager.h
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef COEVALIGNEMENTMANAGER_H_
#define COEVALIGNEMENTMANAGER_H_

#include <stddef.h>
#include <map>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "PairProfileInfo.h"

#include "Utils/MolecularEvolution/DataLoader/Alignment/Alignment.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"
#include "Utils/MolecularEvolution/Definitions/Types.h"

namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;

//typedef float score_t;
typedef unsigned short int position_t;

/*struct PairProfileInfo {
	bool uniform;
	score_t score;
	std::vector<MD::idProfile_t> profilesId;
	std::vector<score_t> profileProb;
};*/

typedef std::map< std::pair<position_t, position_t>, PairProfileInfo > mapPairProfilesInfo_t;

class CoevAlignementManager {
public:
	typedef boost::shared_ptr<CoevAlignementManager> sharedPtr_t;

	static const bool USE_UNIFORM_WHEN_ALL_PROFILE_OBSERVED;
	const std::vector<MD::idProfile_t> FULL_PROFILE_ID;

public:
	CoevAlignementManager(DL::MSA &msa);
	CoevAlignementManager(DL::MSA &msa, const std::string &aCacheFileName);
	~CoevAlignementManager();

	MD::bitset16b_t getSiteCode(const std::string &alignementName, size_t pos) const;
	std::pair<MD::bitset16b_t, MD::bitset16b_t> getPairSitesCode(const std::string &alignementName, size_t pos1, size_t pos2) const;

	std::vector<MD::idProfile_t> getPossibleProfilesId(size_t pos1, size_t pos2) const;
	MD::vecProfiles_t getPossibleProfiles(size_t pos1, size_t pos2) const;

	PairProfileInfo getPairProfileInfo(size_t pos1, size_t pos2) const;

	const std::vector<score_t>& getPositionsScore() const;
	const std::vector<score_t>& getPairsIncludingPositionScore(size_t aPosition) const;

	//void defineProbablePairs(std::vector< std::pair<position_t, position_t > > &pairs, std::vector<PairProfileInfo> &ppis) const;

private:

	static const size_t SCORE_VERSION;
	static const bool SKIP_NON_INFORMATIVE_PAIRS;
	static const size_t MAX_ALIGNEMENT_SIZE_FOR_PRECOMPUT;
	static const double MIN_SCORE, MAX_VARIANCE, CONFLICT_TOLERANCE_RATIO;
	static const double PROFILE_NUMBER_SCORE_WEIGHT, MIN_CONFLICTS_SCORE_WEIGHT, PAIR_VARIANCE_SCORE_WEIGHT;

	const size_t N_SITE, N_ORDERED_PAIRS;
	const double MIN_ACCEPTABLE_CONFLICTS;

	const std::string cacheFileName;

	typedef std::vector< MD::bitset16b_t > sitesCode_t;
	typedef std::map< std::string, sitesCode_t > mapSitesCode_t;
	const mapSitesCode_t mapSitesCode;

	//const mapPairProfilesInfo_t allMSAPairProfilesInfo;
	const std::vector<PairProfileInfo> vecPairsProfilesInfo;

	std::vector<score_t> positionsScore;
	std::vector< std::vector<score_t> > positionsToPairsScore;

	void init(DL::MSA &msa);

	mapSitesCode_t constructSiteMap(DL::MSA &msa) const;
	std::vector<MD::idProfile_t> constructFullProfileId() const;
	std::vector<PairProfileInfo> constructVecPairsProfilesInfo(DL::MSA &msa) const;

	void saveVecPairsProfilesInfo(const std::vector<PairProfileInfo> &toSave) const;
	bool loadVecPairsProfilesInfo(std::vector<PairProfileInfo> &toLoad) const;

	MD::bitset16b_t defineObservablePairs(size_t pos1, size_t pos2, bool skipUninformativePairs) const;
	void definePairProfilesInformation(size_t pos1, size_t pos2, PairProfileInfo &ppi) const;
	void computePairProfileScores(PairProfileInfo &ppi, const std::vector<double> conflictsCount,
								  const std::vector< std::vector<double> > &countInPairProfile) const;

	double scaleWithWithOneMinusLog(double value, double minValue, double maxValue, double minScaledValue) const;

	std::pair<position_t, position_t> linearToTriangularUpperCoordinate(size_t linearPosition) const;
	size_t triangularUpperToLinearCoordinate(std::pair<position_t, position_t> triPositions) const;

	void DBG_CHECK_SCORES_FOR_16SRNA() const;
	void DBG_COMPUTE_SCORES_FOR_16SRNA() const;

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COEVALIGNEMENTMANAGER_H_ */
