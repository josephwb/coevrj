//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SwapGTRMoveRJ.cpp
 *
 * @date May 5, 2017
 * @author meyerx
 * @brief
 */
#include "SwapGTRMoveRJ.h"

#include "Parallel/RNG/RNG.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include <boost/math/distributions/gamma.hpp>

namespace Sampler { class ModelSpecificSample; }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

size_t SwapGTRMoveRJ::countAccepted = 0;
size_t SwapGTRMoveRJ::countRejected = 0;

SwapGTRMoveRJ::SwapGTRMoveRJ(Sampler::Sample &aCurrentSample, Base* aPtrLik, RNG *aRng) :
		Sampler::RJMCMC::RJMove(FORWARD, aCurrentSample), USE_COEV_INFORMATION(INFORMED_KEPT_FIRST_SWAP),
		ptrLik(aPtrLik), rng(aRng),	utilsCRJ(ptrLik, rng, proposedSample) {
	newIdProfile = 0;

	canApply = true;

	if(USE_COEV_INFORMATION == UNINFORMED_SWAP) {
		defineMoveWithoutCoevInformation();
	} else if(USE_COEV_INFORMATION == INFORMED_GTR_FIRST_SWAP) {
		defineMoveWithCoevInformationV1();
	} else if(USE_COEV_INFORMATION == INFORMED_KEPT_FIRST_SWAP) {
		defineMoveWithCoevInformationV2();
	} else {
		assert(false && "Unknown GTR SWAP type.");
	}

	canApply = !newVecProfilesId.empty();
	if(!canApply) moveState = IMPOSSIBLE;
}

SwapGTRMoveRJ::~SwapGTRMoveRJ() {
}

std::string SwapGTRMoveRJ::getStringStats(std::string &prefix) {
	std::stringstream ss;
	double ratio = (countAccepted+countRejected == 0) ? 0. : (double)countAccepted/(double)(countAccepted+countRejected);
	ss << prefix << "[SwapGTRMoveRJ] Move acceptance ratio = " << ratio;
	ss << " ( " << countAccepted << " / " << (countAccepted+countRejected) << " )" << std::endl;
	return ss.str();
}

void SwapGTRMoveRJ::defineMoveWithoutCoevInformation() {

	/// OLD PAIR : First we draw a coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	if(N_COEV < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	PermCoevLikelihoodInfo* ptrCLI = utilsCRJ.drawCoevLik();
	oldSubSpaceUID = ptrCLI->getUIDSubSpace();
	oldVecProfilesId = ptrCLI->getProfilesId();
	oldMSS = currentSample.getMSS(oldSubSpaceUID);

	/// NEW PAIR : Then we draw a GTR cluster
	const std::set<size_t> &segSitesGTR(ptrLik->getSegregatingSitesGTR());
	const size_t N_SEG_SITES_GTR = segSitesGTR.size();

	// We then have to find one segregated GTR site
	if(segSitesGTR.size() < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_GTR < 1!");
		return;
	}
	std::vector<size_t> positions = utilsCRJ.drawSitesPosUniform(1, segSitesGTR);
	size_t posGTR = positions[0];

	// Draw wich position is changed in the pair
	bool changeFirst = rng->genUniformInt(0, 1) == 0;
	size_t removedPos, keptPos;
	if(changeFirst) {
		removedPos = ptrCLI->getPositions().first;
		keptPos = ptrCLI->getPositions().second;
	} else {
		removedPos = ptrCLI->getPositions().second;
		keptPos = ptrCLI->getPositions().first;
	}

	// Define new pair of positions
	size_t pos1 = keptPos;
	size_t pos2 = posGTR;
	if(pos1 > pos2) std::swap(pos1, pos2);
	newSubSpaceUID.reset(new SubSpaceUID(pos1, pos2));
	PairProfileInfo newPPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
	newVecProfilesId = ptrLik->getCoevAlignManager()->getPossibleProfilesId(pos1, pos2);

	// Define the moves probability
	double kPairs = ptrLik->getNActiveCoevLikelihood();
	double kSites = N_SEG_SITES_GTR;

	// Prob of changing P1 or P2 (1/2), prob of changing this pair (1/kPairs), prob of inserting this site (1/kSites)
	fwMoveProb = (1./(2.*kPairs*kSites));
	// Prob of reverse move is symmetrical
	bwMoveProb = (1./(2.*kPairs*kSites));

	/// [Optional] Draws the parameter values and define the prob
	canApply = !newVecProfilesId.empty();
	if(!canApply) {
		moveState = IMPOSSIBLE;
	} else {
		int idRand = rng->genUniformInt(0, newVecProfilesId.size()-1);
		newIdProfile = newVecProfilesId[idRand];
	}

	fwRandomProb = 1.0/newVecProfilesId.size();
	bwRandomProb = 1.0/oldVecProfilesId.size();

	// Jacobian is 1. because no change of dimension
	jacobian = 1.;

}

/// First chose the GTR position, then conditionned on that chose the poisition to remove from the pair
void SwapGTRMoveRJ::defineMoveWithCoevInformationV1() {

	/// OLD PAIR : First we draw a coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	if(N_COEV < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	PermCoevLikelihoodInfo* ptrCLI = utilsCRJ.drawCoevLik();
	oldSubSpaceUID = ptrCLI->getUIDSubSpace();
	oldVecProfilesId = ptrCLI->getProfilesId();
	oldMSS = currentSample.getMSS(oldSubSpaceUID);
	PairProfileInfo oldPPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI->getPositions().first, ptrCLI->getPositions().second);
	//std::cout << "[SwapGTRMoveRJ V1] SS UID = " << oldSubSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG



	/// NEW PAIR : Draw a GTR position proportionally to its score
	const std::set<size_t> &segSitesGTR(ptrLik->getSegregatingSitesGTR());

	// We then have to find one segregated GTR site
	if(segSitesGTR.size() < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_GTR < 1!");
		return;
	}

	boost::dynamic_bitset<> sitesAvailability(ptrLik->getSitesAvailability());
	// "Full" availability
	boost::dynamic_bitset<> fullSiteAvailability(ptrLik->getSitesAvailability());
	fullSiteAvailability[ptrCLI->getPositions().first] = true;
	fullSiteAvailability[ptrCLI->getPositions().second] = true;

	UtilsCoevRJ::UnormalizedProbabilities uOldProbFirst = utilsCRJ.defineProbabilityBySitesAtFirstPosition(sitesAvailability);
	size_t posGTR = rng->drawFromLargeUnormalizedVector(uOldProbFirst.normalizingConstant, uOldProbFirst.probabilities);
	double probPosGTR = uOldProbFirst.probabilities[posGTR]/uOldProbFirst.normalizingConstant;
	//std::cout << "[SwapGTRMoveRJ V1] Drawing " << posGTR << " with probability = " << probPosGTR << std::endl; // FIXME DEBUG

	// Select one of the position already in the coev pair to stay proportionally to its score conditioned on posGTR
	UtilsCoevRJ::UnormalizedProbabilities uOldProbSec = utilsCRJ.defineProbabilityBySitesAtSecondPosition(posGTR, fullSiteAvailability);
	double probOldPos1 = uOldProbSec.probabilities[ptrCLI->getPositions().first];
	double probOldPos2 = uOldProbSec.probabilities[ptrCLI->getPositions().second];
	double sumOldProb = probOldPos1 + probOldPos2;
	//std::cout << "[SwapGTRMoveRJ V1] probOldPos1 = " << probOldPos1 << "\tprobOldPos2 = " << probOldPos2 << std::endl; // FIXME DEBUG
	//std::cout << "[SwapGTRMoveRJ V1] sumOldProb = " << sumOldProb  << std::endl; // FIXME DEBUG

	bool keepFirst = rng->genUniformDbl() < probOldPos1/sumOldProb;

	double fwProbOfChange;
	size_t removedPos, keptPos;
	if(keepFirst) {
		removedPos = ptrCLI->getPositions().second;
		keptPos = ptrCLI->getPositions().first;
		fwProbOfChange = probOldPos1/sumOldProb;
		//std::cout << "[SwapGTRMoveRJ V1] Keeping first with probability = " << fwProbOfChange << std::endl; // FIXME DEBUG
	} else {
		removedPos = ptrCLI->getPositions().first;
		keptPos = ptrCLI->getPositions().second;
		fwProbOfChange = probOldPos2/sumOldProb;
		//std::cout << "[SwapGTRMoveRJ V1] Keeping second with probability = " << fwProbOfChange << std::endl; // FIXME DEBUG
	}

	// Define new pair of positions
	size_t pos1 = keptPos;
	size_t pos2 = posGTR;
	if(pos1 > pos2) std::swap(pos1, pos2);
	newSubSpaceUID.reset(new SubSpaceUID(pos1, pos2));
	PairProfileInfo newPPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
	newVecProfilesId = utilsCRJ.copyProfileIds(newPPI);

	// Define the moves probability
	double kPairs = ptrLik->getNActiveCoevLikelihood();

	// Informed prob of changing P1 or P2, prob of changing this pair (1/kPairs), informed prob of inserting this site
	fwMoveProb = fwProbOfChange*(1./(double)kPairs)*probPosGTR;
	// Prob of reverse move is not symmetrical
	sitesAvailability[removedPos] = true;
	sitesAvailability[posGTR] = false;
	UtilsCoevRJ::UnormalizedProbabilities uNewProbFirst = utilsCRJ.defineProbabilityBySitesAtFirstPosition(sitesAvailability);
	// Prob of chosing the removed position
	double probPosRemoved = uNewProbFirst.probabilities[removedPos]/uNewProbFirst.normalizingConstant;

	// Prob of of building back this pair
	UtilsCoevRJ::UnormalizedProbabilities uNewProbSec = utilsCRJ.defineProbabilityBySitesAtSecondPosition(removedPos, fullSiteAvailability);
	double probNewPos1 = uNewProbSec.probabilities[posGTR];
	double probNewPos2 = uNewProbSec.probabilities[keptPos];
	double sumNewProb = probNewPos1 + probNewPos2;
	double bwProbOfChange = probNewPos2 / sumNewProb;

	bwMoveProb = bwProbOfChange*(1./(double)kPairs)*probPosRemoved;

	//std::cout << "[SwapGTRMoveRJ V1] BW Probability of keeping pos = " << bwProbOfChange << std::endl; // FIXME DEBUG
	//std::cout << "[SwapGTRMoveRJ V1] BW Probability of drawing removed pos = " << probPosRemoved << std::endl; // FIXME DEBUG
	//std::cout << "[SwapGTRMoveRJ V1] FwMoveProb = " << fwMoveProb << "\tbwMoveprob = "<< bwMoveProb << "\tratio=" << bwMoveProb/fwMoveProb << std::endl; // FIXME DEBUG

	/// Draw from profile prob
	std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(newPPI);
	newIdProfile = randProf.first;
	double newProfileProb = randProf.second;
	/*size_t idRand = rng->drawFrom(newPPI.profileProb);
	newIdProfile = newVecProfilesId[idRand];
	double newProfileProb = newPPI.profileProb[idRand];*/

	double oldProfileProb = utilsCRJ.getProfileProbability(oldMSS.getIntValues()[0], oldPPI);
	/*for(size_t iP=0; iP<oldPPI.profilesId.size(); ++iP) {
		if(oldPPI.profilesId[iP] == (size_t)oldMSS.getIntValues()[0]) {
			oldProfileProb = oldPPI.profileProb[iP];
			break;
		}
	}
	assert(oldProfileProb != 0.);*/

	fwRandomProb = newProfileProb;
	bwRandomProb = oldProfileProb;
	//std::cout << "[SwapGTRMoveRJ V1] fwRandomProb = " << fwRandomProb << "\tbbwRandomProb = "<< bwRandomProb << "\tratio=" << bwRandomProb/fwRandomProb << std::endl; // FIXME DEBUG
	//std::cout << "[SwapGTRMoveRJ V1] -------------------------------------------------" << std::endl; // FIXME DEBUG
	// Jacobian is 1. because no change of dimension
	jacobian = 1.;
}

/// First chose a position to remove, then conditioned on that chose a GTR position
void SwapGTRMoveRJ::defineMoveWithCoevInformationV2() {

	/// OLD PAIR : First we draw a coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	if(N_COEV < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	PermCoevLikelihoodInfo* ptrCLI = utilsCRJ.drawCoevLik();
	oldSubSpaceUID = ptrCLI->getUIDSubSpace();
	oldVecProfilesId = ptrCLI->getProfilesId();
	oldMSS = currentSample.getMSS(oldSubSpaceUID);
	PairProfileInfo oldPPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI->getPositions().first, ptrCLI->getPositions().second);

	/// NEW PAIR : Draw a GTR position proportionally to its score
	const std::set<size_t> &segSitesGTR(ptrLik->getSegregatingSitesGTR());
	const std::vector<score_t> &positionsScore = ptrLik->getCoevAlignManager()->getPositionsScore();

	// We then have to find one segregated GTR site
	if(segSitesGTR.size() < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_GTR < 1!");
		return;
	}

	// Select one of the position already in the coev pair to stay proportionally to its score conditionned on posGTR
	double probOldPos1 = positionsScore[ptrCLI->getPositions().first];
	double probOldPos2 = positionsScore[ptrCLI->getPositions().second];
	double sumOldProb = probOldPos1 + probOldPos2;

	bool keepFirst = rng->genUniformDbl() < probOldPos1/sumOldProb;

	double fwProbOfChange;
	size_t removedPos, keptPos;
	if(keepFirst) {
		removedPos = ptrCLI->getPositions().second;
		keptPos = ptrCLI->getPositions().first;
		fwProbOfChange = probOldPos1/sumOldProb;
	} else {
		removedPos = ptrCLI->getPositions().first;
		keptPos = ptrCLI->getPositions().second;
		fwProbOfChange = probOldPos2/sumOldProb;
	}

	boost::dynamic_bitset<> sitesAvailability(ptrLik->getSitesAvailability());
	UtilsCoevRJ::UnormalizedProbabilities uOldProbSec = utilsCRJ.defineProbabilityBySitesAtSecondPosition(keptPos, sitesAvailability);
	size_t posGTR = rng->drawFromLargeUnormalizedVector(uOldProbSec.normalizingConstant, uOldProbSec.probabilities);
	double probPosGTR = uOldProbSec.probabilities[posGTR]/uOldProbSec.normalizingConstant;

	// Define new pair of positions
	size_t pos1 = keptPos;
	size_t pos2 = posGTR;
	if(pos1 > pos2) std::swap(pos1, pos2);
	newSubSpaceUID.reset(new SubSpaceUID(pos1, pos2));
	PairProfileInfo newPPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(pos1, pos2);
	newVecProfilesId = utilsCRJ.copyProfileIds(newPPI);

	// Define the moves probability
	double kPairs = ptrLik->getNActiveCoevLikelihood();

	// Informed prob of changing P1 or P2, prob of changing this pair (1/kPairs), informed prob of inserting this site
	fwMoveProb = fwProbOfChange*(1./(double)kPairs)*probPosGTR;
	// Prob of reverse move is not symmetrical
	// Prob of of building back this pair
	sitesAvailability[removedPos] = true;
	sitesAvailability[posGTR] = false;

	UtilsCoevRJ::UnormalizedProbabilities uNewProbSec = utilsCRJ.defineProbabilityBySitesAtSecondPosition(keptPos, sitesAvailability);
	double probNewPos1 = positionsScore[posGTR];
	double probNewPos2 = positionsScore[keptPos];
	double sumNewProb = probNewPos1 + probNewPos2;
	double bwProbOfChange = probNewPos2 / sumNewProb;
	// Prob of chosing the removed position
	double probPosRemoved = uNewProbSec.probabilities[removedPos]/uNewProbSec.normalizingConstant;
	bwMoveProb = bwProbOfChange*(1./(double)kPairs)*probPosRemoved;

	/// Draw from profile prob
	std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(newPPI);
	newIdProfile = randProf.first;
	double newProfileProb = randProf.second;
	double oldProfileProb = utilsCRJ.getProfileProbability(oldMSS.getIntValues()[0], oldPPI);

	fwRandomProb = newProfileProb;
	bwRandomProb = oldProfileProb;

	// Jacobian is 1. because no change of dimension
	jacobian = 1.;
}



bool SwapGTRMoveRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	if(canApply) {
		// Disable old pair
		disabledSSInfo = utilsCRJ.disableSubSpace(oldSubSpaceUID, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo->getParametersId().begin(), disabledSSInfo->getParametersId().end());

		// Create new pair
		// deletedSSInfo is NULL, and should be NULL
		createdSSInfo = utilsCRJ.createTempSubSpace(newSubSpaceUID, model, blockDispatcher); // FIXME
		utilsCRJ.setValuesMSS(createdSSInfo, newIdProfile, oldMSS.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo->getParametersId().begin(), createdSSInfo->getParametersId().end());
	}
	return false;
}

bool SwapGTRMoveRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(canApply);

	utilsCRJ.deleteSubSpaceAfterDisable(oldSubSpaceUID);
	listSSInfo.remove(disabledSSInfo);
	listSSInfo.push_back(createdSSInfo);

	// Remove temp likelihood and register into permutedLikelihoods
	utilsCRJ.validateTempSubSpace(newSubSpaceUID);

	countAccepted++;
	//std::cout << "[SwapGTRMoveRJ] Accepted from : " << oldSubSpaceUID->getLabelUID() << "\t-->\t to : " << newSubSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}

bool SwapGTRMoveRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	if(canApply) {
		// 1) Delete new
		ptrSSInfo_t deletedSSInfo = utilsCRJ.deleteTempSubSpace(createdSSInfo, model, blockDispatcher);

		// 2) Enable old
		ptrSSInfo_t reCreatedSSInfo = utilsCRJ.createSubSpaceAfterDisable(oldSubSpaceUID, disabledSSInfo, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo, oldMSS.getIntValues()[0], oldMSS.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo->getParametersId().begin(), reCreatedSSInfo->getParametersId().end());
		// It has been delete, then re-created
		// The indices of the parameters and block may thus have changed
		// We must signal it to the RJMCMC Handler
		// There we keep  createdSSRessources and deletedSSRessources as such
		listSSInfo.remove(disabledSSInfo); 			// Remove the original SSInfo
		listSSInfo.push_back(reCreatedSSInfo); 		// Add the new one
	}
	countRejected++;
	//std::cout << "[SwapGTRMoveRJ] Rejected from : " << oldSubSpaceUID->getLabelUID() << "\t-->\t to : " << newSubSpaceUID->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
