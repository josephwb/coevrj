//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file UtilsCoevRJ.h
 *
 * @date May 5, 2017
 * @author meyerx
 * @brief
 */
#ifndef UTILSCOEVRJ_H_
#define UTILSCOEVRJ_H_

#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"

class RNG;
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class UtilsCoevRJ {
public:
	typedef Sampler::Proposal::BlockDispatcher BlockDispatcher_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::listSSInfo_t listSSInfo_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::sharedPtr_t ptrSSInfo_t;

	struct UnormalizedProbabilities {
		double normalizingConstant;
		std::vector<float> probabilities;
	};

	static const double RATE_COEV_PARAM;
	static const std::vector<std::string> PARAM_NAMES;

public:
	UtilsCoevRJ(Base* aPtrLik, RNG *aRng, Sampler::Sample &aProposedSample);
	~UtilsCoevRJ();

	/// Create a SubSpace with UID being the subSpaceUID variable -- update the likelihood and the createdSSRessources
	ptrSSInfo_t createTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID,
								StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	/// Create a SubSpace with UID being the subSpaceUID variable -- do not touch the Sample
	ptrSSInfo_t createTempSubSpaceWitoutSample(const SubSpaceUID::sharedPtr_t &subSpaceUID,
											   StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	/// Delete a SubSpace with UID being the subSpaceUID variable -- update the likelihood and the deletedSSRessources
	ptrSSInfo_t deleteTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
			            	   StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	ptrSSInfo_t deleteTempSubSpace(const ptrSSInfo_t &subSpaceInfo,
				               StatisticalModel::Model &model,
				               BlockDispatcher_t &blockDispatcher);

	ptrSSInfo_t disableSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
								StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	ptrSSInfo_t disableSubSpace(const ptrSSInfo_t &subSpaceInfo,
								StatisticalModel::Model &model,
								BlockDispatcher_t &blockDispatcher);

	/// Delete a SubSpace with UID being the subSpaceUID variable -- do not touch the Sample
	ptrSSInfo_t disableSubSpaceWithoutSample(const SubSpaceUID::sharedPtr_t &subSpaceUID, listSSInfo_t &listSSInfo,
								StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	ptrSSInfo_t disableSubSpaceWithoutSample(const ptrSSInfo_t &subSpaceInfo,
								StatisticalModel::Model &model,
								BlockDispatcher_t &blockDispatcher);

	void deleteSubSpaceAfterDisable(const SubSpaceUID::sharedPtr_t &subSpaceUID);
	ptrSSInfo_t createSubSpaceAfterDisable(const SubSpaceUID::sharedPtr_t &subSpaceUID, ptrSSInfo_t &disabledSSInfo,
									  StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	void validateTempSubSpace(const SubSpaceUID::sharedPtr_t &subSpaceUID);

	void setValuesMSS(ptrSSInfo_t &createdSSInfo, size_t idProfile, const std::vector<double> &vals);
	void setValuesMSS(ptrSSInfo_t &createdSSInfo, size_t idProfile, double r1, double r2, double s, double d);

	/// Draw the pair of sites randomly
	std::vector<size_t> drawSitesPosUniform(const size_t N_GTR, std::set<size_t> sitesGTR);
	UnormalizedProbabilities defineProbabilityBySitesAtFirstPosition(const boost::dynamic_bitset<> &sitesAvailability);
	UnormalizedProbabilities defineProbabilityBySitesAtSecondPosition(size_t iPos1, const boost::dynamic_bitset<> &sitesAvailability);
	/// Draw the coev likelihood to suppress
	PermCoevLikelihoodInfo* drawCoevLik();

	// define a profile probability
	std::vector<MD::idProfile_t> copyProfileIds(const PairProfileInfo &ppi) const;
	std::pair<MD::idProfile_t, double> drawProfile(const PairProfileInfo &ppi) const;
	score_t getProfileProbability(size_t profileId, const PairProfileInfo &ppi) const;

	// Scaling
	ptrSSInfo_t createCoevScalingBlock(StatisticalModel::Model &model,
            						   BlockDispatcher_t &blockDispatcher);

	ptrSSInfo_t deleteCoevScalingBlock(listSSInfo_t &listSSInfo,
            						   BlockDispatcher_t &blockDispatcher);

	// Parameters ( only for SINGLE_PARAM )
	ptrSSInfo_t createSingleParamBlock(StatisticalModel::Model &model,
	            					   BlockDispatcher_t &blockDispatcher);

	ptrSSInfo_t deleteSingleParamBlock(listSSInfo_t &listSSInfo,
									   BlockDispatcher_t &blockDispatcher);


private:

	Base* ptrLik;
	RNG* rng;
	Sampler::Sample &proposedSample;

	/// Create and delete the parameters in function of the CoevLIkelihoodInformations
	void createParameters(ptrSSInfo_t &createdSSInfo, const std::vector<MD::idProfile_t>& profilesId, StatisticalModel::Model &model);
	void deleteParameters(const ptrSSInfo_t &deletedSSInfo, StatisticalModel::Model &model);

	/// Create and delete the blocks
	void createBlocks(ptrSSInfo_t &createdSSInfo, std::pair<size_t, size_t> positions,
			          StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	void deleteBlocks(const ptrSSInfo_t &deletedSSInfo, BlockDispatcher_t &blockDispatcher);

	void defineCoevBlocksForSingleParam(ptrSSInfo_t &createdSSInfo, std::pair<size_t, size_t> positions,
				   	   	   	   	   	    StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	/// Create and delete (+register/unregister) the ModelSpecificSample
	void createMSS(ptrSSInfo_t &createdSSInfo);
	void deleteMSS(const ptrSSInfo_t &deletedSSInfo);

};



} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* UTILSCOEVRJ_H_ */
