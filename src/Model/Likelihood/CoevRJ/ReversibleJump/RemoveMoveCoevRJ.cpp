//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RemoveMoveCoevRJ.cpp
 *
 * @date Oct 6, 2017
 * @author meyerx
 * @brief
 */
#include "RemoveMoveCoevRJ.h"

#include "Model/Likelihood/CoevRJ/ReversibleJump/DefineConstantsCoev.h"
#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

RemoveMoveCoevRJ::RemoveMoveCoevRJ(const std::string &aLabel, Sampler::Sample &aDummySample, Base* aPtrLik) :
		Sampler::RJMCMC::RJMove(BACKWARD, aDummySample),
		ptrLik(aPtrLik),
		utilsCRJ(ptrLik, rng, proposedSample) {


	std::vector<string> pairs;
	boost::split(pairs, aLabel, boost::is_any_of("_"));
	assert(pairs.size() == 3); // 3 because it has a word after each "_": eg. {1}_{2}_{}
	pos1 = atoi(pairs[0].c_str());
	pos2 = atoi(pairs[1].c_str());

	isWithCoevScalingChange = false;
}

RemoveMoveCoevRJ::~RemoveMoveCoevRJ() {
}


// Have to be called first
bool RemoveMoveCoevRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	// Remove scaling if needed
	isWithCoevScalingChange = mustRemoveCoevScaling();

	// Disable the susbspace
	subSpaceUID.reset(new SubSpaceUID(pos1, pos2));
	deletedSSInfo = utilsCRJ.disableSubSpaceWithoutSample(subSpaceUID, listSSInfo, model, blockDispatcher);
	updatedParameters = deletedSSInfo->getParametersId();

	return true;
}

// Have to be called second
bool RemoveMoveCoevRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(moveState == APPLIED);

	// Delete subspace
	utilsCRJ.deleteSubSpaceAfterDisable(subSpaceUID);
	listSSInfo.remove(deletedSSInfo);

	// Remove coev scaling sampling
	if(isWithCoevScalingChange) {
		ptrSSInfo_t deletedBlockHPSSInfo = utilsCRJ.deleteCoevScalingBlock(listSSInfo, blockDispatcher);
		listSSInfo.remove(deletedBlockHPSSInfo);
	}

	// Remove parameter sampling
	if(ptrLik->getNActiveCoevLikelihood() == 0) {
		ptrSSInfo_t deletedBlockParamInfo = utilsCRJ.deleteSingleParamBlock(listSSInfo, blockDispatcher);
		listSSInfo.remove(deletedBlockParamInfo);
	}

	return true;
}

bool RemoveMoveCoevRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(false);
	return false;
}

bool RemoveMoveCoevRJ::mustAddCoevScaling() const {
	assert(moveDirection == FORWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 0; // Going from 0 Coev to 1

	return isTheMove;
}

bool RemoveMoveCoevRJ::mustRemoveCoevScaling() const {
	assert(moveDirection == BACKWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 1; // Going from 1 Coev to 0

	return isTheMove;
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
