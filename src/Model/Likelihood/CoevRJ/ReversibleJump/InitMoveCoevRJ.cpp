//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InitMoveCoevRJ.cpp
 *
 * @date Jul 6, 2017
 * @author meyerx
 * @brief
 */
#include "InitMoveCoevRJ.h"

#include "Utils/MolecularEvolution/Definitions/MolecularDefIncl.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

InitMoveCoevRJ::InitMoveCoevRJ(size_t aPos1, size_t aPos2,  std::string aProfile, std::vector<double> aValues,  Sampler::Sample &aDummySample, Base* aPtrLik) :
		Sampler::RJMCMC::RJMove(FORWARD, aDummySample),
		pos1(aPos1), pos2(aPos2), profile(aProfile), values(aValues),
		ptrLik(aPtrLik),
		utilsCRJ(ptrLik, rng, proposedSample) {

	idProfile = 0;

	std::vector<string> pairs;
	profile = profile.substr(1, profile.size()-2);
	boost::split(pairs, profile, boost::is_any_of("|"));

	const MolecularEvolution::Definition::vecProfiles_t &ALL_PROFILES =
			MolecularEvolution::Definition::getNucleotidesPairProfiles()->ALL_POSSIBLE_PROFILES;

	bool found = false;
	for(size_t iP=0; iP<ALL_PROFILES.size(); ++iP) {
		if(pairs.size() != ALL_PROFILES[iP].size()) continue;

		bool same = true;
		for(size_t iN=0; iN<pairs.size(); ++iN) {
			same = same && (pairs[iN] == ALL_PROFILES[iP][iN]);
		}
		if(same) {
			found = true;
			idProfile = iP;
			break;
		}
	}
	assert(found);

	isWithCoevScalingChange = false;
}

InitMoveCoevRJ::~InitMoveCoevRJ() {
}


// Have to be called first
bool InitMoveCoevRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	isWithCoevScalingChange = mustAddCoevScaling();

	subSpaceUID.reset(new SubSpaceUID(pos1, pos2));
	//std::cout << "[InitMoveCoevRJ] apply " << subSpaceUID->getLabelUID() << std::endl;
	createdSSInfo = utilsCRJ.createTempSubSpace(subSpaceUID, model, blockDispatcher);
	utilsCRJ.setValuesMSS(createdSSInfo, idProfile, values[0], values[1], values[2], values[3]);
	updatedParameters = createdSSInfo->getParametersId();

	return true;
}

// Have to be called second
bool InitMoveCoevRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	// Remove temp likelihood and register into permutedLikelihoods
	utilsCRJ.validateTempSubSpace(subSpaceUID);

	listSSInfo.push_back(createdSSInfo);

	/// Coev scaling
	if(isWithCoevScalingChange) {
		ptrSSInfo_t createdBlockHPSSInfo = utilsCRJ.createCoevScalingBlock(model, blockDispatcher);
		listSSInfo.push_back(createdBlockHPSSInfo);
	}

	if(ptrLik->getNActiveCoevLikelihood() == 1) {
		ptrSSInfo_t createdBlockCPInfo = utilsCRJ.createSingleParamBlock(model, blockDispatcher);
		listSSInfo.push_back(createdBlockCPInfo);
	}

	// Keep value, nothing to do

	return true;
}

bool InitMoveCoevRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(false);
	return false;
}

bool InitMoveCoevRJ::mustAddCoevScaling() const {
	assert(moveDirection == FORWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 0; // Going from 0 Coev to 1

	return isTheMove;
}

bool InitMoveCoevRJ::mustRemoveCoevScaling() const {
	assert(moveDirection == BACKWARD);
	bool isTheMove = ptrLik->getNActiveCoevLikelihood() == 1; // Going from 1 Coev to 0

	return isTheMove;
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
