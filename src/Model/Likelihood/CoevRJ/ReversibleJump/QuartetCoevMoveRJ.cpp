//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file QuartetCoevMoveRJ.cpp
 *
 * @date Oct 5, 2017
 * @author meyerx
 * @brief
 */
#include "QuartetCoevMoveRJ.h"

#include <assert.h>
#include <stddef.h>

#include "Model/Model.h"
#include "Parallel/RNG/RNG.h"
#include "Model/Likelihood/CoevRJ/Base.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

size_t QuartetCoevMoveRJ::countAccepted = 0;
size_t QuartetCoevMoveRJ::countRejected = 0;

QuartetCoevMoveRJ::QuartetCoevMoveRJ(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample, Base* aPtrLik, RNG *aRng) :
							   Sampler::RJMCMC::RJMove(aMoveDirection, aCurrentSample),
							   ptrLik(aPtrLik), rng(aRng), utilsCRJ(ptrLik, rng, proposedSample) {

	canApply = true;
	idProfile_A = idProfile_B = idProfile_C = 0;

	if(moveDirection == FORWARD) { // Forward
		defineForwardMove();
	} else { // Backward
		defineBackwardMove();
		}
}

QuartetCoevMoveRJ::~QuartetCoevMoveRJ() {
}

std::string QuartetCoevMoveRJ::getStringStats(std::string &prefix) {
	std::stringstream ss;
	double ratio = (countAccepted+countRejected) == 0 ? 0. : (double)countAccepted/(double)(countAccepted+countRejected);
	ss << prefix << "[QuartetCoevMoveRJ] Move acceptance ratio = " << ratio;
	ss << " ( " << countAccepted << " / " << (countAccepted+countRejected) << " )" << std::endl;
	return ss.str();
}

void QuartetCoevMoveRJ::defineForwardMove() {

	/// OLD PAIRS : First we draw two coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	//std::cout << "-------------------------------------------------------------------------------" << std::endl; // FIXME
	//std::cout << "[FORWARD QUARTET] N_PAIRS = " << N_COEV << std::endl; // FIXME
	if(N_COEV < 1) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	// Draw a pair
	PermCoevLikelihoodInfo* ptrCLI_A = utilsCRJ.drawCoevLik();
	subSpaceUID_A = ptrCLI_A->getUIDSubSpace();
	vecProfilesId_A = ptrCLI_A->getProfilesId();
	MSS_A = currentSample.getMSS(subSpaceUID_A);
	PairProfileInfo PPI_A = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI_A->getPositions().first, ptrCLI_A->getPositions().second);
	double profileProb_A = utilsCRJ.getProfileProbability(MSS_A.getIntValues()[0], PPI_A);
	double kPairs = ptrLik->getNActiveCoevLikelihood();
	double probPairDraw = 1./kPairs;
	//std::cout << "[FORWARD QUARTET] PAIR A = " << ptrCLI_A->toString() << std::endl; // FIXME

	// Draw two positions
	boost::dynamic_bitset<> sitesAvailability = ptrLik->getSitesAvailability();

	size_t pos1PairB = ptrCLI_A->getPositions().first;
	size_t pos1PairC = ptrCLI_A->getPositions().second;

	// Order of positions
	double rndOrder = rng->genUniformInt(0,1);
	if(rndOrder == 1) {
		std::swap(pos1PairB, pos1PairC);
	}
	double probOrder = 0.5;

	// Position 2 for pair B
	UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1PairB, sitesAvailability);
	size_t rndPos2PairB = rng->drawFromLargeUnormalizedVector(uProb1.normalizingConstant, uProb1.probabilities);
	double probPosition1 = uProb1.probabilities[rndPos2PairB]/uProb1.normalizingConstant;
	sitesAvailability[rndPos2PairB] = false;
	//std::cout << "[FORWARD QUARTET] DRAW POS = " << rndPos2PairB << " with prob = " << probPosition1 << std::endl; // FIXME

	// Position 2 for pair C
	UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1PairC, sitesAvailability);
	size_t rndPos2PairC = rng->drawFromLargeUnormalizedVector(uProb2.normalizingConstant, uProb2.probabilities);
	double probPosition2 = uProb2.probabilities[rndPos2PairC]/uProb2.normalizingConstant;
	//std::cout << "[FORWARD QUARTET] DRAW POS = " << rndPos2PairC << " with prob = " << probPosition2 << std::endl; // FIXME


	// Define new pairs
	// Pair B
	std::pair<size_t, size_t> posPairB = std::make_pair(pos1PairB, rndPos2PairB);
	if(posPairB.first > posPairB.second) std::swap(posPairB.first, posPairB.second);
	subSpaceUID_B.reset(new SubSpaceUID(posPairB.first, posPairB.second));
	PairProfileInfo PPI_B = ptrLik->getCoevAlignManager()->getPairProfileInfo(posPairB.first, posPairB.second);
	vecProfilesId_B = utilsCRJ.copyProfileIds(PPI_B);
	//std::cout << "[FORWARD QUARTET] CREATED B = " << subSpaceUID_B->getLabelUID() << std::endl; // FIXME

	// Pair C
	std::pair<size_t, size_t> posPairC = std::make_pair(pos1PairC, rndPos2PairC);
	if(posPairC.first > posPairC.second) std::swap(posPairC.first, posPairC.second);
	subSpaceUID_C.reset(new SubSpaceUID(posPairC.first, posPairC.second));
	PairProfileInfo PPI_C = ptrLik->getCoevAlignManager()->getPairProfileInfo(posPairC.first, posPairC.second);
	vecProfilesId_C = utilsCRJ.copyProfileIds(PPI_C);
	//std::cout << "[FORWARD QUARTET] CREATED C = " << subSpaceUID_C->getLabelUID() << std::endl; // FIXME

	/// Define the move probability
	fwMoveProb = probPairDraw * probOrder * probPosition1 * probPosition2;

	/// Define backward move prob
	// Reverse move count kPairs+1 and must select the two same pairs in any order
	double probPairs = 2./((kPairs+1)*kPairs);
	// Must chose this specific scenario for the move
	std::vector< std::pair<size_t, size_t> > possibleScenarios = definePossibleBackwardScenarios(posPairB, posPairC);
	double probScenario = 1./(double)possibleScenarios.size();
	bwMoveProb = probPairs*probScenario;
	//std::cout << "[FORWARD QUARTET] moveProb FW = " << log(fwMoveProb) << "\t BW = " << log(bwMoveProb) << "\t ratio = " << log(bwMoveProb) - log(fwMoveProb)  << std::endl; // FIXME


	/// Draws the parameter values and define the prob
	double profileProb_B;
	{ // Draw the new profile for new pair B
		std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(PPI_B);
		idProfile_B = randProf.first;
		profileProb_B = randProf.second;
	}

	double profileProb_C;
	{ // Draw the new profile for new pair C
		std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(PPI_C);
		idProfile_C = randProf.first;
		profileProb_C = randProf.second;
	}

	/// Probability of drawing profile are not symmetrical
	// For forward move we draw one over newVecProfilesId_B and newVecProfilesId_C
	fwRandomProb = profileProb_B*profileProb_C;
	// For backward move its the probability of drawing IDProfileA
	bwRandomProb = profileProb_A;
	//std::cout << "[FORWARD QUARTET] RandomProb FW = " << log(fwRandomProb) << "\t BW = " << log(bwRandomProb) << "\t ratio = " << log(bwRandomProb) - log(fwRandomProb)  << std::endl; // FIXME

	// Jacobian is 1. because no change of dimension
	jacobian = 1.;

	canApply = !vecProfilesId_B.empty() && !vecProfilesId_C.empty();
	if(!canApply) moveState = IMPOSSIBLE;
}

void QuartetCoevMoveRJ::defineBackwardMove() {

	/// Draw two pairs : First we draw two coev cluster
	const size_t N_COEV = ptrLik->getNActiveCoevLikelihood();
	//std::cout << "-------------------------------------------------------------------------------" << std::endl; // FIXME
	//std::cout << "[BACKWARD QUARTET] N_PAIRS = " << N_COEV << std::endl; // FIXME
	if(N_COEV < 2) {
		moveState = IMPOSSIBLE;
		assert(false && "Impossible move : N_COEV < 1!");
		return;
	}
	// Draw first

	PermCoevLikelihoodInfo* ptrCLI_B = utilsCRJ.drawCoevLik(); // FIXME
	//PermCoevLikelihoodInfo* ptrCLI_B = ptrLik->getActivePermutedCoevLikelihood("326_346_");
	subSpaceUID_B = ptrCLI_B->getUIDSubSpace();
	vecProfilesId_B = ptrCLI_B->getProfilesId();
	MSS_B = currentSample.getMSS(subSpaceUID_B);
	PairProfileInfo PPI_B = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI_B->getPositions().first, ptrCLI_B->getPositions().second);
	double profileProb_B = utilsCRJ.getProfileProbability(MSS_B.getIntValues()[0], PPI_B);
	//std::cout << "[BACKWARD QUARTET] PAIR B = " << ptrCLI_B->toString() << std::endl; // FIXME

	// Draw second
	PermCoevLikelihoodInfo* ptrCLI_C = utilsCRJ.drawCoevLik(); // ptrLik->getActivePermutedCoevLikelihood("326_346_");
	while(ptrCLI_B == ptrCLI_C) { // We want a different coev pair
		ptrCLI_C = utilsCRJ.drawCoevLik();
	}
	subSpaceUID_C = ptrCLI_C->getUIDSubSpace();
	vecProfilesId_C = ptrCLI_C->getProfilesId();
	MSS_C = currentSample.getMSS(subSpaceUID_C);
	PairProfileInfo PPI_C = ptrLik->getCoevAlignManager()->getPairProfileInfo(ptrCLI_C->getPositions().first, ptrCLI_C->getPositions().second);
	double profileProb_C = utilsCRJ.getProfileProbability(MSS_C.getIntValues()[0], PPI_C);
	//std::cout << "[BACKWARD QUARTET] PAIR C = " << ptrCLI_C->toString() << std::endl; // FIXME

	double kPairs = ptrLik->getNActiveCoevLikelihood();
	double probPairDraw = 1./(kPairs-1);
	double probPairsDraw = 2./((kPairs-1)*kPairs);

	/// Get possible scenarios for pairA
	std::vector< std::pair<size_t, size_t> > possibleScenarios = definePossibleBackwardScenarios(ptrCLI_B->getPositions(), ptrCLI_C->getPositions());
	double rndPair = rng->genUniformInt(0, possibleScenarios.size()-1);
	double probScenario = 1./(double)possibleScenarios.size();

	// Define the new pair A
	std::pair<size_t, size_t> posPairA = possibleScenarios[rndPair];
	subSpaceUID_A.reset(new SubSpaceUID(posPairA.first, posPairA.second));
	PairProfileInfo PPI_A = ptrLik->getCoevAlignManager()->getPairProfileInfo(posPairA.first, posPairA.second);
	vecProfilesId_A = utilsCRJ.copyProfileIds(PPI_A);
	//std::cout << "[BACKWARD QUARTET] CREATED A = " << subSpaceUID_A->getLabelUID() << std::endl; // FIXME

	/// Define the move probability
	/// Forward move
	// Which position are back to GTR ?
	size_t keptPosB, keptPosC;
	size_t freedPosB, freedPosC;
	if(ptrCLI_B->getPositions().first != posPairA.first && ptrCLI_B->getPositions().first != posPairA.second) {
		keptPosB = ptrCLI_B->getPositions().second;
		freedPosB = ptrCLI_B->getPositions().first;
	} else {
		keptPosB = ptrCLI_B->getPositions().first;
		freedPosB = ptrCLI_B->getPositions().second;
	}
	if(ptrCLI_C->getPositions().first != posPairA.first && ptrCLI_C->getPositions().first != posPairA.second) {
		keptPosC = ptrCLI_C->getPositions().second;
		freedPosC = ptrCLI_C->getPositions().first;
	} else {
		keptPosC = ptrCLI_C->getPositions().first;
		freedPosC = ptrCLI_C->getPositions().second;
	}

	// Adjust site availability
	boost::dynamic_bitset<> sitesAvailability = ptrLik->getSitesAvailability();
	sitesAvailability[freedPosB] = true;
	sitesAvailability[freedPosC] = true;

	// Order of positions
	size_t pos1 = keptPosB;
	size_t pos2 = keptPosC;
	size_t rndOrder = rng->genUniformInt(0,1);
	double probOrder = 0.5;

	double probPosition1, probPosition2;
	if(rndOrder == 0) {
		UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1, sitesAvailability);
		sitesAvailability[freedPosB] = false;
		UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos2, sitesAvailability);

		probPosition1 = uProb1.probabilities[freedPosB]/uProb1.normalizingConstant;
		probPosition2 = uProb2.probabilities[freedPosC]/uProb2.normalizingConstant;

	} else {
		UtilsCoevRJ::UnormalizedProbabilities uProb1 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos2, sitesAvailability);
		sitesAvailability[freedPosC] = false;
		UtilsCoevRJ::UnormalizedProbabilities uProb2 = utilsCRJ.defineProbabilityBySitesAtSecondPosition(pos1, sitesAvailability);

		probPosition1 = uProb1.probabilities[freedPosC]/uProb1.normalizingConstant;
		probPosition2 = uProb2.probabilities[freedPosB]/uProb2.normalizingConstant;
	}

	fwMoveProb = probPairDraw * probOrder * probPosition1 * probPosition2;

	/// Define backward move prob
	bwMoveProb = probPairsDraw*probScenario;
	//std::cout << "[BACKWARD QUARTET] moveProb FW = " << log(fwMoveProb) << "\t BW = " << log(bwMoveProb) << "\t ratio = " << log(bwMoveProb) - log(fwMoveProb) << std::endl; // FIXME

	/// Draws the parameter values and define the prob
	// Draw the new profile for new pair A
	std::pair<MD::idProfile_t, double> randProf = utilsCRJ.drawProfile(PPI_A);
	idProfile_A = randProf.first;
	double profileProb_A = randProf.second;

	/// Probability of drawing profile are not symmetrical
	// For forward move we draw one over newVecProfilesId_B and newVecProfilesId_C
	fwRandomProb = profileProb_B*profileProb_C;
	// For backward move its the probability of drawing IDProfileA
	bwRandomProb = profileProb_A;
	//std::cout << "[BACKWARD QUARTET] RandomProb FW = " << log(fwRandomProb) << "\t BW = " << log(bwRandomProb) << "\t ratio = " << log(bwRandomProb) - log(fwRandomProb) << std::endl; // FIXME

	// Jacobian is 1. because no change of dimension
	jacobian = 1.;

	canApply = !possibleScenarios.empty();
	if(!canApply) moveState = IMPOSSIBLE;
}


bool QuartetCoevMoveRJ::doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {

	if(!canApply) return false;
	//std::cout << "[QUARTET] APPLY MOVE!" << std::endl; // FIXME

	if(moveDirection == FORWARD) {
		// Disable old pair
		// A
		disabledSSInfo_A = utilsCRJ.disableSubSpace(subSpaceUID_A, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo_A->getParametersId().begin(), disabledSSInfo_A->getParametersId().end());

		// Create two pairs
		// B
		createdSSInfo_B = utilsCRJ.createTempSubSpace(subSpaceUID_B, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo_B, idProfile_B, MSS_A.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo_B->getParametersId().begin(), createdSSInfo_B->getParametersId().end());

		// B
		createdSSInfo_C = utilsCRJ.createTempSubSpace(subSpaceUID_C, model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo_C, idProfile_C, MSS_A.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo_C->getParametersId().begin(), createdSSInfo_C->getParametersId().end());
	} else {
		// Disable old pairs
		// B
		disabledSSInfo_B = utilsCRJ.disableSubSpace(subSpaceUID_B, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo_B->getParametersId().begin(), disabledSSInfo_B->getParametersId().end());

		// C
		disabledSSInfo_C = utilsCRJ.disableSubSpace(subSpaceUID_C, listSSInfo, model, blockDispatcher);
		updatedParameters.insert(updatedParameters.end(), disabledSSInfo_C->getParametersId().begin(), disabledSSInfo_C->getParametersId().end());

		// Create new pair
		// A
		createdSSInfo_A = utilsCRJ.createTempSubSpace(subSpaceUID_A,  model, blockDispatcher);
		utilsCRJ.setValuesMSS(createdSSInfo_A, idProfile_A, MSS_B.getDblValues());
		updatedParameters.insert(updatedParameters.end(), createdSSInfo_A->getParametersId().begin(), createdSSInfo_A->getParametersId().end());

	}
	return false;
}

bool QuartetCoevMoveRJ::doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(canApply);

	//std::cout << "[QUARTET] ACCEPTED MOVE!" << std::endl; // FIXME

	if(moveDirection == FORWARD) {
		// Remove temp likelihood and register into permutedLikelihoods
		utilsCRJ.validateTempSubSpace(subSpaceUID_B);
		utilsCRJ.validateTempSubSpace(subSpaceUID_C);

		// Delete the disabled likelihoods
		utilsCRJ.deleteSubSpaceAfterDisable(subSpaceUID_A);

		// Remove the old ressources
		listSSInfo.remove(disabledSSInfo_A);

		// Register the new ones
		listSSInfo.push_back(createdSSInfo_B);
		listSSInfo.push_back(createdSSInfo_C);
	} else {
		// Remove temp likelihood and register into permutedLikelihoods
		utilsCRJ.validateTempSubSpace(subSpaceUID_A);

		// Delete the disabled likelihoods
		utilsCRJ.deleteSubSpaceAfterDisable(subSpaceUID_B);
		utilsCRJ.deleteSubSpaceAfterDisable(subSpaceUID_C);

		// Remove the old ressources
		listSSInfo.remove(disabledSSInfo_B);
		listSSInfo.remove(disabledSSInfo_C);

		// Register the new ones
		listSSInfo.push_back(createdSSInfo_A);
	}

	countAccepted++;
	//std::cout << "[QuartetCoevMoveRJ] Accepted from : " << oldSubSpaceUID_A->getLabelUID() << " || " << oldSubSpaceUID_B->getLabelUID(); // FIXME DEBUG
	//std::cout << "\t-->\t to : " << newSubSpaceUID_A->getLabelUID() << " || " << newSubSpaceUID_B->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}

bool QuartetCoevMoveRJ::doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher) {
	assert(canApply);

	//std::cout << "[QUARTET] REJECTED MOVE!" << std::endl; // FIXME

	if(moveDirection == FORWARD) {
		// 1) Delete the two subspaces created
		ptrSSInfo_t deletedSSInfo_B = utilsCRJ.deleteTempSubSpace(createdSSInfo_B, model, blockDispatcher);
		ptrSSInfo_t deletedSSInfo_C = utilsCRJ.deleteTempSubSpace(createdSSInfo_C, model, blockDispatcher);

		// 2) Enable old ones
		// A
		ptrSSInfo_t reCreatedSSInfo_A = utilsCRJ.createSubSpaceAfterDisable(subSpaceUID_A, disabledSSInfo_A, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo_A, MSS_A.getIntValues()[0], MSS_A.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo_A->getParametersId().begin(), reCreatedSSInfo_A->getParametersId().end());

		// It has been delete, then re-created
		// The indices of the parameters and block may thus have changed
		// We must signal it to the RJMCMC Handler
		// There we keep  createdSSRessources and deletedSSRessources as such
		listSSInfo.remove(disabledSSInfo_A); 			// Remove the original SSInfo A
		listSSInfo.push_back(reCreatedSSInfo_A); 		// Add the new one A
	} else {
		// 1) Delete the subspace created
		ptrSSInfo_t deletedSSInfo_A = utilsCRJ.deleteTempSubSpace(createdSSInfo_A, model, blockDispatcher);

		// 2) Enable old ones
		// B
		ptrSSInfo_t reCreatedSSInfo_B = utilsCRJ.createSubSpaceAfterDisable(subSpaceUID_B, disabledSSInfo_B, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo_B, MSS_B.getIntValues()[0], MSS_B.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo_B->getParametersId().begin(), reCreatedSSInfo_B->getParametersId().end());

		// C
		ptrSSInfo_t reCreatedSSInfo_C = utilsCRJ.createSubSpaceAfterDisable(subSpaceUID_C, disabledSSInfo_C, model, blockDispatcher);
		utilsCRJ.setValuesMSS(reCreatedSSInfo_C, MSS_C.getIntValues()[0], MSS_C.getDblValues());
		updatedParameters.insert(updatedParameters.end(), reCreatedSSInfo_C->getParametersId().begin(), reCreatedSSInfo_C->getParametersId().end());

		// It has been delete, then re-created
		// The indices of the parameters and block may thus have changed
		// We must signal it to the RJMCMC Handler
		// There we keep  createdSSRessources and deletedSSRessources as such
		listSSInfo.remove(disabledSSInfo_B); 			// Remove the original SSInfo B
		listSSInfo.remove(disabledSSInfo_C); 			// Remove the original SSInfo C
		listSSInfo.push_back(reCreatedSSInfo_B); 		// Add the new one A
		listSSInfo.push_back(reCreatedSSInfo_C); 		// Add the new one A
	}

	countRejected++;
	//std::cout << "[QuartetCoevMoveRJ] Rejected from : " << oldSubSpaceUID_A->getLabelUID() << " || " << oldSubSpaceUID_B->getLabelUID(); // FIXME DEBUG
	//std::cout << "\t-->\t to : " << newSubSpaceUID_A->getLabelUID() << " || " << newSubSpaceUID_B->getLabelUID() << std::endl; // FIXME DEBUG
	return false;
}


bool QuartetCoevMoveRJ::isPossibleBackwardScenario(std::pair<size_t, size_t> &pair) const {
	if(pair.first > pair.second) std::swap(pair.first, pair.second);
	PairProfileInfo PPI = ptrLik->getCoevAlignManager()->getPairProfileInfo(pair.first, pair.second);
	return PPI.uniform || !PPI.profilesId.empty();
}

std::vector< std::pair<size_t, size_t> > QuartetCoevMoveRJ::definePossibleBackwardScenarios(std::pair<size_t, size_t> pairA, std::pair<size_t, size_t> pairB) const {

	std::vector< std::pair<size_t, size_t> > possibleScenarios;

	// Assuming pairs (A,B) and (C,D)
	{ // (A,C)
		std::pair<size_t, size_t> pairAC = std::make_pair(pairA.first, pairB.first);
		if(isPossibleBackwardScenario(pairAC)) {
			possibleScenarios.push_back(pairAC);
		}
	}

	{ // (A,D)
		std::pair<size_t, size_t> pairAD = std::make_pair(pairA.first, pairB.second);
		if(isPossibleBackwardScenario(pairAD)) {
			possibleScenarios.push_back(pairAD);
		}
	}

	{ // (B,C)
		std::pair<size_t, size_t> pairBC = std::make_pair(pairA.second, pairB.first);
		if(isPossibleBackwardScenario(pairBC)) {
			possibleScenarios.push_back(pairBC);
		}
	}

	{ // (B,D)
		std::pair<size_t, size_t> pairBD = std::make_pair(pairA.second, pairB.second);
		if(isPossibleBackwardScenario(pairBD)) {
			possibleScenarios.push_back(pairBD);
		}
	}

	return possibleScenarios;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
