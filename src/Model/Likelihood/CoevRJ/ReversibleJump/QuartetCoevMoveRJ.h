//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file QuartetCoevMoveRJ.h
 *
 * @date Oct 5, 2017
 * @author meyerx
 * @brief
 */
#ifndef QUARTETCOEVMOVERJ_H_
#define QUARTETCOEVMOVERJ_H_

#include "Sampler/ReversibleJump/Moves/RJMove.h"

#include "UtilsCoevRJ.h"
#include "Model/Likelihood/CoevRJ/Base.h"
#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/Moves/RJMove.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class QuartetCoevMoveRJ: public Sampler::RJMCMC::RJMove {
public:
	QuartetCoevMoveRJ(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample, Base* aPtrLik, RNG *aRng);
	~QuartetCoevMoveRJ();

	static std::string getStringStats(std::string &prefix);

private:

	typedef Sampler::Proposal::BlockDispatcher BlockDispatcher_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::listSSInfo_t listSSInfo_t;
	typedef Sampler::RJMCMC::RJSubSpaceInfo::sharedPtr_t ptrSSInfo_t;

	static size_t countAccepted, countRejected;

	bool canApply;

	Base* ptrLik;
	RNG* rng;
	UtilsCoevRJ utilsCRJ;

	size_t idProfile_A, idProfile_B, idProfile_C;
	Sampler::ModelSpecificSample MSS_A, MSS_B, MSS_C;
	std::vector<MD::idProfile_t> vecProfilesId_A, vecProfilesId_B, vecProfilesId_C;
	SubSpaceUID::sharedPtr_t subSpaceUID_A, subSpaceUID_B, subSpaceUID_C;
	ptrSSInfo_t createdSSInfo_A, createdSSInfo_B, createdSSInfo_C, disabledSSInfo_A, disabledSSInfo_B, disabledSSInfo_C;

	// Have to be called first
	bool doApplyMove(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	// Have to be called second
	bool doSignalAccepted(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);
	bool doSignalRejected(listSSInfo_t &listSSInfo, StatisticalModel::Model &model, BlockDispatcher_t &blockDispatcher);

	void defineForwardMove();
	void defineBackwardMove();

	bool isPossibleBackwardScenario(std::pair<size_t, size_t> &pair) const;
	std::vector< std::pair<size_t, size_t> > definePossibleBackwardScenarios(std::pair<size_t, size_t> pairA, std::pair<size_t, size_t> pairB) const;


};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* QUARTETCOEVMOVERJ_H_ */

