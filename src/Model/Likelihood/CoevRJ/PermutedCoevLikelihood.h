//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermutedCoevLikelihood.h
 *
 * @date Aug 15, 2017
 * @author meyerx
 * @brief
 */
#ifndef PERMUTEDCOEVLIKELIHOOD_H_
#define PERMUTEDCOEVLIKELIHOOD_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <string>
#include <vector>

#include "Nodes/IncNodes.h"
#include "UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class PermMatrixCoevNode;
class PermRootCoevNode;

namespace MD = ::MolecularEvolution::Definition;

class PermutedCoevLikelihood {
public:
	PermutedCoevLikelihood(size_t profileSize);
	~PermutedCoevLikelihood();

	size_t getProfileSize() const;

	void setRootDAG(PermRootCoevNode *aRootDAG);
	PermRootCoevNode* getRootDAG();

	void setMatrixCoev(PermMatrixCoevNode *aMatrixCoev);
	PermMatrixCoevNode* getMatrixCoev();

	void setLeafNodes(std::vector<PermLeafCoevNode*>& aLeafNodes);
	std::vector<PermLeafCoevNode*>& getLeafNodes();

	void setTreeHash(long int aTreeHash);
	long int getTreeHash() const;

	bool operator== (const PermutedCoevLikelihood &aPLS) const;

	std::string toString() const;

private:

	const size_t PROFILE_SIZE;
	long int treeHash;

	PermRootCoevNode *rootDAG;
	PermMatrixCoevNode* ptrMatrixCoev;
	std::vector<PermLeafCoevNode*> leafNodes;

	friend class Base;
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PERMUTEDCOEVLIKELIHOOD_H_ */
