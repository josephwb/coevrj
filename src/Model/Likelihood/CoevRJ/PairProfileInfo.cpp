//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PairProfileInfo.cpp
 *
 * @date May 18, 2017
 * @author meyerx
 * @brief
 */
#include "PairProfileInfo.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PairProfileInfo::PairProfileInfo() : uniform(false), score(0.) {
}

PairProfileInfo::~PairProfileInfo() {
}

void PairProfileInfo::write(std::ostream &oFile) const {

	oFile.write(reinterpret_cast<const char*>(&uniform), sizeof(bool));
	oFile.write(reinterpret_cast<const char*>(&score), sizeof(score_t));

	size_t profIdSize = profilesId.size();
	oFile.write(reinterpret_cast<const char*>(&profIdSize), sizeof(size_t));
	oFile.write(reinterpret_cast<const char*>(profilesId.data()), profIdSize*sizeof(MD::idProfile_t));

	size_t profProbSize = profileProb.size();
	oFile.write(reinterpret_cast<const char*>(&profProbSize), sizeof(size_t));
	oFile.write(reinterpret_cast<const char*>(profileProb.data()), profProbSize*sizeof(score_t));

}

void PairProfileInfo::read(std::istream &iFile) {

	iFile.read(reinterpret_cast<char*>(&uniform), sizeof(bool));
	iFile.read(reinterpret_cast<char*>(&score), sizeof(score_t));

	size_t profIdSize;
	iFile.read(reinterpret_cast<char*>(&profIdSize), sizeof(size_t));
	profilesId.resize(profIdSize);
	iFile.read(reinterpret_cast<char*>(profilesId.data()), profIdSize*sizeof(MD::idProfile_t));

	size_t profProbSize;
	iFile.read(reinterpret_cast<char*>(&profProbSize), sizeof(size_t));
	profileProb.resize(profProbSize);
	iFile.read(reinterpret_cast<char*>(profileProb.data()), profProbSize*sizeof(score_t));

}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
