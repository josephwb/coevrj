//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixGTRNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "MatrixGTRNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/../Types.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {


MatrixGTRNode::MatrixGTRNode(DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), frequencies(aFrequencies),
		D(frequencies.size()), scaledV(frequencies.size(), frequencies.size()) {

	ptrMF.reset(new MolecularEvolution::MatrixUtils::GTR_MF());

	coefficients.assign(ptrMF->getNBCoefficients(), 0.);
	stFrequencies.assign(3, 0.);
	ptrMF->setFrequencies(frequencies.getStd());
	matrixScaling = 0;
}


MatrixGTRNode::~MatrixGTRNode() {
}

bool MatrixGTRNode::setThetas(const std::vector<double> &aThetas) {
	for(size_t iT=0; iT < aThetas.size(); ++iT) {
		//std::cout << "[" << iT << "] : " << coefficients[iT] << " - " << aThetas[iT] << "\t"; // FIXME DEBUG
		if(aThetas[iT] != coefficients[iT]) {
			//getchar();
			coefficients[iT] = aThetas[iT];
			BaseNode::updated(); // signal that the node must be recomputed
		}
	}
	//std::cout << std::endl;
	return isUpdated();
}

bool MatrixGTRNode::setFrequencies(const std::vector<double> &aFreq) {
	for(size_t iF=0; iF < aFreq.size(); ++iF) {
		if(aFreq[iF] != stFrequencies[iF]) {
			stFrequencies[iF] = aFreq[iF];
			BaseNode::updated(); // signal that the node must be recomputed
		}
	}
	return isUpdated();
}

const double MatrixGTRNode::getMatrixQScaling() const {
	return matrixScaling;
}

const TI_GTR_EigenEIGMatrix_t& MatrixGTRNode::getScaledEigenVectors() const {
	return scaledV;
}


const TI_EigenEIGVector_t& MatrixGTRNode::getEigenValues() const {
	return D;
}


size_t MatrixGTRNode::serializedSize() const {
	const size_t N = ptrMF->getMatrixDimension();
	return (1+ptrMF->getNBCoefficients()+N+N*N)*sizeof(double);
}


void MatrixGTRNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;
	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		tmp[cnt] = coefficients[i]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		tmp[cnt+i] = *(scaledV.data() + i);
	}
	cnt += scaledV.size();
}


void MatrixGTRNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;

	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		hasChanged = hasChanged || coefficients[i] != tmp[cnt];
		coefficients[i] = tmp[cnt]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		*(scaledV.data() + i) = tmp[cnt+i];
	}
	cnt += scaledV.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}
}


void MatrixGTRNode::doProcessing() {

	//std::cout << "Process matrix" << std::endl;

	// shortcut to sqrt frequencies
	const Eigen::VectorXd &sqrtPi = frequencies.getSqrtEigen();
	const Eigen::VectorXd &sqrtInvPi = frequencies.getSqrtInvEigen();

	// Fill the matrix given the new coefficient
	ptrMF->setFrequencies(frequencies.getStd());
	ptrMF->fillMatrix(coefficients);
	matrixScaling = ptrMF->getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = ptrMF->getMatrixDimension();
	Eigen::Map< Eigen::MatrixXd > mappedMat(ptrMF->getMatrix().data(), N, N);
	Q = mappedMat;

	/*Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "]");
	for(size_t iP=0; iP<coefficients.size(); ++iP) std::cout << coefficients[iP] << "\t";
	std::cout << std::endl;
	std::cout << ptrMF->getScalingFactor() << std::endl << Q.format(OctaveFmt) << std::endl << (Q/ptrMF->getScalingFactor()).format(OctaveFmt) << std::endl; getchar(); // FIXME*/
	// Compute
	/*
	 * Q = S * PI
	 * S = Q*PI^(-1)
	 *
	 * A = PI^(1/2)*S*PI^(1/2) = PI^(1/2)*Q*PI^(-1)*PI^(1/2) = PI^(1/2)*Q*PI^(-1/2)
	 */
	A = sqrtPi.asDiagonal()*Q*sqrtInvPi.asDiagonal();

	// If Nucleotides or all symbol are valid
	if(N == 4 || N == frequencies.getNValidFreq()) {
		// Apply eigen decomposition
		Eigen::SelfAdjointEigenSolver<TI_GTR_EigenEIGMatrix_t> es;
		es.compute(A);
		// Apply square of freq and convert to good data
		D = es.eigenvalues().eval();
		scaledV = sqrtPi.asDiagonal() * es.eigenvectors().eval();
	} else { // Other symbols or some symbols are invalids
		comprEIG();
	}
}


bool MatrixGTRNode::processSignal(BaseNode* aChild) {
	return true;
}



void MatrixGTRNode::comprEIG() {
	size_t nValidF = frequencies.getNValidFreq();
	const std::vector<bool> &validF = frequencies.getValidFreq();
	const Eigen::VectorXd &sqrtPi = frequencies.getSqrtEigen();

	Eigen::MatrixXd compactA(nValidF, nValidF);

	compactA.setZero();

	// Compress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					compactA(jR, jC) = A(iR, iC);
					jR++;
				} // end if
			} // end for
			jC++;
		} // end if
	} // end for

	// Do EIG
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es;
	es.compute(compactA);

	// Decompress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			D(iC) = es.eigenvalues()(jC);
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					scaledV(iR, iC) = sqrtPi(iR)*es.eigenvectors()(jR, jC);
					jR++;
				} else {
					scaledV(iR, iC) = 0.;
				} // end if
			} // end for
			jC++;
		} else {
			D(iC) = 0.;
			for(size_t iR=0; iR<(size_t)A.rows(); ++iR) {
				scaledV(iR, iC) = iC == iR ? sqrtPi(iR) : 0.0;
			}
		} // end if
	} // end for

}

std::string MatrixGTRNode::toString() const {
	std::stringstream ss;
	ss << "[MatrixGTRNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
