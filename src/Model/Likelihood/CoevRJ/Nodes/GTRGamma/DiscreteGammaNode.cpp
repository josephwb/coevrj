//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DiscreteGammaNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "DiscreteGammaNode.h"

#include <assert.h>
#include <boost/math/distributions.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/distributions/chi_squared.hpp>


namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

DiscreteGammaNode::DiscreteGammaNode(const size_t aNGamma, gammaEstimation_t aGammaEstimation) :
		DAG::BaseNode(), GAMMA_ESTIMATION(aGammaEstimation), N_GAMMA(aNGamma) {
	alpha = 0.;
	rates.resize(N_GAMMA);
}

DiscreteGammaNode::~DiscreteGammaNode() {
}

double DiscreteGammaNode::getAlpha() const {
	return alpha;
}

bool DiscreteGammaNode::setAlpha(double aAlpha) {
	bool isUpdated = false;
	if(aAlpha != alpha) {
		alpha = aAlpha;
		DAG::BaseNode::updated();
		isUpdated = true;
	}
	return isUpdated;
}

double DiscreteGammaNode::getRate(size_t idGamma) {
	assert(idGamma < N_GAMMA);
	return rates[idGamma];
}
std::vector<double>& DiscreteGammaNode::getRates() {
	return rates;
}

void DiscreteGammaNode::doProcessing() {

	if(N_GAMMA==1) { // No Gamma case
		rates[0] = 1.;
		return;
	}

	// See Yang, Z. 1994. “Maximum Likelihood Phylogenetic Estimation from DNA Sequences with Variable Rates over Sites: Approximate Methods.”
	// Journal of Molecular Evolution 39 (3): 306–14.
	// Computed using the lower incomplete gamma function
	if(GAMMA_ESTIMATION == MEAN_GAMMA) {
		double beta = alpha;
		boost::math::chi_squared_distribution<double> chi2Dist(2.*alpha);

		// Compute cutting point (Eq. 9) (could use gamma quantile instead)
		std::vector<double> cPoints(1, 0.);
		for(size_t iG=1; iG<N_GAMMA; ++iG) {
			double cPoint = boost::math::quantile(chi2Dist, (double)iG/(double)N_GAMMA) / (2.*beta);
			cPoints.push_back(cPoint);
		}

		// Compute all lower incomplete gamma functions required in Eq. 10
		double alphaP1 = alpha + 1;
		std::vector<double> lowerIncGamma(1, 0.);
		for(size_t iL=1; iL<cPoints.size(); ++iL) {
			double liGamma = boost::math::gamma_p(alphaP1, cPoints[iL]*beta);
			lowerIncGamma.push_back(liGamma);
		}
		lowerIncGamma.push_back(1.);

		// Equation 10.
		//std::cout << "Nb rate = " << N_GAMMA << " with alpha = " << alpha << std::endl;
		for(size_t iG=0; iG<N_GAMMA; ++iG) {
			// alpha = beta, thus alpha/beta = 1.0 || k=iG
			rates[iG] = ((double)N_GAMMA)*(lowerIncGamma[iG+1]-lowerIncGamma[iG]);
			//std::cout << "Rate [" << iG << "] = " << rates[iG] << std::endl;
		}
	} else { // GAMMA_ESTIMATION == MEAN_GAMMA
		boost::math::gamma_distribution<> dist(alpha, 1./alpha);
		double sum = 0.;
		for(size_t iG=0; iG<N_GAMMA; ++iG) {
			double percentile = (1.+2.*(double)iG)/(2.*(double)N_GAMMA);
			rates[iG] = boost::math::quantile(dist, percentile);
			sum += rates[iG];
		}
		sum = (double)N_GAMMA/sum;
		for(size_t iG=0; iG<N_GAMMA; ++iG) {
			rates[iG] *= sum;
		}
	}
}

bool DiscreteGammaNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

std::string DiscreteGammaNode::toString() const {
	std::stringstream ss;
	ss << "[DiscreteGammaNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
