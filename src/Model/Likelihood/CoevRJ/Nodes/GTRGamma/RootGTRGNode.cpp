//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootGTRGNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "RootGTRGNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

RootGTRGNode::RootGTRGNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
				   DL_Utils::Frequencies &aFrequencies) :
	 			 ParentGTRGNode(aIDGamma, aSitePos, aFrequencies) {


	nrm.resize(H.cols());
	H.resize(4,0);
}

RootGTRGNode::~RootGTRGNode() {
}

const TI_EigenVector_t& RootGTRGNode::getLikelihood() const {
	return likelihood;
}

void RootGTRGNode::doProcessing() {

	// FIXME std::cout << "ROOT [ " << getId() << "] :: H(" << H.rows() << "x" << H.cols() << ") :: G(" << G.rows() << "x" << G.cols() << ")" << std::endl;
	TI_GTR_EigenMatrix_t G;
	nrm.setZero();

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();
			first = false;
			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				nrm += children[iE]->getNorm();
			}
		}
	}


#if TI_COEV_USE_FLOAT
	likelihood = frequencies.getEigenFlt().transpose() * G;
#else
	//likelihood = G.transpose()*frequencies.getEigen();
	likelihood = frequencies.getEigen().transpose() * G;
#endif

	/*std::cout << "Root : \t NodeId=" << getId() << std::endl;
	std::cout << "G:" << std::endl << G.col(0) << std::endl;
	std::cout << "Freq:" << std::endl << frequencies.getEigen() << std::endl;
	std::cout << "lik:" << std::endl << likelihood(0) << std::endl;
	std::cout << "-----------------------------------------------" << std::endl;
	getchar();*/

	//FIXME std::cout << "ROOT [ " << getId() << "] done " << std::endl;

}

bool RootGTRGNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootGTRGNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVGTRGNode)){
		CPVGTRGNode* cpvNode = dynamic_cast<CPVGTRGNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(LeafGTRGNode)){
		LeafGTRGNode* leafNode = dynamic_cast<LeafGTRGNode*>(aChild);
		addChild(leafNode);
	}
}

void RootGTRGNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CPVGTRGNode)){
		CPVGTRGNode* cpvNode = dynamic_cast<CPVGTRGNode*>(aChild);
		removeChild(cpvNode);
	} else if(childtype == typeid(LeafGTRGNode)){
		LeafGTRGNode* leafNode = dynamic_cast<LeafGTRGNode*>(aChild);
		removeChild(leafNode);
	}
}

std::string RootGTRGNode::toString() const {
	std::stringstream ss;
	ss << "[RootGTRGNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
