//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootGTRGNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef ROOTGTRGNODE_COEVRJ_H_
#define ROOTGTRGNODE_COEVRJ_H_

#include <stddef.h>

#include "Model/Likelihood/CoevRJ/Nodes/GTRGamma/../Types.h"
#include "CPVGTRGNode.h"
#include "LeafGTRGNode.h"
#include "ParentGTRGNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class RootGTRGNode: public ParentGTRGNode {
public:
	RootGTRGNode(const size_t aIDGamma, const std::vector<size_t> &aSitePos,
			 DL_Utils::Frequencies &aFrequencies);
	~RootGTRGNode();

	const TI_EigenVector_t& getLikelihood() const;

	std::string toString() const;

private:

	TI_EigenVector_t likelihood;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
	void doRemoveChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ROOTGTRGNODE_COEVRJ_H_ */
