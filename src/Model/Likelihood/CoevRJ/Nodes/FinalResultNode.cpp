//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalResultNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "FinalResultNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

FinalResultNode::FinalResultNode() : DAG::BaseNode(), DAG::LikelihoodNode() {
}

FinalResultNode::~FinalResultNode() {
}


void FinalResultNode::doProcessing() {

	// LOG_LIKELIHOOD
	likelihood = 0.;

	//std::cout << "Likelihoods :\t";

	// GTRG likelihoods
	for(size_t iC=0; iC<GTRGNodes.size(); ++iC) {
		likelihood += GTRGNodes[iC]->getLikelihood(); // Log likelihood already
		//std::cout << GTRGNodes[iC]->getLikelihood() << "\t";
	}
	//std::cout << "|\t";

	// Coev likelihoods
	for(size_t iC=0; iC<tmpCoevNodes.size(); ++iC) {
		if(!tmpCoevNodes[iC]->isActive()) continue;
		likelihood += tmpCoevNodes[iC]->getLogLikelihood(); 	// Log likelihood already
		//std::cout << tmpCoevNodes[iC]->getLogLikelihood() << "\t";
	}
	//std::cout << "|\t";

	// Permuted Coev likelihoods
	for(size_t iC=0; iC<permCoevNodes.size(); ++iC) {
		likelihood += permCoevNodes[iC]->getLogLikelihood(); 	// Log likelihood already
		//std::cout << permCoevNodes[iC]->getLogLikelihood() << "\t";
	}
	//std::cout << "|\t Final sum = " << likelihood << std::endl;
}

bool FinalResultNode::processSignal(DAG::BaseNode* aChild) {

	return true;
}

void FinalResultNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombineSiteGTRGNode)){
		CombineSiteGTRGNode *node = dynamic_cast<CombineSiteGTRGNode*>(aChild);
		GTRGNodes.push_back(node);
	} else if(childtype == typeid(RootCoevNode)){
		RootCoevNode *node = dynamic_cast<RootCoevNode*>(aChild);
		tmpCoevNodes.push_back(node);
	} else if(childtype == typeid(PermRootCoevNode)){
		PermRootCoevNode *node = dynamic_cast<PermRootCoevNode*>(aChild);
		permCoevNodes.push_back(node);
	}
}

void FinalResultNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombineSiteGTRGNode)){
		CombineSiteGTRGNode* csNode = dynamic_cast<CombineSiteGTRGNode*>(aChild);
		typedef std::vector<CombineSiteGTRGNode*>::iterator itCSGN_t;
		itCSGN_t it = std::find(GTRGNodes.begin(), GTRGNodes.end(), csNode);
		if (it != GTRGNodes.end()) {
			GTRGNodes.erase(it);
		}
	} else if(childtype == typeid(RootCoevNode)){
		RootCoevNode* rcNode = dynamic_cast<RootCoevNode*>(aChild);
		typedef std::vector<RootCoevNode*>::iterator itRCN_t;
		itRCN_t it = std::find(tmpCoevNodes.begin(), tmpCoevNodes.end(), rcNode);
		if (it != tmpCoevNodes.end()) {
			tmpCoevNodes.erase(it);
		}
	} else if(childtype == typeid(PermRootCoevNode)){
		PermRootCoevNode* prcNode = dynamic_cast<PermRootCoevNode*>(aChild);
		typedef std::vector<PermRootCoevNode*>::iterator itPRCN_t;
		itPRCN_t it = std::find(permCoevNodes.begin(), permCoevNodes.end(), prcNode);
		if (it != permCoevNodes.end()) {
			permCoevNodes.erase(it);
		}
	}
}

std::string FinalResultNode::toString() const {
	std::stringstream ss;
	ss << "[FinalResultNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
