//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermParentCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermParentCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermEdgeCoevNode.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PermParentCoevNode::PermParentCoevNode(DL_Utils::Frequencies &aFrequencies) :
		PermEdgeCoevNode(aFrequencies) {

}

PermParentCoevNode::~PermParentCoevNode() {
}

std::string PermParentCoevNode::toString() const {
	std::stringstream ss;
	ss << BaseNode::toString();
	/*ss << "Children : ";
	for(size_t i=0; i<children.size(); ++i) {
		ss << children[i]->getId() << ", ";
	}
	ss << std::endl;
	ss << "CPVs : " << std::endl;
	for(size_t i=0; i<vecCPV.size(); i++) {
		ss << vecCPV[i].toString() << std::endl;
	}

	ss << "Blocks : " << std::endl;
	for(size_t i=0; i<hChildrenBlock.size(); i++) {
		ss << hChildrenBlock[i]->getId() << std::endl;
	}

	ss << "hChildrenPos : " << std::endl;
	for(size_t i=0; i<hChildrenPos.size(); i++) {
		ss << hChildrenPos[i].size() << std::endl;
	}

	ss << "H size : " << H.size() << " :: G size : " << G.size() << std::endl;*/
	return ss.str();
}

void PermParentCoevNode::addChild(PermEdgeCoevNode* childrenNode) {
	children.push_back(childrenNode);
}

bool PermParentCoevNode::removeChild(PermEdgeCoevNode* childNode) {
	std::vector<PermEdgeCoevNode*>::iterator pos = std::find(children.begin(), children.end(), childNode);
	if (pos != children.end()) {
		children.erase(pos);
		return true;
	}
	return false;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
