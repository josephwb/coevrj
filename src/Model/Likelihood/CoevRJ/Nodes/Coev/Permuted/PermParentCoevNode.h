//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermParentCoevNode.h
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#ifndef PERM_PARENTCOEVNODE_COEVRJ_H_
#define PERM_PARENTCOEVNODE_COEVRJ_H_

#include "PermEdgeCoevNode.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class PermParentCoevNode : public PermEdgeCoevNode {
public:
	PermParentCoevNode(DL_Utils::Frequencies &aFrequencies);
	virtual ~PermParentCoevNode();

	std::string toString() const;

protected: // Variables

	std::vector<PermEdgeCoevNode*> children;

protected: // Methods
	void addChild(PermEdgeCoevNode* childNode);
	bool removeChild(PermEdgeCoevNode* childNode);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PERM_PARENTCOEVNODE_COEVRJ_H_ */
