//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermEdgeCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermEdgeCoevNode.h"

#include <stddef.h>

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class PermBranchMatrixCoevNode;

const PermEdgeCoevNode::scaling_t PermEdgeCoevNode::SCALING_TYPE = NONE;
const float PermEdgeCoevNode::SCALING_THRESHOLD = 1.; // Scale all with 1.


PermEdgeCoevNode::PermEdgeCoevNode(DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), frequencies(aFrequencies)  {

	bmNode = NULL;
	init();
}

PermEdgeCoevNode::~PermEdgeCoevNode() {
}


void PermEdgeCoevNode::init() {
	// Create vectors
	H.resize(frequencies.size(),0);

	if(SCALING_TYPE == LOG) {
		nrm.resize(0);//nrm.resize(sitePos.size());
	}
}


const TI_Coev_EigenMatrix_t& PermEdgeCoevNode::getH() const {
	return H;
}

const TI_EigenVector_t& PermEdgeCoevNode::getNorm() const {
	return nrm;
}

PermBranchMatrixCoevNode* PermEdgeCoevNode::getBMCNode() {
	return bmNode;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
