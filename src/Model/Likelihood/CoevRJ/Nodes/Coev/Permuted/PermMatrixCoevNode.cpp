//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermMatrixCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermMatrixCoevNode.h"

#include <assert.h>

#include "Parallel/Parallel.h"
#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {


PermMatrixCoevNode::PermMatrixCoevNode(size_t aNbPairsInProfile, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), NB_PAIRS_IN_PROFILE(aNbPairsInProfile), frequencies(aFrequencies), D(frequencies.size()) {

	BaseNode::initAccTime(1e-4);

	//isInitialized = false;
	notInvertible = false;

	/*ptrMF.reset(new MolecularEvolution::MatrixUtils::COEV_MF(aProfile));
	ptrMF->setFrequencies(frequencies.getStd());*/
	matrixScaling = 0;

	coefficients.assign(MolecularEvolution::MatrixUtils::COEV_MF::COEV_NB_COEFF, 0.);
	//std::cout << "[PermMatrixCoevNode = " << getId() << " ] Created! " << std::endl;

	//std::cout << "NB PAIRS IN PROFILE : " << NB_PAIRS_IN_PROFILE << std::endl;
	std::vector< std::string > profile;
	for(size_t iP=0; iP<NB_PAIRS_IN_PROFILE; ++iP) {
		std::string strProf;
		strProf = MolecularEvolution::Definition::getNucleotidesPairs()->getNuclPairString(iP*5); // Will return AA, CC, TT, GG
		profile.push_back(strProf);
	}

	ptrMF.reset(new MolecularEvolution::MatrixUtils::COEV_MF(profile));
	ptrMF->setFrequencies(frequencies.getStd());
}


PermMatrixCoevNode::~PermMatrixCoevNode() {
}

bool PermMatrixCoevNode::setParameters(double r1, double r2, double s, double d) {
	namespace MU = MolecularEvolution::MatrixUtils;

	if((coefficients[MU::COEV_MF::R1_ID] != r1) ||
	   (coefficients[MU::COEV_MF::R2_ID] != r2) ||
	   (coefficients[MU::COEV_MF::S_ID] != s) 	||
	   (coefficients[MU::COEV_MF::D_ID] != d)) 	{

		coefficients[MU::COEV_MF::R1_ID] = r1;
		coefficients[MU::COEV_MF::R2_ID] = r2;
		coefficients[MU::COEV_MF::S_ID] = s;
		coefficients[MU::COEV_MF::D_ID] = d;

		BaseNode::updated(); // signal that the node must be recomputed
	}

	return isUpdated();
}

const TI_EigenEIGVector_t& PermMatrixCoevNode::getStationaryFrequency() const {
	return stationaryFreq;
}

bool PermMatrixCoevNode::isInvertible() const {
	return !notInvertible;
}

const double PermMatrixCoevNode::getMatrixQScaling() const {
	return matrixScaling;
}

const TI_Coev_EigenEIGMatrix_t& PermMatrixCoevNode::getEigenVectors() const {
	return V;
}


const TI_Coev_EigenEIGMatrix_t& PermMatrixCoevNode::getInverseEigenVectors() const {
	return invV;
}


const TI_EigenEIGVector_t& PermMatrixCoevNode::getEigenValues() const {
	return D;
}

const Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t>& PermMatrixCoevNode::getEigenVectorLU() const {
	//if(!isInitialized) std::cout << "[PermMatrixCoevNode = " << getId() << " ] Getter for LU => Not initialiazed..." << std::endl;
	return lu;
}

size_t PermMatrixCoevNode::getNbPairsInProfile() const {
	return NB_PAIRS_IN_PROFILE;
}


size_t PermMatrixCoevNode::serializedSize() const {
	const size_t N = ptrMF->getMatrixDimension();
	return (1+ptrMF->getNBCoefficients()+N+2*N*N)*sizeof(double);
}


void PermMatrixCoevNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;
	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		tmp[cnt] = coefficients[i]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)V.size(); ++i) {
		tmp[cnt+i] = *(V.data() + i);
	}
	cnt += V.size();

}


void PermMatrixCoevNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;

	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		hasChanged = hasChanged || coefficients[i] != tmp[cnt];
		coefficients[i] = tmp[cnt]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)V.size(); ++i) {
		*(V.data() + i) = tmp[cnt+i];
	}
	cnt += V.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}
}


void PermMatrixCoevNode::doProcessing() {
	namespace MU = MolecularEvolution::MatrixUtils;

	// Fill the matrix given the new coefficient
	assert(ptrMF != NULL);
	ptrMF->fillMatrix(coefficients);
	matrixScaling = ptrMF->getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = ptrMF->getMatrixDimension();
	Eigen::Map< Eigen::MatrixXd > mappedMat(ptrMF->getMatrix().data(), N, N);
	Q = mappedMat;

	/*std::cout << "Matrix perm:" << std::endl;
	Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "]");
	for(size_t iP=0; iP<coefficients.size(); ++iP) std::cout << coefficients[iP] << "\t";
	std::cout << std::endl;
	std::cout << ptrMF->getScalingFactor() << std::endl << Q.format(OctaveFmt) << std::endl << (Q/ptrMF->getScalingFactor()).format(OctaveFmt) << std::endl;
	getchar(); */

	// Compute Eigenvectors and their inverse
	Eigen::EigenSolver<TI_Coev_EigenEIGMatrix_t> es(Q, true);

	if(es.info() != Eigen::Success) { // if Eigen Solving as failed
		notInvertible = true;

		V.setZero();
		D.setZero(frequencies.size()); // Trick to get a zero likelihood

		//std::stringstream ss;
		//ss << NB_PAIRS_IN_PROFILE << " : Failed to apply the eigen decomposition.";
		//Parallel::mpiMgr().print(ss.str());

	} else {

		D = es.eigenvalues().real().eval();
		V = es.eigenvectors().real().eval();

		//Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t> lu(V);
		lu.compute(V);
		if(!lu.isInvertible()){
			notInvertible = true;

			V.setZero();
			D.setZero(); // Trick to get a zero likelihood

			//std::stringstream ss;
			//ss << NB_PAIRS_IN_PROFILE << " : Failed to apply the invert the matrix.";
			//Parallel::mpiMgr().print(ss.str());

		} else {
			notInvertible = false;
			invV = V.inverse();

			if(coefficients[MU::COEV_MF::D_ID] > 0. && coefficients[MU::COEV_MF::S_ID] > 0.) {

				double ds = coefficients[MU::COEV_MF::D_ID]/coefficients[MU::COEV_MF::S_ID];
				double sd = 1./ds;
				double freqInProf = 1./((double)NB_PAIRS_IN_PROFILE+(16.-NB_PAIRS_IN_PROFILE)*sd);
				double freqOutProf = 1./((16.-(double)NB_PAIRS_IN_PROFILE)+NB_PAIRS_IN_PROFILE*ds);

				stationaryFreq = freqOutProf*Eigen::VectorXd::Ones(16);
				for(size_t i=0; i<NB_PAIRS_IN_PROFILE; ++i) {
					stationaryFreq(i*5) = freqInProf;
				}
			}
		}
	}
}


bool PermMatrixCoevNode::processSignal(BaseNode* aChild) {
	return true;
}

std::string PermMatrixCoevNode::toString() const {
	std::stringstream ss;
	ss << "[PermMatrixCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
