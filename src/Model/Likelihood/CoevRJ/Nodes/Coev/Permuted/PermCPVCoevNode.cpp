//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermCPVCoevNode.cpp
 *
 * @date Aug 11, 2017
 * @author meyerx
 * @brief
 */
#include "PermCPVCoevNode.h"

#include "Model/Likelihood/CoevRJ/Nodes/Types.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermBranchMatrixCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermEdgeCoevNode.h"
#include "Model/Likelihood/CoevRJ/Nodes/Coev/Permuted/PermParentCoevNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

PermCPVCoevNode::PermCPVCoevNode(DL_Utils::Frequencies &aFrequencies) :
				PermParentCoevNode(aFrequencies) {

	BaseNode::initAccTime(1e-6);
}

PermCPVCoevNode::~PermCPVCoevNode() {
}

void PermCPVCoevNode::doProcessing() {

	nrm.resize(0);

	TI_Coev_EigenMatrix_t G;

	if(children.front()->getH().cols() == 0) {
		H = children.front()->getH();
		return;
	}

	const TI_Coev_EigenSquareMatrix_t &Z = bmNode->getZ();

	// Prepare G
	// Check block at first
	for(size_t iE=0; iE<children.size(); ++iE) {
		if(iE == 0) {
			G = children[iE]->getH();

			if(SCALING_TYPE == LOG  && children[iE]->getNorm().size() > 0) {
				nrm = children[iE]->getNorm();
			}
		} else {
			G = G.cwiseProduct(children[iE]->getH());
			if (SCALING_TYPE == LOG && children[iE]->getNorm().size() > 0) {
				if(nrm.size() == 0) {
					nrm = children[iE]->getNorm();
				} else {
					nrm += children[iE]->getNorm();
				}
			}
		}
	}

	// Multiplication
	H = Z*G;

	if(SCALING_TYPE == LOG) {
		TI_EigenVector_t vecMax = H.colwise().maxCoeff();
		if(SCALING_THRESHOLD >= 1. || vecMax.minCoeff() < SCALING_THRESHOLD) {
			if(nrm.size() == 0) {
				nrm = TI_EigenVector_t::Zero(vecMax.size());
			}
			for(size_t iC=0; iC<(size_t)vecMax.size(); ++iC){
				if(vecMax(iC) > 0 && (SCALING_THRESHOLD >= 1. || vecMax(iC) < SCALING_THRESHOLD)){
					int exponent;
					frexp(vecMax(iC), &exponent);
					nrm(iC) += exponent*LOG_OF_2;
					H.col(iC) /= ldexp((float)1., exponent);
				}
			}
		}
	}

	/*Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", ";");
	std::cout << "[PERM] " << " >> H : " << std::endl << H.format(CommaInitFmt) << std::endl;*/


}

bool PermCPVCoevNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void PermCPVCoevNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermBranchMatrixCoevNode)){
		bmNode = dynamic_cast<PermBranchMatrixCoevNode*>(aChild);
	} else if(childtype == typeid(PermCPVCoevNode)){
		PermCPVCoevNode* cpvNode = dynamic_cast<PermCPVCoevNode*>(aChild);
		addChild(cpvNode);
	} else if(childtype == typeid(PermLeafCoevNode)){
		PermLeafCoevNode* leafNode = dynamic_cast<PermLeafCoevNode*>(aChild);
		addChild(leafNode);
	}
}

void PermCPVCoevNode::doRemoveChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(PermCPVCoevNode)){
		PermCPVCoevNode* cpvNode = dynamic_cast<PermCPVCoevNode*>(aChild);
		bool isRemoved = removeChild(cpvNode);
		assert(isRemoved);
	} else if(childtype == typeid(PermLeafCoevNode)){
		PermLeafCoevNode* leafNode = dynamic_cast<PermLeafCoevNode*>(aChild);
		bool isRemoved = removeChild(leafNode);
		assert(isRemoved);
	}
}

std::string PermCPVCoevNode::toString() const {
	std::stringstream ss;
	ss << "[PermCPVCoevNode] " << BaseNode::toString();
	return ss.str();
}


} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
