//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixCoevNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "MatrixCoevNode.h"

#include <assert.h>

#include "Model/Likelihood/CoevRJ/Nodes/Coev/../Types.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {


MatrixCoevNode::MatrixCoevNode(DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), frequencies(aFrequencies), D(frequencies.size()),  stationaryFreq(frequencies.size()) {

	BaseNode::initAccTime(1e-4);

	//isInitialized = false;
	notInvertible = false;

	/*ptrMF.reset(new MolecularEvolution::MatrixUtils::COEV_MF(aProfile));
	ptrMF->setFrequencies(frequencies.getStd());*/
	matrixScaling = 0;

	coefficients.assign(MolecularEvolution::MatrixUtils::COEV_MF::COEV_NB_COEFF, 0.);
	//std::cout << "[MatrixCoevNode = " << getId() << " ] Created! " << std::endl;
}


MatrixCoevNode::~MatrixCoevNode() {
}

bool MatrixCoevNode::setParameters(double r1, double r2, double s, double d) {
	namespace MU = MolecularEvolution::MatrixUtils;

	//std::cout << std::scientific << r1 << "<-->" << std::scientific  << coefficients[MU::COEV_MF::R1_ID] << "\t"; // FIXME DEBUG REM
	//std::cout << std::scientific  << r2 << "<-->" << std::scientific  << coefficients[MU::COEV_MF::R2_ID] << "\t"; // FIXME DEBUG REM
	//std::cout << std::scientific  << s << "<-->" << std::scientific  << coefficients[MU::COEV_MF::S_ID] << "\t"; // FIXME DEBUG REM
	//std::cout << std::scientific  << d << "<-->" << std::scientific  << coefficients[MU::COEV_MF::D_ID] << "\t"; // FIXME DEBUG REM

	//r1  = 1.; r2 = 1.; s = 10; d=50; // FIXME REMOVE


	if(coefficients[MU::COEV_MF::R1_ID] != r1 ||
	   coefficients[MU::COEV_MF::R2_ID] != r2 ||
	   coefficients[MU::COEV_MF::S_ID] != s ||
	   coefficients[MU::COEV_MF::D_ID] != d ) {

		coefficients[MU::COEV_MF::R1_ID] = r1;
		coefficients[MU::COEV_MF::R2_ID] = r2;
		coefficients[MU::COEV_MF::S_ID] = s;
		coefficients[MU::COEV_MF::D_ID] = d;

		BaseNode::updated(); // signal that the node must be recomputed
	}

	//std::cout << "[MatrixCoevNode = " << getId() << " ] " << (isUpdated() ? "UPDATED" : "NOT UPDATED") << std::endl;
	//std::cout << (isUpdated() ? "UPDATED" : "NOT UPDATED") << std::endl; // FIXME DEBUG REM

	return isUpdated();
}

bool MatrixCoevNode::setProfile(const std::vector<std::string> &aProfile) {
	// Check if the profile is the same
	bool isSame = aProfile.size() == profile.size();
	if(isSame) {
		for(size_t iP=0; iP<profile.size(); ++iP) {
			isSame = isSame && (profile[iP] == aProfile[iP]);
		}
	}

	if(!isSame) { // If it is not, update it
		profile = aProfile;
		ptrMF.reset(new MolecularEvolution::MatrixUtils::COEV_MF(profile));
		ptrMF->setFrequencies(frequencies.getStd());
		BaseNode::updated(); // signal that the node must be recomputed
	}

	return isUpdated();
}

bool MatrixCoevNode::isInvertible() const {
	return !notInvertible;
}

const double MatrixCoevNode::getMatrixQScaling() const {
	return matrixScaling;
}

const TI_Coev_EigenEIGMatrix_t& MatrixCoevNode::getEigenVectors() const {
	return V;
}

const TI_EigenEIGVector_t& MatrixCoevNode::getEigenValues() const {
	return D;
}

const Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t>& MatrixCoevNode::getEigenVectorLU() const {
	//if(!isInitialized) std::cout << "[MatrixCoevNode = " << getId() << " ] Getter for LU => Not initialiazed..." << std::endl;
	return lu;
}

const TI_EigenEIGVector_t& MatrixCoevNode::getStationaryFrequency() const {
	return stationaryFreq;
}

size_t MatrixCoevNode::serializedSize() const {
	const size_t N = ptrMF->getMatrixDimension();
	return (1+ptrMF->getNBCoefficients()+N+2*N*N)*sizeof(double);
}


void MatrixCoevNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;
	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		tmp[cnt] = coefficients[i]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)V.size(); ++i) {
		tmp[cnt+i] = *(V.data() + i);
	}
	cnt += V.size();

}


void MatrixCoevNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;

	for(size_t i=0; i<ptrMF->getNBCoefficients(); ++i) {
		hasChanged = hasChanged || coefficients[i] != tmp[cnt];
		coefficients[i] = tmp[cnt]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)V.size(); ++i) {
		*(V.data() + i) = tmp[cnt+i];
	}
	cnt += V.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}
}


void MatrixCoevNode::doProcessing() {
	namespace MU = MolecularEvolution::MatrixUtils;
	namespace MD = ::MolecularEvolution::Definition;


	//std::cout << "[MatrixCoevNode = " << getId() << " ] " << " is being computed." << std::endl;

	// shortcut to sqrt frequencies
	//isInitialized = true;

	// Fill the matrix given the new coefficient
	assert(ptrMF != NULL);
	ptrMF->fillMatrix(coefficients);
	matrixScaling = ptrMF->getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = ptrMF->getMatrixDimension();
	Eigen::Map< Eigen::MatrixXd > mappedMat(ptrMF->getMatrix().data(), N, N);
	Q = mappedMat;

	/*Eigen::IOFormat OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "]");
	for(size_t iP=0; iP<coefficients.size(); ++iP) std::cout << coefficients[iP] << "\t";
	std::cout << std::endl;
	std::cout << ptrMF->getScalingFactor() << std::endl << Q.format(OctaveFmt) << std::endl << (Q/ptrMF->getScalingFactor()).format(OctaveFmt) << std::endl; getchar(); // FIXME*/

	// Compute Eigenvectors and their inverse
	Eigen::EigenSolver<TI_Coev_EigenEIGMatrix_t> es(Q, true);

	if(es.info() != Eigen::Success) { // if Eigen Solving as failed
		notInvertible = true;

		V.setZero();
		D.setZero(frequencies.size()); // Trick to get a zero likelihood
	} else {
		D = es.eigenvalues().real().eval();
		V = es.eigenvectors().real().eval();
		/// [WARNING] We do not compute V^-1 but we use the LU for that matter
		/// LU is used in order to be able to check if inversion of V is possible

		//Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t> lu(V);
		lu.compute(V);
		if(!lu.isInvertible()){
			notInvertible = true;

			V.setZero();
			D.setZero(); // Trick to get a zero likelihood
		} else {
			notInvertible = false;

			if(coefficients[MU::COEV_MF::D_ID] > 0. && coefficients[MU::COEV_MF::S_ID] > 0.) {
				std::vector<size_t> profileIdx;
				for(size_t iP=0; iP<profile.size(); ++iP) {
					int iNucl1 = MD::getNucleotides()->getNucleotideIdx(profile[iP][0]);
					int iNucl2 = MD::getNucleotides()->getNucleotideIdx(profile[iP][1]);

					size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
					profileIdx.push_back(profileCode);
				}


				double ds = coefficients[MU::COEV_MF::D_ID]/coefficients[MU::COEV_MF::S_ID];
				double sd = 1./ds;
				const size_t NB_PAIRS_IN_PROFILE = profile.size();
				double freqInProf = 1./((double)NB_PAIRS_IN_PROFILE+(16.-NB_PAIRS_IN_PROFILE)*sd);
				double freqOutProf = 1./((16.-(double)NB_PAIRS_IN_PROFILE)+NB_PAIRS_IN_PROFILE*ds);

				stationaryFreq = freqOutProf*Eigen::VectorXd::Ones(16);
				for(size_t i=0; i<NB_PAIRS_IN_PROFILE; ++i) {
					stationaryFreq(profileIdx[i]) = freqInProf;
				}
			}

		}
	}
}


bool MatrixCoevNode::processSignal(BaseNode* aChild) {
	return true;
}

std::string MatrixCoevNode::toString() const {
	std::stringstream ss;
	ss << "[MatrixCoevNode] " << BaseNode::toString();
	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
