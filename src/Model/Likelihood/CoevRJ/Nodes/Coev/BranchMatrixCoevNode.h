//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixCoevNode.h
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#ifndef BRANCHMATRIXCOEVNODE_COEVRJ_H_
#define BRANCHMATRIXCOEVNODE_COEVRJ_H_

#include <stddef.h>

#include "../Types.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "MatrixCoevNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class BranchMatrixCoevNode: public DAG::BaseNode {
public:
	BranchMatrixCoevNode(DL_Utils::Frequencies &aFrequencies);
	~BranchMatrixCoevNode();

	bool setBranchLength(const double aBL);
	double getBranchLength() const;

	bool setCoevScaling(const double aV);

	const TI_Coev_EigenSquareMatrix_t& getZ() const;
	const Eigen::FullPivLU<TI_Coev_EigenEIGMatrix_t>& getEigenVectorLU() const;

	std::string toString() const;

private:

	double branchLength, coevScaling;
	DL_Utils::Frequencies &frequencies;

	MatrixCoevNode *matrixNode;

	TI_Coev_EigenSquareMatrix_t Z;


	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BRANCHMATRIXCOEVNODE_COEVRJ_H_ */
