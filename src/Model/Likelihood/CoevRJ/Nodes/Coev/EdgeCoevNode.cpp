//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeCoevNode.cpp
 *
 * @date Mar 30, 2017
 * @author meyerx
 * @brief
 */
#include "EdgeCoevNode.h"

#include <stddef.h>

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class BranchMatrixCoevNode;

const EdgeCoevNode::scaling_t EdgeCoevNode::SCALING_TYPE = NONE;
const float EdgeCoevNode::SCALING_THRESHOLD = 1.; // Scale all with 1.


EdgeCoevNode::EdgeCoevNode(DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), frequencies(aFrequencies)  {

	bmNode = NULL;
	init();
}

EdgeCoevNode::~EdgeCoevNode() {
}


void EdgeCoevNode::init() {
	// Create vectors
	H.resize(frequencies.size());

	if(SCALING_TYPE == LOG) {
		nrm.resize(0);//nrm.resize(sitePos.size());
	}
}


const TI_EigenVector_t& EdgeCoevNode::getH() const {
	return H;
}

const TI_EigenVector_t& EdgeCoevNode::getNorm() const {
	return nrm;
}

BranchMatrixCoevNode* EdgeCoevNode::getBMCNode() {
	return bmNode;
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
