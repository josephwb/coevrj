//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PermutedCoevLikelihood.cpp
 *
 * @date Aug 15, 2017
 * @author meyerx
 * @brief
 */
#include "PermutedCoevLikelihood.h"

#include "Model/Likelihood/CoevRJ/UID/SubSpaceUID.h"

namespace StatisticalModel {
namespace Likelihood {
namespace CoevRJ {

class MatrixCoevNode;
class RootCoevNode;

PermutedCoevLikelihood::PermutedCoevLikelihood(size_t profileSize) :
		PROFILE_SIZE(profileSize) {

	treeHash = 0;

	rootDAG = NULL;
	ptrMatrixCoev = NULL;
}

PermutedCoevLikelihood::~PermutedCoevLikelihood() {
}

size_t PermutedCoevLikelihood::getProfileSize() const {
	return PROFILE_SIZE;
}

void PermutedCoevLikelihood::setRootDAG(PermRootCoevNode *aRootDAG) {
	rootDAG = aRootDAG;
}

PermRootCoevNode* PermutedCoevLikelihood::getRootDAG() {
	return rootDAG;
}

void PermutedCoevLikelihood::setMatrixCoev(PermMatrixCoevNode *aMatrixCoev) {
	ptrMatrixCoev = aMatrixCoev;
}

PermMatrixCoevNode* PermutedCoevLikelihood::getMatrixCoev() {
	return ptrMatrixCoev;
}

void PermutedCoevLikelihood::setLeafNodes(std::vector<PermLeafCoevNode*>& aLeafNodes) {
	leafNodes = aLeafNodes;
}

std::vector<PermLeafCoevNode*>& PermutedCoevLikelihood::getLeafNodes() {
	return leafNodes;
}

void PermutedCoevLikelihood::setTreeHash(long int aTreeHash) {
	treeHash = aTreeHash;
}

long int PermutedCoevLikelihood::getTreeHash() const {
	return treeHash;
}

bool PermutedCoevLikelihood::operator== (const PermutedCoevLikelihood &aCLI) const {
	bool samePosition = (PROFILE_SIZE == aCLI.PROFILE_SIZE);
	return samePosition;
}

std::string PermutedCoevLikelihood::toString() const {
	std::stringstream ss;

	ss << "Profile size :  = " << PROFILE_SIZE;
	ss << ", lik = " << rootDAG->getLogLikelihood() << std::endl;

	return ss.str();
}

} /* namespace CoevRJ */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
