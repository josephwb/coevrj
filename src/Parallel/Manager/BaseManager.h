//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseManager.h
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef BASEMANAGER_H_
#define BASEMANAGER_H_

#include <string>

#include "../Topology/BaseTopology.h"
#include "../Topology/CommunicationLayer.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Parallel {

class BaseManager {
public:
	enum role_t {None=0, Worker=1, Manager=2, Sequential=3};

public:
	BaseManager();
	virtual ~BaseManager();

	void setActive(const bool aActive);
	bool isActive() const;

	BaseTopology::sharedPtr_t getTopology();

protected:

	bool active;
	BaseTopology::sharedPtr_t ptrBT;

	role_t defineRole(const CommunicationLayer* commLayer);
	std::string roleToString(role_t myRole) const;

	enum serializedObjectSize_t {STATIC_SIZE=0, DYNAMIC_SIZE=1};

	template <serializedObjectSize_t SIZE_T>
	void sendSerializedObject(
			const Utils::Serialize::buffer_t &object, const int dest,
			const int tag, const CommunicationLayer *commL) const ;

	template <serializedObjectSize_t SIZE_T>
	void recvSerializedObject(
			Utils::Serialize::buffer_t &object, const int src,
			const int tag, const CommunicationLayer *commL) const;

	template <serializedObjectSize_t SIZE_T>
	void exchangeSerializedObject(
			const Utils::Serialize::buffer_t &objectSend, Utils::Serialize::buffer_t &objectRecv,
			const int other, const int tag, const CommunicationLayer *commL) const;

	template <serializedObjectSize_t SIZE_T>
	void broadcastSerializedObject(
			Utils::Serialize::buffer_t &object, const int root,
			const CommunicationLayer *commL) const;
};

} /* namespace Parallel */

#endif /* BASEMANAGER_H_ */
