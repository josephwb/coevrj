//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotidesPairs.cpp
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#include "NucleotidesPairs.h"

#include <assert.h>
#include <stdlib.h>

#include "MolecularDefIncl.h"

namespace MolecularEvolution {
namespace Definition {

NucleotidesPairs* NucleotidesPairs::instance = NULL;


const NucleotidesPairs* NucleotidesPairs::getInstance() {
	if(!instance) {
		instance = new NucleotidesPairs;
	}
	return instance;
}

NucleotidesPairs::NucleotidesPairs() :
		NUCL_PAIR(initNuclPairs()), NB_NUCL_PAIR(NUCL_PAIR.size()) {
}

NucleotidesPairs::~NucleotidesPairs() {
}

std::vector<std::string> NucleotidesPairs::initNuclPairs() const {
	std::vector<std::string> pairsNucl;
	for(size_t iNucl1=0; iNucl1<getNucleotides()->NB_BASE_NUCL; ++iNucl1) {
		for(size_t iNucl2=0; iNucl2<getNucleotides()->NB_BASE_NUCL; ++iNucl2) {
			std::string str(2, ' ');
			str[0] = getNucleotides()->NUCL_BASE[iNucl1];
			str[1] = getNucleotides()->NUCL_BASE[iNucl2];
			pairsNucl.push_back(str);
		}
	}
	return pairsNucl;
}

std::string NucleotidesPairs::getNuclPairString(size_t idNuclPair) const {
	assert(idNuclPair < NB_NUCL_PAIR);
	return NUCL_PAIR[idNuclPair];
}

std::vector<std::string> NucleotidesPairs::getNuclPairsString(bitset16b_t codedIdPairs) const {
	std::vector<std::string> nuclPairs;
	for(size_t iC=0; iC<codedIdPairs.size(); ++iC) {
		if(codedIdPairs[iC]) nuclPairs.push_back(NUCL_PAIR[iC]);
	}
	return nuclPairs;
}

bitset16b_t NucleotidesPairs::stringToCodedIdPairs(std::string pairString) const {
	bitset16b_t code;
	for(size_t iP=0; iP<NUCL_PAIR.size(); ++iP) {
		if(NUCL_PAIR[iP] == pairString) {
			code[iP] = true;
			return code;
		}
	}
	assert(false && "Code not found.");
	return code;
}



std::string NucleotidesPairs::codedIdPairsToString(bitset16b_t codePairNucl) const {
	std::stringstream sstr;
	for(size_t iN=0; iN<NB_NUCL_PAIR; ++iN) {
		if(codePairNucl[iN]) sstr << NUCL_PAIR[iN] << "|";
	}
	return sstr.str();
}

} /* namespace Definition */
} /* namespace MolecularEvolution */
