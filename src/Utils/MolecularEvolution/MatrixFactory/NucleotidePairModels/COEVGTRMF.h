//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file COEVGTRMF.h
 *
 * @date Mar 29, 2017
 * @author meyerx
 * @brief The COEVGTR_MF class represent the required implementation for the COEV+GTR model
 *
 * This class represent the required implementation for the COEV+GTR model. The diagonal operation
 * (Operations::Diagonal) is used to ensure that the sum of a row is equal to 0. The diagonal object
 * is already pre-processed during the scan of the nucleotides pair.
 * The vector of coefficient contains [theta1, theta2, theta3, theta4, theta5, s, d]. (GTR: a=theta1, b=theta2, c=theta3... f=1)
 * We use the operation Operations::CoeffsDotFreq to couple each substitution with its coefficient
 * and nucleotide frequency.
 */
#ifndef COEVGTRMF_H_
#define COEVGTRMF_H_

#include <boost/assign/list_of.hpp>
#include <stddef.h>

#include "Utils/MolecularEvolution/MatrixFactory/NucleotidePairModels/NucleotidePairMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/CoeffsDotFreq.h"

namespace MolecularEvolution {
namespace MatrixUtils {

class COEVGTR_MF: public NucleotidePairMF {
public:
	static const size_t COEV_NB_COEFF;
	static const std::vector<std::string> PARAMETERS_NAME;

	enum COEFF_IDX {THETA1_ID=0, THETA2_ID=1, THETA3_ID=0, THETA4_ID=1, THETA5_ID=0, S_ID=2, D_ID=3};

public:
	COEVGTR_MF(const std::vector<std::string> &aProfile);
	COEVGTR_MF(const std::vector<std::string> &aProfile, const bool aDoRowsSumtoZero);
	~COEVGTR_MF();

private:

	typedef NucleotidePairMF super;

	std::vector<std::string> profile;
	std::vector<size_t> profileIdx;

	void doInitOperations();

	Operations::Base* defineOperation(size_t iFrom, size_t iTo) const;

	void defineProfileIdx();
	int getProfileIdx(size_t profileCode) const;
};

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* COEVGTRMF_H_ */
