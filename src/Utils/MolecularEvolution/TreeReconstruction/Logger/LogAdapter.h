//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LogAdapter.h
 *
 * @date Nov 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef LOGADAPTER_H_
#define LOGADAPTER_H_

#include "LogTreeLoader.h"
#include "Sampler/TraceWriter/BaseWriter.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"

#include <stddef.h>
#include <map>
#include <string>
#include <vector>

#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variance.hpp>

namespace Sampler { class BaseSampler; }

namespace MolecularEvolution {
namespace TreeReconstruction {
namespace Logger {

class LogAdapter {
public:
	LogAdapter(Sampler::TraceWriter::BaseWriter::sharedPtr_t aPtrWriter);
	~LogAdapter();

	bool adaptLog(bool withSplitFrequencies=false);
	bool adaptLog2(bool withSplitFrequencies=false);

	void setBurninIteration(size_t aBurninIteration);
	void setBurninRatio(double aBurninRatio);

private:

	size_t burninIteration, nLine;
	double burninRatio;

	//Sampler::BaseSampler *sampler;
	Sampler::TraceWriter::BaseWriter::sharedPtr_t ptrWriter;
	LogTreeLoader logTreeLoader;

	typedef boost::accumulators::tag::variance boostAccVarianceTag_t;
	typedef boost::accumulators::stats< boostAccVarianceTag_t > boostAccStats_t;
	typedef boost::accumulators::accumulator_set< double, boostAccStats_t > boostAccVariance_t;
	struct resultCount_t {
		std::vector<size_t> countTree;
		std::vector< std::vector< boostAccVariance_t > > branchLengths;
	};

	resultCount_t countTreeAndBLengthBinaryLog(std::vector<std::string> &idStrTrees, std::map<long int, size_t> &mapping);
	std::vector<long int> adaptBinaryLog();

	size_t getTreeHashPosition(std::string &aLine) const;
	std::vector<size_t> getBranchLengthPosition(std::string &aLine) const;


	struct mappingBranch_t {
		size_t id;
		std::vector<size_t> mappingBranch;
	};

	void summarizeUniqueTrees(const std::vector<std::string> &idStrTrees, const std::map<long int, size_t> &mapping,
							  std::vector<std::string> &uniqueStrTrees, std::map<long int, mappingBranch_t> &uniqueMapping,
							  std::vector<Tree::sharedPtr_t> &uniqueTrees);

	resultCount_t countTreeAndBLengthTracerLog(std::vector<std::string> &idStrTrees, std::map<long int, size_t> &mapping,
												std::vector<std::string> &uniqueStrTrees, std::map<long int, mappingBranch_t> &uniqueMapping);

	void writeTrees(std::vector<Tree::sharedPtr_t> &uniqueTrees, std::vector<size_t> &orderedId, resultCount_t &resCount) const;

	std::vector<long int> adaptTracerLog(std::map<long int, mappingBranch_t> &uniqueMapping, std::map<size_t, size_t> &mappingOrderedId);

};

} /* namespace Logger */
} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */

#endif /* LOGADAPTER_H_ */
