//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalNode.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include <Utils/MolecularEvolution/TreeReconstruction/Logger/FinalNode.h>

namespace MolecularEvolution {
namespace TreeReconstruction {

FinalNode::FinalNode() {

}

FinalNode::~FinalNode() {
}


double FinalNode::getFrequency(size_t iter) {
	std::vector<size_t>::iterator it;
	it = std::find_if(iterations.begin(), iterations.end(),
			FindFinalNodeIfGreaterThan(iter));

	double freq = 0.;
	if(it == iterations.end()) { // We reached the end
		freq = (double)iterations.size()/(double)iter;
	} else { // There are still other sample after
		// Get the number of split apparition (we are past the last sample : -1)
		size_t dist = std::distance(iterations.begin(), it)-1;

		// Define the frequency
		freq = (double)dist/(double)iter;
	}
	return freq;
}

Eigen::VectorXd FinalNode::getFrequencies(const size_t MAX_ITER) {
	Eigen::VectorXd freqs(MAX_ITER);

	size_t cnt = 0;
	for(size_t iI=0; iI<MAX_ITER; ++iI) {
		if(cnt < iterations.size() && iterations[cnt] == iI) {
			cnt++;
		}

		freqs(iI) = (double) cnt / (double)(iI+1.);
	}
	return freqs;
}

Eigen::VectorXi FinalNode::getCounts(const size_t MAX_ITER) {
	Eigen::VectorXi counts(MAX_ITER);

	size_t cnt = 0;
	for(size_t iI=0; iI<MAX_ITER; ++iI) {
		if(cnt < iterations.size() && iterations[cnt] == iI) {
			cnt++;
		}

		counts(iI) = (double) cnt;
	}
	return counts;
}


std::string FinalNode::getSplitAsString() const {
	std::stringstream ss;
	for(size_t iN=0; iN<partition.size(); ++iN) {
		if(partition[iN]) {
			ss << "*";
		} else {
			ss << ".";
		}
	}
	return ss.str();
}

} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */
