//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.cpp
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderLikelihoodXML.h"

#include <stddef.h>

#include "Model/Likelihood/CoevRJ/Base.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/TmpNode.h"

class TiXmlElement;

namespace XML {

ReaderLikelihoodXML::ReaderLikelihoodXML(const std::string &aName, TiXmlElement *aPRoot) :
	name(aName), pRoot(aPRoot) {

	readLikelihood();
}

ReaderLikelihoodXML::~ReaderLikelihoodXML() {
}

StatisticalModel::Model* ReaderLikelihoodXML::getModelPtr() {
	return ptrModel.get();
}

void ReaderLikelihoodXML::readLikelihood() {
	if (name == Tag::COEV_REVERSIBLE_JUMP_NAME) {
		readCoevRJ();
	} else {
		std::stringstream ss;
		ss << "Likelihood '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

}

void ReaderLikelihoodXML::readCoevRJ() {
	namespace DL = MolecularEvolution::DataLoader;
	namespace TR = MolecularEvolution::TreeReconstruction;

	size_t nThread = 1;
	//XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	std::string alignName, treeName, coevFileName, coevRestartFileName;
	XML::readElement(__func__, Tag::ALIGN_FILE_TAG, pRoot, alignName, MANDATORY);
	bool isInitTreeGiven = XML::readElement(__func__, Tag::TREE_FILE_TAG, pRoot, treeName, OPTIONAL);
	//bool isFixedClusterGiven = XML::readElement(__func__, Tag::COEV_CLUSTER_FILE_TAG, pRoot, coevFileName, OPTIONAL);
	bool isRestartFileGiven = XML::readElement(__func__, Tag::COEV_RESTART_FILE_TAG, pRoot, coevRestartFileName, OPTIONAL);

	DL::NewickParser *np = NULL;
	if(isInitTreeGiven) {
		np = new DL::NewickParser(treeName);
	}

	size_t nGamma = 4;
	XML::readElement(__func__, Tag::NGAMMA_TAG, pRoot, nGamma, OPTIONAL);


	bool useStationaryFrequencyGTR = readBool(pRoot, Tag::USE_STATIONARY_FREQ_GTR_TAG, OPTIONAL, true);

	size_t iTreeStrat = 0;
	XML::readElement(__func__, Tag::TREE_SAMPLING_TAG, pRoot, iTreeStrat, MANDATORY);
	StatisticalModel::Likelihood::CoevRJ::Base::treeUpdateStrategy_t treeStrat;
	treeStrat = static_cast<StatisticalModel::Likelihood::CoevRJ::Base::treeUpdateStrategy_t>(iTreeStrat);

	bool useCachePairProfileInfo = readBool(pRoot, Tag::USE_CACHE_PPI_TAG, OPTIONAL, true);

	// Likelihood
	ptrLik = LikelihoodFactory::createCoevRJ(false, useStationaryFrequencyGTR, useCachePairProfileInfo,
											 nGamma, nThread, np, alignName, coevFileName, treeStrat);

	if(isRestartFileGiven) {
		static_cast<StatisticalModel::Likelihood::CoevRJ::Base*>(ptrLik.get())->setRestartFileName(coevRestartFileName);
	}

	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	hlp->defineParameters(ptrModel->getParams());

	if(np != NULL) {
		delete np;
	}
}

/* Read helpers */
void ReaderLikelihoodXML::readDefaultParametersType(const Helper::HelperInterface::sharedPtr_t &hlp) {
	std::string paramType;
	if(XML::readElement(__func__, Tag::PARAMETERS_TAG, pRoot, paramType, OPTIONAL)) {
		if(paramType == Tag::DEFAULT) {
			hlp->defineParameters(ptrModel->getParams());
		} else {
			std::stringstream ss;
			ss << "Parameters '" << paramType << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineParameters(ptrModel->getParams());
	}

//	for(size_t i=0; i<ptrModel->getParams().size(); ++i) {
//		std::cout << ptrModel->getParams().getName(i) << " :: ";
//		std::cout << ptrModel->getParams().getBoundMin(i) << " --> ";
//		std::cout << ptrModel->getParams().getBoundMax(i) << std::endl;
//	}

}



} /* namespace XML */
