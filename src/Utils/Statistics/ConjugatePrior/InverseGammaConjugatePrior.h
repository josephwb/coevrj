//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InverseGammaConjugatePrior.h
 *
 * @date May 28, 2017
 * @author meyerx
 * @brief Implementation for the Inverse Gamma distribution.
 * This distribution is the conjugate to the Normal distribution with unknown variance.
 * See (saved in zotero) :
 * - Conjugate analysis of the Gaussian Distribution by Murphy Kevin
 * - Conjugate prior of the normal distribution by Jordan Michael
 * - or wikipedia
 */
#ifndef INVERSEGAMMACONJUGATEPRIOR_H_
#define INVERSEGAMMACONJUGATEPRIOR_H_

#include <utility>
#include <vector>
#include <boost/smart_ptr/shared_ptr.hpp>

class RNG;

namespace Utils {
namespace Statistics {

class InverseGammaConjugatePrior {
public:
	typedef boost::shared_ptr<InverseGammaConjugatePrior> sharedPtr_t;
public:
	InverseGammaConjugatePrior(double aAlpha, double aBeta);
	~InverseGammaConjugatePrior();

	double drawFromPrior(RNG *aRNG) const;
	double drawFromPosterior(RNG *aRNG, const std::vector<double> &data) const;

	double computePDF(double variance) const;

private:

	/// Assuming a distribution normal(mu, var)
	/// MU0 represents the observed mu
	/// N0 represents the number of sample on which this observation is based
	/// 2*ALPHA represents the number of sample on which the observation variance is based
	/// 2*BETA represents the Sum of the Squared Deviations from the mean (SSD or SSE)
	/// --> SSD = SUM [mean - x]^2 ==> Sample variance sigma^2 = SSD/(n-1)
	/// Therefore 2*BETA=SSD, n=2*ALPHA, (2*BETA)/(2*ALPHA)=BETA/ALPHA=SSD/n~= sigma^2
	const double ALPHA, BETA;
};
} /* namespace Statistics */
} /* namespace Utils */

#endif /* INVERSEGAMMACONJUGATEPRIOR_H_ */
