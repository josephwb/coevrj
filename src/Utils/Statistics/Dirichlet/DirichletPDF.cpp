//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletPDF.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "DirichletPDF.h"

#include <assert.h>
#include <boost/math/special_functions/gamma.hpp>

namespace Utils {
namespace Statistics {

DirichletPDF::DirichletPDF(size_t nAlphas, double aAlpha) :
	IN_LOG_SPACE(true) {

	for(size_t i=0; i<nAlphas; ++i) {
		alphas.push_back(aAlpha);
	}

	defineConstBeta();

}

DirichletPDF::DirichletPDF(const std::vector<double> &aAlphas) :
		IN_LOG_SPACE(true), alphas(aAlphas) {

	defineConstBeta();
}


DirichletPDF::DirichletPDF(bool aInLogSpace, const std::vector<double> &aAlphas) :
		IN_LOG_SPACE(aInLogSpace), alphas(aAlphas) {

	defineConstBeta();
}


DirichletPDF::~DirichletPDF() {
}

bool DirichletPDF::isLog() const {
	return IN_LOG_SPACE;
}


double DirichletPDF::computePDF(const std::vector<double> &x) const {
	assert(x.size() == alphas.size());

	if(!IN_LOG_SPACE) {
		/*double sum = 0;
		for(size_t iX=0; iX<x.size(); ++iX) {
			assert(x[iX] >= 0. && x[iX] <= 1.);
			sum += x[iX];
		}
		assert(fabs(sum-1.0) < 1e-7);*/

		double p=1.;
		for(size_t i=0; i<alphas.size(); ++i) {
			p *= std::pow(x[i], alphas[i]-1.);
		}

		//std::cout << "p = " << p << "\t" << constBeta << std::endl;

		return p/constBeta;
	} else {
		/*double sum = 0;
		for(size_t iX=0; iX<x.size(); ++iX) {
			assert(x[iX] >= 0. && x[iX] <= 1.);
			sum += x[iX];
		}
		assert(fabs(sum-1.0) < 1e-7);*/

		double p=0.;
		for(size_t i=0; i<alphas.size(); ++i) {
			//p *= std::pow(x[i], alphas[i]-1.);
			p += (alphas[i]-1.)*log(x[i]);
		}

		//std::cout << "p = " << p << "\t" << constBeta << std::endl;

		return exp(p-constBeta);
	}
}

void DirichletPDF::defineConstBeta() {

	if(!IN_LOG_SPACE) {
		double sumAlpha = 0.;
		double prodGamma = 1.;
		for(size_t i=0; i<alphas.size(); ++i) {
			sumAlpha += alphas[i];
			//std::cout << alphas[i] << std::endl; // FIXME DEBUG
			prodGamma *= boost::math::tgamma(alphas[i]);
		}
		//std::cout << "prodGamma = " << prodGamma << "\t" << sumAlpha << "\t" << boost::math::tgamma(sumAlpha) << std::endl;
		//std::cout << sumAlpha << std::endl; // FIXME DEBUG
		constBeta = prodGamma/boost::math::tgamma(sumAlpha);
	} else {
		double sumAlpha = 0.;
		double sumLogGamma = 0;
		for(size_t i=0; i<alphas.size(); ++i) {
			sumAlpha += alphas[i];
			//std::cout << alphas[i] << std::endl; // FIXME DEBUG
			sumLogGamma += boost::math::lgamma(alphas[i]);
		}
		//std::cout << "prodGamma = " << prodGamma << "\t" << sumAlpha << "\t" << boost::math::tgamma(sumAlpha) << std::endl;
		//std::cout << sumAlpha << std::endl; // FIXME DEBUG
		constBeta = sumLogGamma-boost::math::lgamma(sumAlpha);
	}
}

} /* namespace Statistics */
} /* namespace Utils */
