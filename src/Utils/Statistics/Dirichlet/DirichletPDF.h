//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletPDF.h
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef DIRICHLETPDF_H_
#define DIRICHLETPDF_H_

#include <boost/math/distributions.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <vector>

#include "Parallel/RNG/RNG.h"
#include "Sampler/Samples/Sample.h"

class RNG;

namespace Utils {
namespace Statistics {

class DirichletPDF {
public:
	typedef boost::shared_ptr<DirichletPDF> sharedPtr_t;

public:
	DirichletPDF(size_t nAlphas, double aAlpha);
	DirichletPDF(const std::vector<double> &aAlphas);
	DirichletPDF(bool aInLogSpace, const std::vector<double> &aAlphas);
	~DirichletPDF();

	double computePDF(const std::vector<double> &x) const;

	bool isLog() const;

private:
	const bool IN_LOG_SPACE;
	const RNG *r;
	double constBeta;
	std::vector<double> alphas;

	void defineConstBeta();

};

} /* namespace Statistics */
} /* namespace Utils */

#endif /* DIRICHLETPDF_H_ */
