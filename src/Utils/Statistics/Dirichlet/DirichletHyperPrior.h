//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletHyperPrior.h
 * @date May 25, 2017
 * @author meyerx
 * @brief
 * This class is based on :
 * http://tdunning.blogspot.ch/2009/04/sampling-dirichlet-distributions.html
 * and
 * http://tdunning.blogspot.ch/2010/04/sampling-dirichlet-distribution-revised.html
 * see also
 * http://andrewgelman.com/2009/04/29/conjugate_prior/
 *
 */
#ifndef DIRICHLETHYPERPRIOR_H_
#define DIRICHLETHYPERPRIOR_H_

#include <vector>

#include <boost/math/distributions/exponential.hpp>
#include <boost/math/distributions/gamma.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

#include "DirichletPDF.h"

namespace Utils {
namespace Statistics {

class DirichletHyperPrior {
public:
	typedef enum {EXP_DIRICHLET_HP_TYPE, GAMMA_HP_TYPE} dirichletHyperPriorType_t;
	typedef boost::shared_ptr<DirichletHyperPrior> sharedPtr_t;

public:
	DirichletHyperPrior(double aAlpha0, const std::vector<double> &aAlphas, dirichletHyperPriorType_t aType);
	DirichletHyperPrior(const std::vector<double> &aParameters, dirichletHyperPriorType_t aType);
	~DirichletHyperPrior();

	std::vector<double> drawFromDistribution(RNG *aRNG) const;
	double computePDF(const std::vector<double> &X) const;

private:

	/// Log version is about 25% slower but more safe.
	const dirichletHyperPriorType_t DIRICHLET_HP_TYPE;

	double alpha0;
	std::vector<double> alphas;

	boost::math::exponential_distribution<> expDistr;
	DirichletPDF dirichletPDF;
	std::vector<boost::math::gamma_distribution<> > gammasDistr;

};

} /* namespace Statistics */
} /* namespace Utils */

#endif /* DIRICHLETHYPERPRIOR_H_ */
