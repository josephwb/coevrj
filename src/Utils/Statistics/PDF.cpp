//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PDF.cpp
 *
 * @date Jan 13, 2017
 * @author meyerx
 * @brief
 */

#include "PDF.h"

#include <math.h>
#include <stddef.h>

#include <cstdlib>
#include <iostream>

namespace Utils {
namespace Statistics {
namespace MultivariateNormal {

double singleCholeskyPDF(const Eigen::VectorXd &x, const Eigen::VectorXd &mu, const Eigen::LLT<Eigen::MatrixXd> &choleskyDecomp) {
	const double logSqrt2Pi = 0.5*std::log(2*M_PI);
	// Handle non positive definite covariance somehow:
	if(choleskyDecomp.info() == Eigen::NumericalIssue) { std::cerr << "error in cholesky decomposition." << std::endl; abort();}
	if(choleskyDecomp.info() == Eigen::NoConvergence) { std::cerr << "error in cholesky decomposition." << std::endl; abort();}
	if(choleskyDecomp.info() == Eigen::InvalidInput) { std::cerr << "error in sigma input." << std::endl; abort();}
	const Eigen::LLT<Eigen::MatrixXd>::Traits::MatrixL& L = choleskyDecomp.matrixL();
	Eigen::VectorXd x0 = x - mu;
	double quadform = (L.solve(x0)).squaredNorm();
	return std::exp(-x.rows()*logSqrt2Pi - 0.5*quadform -log(L.determinant()));
}

Eigen::VectorXd multiCholeskyPDF(const Eigen::MatrixXd &x, const Eigen::VectorXd &mu, const Eigen::LLT<Eigen::MatrixXd> &choleskyDecomp) {
	const double logSqrt2Pi = 0.5*std::log(2*M_PI);
	// Handle non positive definite covariance somehow:
	if(choleskyDecomp.info() == Eigen::NumericalIssue) { std::cerr << "error in cholesky decomposition." << std::endl; abort();}
	if(choleskyDecomp.info() == Eigen::NoConvergence) { std::cerr << "error in cholesky decomposition." << std::endl; abort();}
	if(choleskyDecomp.info() == Eigen::InvalidInput) { std::cerr << "error in sigma input." << std::endl; abort();}
	const Eigen::LLT<Eigen::MatrixXd>::Traits::MatrixL& L = choleskyDecomp.matrixL();

	// New arrays
	Eigen::MatrixXd x0(x);
	Eigen::VectorXd res(x.cols());

	// Pre-calculated
	const double c1 = -x.rows()*logSqrt2Pi;
	const double c3 = -log(L.determinant());

	for(size_t iC=0; iC < (size_t)x0.cols(); ++iC) {
		x0.col(iC) -= mu;
		double quadform = (L.solve(x0)).squaredNorm();
		res(iC) = std::exp(c1 - 0.5*quadform + c3);
	}

	return res;
}

double singleSigmaPDF(const Eigen::VectorXd &x, const Eigen::VectorXd &mu, const Eigen::MatrixXd &sigma) {
	Eigen::LLT<Eigen::MatrixXd> choleskyDecomp(sigma);
	if(mu.size() > 500) {
		std::cerr << "[Warning] in Eigen::VectorXd pdfMVN(...) const;" << std::endl;
		std::cerr << "Using LLDT would be more accurate for such large covariance matrix." << std::endl;
	}
	return singleCholeskyPDF(x, mu, choleskyDecomp);
}

Eigen::VectorXd multiSigmaPDF(const Eigen::MatrixXd &x, const Eigen::VectorXd &mu, const Eigen::MatrixXd &sigma) {
	Eigen::LLT<Eigen::MatrixXd> choleskyDecomp(sigma);
	if(mu.size() > 500) {
		std::cerr << "[Warning] in Eigen::VectorXd pdfMVN(...) const;" << std::endl;
		std::cerr << "Using LLDT would be more accurate for such large covariance matrix." << std::endl;
	}
	return multiCholeskyPDF(x, mu, choleskyDecomp);
}
} /* namespace MultivariateNormal */


}
}
