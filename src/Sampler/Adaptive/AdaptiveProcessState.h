//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveProcessState.h
 *
 * @date Dec 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEPROCESSSTATE_H_
#define ADAPTIVEPROCESSSTATE_H_

#include <stddef.h>

#include "Sampler/Adaptive/AdaptiveBlockState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Sampler {
namespace Adaptive {

class AdaptiveProcess;

class AdaptiveProcessState {
public:
	AdaptiveProcessState();
	~AdaptiveProcessState();

private:

	bool fullConvergence;
	size_t nAdaptiveBlocks, counter;
	size_t convCnt;

	std::vector<Sampler::Adaptive::State::AdaptiveBlockState> abStates;

	// Boost serialization
	friend class AdaptiveProcess;
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()

};

} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* ADAPTIVEPROCESSSTATE_H_ */
