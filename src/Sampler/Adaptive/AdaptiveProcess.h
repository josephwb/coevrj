//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AdaptiveProcess.h
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#ifndef ADAPTIVEPROCESS_H_
#define ADAPTIVEPROCESS_H_

#include <stddef.h>
#include <vector>

#include "AdaptiveProcessState.h"
#include "ParameterBlock/BlockStats/Config/ConfigFactory.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/Adaptive/AdaptiveBlock.h"
#include "Sampler/Information/IncInformations.h"
#include "Sampler/ParameterStatsAccessor/AccessorsManager.h"
#include "Sampler/ParameterStatsAccessor/AdaptiveAccessor.h"
#include "Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.h"
#include "Sampler/Strategy/IncStrategy.h"
#include "Sampler/Strategy/ModifierStrategy/ModifierStrategy.h"

namespace Parallel { class MCMCManager; }
namespace ParameterBlock { class Blocks; }
namespace Sampler { class Sample; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace StatisticalModel { class Model; }


namespace Sampler {
namespace Adaptive {

class AdaptiveProcessState;

using namespace ParameterBlock;

class AdaptiveProcess {
public:
	AdaptiveProcess(Sampler::Sample initSample,
					Config::ConfigFactory::sharedPtr_t aCfgFactory,
					Blocks &aBlocks, StatisticalModel::Model &aModel,
					Strategies::AcceptanceStrategy::sharedPtr_t aPtrAS,
					Strategies::ModifierStrategy::sharedPtr_t aPtrMS,
					Sampler::PStatsAccessor::AccessorsManager &accessorsManager);
	~AdaptiveProcess();

	void adaptBlock(bool accepted, Sample &oldSample, Sample &curSample,
					size_t iBlock, double alpha);
	void adaptBlocks(Sample &oldSample, Sample &curSample, const int acceptedId,
				 	 const vector<size_t> &iBlocks,  const vector<double> &alphas);
	void updateLocalAlphas(Sample &curSample, const std::vector<size_t> &iBlocks);

	size_t getNAdaptiveBlocks();
	size_t& getNAdaptiveBlocksByReference();
	std::vector<Adaptive::AdaptiveBlock::sharedPtr_t>& getAdaptiveBlocks();
	std::vector<Information::BaseInfo::sharedPtr_t>& getInformations();

	bool hasBlockToOptimize() const;

	Sampler::PStatsAccessor::AdaptiveAccessor* getParametersStatsAccessor();
	void refreshAccessor();

	void saveState(AdaptiveProcessState &state) const;
	void loadState(AdaptiveProcessState &state);

private:

	static const size_t NB_BLOCK_RELAXATION_TH;
	static const size_t MINIMAL_ADAPTIVE_PHASE;
	static const size_t CONVERGENCE_REPORTING_FREQUENCY;

	bool fullConvergence;
	size_t nAdaptiveBlocks, counter;
	size_t convCnt;

	const Parallel::MCMCManager &mcmcMgr;

	Config::ConfigFactory::sharedPtr_t cfgFactory;
	Blocks &blocks;
	StatisticalModel::Model &model;
	Strategies::AcceptanceStrategy::sharedPtr_t ptrAS;
	Strategies::ModifierStrategy::sharedPtr_t ptrMS;

	std::vector<Adaptive::AdaptiveBlock::sharedPtr_t> adaptiveBlocks;
	std::vector<Information::BaseInfo::sharedPtr_t> informations;
	Sampler::PStatsAccessor::AdaptiveAccessor pStatsAccessor;

	void init(Sample &initSample, Sampler::PStatsAccessor::AccessorsManager &aAccessorManager);

	//! Check if has converged
	bool hasConverged() const;
	void monitorBlockConvergence(const size_t iB, Adaptive::AdaptiveBlock::conv_signal_t convSignals);
	bool checkConvergence();

	//! Local scaling
	std::vector< std::vector<double> > updateLocalAlpha(Sample &curSample, const std::vector<long  int> &iBlocks);
	std::vector< std::vector<double> > balancedLocalAlphaUpdate(Sample &curSample, const std::vector<long  int> &iBlocks);
	void localAlphaComputation(const size_t iB, Sample &curSample, const std::vector<size_t> &pInd, std::vector<double> &locAlphas);
};

} /* namespace Adaptive */
} /* namespace Sampler */

#endif /* ADAPTIVEPROCESS_H_ */
