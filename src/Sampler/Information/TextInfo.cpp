//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TextInfo.cpp
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#include "TextInfo.h"

#include "InformationDispatcher.h"

namespace Sampler {
namespace Information {

TextInfo::TextInfo(std::string &aStr) :
		strInfo(aStr) {
}

TextInfo::~TextInfo() {
}

const std::string& TextInfo::getString() const {
	return strInfo;
}

infoType_t TextInfo::getType() const {
	return TEXT_INFO;
}

void TextInfo::submitToDispatcher(InformationDispatcher* dispatcher) {
	dispatcher->dispatch(this);
}

TextInfo::sharedPtr_t TextInfo::createTextInfo(std::string &aStr) {
	return TextInfo::sharedPtr_t(new TextInfo(aStr));
}

TextInfo::sharedPtr_t TextInfo::createTextInfo(std::stringstream &aSS) {
	std::string tmpStr(aSS.str());
	return TextInfo::sharedPtr_t(new TextInfo(tmpStr));
}

} /* namespace Information */
} /* namespace Sampler */
