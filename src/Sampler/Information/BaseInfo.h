//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseInfo.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEINFO_H_
#define BASEINFO_H_

#include <boost/shared_ptr.hpp>


namespace Sampler {
namespace Information {

typedef enum {TEXT_INFO=0, MC3_INFO=1, REMOTE_INFO=2} infoType_t;

class InformationDispatcher;

class BaseInfo {
public:
	typedef boost::shared_ptr<BaseInfo> sharedPtr_t;

public:
	BaseInfo();
	virtual ~BaseInfo();

	virtual infoType_t getType() const = 0;

	virtual void submitToDispatcher(InformationDispatcher* dispatcher) = 0;
};

} /* namespace Information */
} /* namespace Sampler */

#endif /* BASEINFO_H_ */
