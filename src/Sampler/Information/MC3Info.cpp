//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MC3Info.cpp
 *
 * @date Dec 19, 2016
 * @author meyerx
 * @brief
 */
#include "MC3Info.h"

#include "Sampler/Information/InformationDispatcher.h"

namespace Sampler {
namespace Information {

MC3Info::MC3Info(std::string &aStr) :
				strInfo(aStr) {
}

MC3Info::~MC3Info() {
}

const std::string& MC3Info::getString() const {
	return strInfo;
}

infoType_t MC3Info::getType() const {
	return MC3_INFO;
}

void MC3Info::submitToDispatcher(InformationDispatcher* dispatcher) {
	dispatcher->dispatch(this);
}

MC3Info::sharedPtr_t MC3Info::createTextInfo(std::string &aStr) {
	return MC3Info::sharedPtr_t(new MC3Info(aStr));
}

MC3Info::sharedPtr_t MC3Info::createTextInfo(std::stringstream &aSS) {
	std::string tmpStr(aSS.str());
	return MC3Info::sharedPtr_t(new MC3Info(tmpStr));
}

} /* namespace Information */
} /* namespace Sampler */
