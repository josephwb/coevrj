//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MC3Info.h
 *
 * @date Dec 19, 2016
 * @author meyerx
 * @brief
 */
#ifndef MC3INFO_H_
#define MC3INFO_H_

#include <sstream>
#include <string>

#include "BaseInfo.h"

namespace Sampler {
namespace Information {

class MC3Info: public BaseInfo {
public:
	typedef boost::shared_ptr<MC3Info> sharedPtr_t;

public:
	MC3Info(std::string &aStr);
	~MC3Info();

	const std::string& getString() const;

	infoType_t getType() const;

	void submitToDispatcher(InformationDispatcher* dispatcher);

	static MC3Info::sharedPtr_t createTextInfo(std::string &aStr);
	static MC3Info::sharedPtr_t createTextInfo(std::stringstream &aSS);

private:
	std::string strInfo;
};

} /* namespace Information */
} /* namespace Sampler */

#endif /* MC3INFO_H_ */
