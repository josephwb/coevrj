//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedTIBS.cpp
 *
 * @date Nov 24, 2015
 * @author meyerx
 * @brief
 */
#include "OptimisedTIBS.h"

#include <assert.h>
#include <math.h>

#include "Sampler/Strategy/BlockSelector/OptimisedBaseBS.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/Manager/MpiManager.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/Blocks.h"
#include "cmath"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

const double OptimisedTIBS::RATE_FREQUENCY = 0.02;

OptimisedTIBS::OptimisedTIBS(const Parameters &aParams, const Blocks &aBlocks,
		const double aFreqTreeMove, const uint aSeed) :
		OptimisedBaseBS(aParams, aBlocks, floor(Parallel::mcmcMgr().getNProposal()*1.5)),
		MOVE_FREQUENCY(aFreqTreeMove), BRANCH_FREQUENCY(1-(RATE_FREQUENCY+MOVE_FREQUENCY)), OTHER_FREQUENCY(BRANCH_FREQUENCY/2.),
		blockCatHelper(boost::assign::list_of(MOVE_FREQUENCY)(RATE_FREQUENCY)(BRANCH_FREQUENCY)(OTHER_FREQUENCY).convert_to_container<std::vector<double> >()) {
	m_rng = RNG::createSitmo11RNG(aSeed);

	catFB = blockCatHelper.defineCategForTreeInf(params, blocks);
	blockVersionForCatFB = blocks.getVersion();

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "**************************************" << std::endl;
		std::cout << "<Block selection frequency> topo move = " << catFB[0].categoryFreq;
		std::cout << " - rate = " << catFB[1].categoryFreq;
		std::cout << " - branch = " << catFB[2].categoryFreq;
		std::cout << " - other = " << catFB[3].categoryFreq << std::endl;
	}
}

OptimisedTIBS::~OptimisedTIBS() {
}

const ParameterBlock::Block::sharedPtr_t OptimisedTIBS::selectBlock(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	if(blocks.getNTotalBlocks() == 1) return blocks.getFirstBlock();

	std::vector<Block::sharedPtr_t> vecBlocks = blocks.getBlocks();
	std::vector<double> freqs = getFrequencies(vecBlocks);

	return drawABlock(vecBlocks, freqs);
}

const std::vector<size_t> OptimisedTIBS::selectBlocks(Sample & sample) {
	return selectSomeBlocks();
}

void OptimisedTIBS::rewind(const int acceptId) {
	size_t nKept;
	if(acceptId < 0) {
		nKept = Parallel::mcmcMgr().getNProposal();
	} else {
		nKept = 1+acceptId;
	}
	rewindSomeSteps(nKept);
}

size_t OptimisedTIBS::getSeed() const {
	return m_rng->getSeed();
}

void OptimisedTIBS::fillPendingBlocks() {

	if(blocks.getVersion() != blockVersionForCatFB) {
		catFB = blockCatHelper.defineCategForTreeInf(params, blocks);
		blockVersionForCatFB = blocks.getVersion();
	}

	while(iBlock.size() < NB_IDBLOCK_BUFFERED) {
		double selType = m_rng->genUniformDbl();
		ParameterBlock::Block::sharedPtr_t ptrBlock;
		for(size_t iC=0; iC<catFB.size(); ++iC) {
			if(selType < catFB[iC].categoryFreq) {
				size_t iBlock = m_rng->genUniformInt(0, catFB[iC].blocks.size()-1);
				ptrBlock = catFB[iC].blocks[iBlock];
				break;
			} else {
				selType -= catFB[iC].categoryFreq;
			}
		}
		iBlock.push_back(ptrBlock->getId());
		nSelected.push_back(0);
	}
}

} /* namespace Strategies */
} /* namespace Sampler */
