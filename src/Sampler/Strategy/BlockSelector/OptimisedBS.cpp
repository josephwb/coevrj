//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedBS.cpp
 *
 * @date Dec 13, 2015
 * @author meyerx
 * @brief
 */
#include "OptimisedBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/OptimisedBaseBS.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/Blocks.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

OptimisedBS::OptimisedBS(const Parameters &aParams, const Blocks &aBlocks,
						 const uint aSeed, const size_t aNTimePerBlock) :
		OptimisedBaseBS(aParams, aBlocks, aNTimePerBlock) {
	m_rng = RNG::createSitmo11RNG(aSeed);
}

OptimisedBS::~OptimisedBS() {
}

const Block::sharedPtr_t OptimisedBS::selectBlock(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	if(blocks.getNTotalBlocks() == 1) return blocks.getFirstBlock();

	std::vector<Block::sharedPtr_t> vecBlocks = blocks.getBlocks();
	std::vector<double> freqs = getFrequencies(vecBlocks);

	return drawABlock(vecBlocks, freqs);
}

const std::vector<size_t> OptimisedBS::selectBlocks(Sample & sample) {
	return selectSomeBlocks();
}

void OptimisedBS::rewind(const int acceptId) {
	size_t nKept;
	if(acceptId < 0) {
		nKept = Parallel::mcmcMgr().getNProposal();
	} else {
		nKept = 1+acceptId;
	}
	rewindSomeSteps(nKept);
}

size_t OptimisedBS::getSeed() const {
	return m_rng->getSeed();
}

void OptimisedBS::fillPendingBlocks() {
	std::vector<Block::sharedPtr_t> vecBlocks = blocks.getBlocks();
	std::vector<double> freqs = getFrequencies(vecBlocks);

	while(iBlock.size() < NB_IDBLOCK_BUFFERED) {
		iBlock.push_back(drawABlock(vecBlocks, freqs)->getId());
		nSelected.push_back(0);
	}
}

} // namespace Strategies
} // namespace Sampler

