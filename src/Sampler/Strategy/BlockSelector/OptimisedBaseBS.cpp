//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimisedBaseBS.cpp
 *
 * @date Apr 12, 2017
 * @author meyerx
 * @brief
 */
#include "OptimisedBaseBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Parallel/Manager/MCMCManager.h"
#include "ParameterBlock/Blocks.h"

namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

const size_t OptimisedBaseBS::NB_IDBLOCK_THRESHOLD = 2;
const size_t OptimisedBaseBS::NB_IDBLOCK_BUFFERED = 4;

OptimisedBaseBS::OptimisedBaseBS(const Parameters &aParams, const Blocks &aBlocks,
							     const size_t aNTimePerBlock) :
		BlockSelector(aParams, aBlocks), N_TIME_PER_BLOCK(aNTimePerBlock),
		lastBlocksVersion(blocks.getVersion()) {

	iBlock.push_back(blocks.getFirstBlock()->getId());
	nSelected.push_back(0);

	assert((int)N_TIME_PER_BLOCK >= Parallel::mcmcMgr().getNProposal());
}

OptimisedBaseBS::~OptimisedBaseBS() {
}


void OptimisedBaseBS::checkPendingBlocks() {

	std::list<size_t>::iterator itIBlock = iBlock.begin();
	std::list<size_t>::iterator itNSelected = nSelected.begin();

	while(itIBlock != iBlock.end()) {
		Block::sharedPtr_t ptrBlock = blocks.getBlock(*itIBlock);
		// If it doesn't exists anymore, push it out of the pending list
		if(ptrBlock == NULL) {
			itIBlock = iBlock.erase(itIBlock);
			itNSelected = nSelected.erase(itNSelected);
		} else { // else we are good
			itIBlock++;
			itNSelected++;
		}
	}
}

/**
 * iBlock contains a list of block randomly selected and to propose.
 * nSelected counts the amount of time these blocks have been selected (accepted/rejected).
 * A block must be proposed N_TIME_PER_BLOCK before a new one is proposed.
 *
 * @return a vector of blocks ID
 */
std::vector<size_t> OptimisedBaseBS::selectSomeBlocks() {
	assert(blocks.getNTotalBlocks()>0);

	size_t nBlocks = Parallel::mcmcMgr().getNProposal();
	std::vector<size_t> blockIDs;

	// If we have only one block its easy
	if(blocks.getNTotalBlocks() == 1) {
		blockIDs.assign(nBlocks, blocks.getFirstBlock()->getId());
		return blockIDs;
	}

	// Maintain pending blockIDs
	if(blocks.getVersion() != lastBlocksVersion) {
		checkPendingBlocks();
		lastBlocksVersion = blocks.getVersion();
	}

	// We check that there are enough remaining element in the pending block list
	if(iBlock.size() < NB_IDBLOCK_THRESHOLD) {
		fillPendingBlocks();
	}

	// Select next blocks
	std::list<size_t>::iterator itBlockID = iBlock.begin();
	// Define the amount of time we need to propose the current block (iBlock.front())
	size_t nCurBlock = std::min(nBlocks, N_TIME_PER_BLOCK-nSelected.front());
	for(size_t i=0; i<nCurBlock; ++i) {
		blockIDs.push_back(*itBlockID);
	}

	// If we have not reached the number of blocks required (nBlocks)
	if(nCurBlock < nBlocks) {
		itBlockID++;
		// Add the remaining required blocks
		for(size_t i=nCurBlock; i<nBlocks; ++i) {
			blockIDs.push_back(*itBlockID);
		}
	}

	return blockIDs;
}

void OptimisedBaseBS::rewindSomeSteps(size_t nKept) {

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[" << Parallel::mpiMgr().getRank() << "]B nStep = " << acceptId << " | nKept = " << nKept << std::endl;
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
	}*/

	// update selected count
	if(nSelected.front() + nKept >= N_TIME_PER_BLOCK) {
		nKept -= N_TIME_PER_BLOCK-nSelected.front();
		iBlock.pop_front();
		nSelected.pop_front();

		if(!iBlock.empty()) { // It there was a second block
			nSelected.front() += nKept; // Update its selection count
		}
	} else { // If it was the first block
		nSelected.front() += nKept; // Update its selection count
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "CurBlock : " << iBlock.front();
		std::cout << " -- selected n = " << nSelected.front() << std::endl ;
		getchar();
	}*/
}

} /* namespace Strategies */
} /* namespace Sampler */
