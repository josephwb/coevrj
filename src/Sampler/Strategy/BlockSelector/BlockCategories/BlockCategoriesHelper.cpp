//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockCategoriesHelper.cpp
 *
 * @date Apr 12, 2017
 * @author meyerx
 * @brief
 */
#include "BlockCategoriesHelper.h"

#include <assert.h>
#include <stddef.h>

#include "Model/Parameter/ParameterType.h"
#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"

namespace Sampler {
namespace Strategies {

BlockCategoriesHelper::BlockCategoriesHelper(const std::vector<double> aDefFrequencies) :
	defaultFreqs(aDefFrequencies){
}

BlockCategoriesHelper::~BlockCategoriesHelper() {
}

vecFreqBlocks_t BlockCategoriesHelper::defineCategForTreeInf(const Parameters &params, const Blocks &blocks) {

	assert(defaultFreqs.size() == 4);
	const size_t OTHER_PARAMS_ID = 3;

	std::vector<Block::sharedPtr_t> vecBlocks(blocks.getBlocks());
	std::vector<FrequenciesAndBlocks> categFB(4);

	for(size_t iB=0; iB<vecBlocks.size(); ++iB) {
		ParameterBlock::Block::sharedPtr_t block = vecBlocks[iB];
		size_t iParam = block->getPIndices().front();
		StatisticalModel::parameterType_t type = params.getType(iParam);
		size_t iType = static_cast<size_t>(type);
		if(iType >= 3) {
			iType = 3 ;
		}

		//std::cout << "Block - " << vecBlocks[iB]->getName() << "\t type = " << iType << std::endl;

		categFB[iType].categoryFreq += block->size();
		categFB[iType].blocks.push_back(block);
		categFB[iType].frequencies.push_back(block->getTotalFreq());
	}

	// categoryFreq contains the average nb parameters per blocks
	for(size_t iC=0; iC < categFB.size(); ++iC) {
		if(!categFB[iC].blocks.empty()){
			categFB[iC].categoryFreq /= (double)categFB[iC].blocks.size();
		}
	}

	double &treeMoveFreq = categFB[StatisticalModel::TREE_PARAM].categoryFreq;
	double &rateMoveFreq = categFB[StatisticalModel::RATE_PARAM].categoryFreq;
	double &branchMoveFreq = categFB[StatisticalModel::BRANCH_LENGTH].categoryFreq;
	double &otherMoveFreq = categFB[OTHER_PARAMS_ID].categoryFreq;

	// Tree move freq
	if(!categFB[StatisticalModel::TREE_PARAM].blocks.empty()) {
		treeMoveFreq = defaultFreqs[StatisticalModel::TREE_PARAM];
	}

	// Rate move freq
	rateMoveFreq = defaultFreqs[StatisticalModel::RATE_PARAM];
	if(!categFB[StatisticalModel::RATE_PARAM].blocks.empty()) { // SMALL TUNING FOR MC3
		if(Parallel::mcmcMgr().isActiveMC3()) {
			int myChainMC3 = Parallel::mcmcMgr().getMyChainMC3();
			if(myChainMC3 > 0) rateMoveFreq = rateMoveFreq/2.;
		}
	} else {
		rateMoveFreq = 0.;
	}

	// Branch move freq
	// Take into account that there is multiple branch per block
	double avgBranchPerBlock = branchMoveFreq;
	branchMoveFreq = defaultFreqs[StatisticalModel::BRANCH_LENGTH]/pow(avgBranchPerBlock, 0.2);


	if(!categFB[OTHER_PARAMS_ID].blocks.empty()) {
		// Frequency of sampling per block containing branch length
		double freqPerBranchBlock = branchMoveFreq/(double)categFB[StatisticalModel::BRANCH_LENGTH].blocks.size();
		// We use this frequency as baseline. We divide this by 3. (nb of block per Coev cluster) and 2 to be sampled 2-time less frequently
		// PREVIOUS VALUES : 
		//otherMoveFreq = freqPerBranchBlock*categFB[OTHER_PARAMS_ID].blocks.size()/(3.*2.);
		//otherMoveFreq = std::min(branchMoveFreq/0.25, otherMoveFreq); // Insure that branch are sampled at least 25% of the time
		otherMoveFreq = freqPerBranchBlock*categFB[OTHER_PARAMS_ID].blocks.size()/(200.);
		otherMoveFreq = std::min(branchMoveFreq/0.9, otherMoveFreq); // Insure that branch are sampled at least 25% of the time
	} else {
		otherMoveFreq = 0.;
	}

	// Rate freq is always equals to RATE_FREQUENCY
	// Then we have to "distribute" the remainder
	double sum = treeMoveFreq + branchMoveFreq + otherMoveFreq;
	branchMoveFreq = (1-rateMoveFreq)*branchMoveFreq/sum;
	treeMoveFreq = (1-rateMoveFreq)*treeMoveFreq/sum;
	otherMoveFreq = (1-rateMoveFreq)*otherMoveFreq/sum;

	/*std::cout << "<Block selection frequency> tree move = " << treeMoveFreq;
	std::cout << " - rate = " << rateMoveFreq;
	std::cout << " - branch = " << branchMoveFreq;
	std::cout << " - other = " << otherMoveFreq;
	std::cout << std::endl;
	std::cout << "**************************************" << std::endl;*/


	return categFB;
}

} /* namespace Strategies */
} /* namespace Sampler */
