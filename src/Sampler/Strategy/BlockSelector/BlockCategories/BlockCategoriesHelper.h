//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockCategoriesHelper.h
 *
 * @date Apr 12, 2017
 * @author meyerx
 * @brief
 */
#ifndef BLOCKCATGORIESHELPER_H_
#define BLOCKCATGORIESHELPER_H_

#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/Blocks.h"

namespace ParameterBlock { class Block; }
namespace ParameterBlock { class Blocks; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;
using ParameterBlock::Blocks;
using StatisticalModel::Parameters;


struct FrequenciesAndBlocks {
	double categoryFreq;
	std::vector<double> frequencies;
	std::vector<Block::sharedPtr_t> blocks;

	FrequenciesAndBlocks() : categoryFreq(0.) {}
	~FrequenciesAndBlocks(){}
};

typedef std::vector<FrequenciesAndBlocks> vecFreqBlocks_t;

class BlockCategoriesHelper {
public:
	BlockCategoriesHelper(const std::vector<double> aDefFrequencies);
	~BlockCategoriesHelper();


	vecFreqBlocks_t defineCategForTreeInf(const Parameters &params, const Blocks &blocks);

private:
	std::vector<double> defaultFreqs;

};

} /* namespace Strategies */
} /* namespace Sampler */

#endif /* BLOCKCATGORIESHELPER_H_ */
