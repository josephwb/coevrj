//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SingleCyclicPS.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "SingleCyclicBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Parallel/Manager/MCMCManager.h"
#include "ParameterBlock/Blocks.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

SingleCyclicBS::SingleCyclicBS(const Parameters &aParams, const Blocks &aBlocks) :
		BlockSelector(aParams, aBlocks), lastBlockID(aBlocks.getFirstBlock()->getId()) {
}

SingleCyclicBS::~SingleCyclicBS() {
}

const Block::sharedPtr_t SingleCyclicBS::selectBlock(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	if(blocks.getNTotalBlocks() == 1) return blocks.getFirstBlock();

	Block::sharedPtr_t ptrBlock = blocks.getNextBlock(lastBlockID);
	if(ptrBlock == NULL) {
		ptrBlock = blocks.getFirstBlock();
	}

	lastBlockID = ptrBlock->getId();

	return ptrBlock;
}

const std::vector<size_t> SingleCyclicBS::selectBlocks(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	size_t nBlocks = Parallel::mcmcMgr().getNProposal();
	std::vector<size_t> blockIDs;

	for(size_t i=0; i<nBlocks; ++i){
		size_t nextBlockID = selectBlock(sample)->getId();
		blockIDs.push_back(nextBlockID);
	}

	return blockIDs;
}

void SingleCyclicBS::rewind(const int acceptId) {
	const size_t nBlock = Parallel::mcmcMgr().getNProposal();

	if(acceptId >= 0) {
		size_t nSteps = nBlock-(acceptId+1);

		for(size_t i=0; i<nSteps; ++i){
			Block::sharedPtr_t ptrBlock = blocks.getPreviousBlock(lastBlockID);
			if(ptrBlock == NULL) {
				ptrBlock = blocks.getLastBlock();
			}

			lastBlockID = ptrBlock->getId();
		}
	}
}

} // namespace Strategies
} // namespace Sampler

