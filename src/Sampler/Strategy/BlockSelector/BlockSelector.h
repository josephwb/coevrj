//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParameterSelector.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKSELECTOR_H_
#define BLOCKSELECTOR_H_

#include <stddef.h>
#include <sys/types.h>
#include <vector>
#include <boost/shared_ptr.hpp>

#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Block.h"
#include "ParameterBlock/Blocks.h"
#include "Utils/Uncopyable.h"

class RNG;
namespace ParameterBlock { class Blocks; }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

using namespace std;

namespace Sampler {
namespace Strategies {

using ParameterBlock::Block;
using ParameterBlock::Blocks;
using StatisticalModel::Parameters;

class BlockSelector : private Uncopyable {
public:
	typedef boost::shared_ptr<BlockSelector> sharedPtr_t;

public:
	BlockSelector(const Parameters &aParams, const Blocks &aBlocks);
	virtual ~BlockSelector();

	virtual const ParameterBlock::Block::sharedPtr_t selectBlock(Sample &sample) = 0;
	virtual const vector<size_t> selectBlocks(Sample & sample) = 0;
	virtual void rewind(const int acceptId) = 0;

	RNG* getRNG() const { return m_rng; }

	static sharedPtr_t createSingleCyclic(const Parameters &aParams, const Blocks &aBlocks);
	static sharedPtr_t createSingleRevCyclic(const Parameters &aParams, const Blocks &aBlocks);
	static sharedPtr_t createRandom(const Parameters &aParams, const Blocks &aBlocks, const uint aSeed);
	static sharedPtr_t createOptimised(const Parameters &aParams, const Blocks &aBlocks, const uint aSeed, const size_t aNTimePerBlock);
	static sharedPtr_t createRandomTreeInference(const Parameters &aParams, const Blocks &aBlocks, const double aFreqTreeMove, const uint aSeed);
	static sharedPtr_t createOptimisedTreeInference(const Parameters &aParams, const Blocks &aBlocks, const double aFreqTreeMove, const uint aSeed);
	static sharedPtr_t createSingle(const Parameters &aParams, const Blocks &aBlocks, const uint aBId);
	static sharedPtr_t createSingleParam(const Parameters &aParams, const Blocks &aBlocks, const uint aPId, const uint aBId);

protected:
	const Parameters& params;
	const Blocks &blocks;
	RNG *m_rng;

	std::vector<double> getFrequencies(std::vector<Block::sharedPtr_t> &vecBlocks);
	Block::sharedPtr_t drawABlock(std::vector<Block::sharedPtr_t> &vecBlocks, std::vector<double> &frequencies);
};

} // namespace Strategies
} // namespace Sampler


#endif /* BLOCKSELECTOR_H_ */
