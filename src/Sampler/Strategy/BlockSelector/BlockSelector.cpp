//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ParameterSelector.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "BlockSelector.h"
#include "OptimisedBS.h"
#include "OptimisedTIBS.h"
#include "RandomBS.h"
#include "RandomTreeInferenceBS.h"
#include "SingleBS.h"
#include "SingleCyclicBS.h"
#include "SingleParamBS.h"
#include "SingleRevCyclicBS.h"

namespace ParameterBlock { class Blocks; }
namespace StatisticalModel { class Parameters; }


namespace Sampler {
namespace Strategies {

BlockSelector::BlockSelector(const Parameters &aParams, const Blocks &aBlocks) :
		params(aParams), blocks(aBlocks) {
	m_rng = NULL;
}

BlockSelector::~BlockSelector() {

}


BlockSelector::sharedPtr_t BlockSelector::createSingleCyclic(const Parameters &aParams, const Blocks &aBlocks){
	return sharedPtr_t(new SingleCyclicBS(aParams, aBlocks));
}

BlockSelector::sharedPtr_t BlockSelector::createSingleRevCyclic(const Parameters &aParams, const Blocks &aBlocks){
	return sharedPtr_t(new SingleRevCyclicBS(aParams, aBlocks));
}

BlockSelector::sharedPtr_t BlockSelector::createRandom(const Parameters &aParams, const Blocks &aBlocks, const uint aSeed){
	return sharedPtr_t(new RandomBS(aParams, aBlocks, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createOptimised(const Parameters &aParams, const Blocks &aBlocks, const uint aSeed, const size_t aNTimePerBlock){
	return sharedPtr_t(new OptimisedBS(aParams, aBlocks, aSeed, aNTimePerBlock));
}

BlockSelector::sharedPtr_t BlockSelector::createRandomTreeInference(const Parameters &aParams, const Blocks &aBlocks, const double aFreqTreeMove, const uint aSeed){
	return sharedPtr_t(new RandomTreeInferenceBS(aParams, aBlocks, aFreqTreeMove, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createOptimisedTreeInference(const Parameters &aParams, const Blocks &aBlocks, const double aFreqTreeMove, const uint aSeed){
	return sharedPtr_t(new OptimisedTIBS(aParams, aBlocks, aFreqTreeMove, aSeed));
}

BlockSelector::sharedPtr_t BlockSelector::createSingle(const Parameters &aParams, const Blocks &aBlocks, const uint aBId){
	return sharedPtr_t(new SingleBS(aParams, aBlocks, aBId));
}

BlockSelector::sharedPtr_t BlockSelector::createSingleParam(const Parameters &aParams, const Blocks &aBlocks, const uint aPId, const uint aBId){
	return sharedPtr_t(new SingleParamBS(aParams, aBlocks, aPId, aBId));
}

std::vector<double> BlockSelector::getFrequencies(std::vector<Block::sharedPtr_t> &vecBlocks) {
	std::vector<double> frequencies(vecBlocks.size());
	for(size_t iB=0; iB<vecBlocks.size(); ++iB) {
		frequencies[iB] = vecBlocks[iB]->getTotalFreq();
	}
	return frequencies;
}

Block::sharedPtr_t BlockSelector::drawABlock(std::vector<Block::sharedPtr_t> &vecBlocks, std::vector<double> &frequencies) {
	size_t iBlock = m_rng->drawFrom(frequencies);
	return vecBlocks[iBlock];
}

} // namespace Strategies
} // namespace Sampler

