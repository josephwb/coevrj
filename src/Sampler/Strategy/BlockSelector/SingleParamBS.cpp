//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SingleParamBS.cpp
 *
 * @date Jan 29, 2014
 * @author meyerx
 * @brief
 */
#include "SingleParamBS.h"

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "ParameterBlock/Blocks.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

SingleParamBS::SingleParamBS(const Parameters &aParams, const Blocks &aBlocks,
							 const int aParId, const int aBId) :
		BlockSelector(aParams, aBlocks), parId(aParId), bId(aBId) {
	block->addParameter(blocks.getBlock(bId)->getPIndices()[aParId]);
	ParameterBlock::BlockModifier::sharedPtr_t bm = blocks.getBlock(bId)->getBlockModifier(aParId);
	block->setBlockModifier(0, bm);
}

SingleParamBS::~SingleParamBS() {

}

const Block::sharedPtr_t SingleParamBS::selectBlock(Sample &sample) {
	return block;
}

const vector<size_t> SingleParamBS::selectBlocks(Sample & sample) {
	std::vector<size_t> blockIDs;
	cerr << "Cannot be implemented (SingleParamBS::selectBlocks)" << endl;
	abort();
	return blockIDs;
}

void SingleParamBS::rewind(const int acceptId) {
	return;
}

} // namespace Strategies
} // namespace Sampler

