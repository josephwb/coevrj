//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SingleRevCyclicPS.cpp
 *
 *  Created on: 24 sept. 2013
 *      Author: meyerx
 */

#include "SingleRevCyclicBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Parallel/Manager/MCMCManager.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

SingleRevCyclicBS::SingleRevCyclicBS(const Parameters &aParams, const Blocks &aBlocks) :
		BlockSelector(aParams, aBlocks), lastBlockID(aBlocks.getFirstBlock()->getId()), direction(UPWARD) {

}

SingleRevCyclicBS::~SingleRevCyclicBS() {
}

const Block::sharedPtr_t SingleRevCyclicBS::selectBlock(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	if(blocks.getNTotalBlocks() == 1) return blocks.getFirstBlock();

	Block::sharedPtr_t ptrBlock;
	if(direction == UPWARD){
		// Get next block
		ptrBlock = blocks.getNextBlock(lastBlockID);

		if(ptrBlock == NULL) { // There is no next, changing direction
			direction = DOWNWARD;
			// When changing direction, the extremity blocks should be called twice
			ptrBlock = blocks.getBlock(lastBlockID);
		} else {					// There is a next block
			lastBlockID = ptrBlock->getId();
		}
	} else if(direction == DOWNWARD) {
		// Get previous block
		ptrBlock = blocks.getPreviousBlock(lastBlockID);

		if(ptrBlock == NULL) { 		// There is no previous, changing direction
			direction = UPWARD;
			// When changing direction, the extremity blocks should be called twice
			ptrBlock = blocks.getBlock(lastBlockID);
		} else {					// There is a previous block
			lastBlockID = ptrBlock->getId();
		}
	}

	// If the ptrBlock is still null, it means that:
	// 1) the direction has changed
	// 2) the block has been deleted since then
	// We then get the closest one
	if(ptrBlock == NULL) {
		if(direction == UPWARD) {
			ptrBlock = blocks.getNextBlock(lastBlockID);
		} else { // direction == DOWNWARD
			ptrBlock = blocks.getPreviousBlock(lastBlockID);
		}
	}

	return ptrBlock;
}

const std::vector<size_t> SingleRevCyclicBS::selectBlocks(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);

	size_t nBlocks = Parallel::mcmcMgr().getNProposal();
	std::vector<size_t> blockIDs;
	if(blocks.getNTotalBlocks() == 1) {
		blockIDs.assign(nBlocks, blocks.getFirstBlock()->getId());
	} else {
		for(size_t iB=0; iB < nBlocks; ++iB) {
			size_t nextBlockID = selectBlock(sample)->getId();
			blockIDs.push_back(nextBlockID);
		}
	}
	return blockIDs;
}

void SingleRevCyclicBS::rewind(const int acceptId) {

	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	size_t nSteps = 0;
	if(acceptId >= 0) {
		// Define nb steps
		nSteps = nBlock-(acceptId+1);
		// save direction
		direction_t savedDirection = direction;
		// swap state
		direction = (direction == UPWARD) ? DOWNWARD : UPWARD;
		// Fake sample, no impact.
		Sample dummySample;
		for(size_t i=0; i<nSteps; ++i){
			lastBlockID = selectBlock(dummySample)->getId();
		}
		direction = savedDirection;
	}
}

} // namespace Strategies
} // namespace Sampler

