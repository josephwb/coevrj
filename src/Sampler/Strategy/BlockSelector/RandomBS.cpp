//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomBS.cpp
 *
 * @date Jan 9, 2014
 * @author meyerx
 * @brief
 */
#include "RandomBS.h"

#include <assert.h>

#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Parallel/RNG/RNG.h"
#include "ParameterBlock/Blocks.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Sampler {
namespace Strategies {

RandomBS::RandomBS(const Parameters &aParams, const Blocks &aBlocks, const uint aSeed) :
		BlockSelector(aParams, aBlocks) {
	m_rng = RNG::createSitmo11RNG(aSeed);
}

RandomBS::~RandomBS() {
}

const Block::sharedPtr_t RandomBS::selectBlock(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);
	if(blocks.getNTotalBlocks() == 1) return blocks.getFirstBlock();

	std::vector<Block::sharedPtr_t> vecBlocks = blocks.getBlocks();
	std::vector<double> freqs = getFrequencies(vecBlocks);

	return drawABlock(vecBlocks, freqs);
}

const std::vector<size_t> RandomBS::selectBlocks(Sample &sample) {
	assert(blocks.getNTotalBlocks()>0);

	size_t nBlocks = Parallel::mcmcMgr().getNProposal();
	std::vector<size_t> blockIDs;

	if(blocks.getNBaseBlocks() == 1) {
		blockIDs.assign(nBlocks, blocks.getFirstBlock()->getId());
	} else {
		std::vector<Block::sharedPtr_t> vecBlocks = blocks.getBlocks();
		std::vector<double> freqs = getFrequencies(vecBlocks);

		for(size_t i=0; i<nBlocks; ++i){
			blockIDs.push_back(drawABlock(vecBlocks, freqs)->getId());
		}
	}
	return blockIDs;
}

void RandomBS::rewind(const int acceptId) {
	const size_t nBlock = Parallel::mcmcMgr().getNProposal();
	uint nSteps = 0;
	if(acceptId >= 0) {
		nSteps = nBlock-(acceptId+1);
		m_rng->rewind(nSteps);
	}
}

size_t RandomBS::getSeed() const {
	return m_rng->getSeed();
}

} // namespace Strategies
} // namespace Sampler

