//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SamplerState.h
 *
 * @date Dec 19, 2016
 * @author meyerx
 * @brief
 */
#ifndef SAMPLERSTATE_H_
#define SAMPLERSTATE_H_

#include <stddef.h>
#include <vector>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Sampler {
namespace State {

class SamplerState {
public:
	SamplerState(size_t &aIT);
	~SamplerState();

	size_t getIT() const;

	double getInverseTemperature() const;
	void setInverseTemperature(double aInverseTemperature);

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t &iT;
	double inverseTemperature;

};

} /* namespace State */
} /* namespace Sampler */

#endif /* SAMPLERSTATE_H_ */
