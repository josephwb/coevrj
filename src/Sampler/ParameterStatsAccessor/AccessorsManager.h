//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AccessorsManager.h
 *
 * @date Jan 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef ACCESSORSMANAGER_H_
#define ACCESSORSMANAGER_H_

#include <assert.h>
#include <vector>

#include "BaseAccessor.h"

namespace Sampler {
namespace PStatsAccessor {

class AccessorsManager : public BaseAccessor {
public:
	AccessorsManager();
	~AccessorsManager();

	// For mandatory accesses
	double getMean(size_t pId);
	double getVariance(size_t pId);
	double getScaling(size_t pId);
	double getCovariance(size_t pId1, size_t pId2);

	// For optional accesses
	queryResult_t requestMean(size_t pId);
	queryResult_t requestVariance(size_t pId);
	queryResult_t requestScaling(size_t pId);
	queryResult_t requestCovariance(size_t pId1, size_t pId2);

	// Register accessors
	void registerProposalAccessor(BaseAccessor* aAccessor);
	void registerGlobalAccessor(BaseAccessor* aAccessor);

private:

	BaseAccessor* globalAccessor;
	std::vector< BaseAccessor* > proposalAccessors;

};

} /* namespace PStatsAccessor */
} /* namespace Sampler */

#endif /* ACCESSORSMANAGER_H_ */
