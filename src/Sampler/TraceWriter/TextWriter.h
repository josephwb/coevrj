//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TextWriter.h
 *
 * @date Dec 6, 2016
 * @author meyerx
 * @brief
 */
#ifndef TEXTWRITER_H_
#define TEXTWRITER_H_

#include <stddef.h>

#include "BaseWriter.h"

namespace Sampler { class SampleVector; }
namespace Sampler { namespace Checkpoint { class CheckpointManager; } }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace TraceWriter {

class TextWriter: public BaseWriter {
public:
	typedef boost::shared_ptr< TextWriter > sharedPtr_t;

public:
	TextWriter(const std::string &aPrefix, const std::string &aSuffix, size_t aThinning);
	~TextWriter();

	void writeHeader(SampleVector &samples, Checkpoint::CheckpointManager &ckpManager, StatisticalModel::Model &model);
	void writeSamples(const size_t offset, SampleVector &samples, StatisticalModel::Model &model);

};

} /* namespace TraceWriter */
} /* namespace Sampler */

#endif /* TEXTWRITER_H_ */
