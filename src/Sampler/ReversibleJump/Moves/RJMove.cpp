//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJMove.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "RJMove.h"

#include <assert.h>

namespace Sampler { namespace Proposal { class BlockDispatcher; } }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace RJMCMC {

RJMove::RJMove(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample) :
		moveDirection(aMoveDirection), currentSample(aCurrentSample),
		proposedSample(aCurrentSample) {

	moveState = UNAPPLIED;

	proposedSample.setEvaluated(false);

	fwProposalProb = bwProposalProb = 1.;
	fwMoveProb = bwMoveProb = 1.;
	fwRandomProb = bwRandomProb = 1.;
	jacobian = 1.;
}

RJMove::~RJMove() {
}

void RJMove::setForwardProposalProb(double aFwProposalProp) {
	fwProposalProb = aFwProposalProp;
}

void RJMove::setBackwardProposalProb(double aBwProposalProp) {
	bwProposalProb = aBwProposalProp;
}

void RJMove::setForwardMoveProb(double aFwMoveProb) {
	fwMoveProb = aFwMoveProb;
}

void RJMove::setBackwardMoveProb(double aBwMoveProb) {
	bwMoveProb = aBwMoveProb;
}

void RJMove::setForwardRandomProb(double aFwRandomProb) {
	fwRandomProb = aFwRandomProb;
}

void RJMove::setBackwardRandomProb(double aBwRandomProb) {
	bwRandomProb = aBwRandomProb;
}

void RJMove::setJacobian(double aJacobian) {
	jacobian = aJacobian;
}

double RJMove::getForwardProposalProb() {
	return fwProposalProb;
}

double RJMove::getBackwardProposalProb() {
	return bwProposalProb;
}

double RJMove::getForwardMoveProb() {
	return fwMoveProb;
}

double RJMove::getBackwardMoveProb() {
	return bwMoveProb;
}

double RJMove::getForwardRandomProb() {
	return fwRandomProb;
}

double RJMove::getBackwardRandomProb() {
	return bwRandomProb;
}

double RJMove::getRatioProposalProb() {
	if(moveDirection == FORWARD || moveDirection == NOT_TRANSDIM) {
		return bwProposalProb/fwProposalProb;
	} else {
		return fwProposalProb/bwProposalProb;
	}
}

double RJMove::getRatioRandomProb() {
	if(moveDirection == FORWARD || moveDirection == NOT_TRANSDIM) {
		return bwRandomProb/fwRandomProb;
	} else {
		return fwRandomProb/bwRandomProb;
	}
}

double RJMove::getRatioMoveProb() {
	if(moveDirection == FORWARD || moveDirection == NOT_TRANSDIM) {
		return bwMoveProb/fwMoveProb;
	} else {
		return fwMoveProb/bwMoveProb;
	}
}

double RJMove::getJacobian() {
	if(moveDirection == FORWARD || moveDirection == NOT_TRANSDIM) {
		return jacobian;
	} else {
		return 1./jacobian;
	}
}

std::vector<size_t>& RJMove::getUpdatedParametersID() {
	return updatedParameters;
}

Sampler::Sample& RJMove::getProposedSample() {
	return proposedSample;
}

// Have to be called first - enable temporary changes
bool RJMove::applyMove(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
			 	 	   Proposal::BlockDispatcher &blockDispatcher) {

	bool doMonitorRJ_DBG = false;

	if(moveState != IMPOSSIBLE) {
		assert(moveState == UNAPPLIED);
		doMonitorRJ_DBG = doApplyMove(listSSInfo, model, blockDispatcher);
		moveState = APPLIED;
	}

	return doMonitorRJ_DBG;
}

// Have to be called second - let the choice between removing temporary changes or adapting lik
bool RJMove::signalAccepted(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
							Proposal::BlockDispatcher &blockDispatcher) {
	bool doMonitorRJ_DBG = false;

	assert(moveState != IMPOSSIBLE);
	assert(moveState == APPLIED);
	doMonitorRJ_DBG = doSignalAccepted(listSSInfo, model, blockDispatcher);
	moveState = ACCEPTED;

	return doMonitorRJ_DBG;
}

bool RJMove::signalRejected(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
							Proposal::BlockDispatcher &blockDispatcher) {

	bool doMonitorRJ_DBG = false;

	assert(moveState == IMPOSSIBLE || moveState == APPLIED);
	doMonitorRJ_DBG = doSignalRejected(listSSInfo, model, blockDispatcher);
	moveState = REJECTED;

	return doMonitorRJ_DBG;
}



RJMove::moveState_t RJMove::getMoveState() const {
	return moveState;
}

void RJMove::updateSamplesForInit(const Sampler::Sample &aCurrentSample) {
	currentSample = aCurrentSample;
	proposedSample = currentSample;
	proposedSample.setEvaluated(false);
}

/*template<class Archive>
void RJMove::save(Archive & ar, const unsigned int version) const {

	ar << moveDirection;

	ar << fwMoveProb;
	ar << bwMoveProb;

	ar << fwRandomProb;
	ar << bwRandomProb;
	ar << jacobian;

	ar << updatedParameters;

	ar << createdSSRessources;
	ar << deletedSSRessources;

}*/

/*template<class Archive>
void RJMove::load(Archive & ar, const unsigned int version) {

	ar >> moveDirection;

	ar >> fwMoveProb;
	ar >> bwMoveProb;

	ar >> fwRandomProb;
	ar >> bwRandomProb;
	ar >> jacobian;

	ar >> updatedParameters;

	ar >> createdSSRessources;
	ar >> deletedSSRessources;

}*/

/*template void RJMove::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void RJMove::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void RJMove::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void RJMove::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);*/

} /* namespace RJMCMC */
} /* namespace Sampler */
