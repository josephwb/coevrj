//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJMove.h
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef RJMOVE_H_
#define RJMOVE_H_

#include <boost/shared_ptr.hpp>
#include <vector>

#include "Model/Parameter/Parameters.h"
#include "ParameterBlock/Blocks.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"
#include "Sampler/Samples/Sample.h"

namespace Sampler { namespace Proposal { class BlockDispatcher; } }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace RJMCMC {

class RJMove {
public:
	typedef boost::shared_ptr<RJMove> sharedPtr_t;

	enum moveDirection_t {FORWARD, BACKWARD, NOT_TRANSDIM};
	enum moveState_t {UNAPPLIED, APPLIED, ACCEPTED, REJECTED, IMPOSSIBLE};

public:
	RJMove(moveDirection_t aMoveDirection, Sampler::Sample &aCurrentSample);
	virtual ~RJMove();

	// Value from move
	void setForwardProposalProb(double aFwProposalProp);
	void setBackwardProposalProb(double aBwProposalProp);
	void setForwardMoveProb(double aFwMoveProp);
	void setBackwardMoveProb(double aBwMoveProp);
	void setForwardRandomProb(double aFwRandomProp);
	void setBackwardRandomProb(double aBwRandomProp);
	void setJacobian(double aJacobian);

	// Get probabilities
	double getForwardProposalProb();
	double getBackwardProposalProb();
	double getForwardMoveProb();
	double getBackwardMoveProb();
	double getForwardRandomProb();
	double getBackwardRandomProb();

	// Get ratios and Jacobian
	double getRatioProposalProb();
	double getRatioMoveProb();
	double getRatioRandomProb();
	double getJacobian();

	// Change on the model
	std::vector<size_t>& getUpdatedParametersID();
	Sampler::Sample& getProposedSample();

	// Modifiers
	// Have to be called first - enable temporary changes
	bool applyMove(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
				   Proposal::BlockDispatcher &blockDispatcher);

	// Have to be called second - let the choice between removing temporary changes or adapting lik
	bool signalAccepted(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
						Proposal::BlockDispatcher &blockDispatcher);
	bool signalRejected(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
						Proposal::BlockDispatcher &blockDispatcher);

	// Get the state
	moveState_t getMoveState() const;

	// Serialize calls
	//virtual void load(const Utils::Serialize::buffer_t &buffer) = 0;
	//virtual Utils::Serialize::buffer_t save() const = 0;

	// update the current sample values (hack for RJMCMC init)
	void updateSamplesForInit(const Sampler::Sample &aCurrentSample);

protected:

	moveState_t moveState;
	moveDirection_t moveDirection;

	double fwProposalProb, bwProposalProb;
	double fwMoveProb, bwMoveProb;
	double fwRandomProb, bwRandomProb;
	double jacobian;

	std::vector<size_t> updatedParameters;

	Sampler::Sample currentSample;
	Sampler::Sample proposedSample;

	// Have to be called first - enable temporary changes
	virtual bool doApplyMove(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
							 Proposal::BlockDispatcher &blockDispatcher) = 0;

	// Have to be called second - let the choice between removing temporary changes or adapting lik
	virtual bool doSignalAccepted(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
								  Proposal::BlockDispatcher &blockDispatcher) = 0;
	virtual bool doSignalRejected(RJSubSpaceInfo::listSSInfo_t &listSSInfo, StatisticalModel::Model &model,
								  Proposal::BlockDispatcher &blockDispatcher) = 0;

	// Serialization
	//friend class boost::serialization::access;

	//template<class Archive>
	//void save(Archive & ar, const unsigned int version) const;

	//template<class Archive>
	//void load(Archive & ar, const unsigned int version);
	//BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace RJMCMC */
} /* namespace Sampler */

#endif /* RJMOVE_H_ */
