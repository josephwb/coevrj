//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJMoveManager.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "RJMoveManager.h"

#include "Sampler/ReversibleJump/Proposals/RJMoveProposal.h"
#include "Parallel/RNG/RNG.h"

namespace Sampler {
namespace RJMCMC {

RJMoveManager::RJMoveManager(size_t aSeed) :
		rng(RNG::createSitmo11RNG(aSeed)) {
}

RJMoveManager::~RJMoveManager() {
}

void RJMoveManager::addProposal(RJMoveProposal::sharedPtr_t aProposal) {
	aProposal->setRNG(rng);
	proposals.push_back(aProposal);
}

RJMoveProposal::sharedPtr_t RJMoveManager::drawProposal() {
	std::vector<double> frequencies(proposals.size());
	for(size_t iP=0; iP<proposals.size(); ++iP) {
		frequencies[iP] = proposals[iP]->getFrequency();
	}
	// Frequencies are normalized in this function
	int iProposal = rng->drawFrom(frequencies);

	// Return the proposal
	return proposals[iProposal];
}

RNG* RJMoveManager::getRng() {
	return rng;
}

std::string RJMoveManager::getStringStats(std::string &prefix) const {
	std::stringstream ss;
	for(size_t iP=0; iP<proposals.size(); ++iP) {
		ss << proposals[iP]->getStringStats(prefix);

	}
	return ss.str();
}


} /* namespace RJMCMC */
} /* namespace Sampler */
