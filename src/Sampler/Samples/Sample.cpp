//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sample.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Sample.h"

#include <assert.h>

#include "Utils/Code/BinSerializationSupport.h" // IWYU pragma: keep
#include "Sampler/Samples/ModelSpecific/ModelSpecificSample.h"

namespace Sampler {

Sample::Sample() :  changed(true), evaluated(false), prior(0.0), likelihood(0.0), posterior(0.0) {
}

Sample::Sample(const Sample &toCopy) {
	copy(toCopy);
}

Sample::~Sample() {
}

Sample& Sample::operator=(const Sample &toCopy) {
	copy(toCopy);
    return *this;
}

bool Sample::isInteger(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return true;
		} else {
			return false;
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		return modelSpecificSamples[vLoc.indMSS].mss.isInteger(vLoc.indLocalVal);
	}
}

bool Sample::isDouble(int aInd) const {
	return !isInteger(aInd);
}

void Sample::incParameter(int aInd, double aIncr){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] += aIncr;
		} else {
			dblValues[aInd-ivSize] += aIncr;
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		modelSpecificSamples[vLoc.indMSS].mss.incParameter(vLoc.indLocalVal, aIncr);
	}
}

void Sample::multParameter(int aInd, double aMult){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] *= aMult;
		} else {
			dblValues[aInd-ivSize] *= aMult;
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		modelSpecificSamples[vLoc.indMSS].mss.multParameter(vLoc.indLocalVal, aMult);
	}
}

double Sample::getDoubleParameter(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return intValues[aInd];
		} else {
			return dblValues[aInd-ivSize];
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		return modelSpecificSamples[vLoc.indMSS].mss.getDoubleParameter(vLoc.indLocalVal);
	}
}

void Sample::setDoubleParameter(int aInd, double aVal){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] = aVal;
		} else {
			dblValues[aInd-ivSize] = aVal;
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		modelSpecificSamples[vLoc.indMSS].mss.setDoubleParameter(vLoc.indLocalVal, aVal);
	}
}

long int Sample::getIntParameter(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return intValues[aInd];
		} else {
			return dblValues[aInd-ivSize];
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		return modelSpecificSamples[vLoc.indMSS].mss.getIntParameter(vLoc.indLocalVal);
	}
}

void Sample::setIntParameter(int aInd, long int aVal){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] = aVal;
		} else {
			dblValues[aInd-ivSize] = aVal;
		}
	} else {
		ValueLocator vLoc(getValueLocator(aInd));
		modelSpecificSamples[vLoc.indMSS].mss.setIntParameter(vLoc.indLocalVal, aVal);
	}
}

void Sample::registerMSS(const ModelSpecificSample &aMSS) {
	size_t insertPos = 0;
	bool isRegistered = false;

	// Is there an empty spot ?
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(modelSpecificSamples[iM].free) {
			insertPos = iM;
			isRegistered = true;
			modelSpecificSamples[iM].free = false;
			modelSpecificSamples[iM].mss = aMSS;
			break;
		}
	}

	// No empty spot, add at the end
	if(!isRegistered) {
		insertPos = modelSpecificSamples.size();
		isRegistered = true;
		modelSpecificSamples.push_back(MonitoredMSS(false, aMSS));
	}

	// Register the parameters
	for(size_t iP=0; iP<aMSS.pInd.size(); ++iP) {
		size_t key = aMSS.pInd[iP];
		ValueLocator valLocator(insertPos, iP);
		valueLocators.insert(std::make_pair(key, valLocator));
	}

	//std::cout << "Inserted MSS : " << insertPos << " --> " << aMSS.getLabelSubSpaceUID() << std::endl; // TODO REMOVE

}

void Sample::resetPIndMSS(std::vector< std::pair< Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t, std::vector<size_t> > > &newPInd) {

	valueLocators.clear();

	for(size_t iP=0; iP<newPInd.size(); ++iP) {

		// Get the positions
		//std::cout << "Searching : " << newPInd[iP].first->getLabelUID() << std::endl;
		bool found = false;
		size_t pos = 0;
		for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
			if(!modelSpecificSamples[iM].free) {
				if(modelSpecificSamples[iM].mss.labelSubSpaceUID == newPInd[iP].first->getLabelUID()) {
					pos = iM;
					found = true;
				}
			}
		}

		// If there is no MSS and no parameters, just exit this function.
		if(!found) {
			assert(newPInd[iP].second.empty() && "There should be an existing MSS for this label.");
			continue;
		}

		// Update new pInd
		ModelSpecificSample& mss = modelSpecificSamples[pos].mss;
		assert(mss.pInd.size() == newPInd[iP].second.size());
		mss.pInd = newPInd[iP].second;

		// Register new pInd
		for(size_t iP=0; iP<mss.pInd.size(); ++iP) {
			size_t key = mss.pInd[iP];
			ValueLocator valLocator(pos, iP);
			valueLocators.insert(std::make_pair(key, valLocator));
			//std::cout << "Add : " << mss.pInd[iP] << std::endl;
		}
	}
}



void Sample::removeMSS(Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t aUidSubSpace) {
	size_t removePos = 0;
	bool isUnregistered = false;

	// Is there an empty spot ?
	size_t cntOccupied = 0;
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(!modelSpecificSamples[iM].free) {
			if(modelSpecificSamples[iM].mss.labelSubSpaceUID == aUidSubSpace->getLabelUID()) {
				removePos = iM;
				isUnregistered = true;
				modelSpecificSamples[iM].free = true;
			} else {
				cntOccupied++;
			}
		}
	}

	assert(isUnregistered);
	for(size_t iP=0; iP<modelSpecificSamples[removePos].mss.pInd.size(); ++iP) {
		size_t key = modelSpecificSamples[removePos].mss.pInd[iP];
		size_t nErased = valueLocators.erase(key);
		assert(nErased >= 1);
	}

	// If there is less than 50% occupation, ask for a cleanup
	if(modelSpecificSamples.size() > 10 && cntOccupied*2 < modelSpecificSamples.size()) {
		cleanupMSS();
	}

	//std::cout << "Removed MSS : " << removePos << " --> " << aUidSubSpace->getLabelUID() << std::endl; // TODO REMOVE
}

ModelSpecificSample& Sample::getMSS(Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t aUidSubSpace) {
	return const_cast<ModelSpecificSample&>(static_cast<const Sample*>(this)->getMSS(aUidSubSpace));
}

const ModelSpecificSample& Sample::getMSS(Sampler::RJMCMC::RJSubSpaceUID::sharedPtr_t aUidSubSpace) const {

	bool found = false;
	size_t iMSS = 0;
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(!modelSpecificSamples[iM].free && modelSpecificSamples[iM].mss.labelSubSpaceUID == aUidSubSpace->getLabelUID()) {
			found = true;
			iMSS = iM;
		}
	}
	if(!found) {
		std::cout << "[ERROR] Searched the MSS with UID : " << aUidSubSpace->getLabelUID() << std::endl;
		std::cout << "Existing UID are : " << std::endl;
		for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
			if(!modelSpecificSamples[iM].free) {
				std::cout << modelSpecificSamples[iM].mss.labelSubSpaceUID << std::endl;
			}
		}
		assert(found);
	}

	return modelSpecificSamples[iMSS].mss;
}

void Sample::cleanupMSS() {
	size_t insertPos=0;
	vecMSS_t tmpMSS;

 	// For each element
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		// If the element is occupied
		if(!modelSpecificSamples[iM].free) {

			// Put it at the end of the tmp container
			tmpMSS.push_back(modelSpecificSamples[iM]);

			// Change the mapping
			for(size_t iI=0; iI<tmpMSS.back().mss.pInd.size(); ++iI) {
				size_t key = tmpMSS.back().mss.pInd[iI];
				valueLocators[key].indMSS = insertPos;
			}

			// Increment the insertPos pointer
			insertPos++;
		}
	}

	// Swap containers
	std::swap(modelSpecificSamples, tmpMSS);
}

std::vector<ModelSpecificSample> Sample::getAllMSS() const {
	std::vector<ModelSpecificSample> allMSS;
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(!modelSpecificSamples[iM].free) {
			allMSS.push_back(modelSpecificSamples[iM].mss);
		}
	}
	return allMSS;
}

std::vector<size_t> Sample::getModelSpecificPInd() const {
	std::vector<size_t> mssPInd;
	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(!modelSpecificSamples[iM].free) {
			mssPInd.insert(mssPInd.end(), modelSpecificSamples[iM].mss.pInd.begin(), modelSpecificSamples[iM].mss.pInd.end());
		}
	}
	return mssPInd;
}


size_t Sample::getNbParameters() const {
	return intValues.size() + dblValues.size();
}

void Sample::write(ostream &oFile, char sep) const {
	oFile << prior << sep;

	// Get rid of minus inf when LOG LIK
	if(likelihood == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max() << sep;
	} else {
		oFile << likelihood << sep;
	}

	// Get rid of minus inf when LOG LIK
	if(posterior == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max();
	} else {
		oFile << posterior;
	}

	for(size_t i=0; i<markers.size(); ++i){
		oFile << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		oFile << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		oFile << sep << dblValues[i];
	}

	oFile << endl;
}

void Sample::writeBin(ostream &oFile) const {
	// Write Prior, Likelihood, Posterior
	oFile.write(reinterpret_cast<const char*>(&prior), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&likelihood), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&posterior), sizeof(double));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&markers[0]), markers.size()*sizeof(double));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&dblValues[0]), dblValues.size()*sizeof(double));
}

void Sample::writeBinFloat(ostream &oFile) const {

	float tmp;
	vector<float> tmpMarker(markers.begin(), markers.end());
	vector<float> tmpDbl(dblValues.begin(), dblValues.end());
	// Write Prior, Likelihood, Posterior
	tmp = static_cast<float>(prior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(likelihood);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(posterior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&tmpMarker[0]), tmpMarker.size()*sizeof(float));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&tmpDbl[0]), tmpDbl.size()*sizeof(float));
}

string Sample::toString(char sep) const {
	stringstream ss;
//	ss << (changed ? "[t] " : "[f] "); // TODO REMOVE
	ss << prior << sep << likelihood << sep << posterior;

	for(size_t i=0; i<markers.size(); ++i){
		ss << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		ss << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		ss << sep << dblValues[i];
	}
	return ss.str();
}

void Sample::setMarkers(const std::vector<double> &aMarkers) {
	if(!aMarkers.empty()){
		markers = aMarkers;
	}
}

void Sample::setMSSMarkers(const SampleUtils::vecSSMarkers_t &aSSMarkers) {
	for(size_t iM=0; iM<aSSMarkers.size(); ++iM) {
		ModelSpecificSample &mss = getMSS(aSSMarkers[iM].first);
		mss.setMarkers(aSSMarkers[iM].second);
	}
}

void Sample::setChanged(bool aChanged) {
	changed = aChanged;
}

bool Sample::hasChanged() const {
	return changed;
}

void Sample::setEvaluated(bool aEvaluated) {
	evaluated = aEvaluated;
}

bool Sample::isEvaluated() const {
	return evaluated;
}

bool Sample::hasSameContinuousParameters(const Sample &other) const {
	for(size_t iX=0; iX<dblValues.size(); ++iX) {
		if(dblValues[iX] != other.dblValues[iX]) return false;
	}

	for(size_t iM=0; iM<modelSpecificSamples.size(); ++iM) {
		if(modelSpecificSamples[iM].free != other.modelSpecificSamples[iM].free) return false;
		if(!modelSpecificSamples[iM].free) {
			if(!modelSpecificSamples[iM].mss.hasSameContinuousParameter(other.modelSpecificSamples[iM].mss)) return false;
		}
	}
	return true;
}

Sample::ValueLocator Sample::getValueLocator(int aInd) const {
	mapValueLocators_t::const_iterator itFind = valueLocators.find(aInd);
	if(itFind == valueLocators.end()) std::cout << aInd << std::endl;
	assert(itFind != valueLocators.end());									// TODO remove when tested
	size_t indMSS = itFind->second.indMSS;
	size_t indLoc = itFind->second.indLocalVal;
	assert(!modelSpecificSamples[indMSS].free);								// TODO remove when tested
	assert(modelSpecificSamples[indMSS].mss.pInd[indLoc] == (size_t)aInd);	// TODO remove when tested
	return itFind->second;
}


void Sample::copy(const Sample &toCopy) {
	changed = toCopy.changed;
	evaluated = toCopy.evaluated;
	prior = toCopy.prior;
	likelihood = toCopy.likelihood;
	posterior = toCopy.posterior;
	intValues = toCopy.intValues;
	dblValues = toCopy.dblValues;
	markers = toCopy.markers;

	modelSpecificSamples = toCopy.modelSpecificSamples;
	valueLocators = toCopy.valueLocators;
}

Utils::Serialize::buffer_t Sample::save() const {
	Utils::Serialize::buffer_t serializedBuffer;
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}

void Sample::load(const Utils::Serialize::buffer_t &buffer) {
	Utils::Serialize::load(buffer, *this);
}

template<class Archive>
void Sample::save(Archive & ar, const unsigned int version) const {
	ar & changed;
	ar & evaluated;

	ar & prior;
	ar & likelihood;
	ar & posterior;

	ar & intValues;
	ar & dblValues;
	ar & markers;

	ar & modelSpecificSamples;

	ar & valueLocators;
}

template<class Archive>
void Sample::load(Archive & ar, const unsigned int version) {
	ar & changed;
	ar & evaluated;

	ar & prior;
	ar & likelihood;
	ar & posterior;

	ar & intValues;
	ar & dblValues;
	ar & markers;

	ar & modelSpecificSamples;

	ar & valueLocators;
}

} // namespace Sampler
