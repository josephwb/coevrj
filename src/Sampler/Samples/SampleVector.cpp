//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * SampleVector.cpp
 *
 *  Created on: 2 oct. 2013
 *      Author: meyerx
 */

#include "SampleVector.h"

#include "Sampler/Samples/Sample.h"

namespace Sampler {

SampleVector::SampleVector() : samples() {
}

SampleVector::SampleVector(int size, Sample defValue) : samples(size, defValue){
}

SampleVector::~SampleVector() {
}

SampleUtils::vecPairItSample_t SampleVector::write(size_t &cntAccepted, std::ostream &oFile, const size_t offset, const char sep, const size_t thinning) const {
	SampleUtils::vecPairItSample_t wrtSamples;
	for(size_t i=0; i<samples.size(); ++i){
		if(cntAccepted % thinning == 0) {
			oFile << i+offset << sep;
			samples[i].write(oFile, sep);
			wrtSamples.push_back(std::make_pair(i+offset, samples[i]));
		}
		++cntAccepted;
	}
	return wrtSamples;
}

SampleUtils::vecPairItSample_t SampleVector::writeAllButFront(size_t &cntAccepted, std::ostream &oFile, const size_t offset, const char sep, const size_t thinning) const {
	SampleUtils::vecPairItSample_t wrtSamples;
	for(size_t i=1; i<samples.size(); ++i){
		if(cntAccepted % thinning == 0) {
			oFile << i+offset-1 << sep;
			samples[i].write(oFile, sep);
			wrtSamples.push_back(std::make_pair(i+offset-1, samples[i]));
		}
		++cntAccepted;
		//}
	}
	return wrtSamples;
}

SampleUtils::vecPairItSample_t SampleVector::writeBin(size_t &cntAccepted, std::ostream &oFile, const size_t offset, const char sep, const size_t thinning, const bool writeFloat) const {
	SampleUtils::vecPairItSample_t wrtSamples;
	for(size_t i=0; i<samples.size(); ++i){
		size_t tmp = i+offset;
		if(samples[i].hasChanged()){
			if(cntAccepted % thinning == 0) {
				oFile.write(reinterpret_cast<char*>(&tmp), sizeof(size_t));
				if(writeFloat){
					samples[i].writeBinFloat(oFile);
				} else {
					samples[i].writeBin(oFile);
				}
				wrtSamples.push_back(std::make_pair(tmp, samples[i]));
			}
			++cntAccepted;
		}
	}
	return wrtSamples;
}

SampleUtils::vecPairItSample_t SampleVector::writeBinAllButFront(size_t &cntAccepted, std::ostream &oFile, const size_t offset, const char sep, const size_t thinning, const bool writeFloat) const {
	SampleUtils::vecPairItSample_t wrtSamples;
	for(size_t i=1; i<samples.size(); ++i){
		size_t tmp = i+offset-1;
		if(samples[i].hasChanged()){
			if(cntAccepted % thinning == 0) {
				oFile.write(reinterpret_cast<char*>(&tmp), sizeof(size_t));
				if(writeFloat){
					samples[i].writeBinFloat(oFile);
				} else {
					samples[i].writeBin(oFile);
				}
				wrtSamples.push_back(std::make_pair(tmp, samples[i]));
			}
			++cntAccepted;
		}
	}

	return wrtSamples;
}

string SampleVector::toString() const {
	std::stringstream ss;
	for(size_t i=0; i<samples.size(); ++i){
		ss << samples[i].toString() << endl;
	}
	return ss.str();
}

} // namespace Sampler


