//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proposals.h
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#ifndef PROPOSALS_H_
#define PROPOSALS_H_

#include <boost/serialization/access.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <stddef.h>
#include <vector>

#include "BaseProposal.h"

namespace Sampler {
namespace Proposal {

class Proposals {
public:
	Proposals();
	~Proposals();

	bool empty();
	size_t size();

	void addProposal(BaseProposal::sharedPtr_t aProp);
	BaseProposal::sharedPtr_t getProposal(size_t i);

	std::vector<BaseProposal::sharedPtr_t>& getProposals();
	const std::vector<BaseProposal::sharedPtr_t>& getProposals() const;

private:
	std::vector<BaseProposal::sharedPtr_t> vecProposals;

	// Boost serialization
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* PROPOSALS_H_ */
