//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proposals.cpp
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#include "Proposals.h"

#include <assert.h>

#include "Sampler/Proposal/BaseProposal.h"
#include <boost/serialization/nvp.hpp>

namespace Sampler {
namespace Proposal {

Proposals::Proposals() {
}

Proposals::~Proposals() {
}

bool Proposals::empty() {
	return vecProposals.empty();
}

size_t Proposals::size() {
	return vecProposals.size();
}

void Proposals::addProposal(BaseProposal::sharedPtr_t aProp) {
	vecProposals.push_back(aProp);
}

BaseProposal::sharedPtr_t Proposals::getProposal(size_t iProp) {
	assert(iProp < size());
	return vecProposals[iProp];
}

std::vector<BaseProposal::sharedPtr_t>& Proposals::getProposals() {
	return vecProposals;
}

const std::vector<BaseProposal::sharedPtr_t>& Proposals::getProposals() const {
	return vecProposals;
}

template<class Archive>
void Proposals::save(Archive & ar, const unsigned int version) const {

	std::vector< State::BaseProposalState::sharedPtr_t > pStates(vecProposals.size());
	for(size_t iP=0; iP<vecProposals.size(); ++iP) {
		pStates[iP] = vecProposals[iP]->saveState();
	}
	ar & BOOST_SERIALIZATION_NVP( pStates );
}

template<class Archive>
void Proposals::load(Archive & ar, const unsigned int version) {

	std::vector< State::BaseProposalState::sharedPtr_t > pStates;
	ar & BOOST_SERIALIZATION_NVP( pStates );
	assert(pStates.size() == vecProposals.size());

	for(size_t iP=0; iP<vecProposals.size(); ++iP) {
		vecProposals[iP]->loadState(pStates[iP]);
	}
}

template void Proposals::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void Proposals::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void Proposals::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void Proposals::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Proposal */
} /* namespace Sampler */
