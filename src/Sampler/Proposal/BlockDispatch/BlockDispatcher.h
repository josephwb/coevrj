//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockDispatcher.h
 *
 * @date Apr 14, 2017
 * @author meyerx
 * @brief
 */
#ifndef BLOCKDISPATCHER_H_
#define BLOCKDISPATCHER_H_

#include <boost/shared_ptr.hpp>
#include <map>
#include <vector>

namespace ParameterBlock {
class Block;
class Blocks;
typedef boost::shared_ptr<Block> blockSharedPtr_t;
}

namespace Sampler {
namespace Proposal {

class ProposalPFAMCMC;

class BlockDispatcher {
public:
	enum blocksSource_t { PFAMCMC_SOURCE, GRADIENT_SOURCE };
	struct BlocksAndSource {
		blocksSource_t source;
		ParameterBlock::Blocks* ptrBlocks;
	};
	typedef std::vector<BlocksAndSource> vecBlocksAndSource_t;

public:
	BlockDispatcher();
	~BlockDispatcher();

	void registerBlocks(ProposalPFAMCMC* ptrProposal);

	size_t addDynamicBlock(ParameterBlock::blockSharedPtr_t ptrBlock);
	size_t removeDynamicBlock(ParameterBlock::blockSharedPtr_t ptrBlock);
	size_t removeDynamicBlock(size_t idBlock);

	vecBlocksAndSource_t getBlocksSources() const;
	vecBlocksAndSource_t getBlocksSource(ParameterBlock::blockSharedPtr_t ptrBlock);

private:

	vecBlocksAndSource_t vecBlocksAndSource;

	std::vector<blocksSource_t> getBlockSources(ParameterBlock::blockSharedPtr_t ptrBlock) const;
};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BLOCKDISPATCHER_H_ */
