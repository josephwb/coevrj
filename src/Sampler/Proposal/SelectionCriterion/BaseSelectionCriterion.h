//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseSelectionCriterion.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASESELECTIONCRITERION_H_
#define BASESELECTIONCRITERION_H_

#include <boost/shared_ptr.hpp>

// Forward declaration
namespace Sampler {
namespace Proposal {
	class Selector;
	class BaseProposal;
}
}

namespace Sampler {
namespace Proposal {
namespace Utils {

class BaseSelectionCriterion {
public:
	typedef boost::shared_ptr<BaseSelectionCriterion> sharedPtr_t;

public:
	BaseSelectionCriterion();
	virtual ~BaseSelectionCriterion();

	virtual void registerToSelector(Selector* aSelector, BaseProposal* aProp) = 0;
};

} /* namespace Utils */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BASESELECTIONCRITERION_H_ */
