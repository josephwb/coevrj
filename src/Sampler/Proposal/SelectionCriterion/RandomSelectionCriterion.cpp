//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RandomSelectionCriterion.cpp
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#include "RandomSelectionCriterion.h"

#include "Sampler/Proposal/Selector/Selector.h"

namespace Sampler {
namespace Proposal {
namespace Utils {

RandomSelectionCriterion::RandomSelectionCriterion(double aFrequency) :
		frequency(aFrequency) {
}

RandomSelectionCriterion::~RandomSelectionCriterion() {
}

double RandomSelectionCriterion::getFrequency() const {
	return frequency;
}

void RandomSelectionCriterion::registerToSelector(Selector* aSelector, BaseProposal* aProp) {
	aSelector->doRegister(this, aProp);
}


} /* namespace Utils */
} /* namespace Proposal */
} /* namespace Sampler */
