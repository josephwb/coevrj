//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateProposalMC3.h
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#ifndef STATEPROPOSALMC3_H_
#define STATEPROPOSALMC3_H_

#include "BaseProposalState.h"
#include "Parallel/RNG/StateRNG/StateRNG.h"

namespace Sampler {
namespace Proposal {
	class ProposalMC3;
}
}

namespace Sampler {
namespace Proposal {
namespace State {

class StateProposalMC3 : public BaseProposalState {
public:
	typedef boost::shared_ptr<StateProposalMC3> sharedPtr_t;

public:
	StateProposalMC3();
	~StateProposalMC3();

private:

	StateRNG stateRNG;

	// Boost serialization
	friend class Sampler::Proposal::ProposalMC3;
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace State */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* STATEPROPOSALMC3_H_ */
