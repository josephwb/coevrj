//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateProposalPFAMCMC.h
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#ifndef STATEPROPOSALPFAMCMC_H_
#define STATEPROPOSALPFAMCMC_H_

#include <vector>

#include "BaseProposalState.h"
#include "Parallel/RNG/StateRNG/StateRNG.h"
#include "ParameterBlock/BlockState.h"

namespace Sampler {
namespace Proposal {
	class ProposalPFAMCMC;
}
}

namespace Sampler {
namespace Proposal {
namespace State {

class StateProposalPFAMCMC : public BaseProposalState {
public:
	typedef boost::shared_ptr<StateProposalPFAMCMC> sharedPtr_t;

public:
	StateProposalPFAMCMC();
	~StateProposalPFAMCMC();

private:

	StateRNG blockSelRNGState;
	std::vector<ParameterBlock::State::BlockState> bStates;

	// Boost serialization
	friend class Sampler::Proposal::ProposalPFAMCMC;
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()

};

} /* namespace State */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* STATEPROPOSALPFAMCMC_H_ */
