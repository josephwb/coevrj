//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseProposalState.h
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#ifndef BASEPROPOSALSTATE_H_
#define BASEPROPOSALSTATE_H_

#include <boost/shared_ptr.hpp>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Sampler {
namespace Proposal {
namespace State {

class BaseProposalState {
public:
	typedef boost::shared_ptr<BaseProposalState> sharedPtr_t;

public:
	BaseProposalState();
	virtual ~BaseProposalState();

private:
	// Boost serialization
	friend class boost::serialization::access;

    template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} /* namespace State */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* BASEPROPOSALSTATE_H_ */
