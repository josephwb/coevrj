//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateProposalRJMCMC.cpp
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#include "StateProposalRJMCMC.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>

BOOST_CLASS_EXPORT_GUID(Sampler::Proposal::State::StateProposalRJMCMC, "Sampler_Proposal_State_RJMCMC")

namespace Sampler {
namespace Proposal {
namespace State {

StateProposalRJMCMC::StateProposalRJMCMC() {
}

StateProposalRJMCMC::~StateProposalRJMCMC() {
}

template<class Archive>
void StateProposalRJMCMC::save(Archive & ar, const unsigned int version) const {
    ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseProposalState);
    ar << BOOST_SERIALIZATION_NVP( stateRNG );
    ar << BOOST_SERIALIZATION_NVP( stateRJMoveManagerRNG );
}

template<class Archive>
void StateProposalRJMCMC::load(Archive & ar, const unsigned int version) {
    ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseProposalState);
    ar >> BOOST_SERIALIZATION_NVP( stateRNG );
    ar >> BOOST_SERIALIZATION_NVP( stateRJMoveManagerRNG );
}

template void StateProposalRJMCMC::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void StateProposalRJMCMC::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void StateProposalRJMCMC::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void StateProposalRJMCMC::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace Proposal */
} /* namespace Sampler */
