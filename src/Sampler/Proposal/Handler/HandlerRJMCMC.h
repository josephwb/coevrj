//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerRJMCMC.h
 *
 * @date Apr 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef HANDLERRJMCMC_H_
#define HANDLERRJMCMC_H_

#include <stddef.h>
#include <list>

#include "Sampler/Proposal/Handler/../Outcome/BaseOutcome.h"
#include "Sampler/Proposal/Handler/State/BaseHandlerState.h"
#include "BaseHandler.h"
#include "Sampler/ParameterStatsAccessor/AccessorsManager.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "Sampler/Proposal/Outcome/OutcomeRJMCMC.h"
#include "Sampler/Proposal/ProposalRJMCMC.h"
#include "Sampler/ReversibleJump/SubSpaceRessources/RJSubSpaceInfo.h"

namespace Parallel { class MCMCManager; }
namespace Sampler { class Sample; }
namespace Sampler { class SampleVector; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class BlockDispatcher; } }
namespace Sampler { namespace Proposal { class ProposalRJMCMC; } }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace RJMCMC {
	class RJMove;
}
}

namespace Sampler {
namespace Proposal {
namespace Handler {

class HandlerRJMCMC: public BaseHandler {
public:
	typedef boost::shared_ptr<HandlerRJMCMC> sharedPtr_t;
public:
	HandlerRJMCMC(Sampler::Sample &initSample, ProposalRJMCMC* aProposal,
			   	  Sampler::State::SamplerState &aSamplerState,
			   	  BlockDispatcher &aBlockDispatcher,
			      Sampler::PStatsAccessor::AccessorsManager &accessorsManager);
	~HandlerRJMCMC();

	State::BaseHandlerState::sharedPtr_t saveState() const;
	void loadState(State::BaseHandlerState::sharedPtr_t state);

	void doPreProcessing(Sampler::Sample &curSample);
	void applyProposal(Sampler::Sample &curSample);
	void doPostProcessing(Sampler::Sample &curSample);
	void updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples);

	Outcome::BaseOutcome::sharedPtr_t getOutcome();

	std::string reportStatus(std::string &prefix, size_t iT, double timeTotal) const;

	void updateStateFromRemote(Sampler::Sample &curSample, std::vector< Sampler::RJMCMC::RJMove::sharedPtr_t > &moves);

private:

	const Parallel::MCMCManager &mcmcMgr;

	double sTimeRJMCMC, eTimeRJMCMC;
	double timeApplyMove, timeComputeMove, timeAcceptRejectMove, timeRJMCMC;

	size_t counter;
	ProposalRJMCMC* proposal;
	BlockDispatcher &blockDispatcher;

	RJMCMC::RJSubSpaceInfo::listSSInfo_t subSpaceRessources; // to avoid copy errors.

	Outcome::OutcomeRJMCMC::sharedPtr_t outcome;

	void initialize(Sampler::Sample &initSample);
	void updatePIndMSS(Sampler::Sample &curSample);

};

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* HANDLERRJMCMC_H_ */
