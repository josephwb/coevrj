//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Manager.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef MANAGER_H_
#define MANAGER_H_

#include <assert.h>
#include <map>

#include "../IncProposals.h"
#include "Sampler/Proposal/Handler/../BaseProposal.h"
#include "BaseHandler.h"
#include "Sampler/ParameterStatsAccessor/AccessorsManager.h"
#include "Sampler/Proposal/BlockDispatch/BlockDispatcher.h"
#include "State/BaseHandlerState.h"

namespace Sampler { class Sample; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class ProposalMC3; } }
namespace Sampler { namespace Proposal { class ProposalPFAMCMC; } }
namespace Sampler { namespace Proposal { class ProposalRJMCMC; } }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

class Manager {
public:
	Manager(Sampler::State::SamplerState &aSamplerState,
			Sampler::PStatsAccessor::AccessorsManager &aAccessorsManager);
	~Manager();

	void doRegister(Sampler::Sample &initSample, ProposalPFAMCMC* aProposal);
	void doRegister(Sampler::Sample &initSample, ProposalMC3* aProposal);
	void doRegister(Sampler::Sample &initSample, ProposalRJMCMC* aProposal);

	//  Langevin & Accessor example
	// void doRegister(Sampler::Sample &initSample, ProposalXXX* aProposalDep);

	BaseHandler::sharedPtr_t getHandler(BaseProposal::sharedPtr_t aProp);
	BaseHandler::sharedPtr_t getHandler(BaseProposal* aProp);

	State::BaseHandlerState::sharedPtr_t getHandlerState(BaseProposal::sharedPtr_t aProp) const;
	State::BaseHandlerState::sharedPtr_t getHandlerState(BaseProposal* aProp) const;

	void setHandlerState(BaseProposal::sharedPtr_t aProp, State::BaseHandlerState::sharedPtr_t state);
	void setHandlerState(BaseProposal* aProp, State::BaseHandlerState::sharedPtr_t state);

	const BlockDispatcher& getBlockDispatcher() const;

private:

	typedef std::map<BaseProposal*, BaseHandler::sharedPtr_t> mapHandler_t;
	typedef mapHandler_t::iterator itMapHandler_t;
	typedef mapHandler_t::const_iterator cstItMapHandler_t;
	mapHandler_t mapHandler;

	Sampler::State::SamplerState &samplerState;
	BlockDispatcher blockDispatcher;
	Sampler::PStatsAccessor::AccessorsManager &accessorsManager;
};

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* MANAGER_H_ */
