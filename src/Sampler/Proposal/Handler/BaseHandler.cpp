//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseHandler.cpp
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#include "BaseHandler.h"

#include "Sampler/Proposal/Handler/State/BaseHandlerState.h"

namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

BaseHandler::BaseHandler(Sampler::State::SamplerState &aSamplerState) :
	samplerState(aSamplerState) {
}

BaseHandler::~BaseHandler() {
}

State::BaseHandlerState::sharedPtr_t BaseHandler::saveState() const {
	State::BaseHandlerState::sharedPtr_t tmp;
	return tmp;
}

void BaseHandler::loadState(State::BaseHandlerState::sharedPtr_t state) {

}

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
