//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BaseHandlerState.cpp
 *
 * @date Dec 12, 2016
 * @author meyerx
 * @brief
 */
#include "BaseHandlerState.h"

namespace Sampler {
namespace Proposal {
namespace Handler {
namespace State {

BaseHandlerState::BaseHandlerState() {
}

BaseHandlerState::~BaseHandlerState() {
}

template<class Archive>
void BaseHandlerState::save(Archive & ar, const unsigned int version) const {
}

template<class Archive>
void BaseHandlerState::load(Archive & ar, const unsigned int version) {
}

template void BaseHandlerState::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void BaseHandlerState::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void BaseHandlerState::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void BaseHandlerState::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
