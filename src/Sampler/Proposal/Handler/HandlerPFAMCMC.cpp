//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerPFAMCMC.cpp
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#include "HandlerPFAMCMC.h"

#include "Sampler/Proposal/Handler/BaseHandler.h"
#include "Parallel/Manager/MCMCManager.h"
#include "Sampler/Information/OutputManager.h"
#include "Sampler/Samples/Sample.h"
#include "Sampler/SamplerState.h"
#include "Sampler/Samples/SampleVector.h"
#include "State/StateHandlerPFAMCMC.h"
#include "mpi.h"

namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class ProposalPFAMCMC; } }

// Define macro // to avoid time measures
#define TIME_MEASURE_PFAMCMC

namespace Sampler {
namespace Proposal {
namespace Handler {

HandlerPFAMCMC::HandlerPFAMCMC(Sampler::Sample &initSample, ProposalPFAMCMC* aProposal,
		Sampler::State::SamplerState &aSamplerState,
		Sampler::PStatsAccessor::AccessorsManager &accessorsManager) :
		BaseHandler(aSamplerState), mcmcMgr(Parallel::mcmcMgr()), proposal(aProposal),
		adaptiveProc(initSample, proposal->getCfgFactory(), proposal->getBlocks(),
					 proposal->getModel(), proposal->getAccepanceStrategy(),
					 proposal->getModifierStrategy(),
					 accessorsManager),
		prefetching(proposal->getBlocks(), proposal->getModel(),
					proposal->getBlockSelector(), proposal->getAccepanceStrategy(),
					proposal->getModifierStrategy()),
		outcome(new OutcomePFAMCMC()) {

	timeB1 = timeB2 = timeO = timePFAMCMC = 0.;
	sTimePFAMCMC = eTimePFAMCMC = 0.;

	counter = 0;

	// Register accessor --> Moved to AdaptiveProc
	// accessorsManager.registerProposalAccessor(adaptiveProc.getParametersStatsAccessor());
}

HandlerPFAMCMC::~HandlerPFAMCMC() {
}

State::BaseHandlerState::sharedPtr_t HandlerPFAMCMC::saveState() const {
	State::StateHandlerPFAMCMC::sharedPtr_t ptrState(new State::StateHandlerPFAMCMC());

	ptrState->counter = counter;
	ptrState->timeB1 = timeB1;
	ptrState->timeB2 = timeB2;
	ptrState->timeO = timeO;
	ptrState->timePFAMCMC = timePFAMCMC;

	// PREFETCHING
	prefetching.saveState(ptrState->pfState);

	// ADAPTIVE
	adaptiveProc.saveState(ptrState->apState);

	return ptrState;
}

void HandlerPFAMCMC::loadState(State::BaseHandlerState::sharedPtr_t state) {
	State::StateHandlerPFAMCMC* ptrState = dynamic_cast<State::StateHandlerPFAMCMC*>(state.get());

	counter = ptrState->counter;
	timeB1 = ptrState->timeB1;
	timeB2 = ptrState->timeB2;
	timeO = ptrState->timeO;
	timePFAMCMC = ptrState->timePFAMCMC;

	// PREFETCHING
	prefetching.loadState(ptrState->pfState);

	// ADAPTIVE
	adaptiveProc.loadState(ptrState->apState);

}

void HandlerPFAMCMC::doPreProcessing(Sampler::Sample &curSample) {
	outcome->reset();

	TIME_MEASURE_PFAMCMC timeO += 0;

	TIME_MEASURE_PFAMCMC sTimePFAMCMC = MPI_Wtime();
}

void HandlerPFAMCMC::applyProposal(Sampler::Sample &curSample) {
	proposal->getAccepanceStrategy()->setInvTemperature(samplerState.getInverseTemperature());
	prefetching.processNextIteration(curSample, outcome);
}

void HandlerPFAMCMC::doPostProcessing(Sampler::Sample &curSample) {

	// Current sample, is the state at the beginning at the iteration
	Sampler::Sample &oldSample = curSample;
	// Current or new sample is the "new one"
	Sampler::Sample &newSample = outcome->getAcceptedSamples().empty() ? curSample : outcome->getAcceptedSamples().front();

	// Adaptive phase
	TIME_MEASURE_PFAMCMC double sB1 = MPI_Wtime();
	adaptiveProc.adaptBlocks(oldSample, newSample, outcome->getAcceptedId(), outcome->getBlockIds(), outcome->getAlphaScore());
	TIME_MEASURE_PFAMCMC double eB1 = MPI_Wtime();
	TIME_MEASURE_PFAMCMC timeB1 += eB1-sB1;

	TIME_MEASURE_PFAMCMC double sB2 = MPI_Wtime();
	adaptiveProc.updateLocalAlphas(newSample, outcome->getBlockIds());
	TIME_MEASURE_PFAMCMC double eB2 = MPI_Wtime();
	TIME_MEASURE_PFAMCMC timeB2 += eB2-sB2;

	for(size_t iI=0; iI<adaptiveProc.getInformations().size(); ++iI) {
		outcome->addInformation(adaptiveProc.getInformations()[iI]);
	}
	adaptiveProc.getInformations().clear();

	TIME_MEASURE_PFAMCMC eTimePFAMCMC = MPI_Wtime();
	TIME_MEASURE_PFAMCMC timePFAMCMC += eTimePFAMCMC - sTimePFAMCMC;

	counter++;
}

void HandlerPFAMCMC::updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples) {

	if(mcmcMgr.isOutputManager()) {
		for(int iR=0; iR<outcome->getNRejected(); ++iR) {
			samples.push_back(curSample);
			samples.back().setChanged(false);
		}

		if(!outcome->getAcceptedSamples().empty()) {
			samples.push_back(outcome->getAcceptedSamples().front());
			samples.back().setChanged(true);
		}
	}

	if(!outcome->getAcceptedSamples().empty()) {
		curSample = outcome->getAcceptedSamples().front();
		curSample.setChanged(true);
	}
}

Outcome::BaseOutcome::sharedPtr_t HandlerPFAMCMC::getOutcome() {
	return outcome;
}

std::string HandlerPFAMCMC::reportStatus(std::string &prefix, size_t iT, double timeTotal) const {
	std::stringstream sstr;

	sstr << prefix << "[B_OPTI] Time for block optimization :\t" << timeO << "\t" << timeO/(double)counter << "\t" << 100*timeO/timeTotal << std::endl;
	sstr << prefix << "[AMCMC] Time Mu, Sigma, Lambda :\t" << timeB1 << "\t" << timeB1/(double)counter << "\t" << 100*timeB1/timeTotal << std::endl;
	sstr << prefix << "[AMCMC] Time LocLambda :\t" << timeB2 << "\t" << timeB2/(double)counter << "\t" << 100*timeB2/timeTotal << std::endl;

	sstr << prefetching.reportStatus(prefix, counter, timeTotal);

	sstr << prefix << "[PFAMCMC] Time for PFAMCMC :\t" << timePFAMCMC << "\t" << timePFAMCMC/(double)counter << "\t" << 100*timePFAMCMC/timeTotal << std::endl;
	return sstr.str();
}


} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */
