//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerPFAMCMC.h
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#ifndef HANDLERPFAMCMC_H_
#define HANDLERPFAMCMC_H_


#include <stddef.h>

#include "Sampler/Proposal/Handler/../Outcome/BaseOutcome.h"
#include "Sampler/Proposal/Handler/State/BaseHandlerState.h"
#include "BaseHandler.h"
#include "Sampler/Adaptive/AdaptiveProcess.h"
#include "Sampler/ParameterStatsAccessor/AccessorsManager.h"
#include "Sampler/Prefetching/Prefetching.h"
#include "Sampler/Proposal/Outcome/OutcomePFAMCMC.h"
#include "Sampler/Proposal/ProposalPFAMCMC.h"

namespace Parallel { class MCMCManager; }
namespace Sampler { class Sample; }
namespace Sampler { class SampleVector; }
namespace Sampler { namespace PStatsAccessor { class AccessorsManager; } }
namespace Sampler { namespace Proposal { class ProposalPFAMCMC; } }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

class HandlerPFAMCMC: public BaseHandler {
public:
	typedef boost::shared_ptr<HandlerPFAMCMC> sharedPtr_t;

public:
	HandlerPFAMCMC(Sampler::Sample &initSample, ProposalPFAMCMC* aProposal,
				   Sampler::State::SamplerState &aSamplerState,
				   Sampler::PStatsAccessor::AccessorsManager &accessorsManager);
	~HandlerPFAMCMC();

	State::BaseHandlerState::sharedPtr_t saveState() const;
	void loadState(State::BaseHandlerState::sharedPtr_t state);

	void doPreProcessing(Sampler::Sample &curSample);
	void applyProposal(Sampler::Sample &curSample);
	void doPostProcessing(Sampler::Sample &curSample);
	void updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples);

	Outcome::BaseOutcome::sharedPtr_t getOutcome();

	std::string reportStatus(std::string &prefix, size_t iT, double timeTotal) const;

private:

	const Parallel::MCMCManager &mcmcMgr;

	double sTimePFAMCMC, eTimePFAMCMC;
	double timeB1, timeB2, timeO, timePFAMCMC;

	size_t counter;
	ProposalPFAMCMC* proposal;

	Adaptive::AdaptiveProcess adaptiveProc;
	Prefetching prefetching;

	Outcome::OutcomePFAMCMC::sharedPtr_t outcome;

};

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* HANDLERPFAMCMC_H_ */
