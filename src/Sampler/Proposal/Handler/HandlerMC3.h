//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HandlerMC3.h
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#ifndef HANDLERMC3_H_
#define HANDLERMC3_H_

#include <stddef.h>

#include "Sampler/Proposal/Handler/../Outcome/BaseOutcome.h"
#include "Sampler/Proposal/Handler/State/BaseHandlerState.h"
#include "BaseHandler.h"
#include "ParameterBlock/BlockStats/ConvUtils/Updater/Rho/BatchEvoRU.h"
#include "Sampler/Information/MC3Info.h"
#include "Sampler/Information/OutputManager.h"
#include "Sampler/Proposal/Outcome/OutcomeMC3.h"
#include "Sampler/Proposal/ProposalMC3.h"
#include "Sampler/Samples/Sample.h"

namespace ParameterBlock { class BatchEvoRU; }
namespace Sampler { class Sample; }
namespace Sampler { class SampleVector; }
namespace Sampler { namespace State { class SamplerState; } }

namespace Sampler {
namespace Proposal {
namespace Handler {

class HandlerMC3: public BaseHandler {
public:
	typedef boost::shared_ptr<HandlerMC3> sharedPtr_t;

public:
	HandlerMC3(ProposalMC3 *aProposal, Sampler::State::SamplerState &aSamplerState);
	~HandlerMC3();

	void doPreProcessing(Sampler::Sample &curSample);
	void applyProposal(Sampler::Sample &curSample);
	void doPostProcessing(Sampler::Sample &curSample);
	void updateSamples(Sampler::Sample &curSample, Sampler::SampleVector &samples);

	State::BaseHandlerState::sharedPtr_t saveState() const;
	void loadState(State::BaseHandlerState::sharedPtr_t state);

	Outcome::BaseOutcome::sharedPtr_t getOutcome();

	std::string reportStatus(std::string &prefix, size_t iT, double timeTotal) const;

private:

	static const double TARGET_ALPHA;

	const Parallel::MCMCManager &mcmcMgr;

	size_t counter, convIterMC3, nStepMC3, nAccStepMC3, nextWrite;
	double timeMC3;
	ProposalMC3* proposal;
	ParameterBlock::BatchEvoRU *evoMC3;
	Outcome::OutcomeMC3::sharedPtr_t outcome;

	void doMC3(Sampler::Sample &curSample);
	void defineExchange(int &coldChain, int &hotChain, double &unifDbl);
	double defineExchangeRatio(const int myChain, const int otherChain, ::Utils::Serialize::buffer_t &bufferRecv, Sample &curSample, Sample &newSample);
	void acceptExchangeManager(Sample &curSample, Sample &newSample, const int otherChain);
	void acceptExchangeWorker(Sample &curSample, Sample &newSample);

	void updateTemperatures();

	void packInfoMC3(::Utils::Serialize::buffer_t &bufferSend, const bool accepted, const double invTemp, const Sample &sample);
	void unPackInfoMC3(const ::Utils::Serialize::buffer_t &bufferRecv, bool &accepted, double &invTemp, Sample &sample);
	void packPartialInfoMC3(::Utils::Serialize::buffer_t &bufferSend, const bool accepted, const double invTemp);
	void unPackPartialInfoMC3(const ::Utils::Serialize::buffer_t &bufferRecv, bool &accepted, double &invTemp);
	void writeOutputMC3(const Sample &curSample);

};

} /* namespace Handler */
} /* namespace Proposal */
} /* namespace Sampler */

#endif /* HANDLERMC3_H_ */
