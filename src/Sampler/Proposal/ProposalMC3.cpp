//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalMC3.cpp
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#include "ProposalMC3.h"

#include <assert.h>
#include <stddef.h>

#include "Sampler/Proposal/Handler/Manager.h"
#include "Sampler/Proposal/State/StateProposalMC3.h"

namespace Sampler {
namespace Proposal {

ProposalMC3::ProposalMC3(size_t aSeed, StatisticalModel::Model &aModel, ConfigFactory::sharedPtr_t aCfgFactory) :
		BaseProposal(), model(aModel), cfgMC3(aCfgFactory->createConfig(1)),
		rng(RNG::createSitmo11RNG(aSeed)), fsc(getTunedFreqMC3(cfgMC3->FREQ_MC3)) {
	cfgMC3 = aCfgFactory->createConfig(1);
}

ProposalMC3::ProposalMC3(size_t aFreq, size_t aSeed, StatisticalModel::Model &aModel, ConfigFactory::sharedPtr_t aCfgFactory) :
		BaseProposal(), model(aModel), cfgMC3(aCfgFactory->createConfig(1)),
		rng(RNG::createSitmo11RNG(aSeed)), fsc(getTunedFreqMC3(aFreq)) {
	cfgMC3 = aCfgFactory->createConfig(1);
}

ProposalMC3::~ProposalMC3() {
	delete rng;
}

size_t ProposalMC3::getTunedFreqMC3(size_t aFreq) const {
	if(Parallel::mcmcMgr().isOutputManager()) {
		return aFreq;
	} else {
		return 1.0*(double)aFreq; // FIXME 1.0 => 0.9 : Excellent idea - but will cause a deadlock at the end.
	}
}

void ProposalMC3::registerToSelector(Selector *selector) {
	fsc.registerToSelector(selector, this);
}

void ProposalMC3::registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm) {
	hm->doRegister(initSample, this);
}

State::BaseProposalState::sharedPtr_t ProposalMC3::saveState() const {
	State::StateProposalMC3::sharedPtr_t ptrState(new State::StateProposalMC3());
	rng->saveState(ptrState->stateRNG);
	return ptrState;
}

void ProposalMC3::loadState(State::BaseProposalState::sharedPtr_t state) {
	State::StateProposalMC3* ptrState = dynamic_cast<State::StateProposalMC3*>(state.get());
	assert(rng != NULL);
	rng->loadState(ptrState->stateRNG);
}

RNG* ProposalMC3::getRNG() {
	return rng;
}

StatisticalModel::Model& ProposalMC3::getModel() {
	return model;
}

BlockStatCfg::sharedPtr_t ProposalMC3::getConfig() {
	return cfgMC3;
}

} /* namespace Proposal */
} /* namespace Sampler */
