//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Selector.h
 *
 * @date Dec 5, 2016
 * @author meyerx
 * @brief
 */
#ifndef SELECTOR_H_
#define SELECTOR_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <queue>
#include <vector>

#include "../SelectionCriterion/FixedSelectionCriterion.h"
#include "../SelectionCriterion/RandomSelectionCriterion.h"
#include "Parallel/RNG/Sitmo11RNG.h"

class RNG;

namespace Sampler {
namespace Proposal {

class BaseProposal;
namespace Utils { class FixedSelectionCriterion; }
namespace Utils { class RandomSelectionCriterion; }

class Selector {
public:
	Selector(size_t aSeed);
	~Selector();

	void doRegister(Utils::FixedSelectionCriterion* fsc, BaseProposal* aProposal);
	void doRegister(Utils::RandomSelectionCriterion* rsc, BaseProposal* aProposal);

	BaseProposal* selectNext(size_t iteration);

private:

	RNG *myRng;

	// Proposal queue
	std::queue<BaseProposal*> waitingProp;

	// Random proposals
	std::vector<double> frequencies;
	std::vector<BaseProposal*> randomProp;

	// Periodic proposals
	std::vector<size_t> periods;
	std::vector<BaseProposal*> periodicProp;

};

} /* namespace Proposal */
} /* namespace Sampler */

#endif /* SELECTOR_H_ */
