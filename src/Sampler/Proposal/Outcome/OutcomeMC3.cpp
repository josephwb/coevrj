//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OutcomeMC3.cpp
 *
 * @date Dec 16, 2016
 * @author meyerx
 * @brief
 */
#include "OutcomeMC3.h"

#include "Sampler/Proposal/Outcome/BaseOutcome.h"

namespace Sampler {
namespace Proposal {
namespace Outcome {

OutcomeMC3::OutcomeMC3() : BaseOutcome() {
	chainInvolved = false;
}

OutcomeMC3::~OutcomeMC3() {
}

void OutcomeMC3::setChainInvolved(bool isInvolved) {
	chainInvolved = isInvolved;
}

bool OutcomeMC3::chainIsInvolved() const {
	return chainInvolved;
}

void OutcomeMC3::reset() {
	BaseOutcome::reset();
	chainInvolved = false;
}

} /* namespace Outcome */
} /* namespace Proposal */
} /* namespace Sampler */
