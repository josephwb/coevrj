//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ProposalPFAMCMC.cpp
 *
 * @date Dec 7, 2016
 * @author meyerx
 * @brief
 */
#include "ProposalPFAMCMC.h"

#include <assert.h>
#include <stddef.h>

#include "Handler/Manager.h"
#include "ParameterBlock/Block.h"
#include "State/StateProposalPFAMCMC.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

namespace Sampler {
namespace Proposal {

class Selector;

ProposalPFAMCMC::ProposalPFAMCMC(Config::ConfigFactory::sharedPtr_t aCfgFactory, Blocks &aBlocks,
								 StatisticalModel::Model &aModel) :
	cfgFactory(aCfgFactory), model(aModel) {
	initBlocks(aBlocks);
	initStrategies();
}

ProposalPFAMCMC::ProposalPFAMCMC(double aSelectionFreq, Config::ConfigFactory::sharedPtr_t aCfgFactory,
								 Blocks &aBlocks, StatisticalModel::Model &aModel) :
		 cfgFactory(aCfgFactory), rsc(aSelectionFreq), model(aModel) {
	initBlocks(aBlocks);
	initStrategies();
}

ProposalPFAMCMC::~ProposalPFAMCMC() {
}

void ProposalPFAMCMC::initBlocks(Blocks &aBlocks) {

	std::vector<Block::sharedPtr_t> vecBlocks = aBlocks.getBlocks();
	for(size_t iB=0; iB<vecBlocks.size(); ++iB) {
		Block::sharedPtr_t ptrBlock = vecBlocks[iB];
		// Check if this is a PFAMCMC style block
		if( ptrBlock->isNotAdaptive() ||
			ptrBlock->isSingleDouble() ||
			ptrBlock->isMixedDouble() ||
			ptrBlock->isPCADouble() ||
			ptrBlock->isLangevin() ||
			ptrBlock->isSMALA() ||
			ptrBlock->isTreeLangevin()) {

			if(ptrBlock->isBaseBlock()) blocks.addBaseBlock(ptrBlock);
			else blocks.addDynamicBlock(ptrBlock);
		}
	}

	if(blocks.getNTotalBlocks() == 0) {
		std::cerr << "[Error] in void ProposalPFAMCMC::initBlocks(Blocks &aBlocks);" << std::endl;
		std::cerr << "No parameter blocks are defined as 'Adaptive block' use PCA_Adaptive for instance as adaptiveType." << std::endl;
		abort();
	}

}

void ProposalPFAMCMC::initStrategies() {
	ptrMS = Sampler::Strategies::ModifierStrategy::createDefaultModifierStrategy(model);
	ptrAS = Sampler::Strategies::AcceptanceStrategy::createDefaultAcceptanceStrategy(ptrMS, model);
	ptrBS = Sampler::Strategies::BlockSelector::createSingleCyclic(model.getParams(), blocks);
}

ParameterBlock::Blocks& ProposalPFAMCMC::getBlocks() {
	return blocks;
}

const ParameterBlock::Blocks& ProposalPFAMCMC::getBlocks() const {
	return blocks;
}

StatisticalModel::Model& ProposalPFAMCMC::getModel() {
	return model;
}

void ProposalPFAMCMC::registerToSelector(Selector *selector) {
	rsc.registerToSelector(selector, this);
}

void ProposalPFAMCMC::registerToHandlerManager(Sampler::Sample &initSample, Handler::Manager *hm) {
	hm->doRegister(initSample, this);
}

State::BaseProposalState::sharedPtr_t ProposalPFAMCMC::saveState() const {
	State::StateProposalPFAMCMC::sharedPtr_t ptrState(new State::StateProposalPFAMCMC());

	// Block States
	ptrState->bStates.resize(blocks.getNBaseBlocks());
	for(size_t iB=0; iB<blocks.getNBaseBlocks(); ++iB) {
		blocks.getBlock(iB)->saveState(ptrState->bStates[iB]);
	}

	// Block selector RNG
	if(ptrBS->getRNG() != NULL) {
		ptrBS->getRNG()->saveState(ptrState->blockSelRNGState);
	}

	return ptrState;
}

void ProposalPFAMCMC::loadState(State::BaseProposalState::sharedPtr_t state) {
	State::StateProposalPFAMCMC* ptrState = dynamic_cast<State::StateProposalPFAMCMC*>(state.get());
	assert(ptrState != NULL);

	// Block States
	blocks.resetBaseBlocks(); // FIXME TRY THAT TO AVOID LOSING DYNAMIC PARAMS blocks.reset();

	for(size_t iB=0; iB<ptrState->bStates.size(); ++iB) {
		ParameterBlock::Block::sharedPtr_t block(new ParameterBlock::Block());
		block->loadState(ptrState->bStates[iB]);

		// Set block modifiers
		block->setBlockModifier(ParameterBlock::Support::createBlockModifier(
				block->size(), model, ptrState->bStates[iB].getGlobalBMState()));
		for(size_t iBM=0; iBM<ptrState->bStates[iB].getLocalBMState().size(); ++iBM) {
			block->setBlockModifier(iBM, ParameterBlock::Support::createBlockModifier(
					block->size(),  model, ptrState->bStates[iB].getLocalBMState()[iBM]));
		}
		blocks.addBaseBlock(block);
	}

	// Block selector RNG
	if(ptrBS->getRNG() != NULL) {
		ptrBS->getRNG()->loadState(ptrState->blockSelRNGState);
	}
}

Config::ConfigFactory::sharedPtr_t ProposalPFAMCMC::getCfgFactory() {
	return cfgFactory;
}

Strategies::BlockSelector::sharedPtr_t ProposalPFAMCMC::getBlockSelector() {
	return ptrBS;
}

Strategies::AcceptanceStrategy::sharedPtr_t ProposalPFAMCMC::getAccepanceStrategy() {
	return ptrAS;
}

Strategies::ModifierStrategy::sharedPtr_t ProposalPFAMCMC::getModifierStrategy() {
	return ptrMS;
}

void ProposalPFAMCMC::setBlockSelector(Strategies::BlockSelector::sharedPtr_t aPtrBS) {
	if(blocks.getNBaseBlocks() < 1) {
		std::cerr << "[Warning] in void ProposalPFAMCMC::setBlockSelector(...)" << std::endl;
		std::cerr << "Custom block selectors should be defined after blocks." << std::endl;
	}
	ptrBS = aPtrBS;
}

} /* namespace Proposal */
} /* namespace Sampler */
