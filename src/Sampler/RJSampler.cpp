//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RJSampler.cpp
 *
 * @date Oct 5, 2017
 * @author meyerx
 * @brief
 */
#include "RJSampler.h"

#include "Sampler/BaseSampler.h"
#include "Parallel/Manager/MpiManager.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include "mpi.h"

namespace StatisticalModel { class Model; }

namespace Sampler {

class Sample;
namespace Proposal { class Proposals; }

RJSampler::RJSampler(Model &aModel, Proposal::Proposals &aProposals,
					 BaseWriter::sharedPtr_t aTraceWriter) :
					 BaseSampler(aModel, aProposals, aTraceWriter) {

	init();

}

RJSampler::RJSampler(Sampler::Sample &initSample, Model &aModel,
					 Proposal::Proposals &aProposals, BaseWriter::sharedPtr_t aTraceWriter) :
					 BaseSampler(initSample, aModel, aProposals, aTraceWriter) {
	init();
}

RJSampler::~RJSampler() {
}

void RJSampler::init() {
	const std::vector<Proposal::BaseProposal::sharedPtr_t>& propVec = proposals.getProposals();

	/// Memorize RJMCMC Handler
	ptrHandlerRJMCMC = NULL;
	for(size_t iP=0; iP<propVec.size(); ++iP) {
		Proposal::ProposalRJMCMC* propRJ = dynamic_cast<Proposal::ProposalRJMCMC*>(propVec[iP].get());
		if(propRJ != NULL) {
			ptrHandlerRJMCMC = static_cast<Proposal::Handler::HandlerRJMCMC*>(handlerManager.getHandler(propVec[iP]).get());
		}
	}

	assert(ptrHandlerRJMCMC != NULL && "RJSampler can only be used with ReversibleJumpMCMC proposals.");

	/// Memorize lik helper
	ptrLikHelper = (StatisticalModel::Likelihood::Helper::HelperInterface::createHelper(model.getLikelihood().get()));
}


void RJSampler::processNextIteration() {
	using namespace Proposal;
	using namespace Proposal::Handler;
	using namespace Proposal::Outcome;

	// Set the current sample as unchanged (by default)
	curSample.setChanged(false);

	// Select next proposal
	BaseProposal* nextProposal = proposalSelector.selectNext(iT);

	// Get handler
	BaseHandler::sharedPtr_t proposalHandler = handlerManager.getHandler(nextProposal);

	// Pre processing
	proposalHandler->doPreProcessing(curSample);

	// Apply
	proposalHandler->applyProposal(curSample);

	// Post processing
	proposalHandler->doPostProcessing(curSample);

	// Update samples
	proposalHandler->updateSamples(curSample, samples);

	// Process remaining informations
	bool isSampleFromRemote = false;
	std::list<Information::BaseInfo::sharedPtr_t>& infos = proposalHandler->getOutcome()->getInformations();
	typedef std::list<Information::BaseInfo::sharedPtr_t>::iterator itList_t;
	for(itList_t it = infos.begin(); it != infos.end(); ++it) {
		(*it)->submitToDispatcher(&infoDispatcher);

		if((*it)->getType() == Information::REMOTE_INFO) {
			isSampleFromRemote = static_cast< Information::RemoteInfo* >((*it).get())->isSampleFromRemote();
		}
	}

	// Deal with potential remote moves
	if(isSampleFromRemote) {
		std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> moves = ptrLikHelper->createRJUpdateMoves(curSample, model);
		ptrHandlerRJMCMC->updateStateFromRemote(curSample, moves);
	}
}

std::string RJSampler::reportStatus(std::string prefix) {
	return BaseSampler::reportStatus(prefix);
}


void RJSampler::doSerializeToFile(const std::string &aFileName) const {

	double startTime = MPI_Wtime();

	// Write to file
	std::ofstream ofs(aFileName.c_str());
	boost::archive::xml_oarchive oa(ofs);
	oa << boost::serialization::make_nvp( "RJSampler", *this );

	double endTime = MPI_Wtime();

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "[ SAVED CHECKPOINT ID=" << ckpManager.getICKP() << " in " << endTime - startTime << " seconds ]" << std::endl;
	}
}

void RJSampler::doSerializeFromFile(const std::string &aFileName) {
	// Read from file
	std::ifstream ifs(aFileName.c_str());
	if(ifs.good()) {
		boost::archive::xml_iarchive ia(ifs);
		ia >> boost::serialization::make_nvp( "RJSampler", *this );
	}
}

template<class Archive>
void RJSampler::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
}

template<class Archive>
void RJSampler::load(Archive & ar, const unsigned int version) {
	ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(BaseSampler);
	// Update state based on serialized sample
	std::vector<Sampler::RJMCMC::RJMove::sharedPtr_t> moves = ptrLikHelper->createRJUpdateMoves(curSample, model);
	ptrHandlerRJMCMC->updateStateFromRemote(curSample, moves);
}

template void RJSampler::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void RJSampler::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void RJSampler::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void RJSampler::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Sampler */
