//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PrefetchingState.cpp
 *
 * @date Dec 23, 2016
 * @author meyerx
 * @brief
 */
#include <Sampler/Prefetching/PrefetchingState.h>

#include <boost/serialization/nvp.hpp>

namespace Sampler {

PrefetchingState::PrefetchingState() {
	timeP1 = timeP2 = timeI = timeC = timeW = timeTot = timeP1_1 = timeP1_2 = timeP1_3 = 0.;
}

PrefetchingState::~PrefetchingState() {
}

template<class Archive>
void PrefetchingState::save(Archive & ar, const unsigned int version) const {
	ar << BOOST_SERIALIZATION_NVP ( timeP1 );
	ar << BOOST_SERIALIZATION_NVP ( timeP2 );
	ar << BOOST_SERIALIZATION_NVP ( timeI );
	ar << BOOST_SERIALIZATION_NVP ( timeC );
	ar << BOOST_SERIALIZATION_NVP ( timeW );
	ar << BOOST_SERIALIZATION_NVP ( timeTot );
	ar << BOOST_SERIALIZATION_NVP ( timeP1_1 );
	ar << BOOST_SERIALIZATION_NVP ( timeP1_2 );
	ar << BOOST_SERIALIZATION_NVP ( timeP1_3 );

	//if(ptrBS->getRNG() != NULL) {
		//StateRNG blockSelRNGState;
		//ptrBS->getRNG()->saveState(blockSelRNGState);
	ar << BOOST_SERIALIZATION_NVP( blockSelRNGState );
	//}
}

template<class Archive>
void PrefetchingState::load(Archive & ar, const unsigned int version) {
	ar >> BOOST_SERIALIZATION_NVP ( timeP1 );
	ar >> BOOST_SERIALIZATION_NVP ( timeP2 );
	ar >> BOOST_SERIALIZATION_NVP ( timeI );
	ar >> BOOST_SERIALIZATION_NVP ( timeC );
	ar >> BOOST_SERIALIZATION_NVP ( timeW );
	ar >> BOOST_SERIALIZATION_NVP ( timeTot );
	ar >> BOOST_SERIALIZATION_NVP ( timeP1_1 );
	ar >> BOOST_SERIALIZATION_NVP ( timeP1_2 );
	ar >> BOOST_SERIALIZATION_NVP ( timeP1_3 );

	//if(ptrBS->getRNG() != NULL) {
		//StateRNG blockSelRNGState;
	ar >> BOOST_SERIALIZATION_NVP( blockSelRNGState );
		//ptrBS->getRNG()->loadState(blockSelRNGState);
	//}
}

template void PrefetchingState::save<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version) const;
template void PrefetchingState::load<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void PrefetchingState::save<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version) const;
template void PrefetchingState::load<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace Sampler */
