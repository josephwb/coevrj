//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Prefetching.h
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#ifndef PREFETCHING_H_
#define PREFETCHING_H_

#include <sys/types.h>

#include "Model/Model.h"
#include "PrefetchingState.h"
#include "Sampler/Proposal/Outcome/OutcomePFAMCMC.h"
#include "Sampler/Strategy/AcceptanceStrategy/AcceptanceStrategy.h"
#include "Sampler/Strategy/BlockSelector/BlockSelector.h"
#include "Sampler/Strategy/IncStrategy.h"
#include "Sampler/Strategy/ModifierStrategy/ModifierStrategy.h"

namespace Parallel { class MCMCManager; }
namespace ParameterBlock { class Blocks; }
namespace StatisticalModel { class Model; }

namespace Sampler {

class PrefetchingState;
class Sample;

using Proposal::Outcome::OutcomePFAMCMC;

class Prefetching {
public:
	Prefetching(ParameterBlock::Blocks &aBlocks, StatisticalModel::Model &aModel,
			Strategies::BlockSelector::sharedPtr_t aPtrBS,
			Strategies::AcceptanceStrategy::sharedPtr_t aPtrAS,
			Strategies::ModifierStrategy::sharedPtr_t aPtrMS);
	~Prefetching();

	void processNextIteration(Sampler::Sample &curSample, OutcomePFAMCMC::sharedPtr_t outcome);

	void saveState(PrefetchingState &state) const;
	void loadState(PrefetchingState &state);

	std::string reportStatus(std::string &prefix, size_t iT, double timeTotal) const;

private:

	const Parallel::MCMCManager &mcmcMgr;
	double timeP1, timeP2, timeI, timeC, timeW, timeTot, timeP1_1, timeP1_2, timeP1_3;

	ParameterBlock::Blocks &blocks;
	StatisticalModel::Model &model;

	Strategies::BlockSelector::sharedPtr_t ptrBS;
	Strategies::AcceptanceStrategy::sharedPtr_t ptrAS;
	Strategies::ModifierStrategy::sharedPtr_t ptrMS;

	//! Sequential case
	void doSequentialAcceptanceAndUpdate(Sampler::Sample &curSample, std::vector<Sampler::Sample> &propSamples,
										 std::vector<double> &scores, OutcomePFAMCMC::sharedPtr_t outcome);

	//! Prepare scores in order to send them
	void prepareScores(const uint nPropSample, const vector<double> &scores, vector<double> &alphas) const;
	//! apply the acceptance check given the scores
	int acceptanceCheck(const vector<double> &scores, int &nReject);
	//! Define the accepted sample position (processor, idx)
	void defineSamplePosition(const int acceptId, int &aRank, int &aRow, const int propSize);
	//! Exchange the sample with the other member of the parallel proposal group
	void exchangeSample(const int aRank, const int aRow, const vector<Sample> &propSamples,
						OutcomePFAMCMC::sharedPtr_t outcome);
};

} /* namespace Sampler */

#endif /* PREFETCHING_H_ */
