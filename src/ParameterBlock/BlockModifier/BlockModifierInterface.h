//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * BlockModificator.h
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKMODIFICATOR_H_
#define BLOCKMODIFICATOR_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <vector>

#include "Model/Model.h"
#include "BlockModifierState.h"
#include "Utils/Uncopyable.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/MolecularEvolution/TreeReconstruction/IncTreeReconstruction.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeManager.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/TmpNode.h"


namespace ParameterBlock { namespace State { class BlockModifierState; } }
namespace Sampler { class Sample; }
namespace StatisticalModel { class Model; }

using namespace std;

namespace ParameterBlock {

using ::Sampler::Sample;
using ::StatisticalModel::Model;
namespace TR = MolecularEvolution::TreeReconstruction;

class BlockModifier : private Uncopyable {
public:
	typedef boost::shared_ptr<BlockModifier> sharedPtr_t;
	enum bm_name_t {NONE=0, UNIFORM=1, GAUSSIAN_1D=2, GAUSSIAN_MVN=3,
					PCA_MVN=4, MULTIPLIER=5, ESPR=6, STNNI=7, LANGEVIN_SEQ=8,
					LANGEVIN_PAR=9, ESPR_LV=10, DISCRETE_CATEGORIES=11, WEIGHTED_DISCRETE_CATEGORIES=12,
					INDEPENDENT_GAMMA=13, INDEPENDENT_DIRICHLET=14, COEV_PROFILE_DIRICHLET=15, INDEPENDENT_HP_DIRICHLET=16,
					INDEPENDENT_NORMAL=17, COEV_POISSON_CONJUGATE_K=18, COEV_GAMMA_SHAPE_CONJUGATE_PRIOR=19};

public:
	BlockModifier(const bool aSymmetric, Model &aModel);
	virtual ~BlockModifier();

	bool isSymmetric() const {
		return symmetric;
	}

	virtual void updateParameters(const vector<size_t> &pInd, Sample &sample) const = 0;
	virtual double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const = 0;
	virtual double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const = 0;

	virtual void setWindowSize(const vector<double> &aWinSize) = 0;
	virtual vector<double> getWindowSize() const = 0;

	virtual bm_name_t getName() const = 0;

	virtual void moveAccepted(const bool isLocalProposal) {}
	virtual void moveRejected(const bool isLocalProposal) {}
	virtual void moveThrown(const bool isLocalProposal) {}

	virtual void saveState(State::BlockModifierState &state) const;

	static sharedPtr_t createUniformWindow(const double aWinSize, Model &aModel);
	static sharedPtr_t createGaussianWindow(const double aSigma, Model &aModel);
	static sharedPtr_t createMVGaussianWindow(const size_t n, const double aSigma, Model &aModel);
	static sharedPtr_t createMVGaussianWindow(const size_t n, const double *aSigma, Model &aModel);
	static sharedPtr_t createPCAWindow(const size_t n, const vector<double> &aSigma, Model &aModel);

	static sharedPtr_t createMultiplierWindow(const double aB, Model &aModel);

	static BlockModifier::sharedPtr_t createESPRBM(const double aP,
			TR::TreeManager::sharedPtr_t aTM, Model &aModel);
	static BlockModifier::sharedPtr_t createSTNNIBM(TR::TreeManager::sharedPtr_t aTM,
			Model &aModel);

	static sharedPtr_t createDiscreteCategoriesBM(const std::vector<unsigned short int> &aIdProfile, Model &aModel);
	static sharedPtr_t createDiscreteCategoriesBM(const std::vector<size_t> &aIdProfile, Model &aModel);
	static sharedPtr_t createDiscreteCategoriesBM(const std::vector<double> &data, Model &aModel);

	static sharedPtr_t createWeightedDiscreteCategoriesBM(const std::vector<unsigned short int> &aIdProfile,
														  const std::vector<float> &aWeight, Model &aModel);

	static sharedPtr_t createWeightedDiscreteCategoriesBM(const std::vector<size_t> &aIdProfile,
												   	   	  const std::vector<double> &aWeight,
												   	   	  Model &aModel);
	static sharedPtr_t createWeightedDiscreteCategoriesBM(const std::vector<double> &data,
												   	   	  Model &aModel);

	static sharedPtr_t createNormalIndependentBM(double aMu, double aStd, Model &aModel);
	static sharedPtr_t createGammaIndependentBM(double aShape, double aScale, Model &aModel);
	static sharedPtr_t createDirichletIndependentBM(const std::vector<double> &aAlphas, Model &aModel);
	static sharedPtr_t createHyperPriorDirichletIndepBM(double aAlpha0, const std::vector<double> &aAlphas, Model &aModel);
	static sharedPtr_t createHyperPriorDirichletIndepBM(const std::vector<double> &aData, Model &aModel);

	static sharedPtr_t createCoevProfDirichletBM(const std::vector<double> &aData,
	   	   	  	  	  	  	  	  	  	  	  	 Model &aModel);
	static sharedPtr_t createCoevProfDirichletBM(const std::vector<unsigned short int> &aIdProfile,
												 const std::vector<float> &aWeight,
												 const std::vector<double> &aAlphas, Model &aModel);
	static sharedPtr_t createCoevProfDirichletBM(const std::vector<size_t> &aIdProfile,
	   	   	  	  	  	  	  	  	  	  	  	 const std::vector<double> &aWeight,
	   	   	  	  	  	  	  	  	  	  	  	 const std::vector<double> &aAlphas, Model &aModel);

	static sharedPtr_t createCoevPoissonConjugateBM(double aAlpha, double aBeta, Model &aModel);

	static sharedPtr_t createCoevGammaShapeConjugateBM(double aKnownAlpha_R, double aAlpha0_R, double aBeta0_R,
			   	   	   	   	   	   	   	   	   	   	   double aKnownAlpha_D, double aAlpha0_D, double aBeta0_D,
			   	   	   	   	   	   	   	   	   	   	   Model &aModel);

	static sharedPtr_t createDefaultBM(Model &aModel);

protected:
	const bool symmetric;
	Model &model;

	void defaultReflection(const vector<size_t> &pInd, Sample &sample) const;
	void defDblReflection(const size_t pId, Sample &sample) const;

};

} // namespace ParameterBlock

#endif /* BLOCKMODIFICATOR_H_ */
