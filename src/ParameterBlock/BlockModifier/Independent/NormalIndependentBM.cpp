//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NormalIndependentBM.cpp
 *
 * @date May 20, 2017
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockModifier/Independent/NormalIndependentBM.h"

namespace ParameterBlock {

NormalIndependentBM::NormalIndependentBM(double aMu, double aStd, Model &aModel) :
		BlockModifier(true, aModel), mu(aMu), std(aStd), distribution(mu, std) {
}

NormalIndependentBM::~NormalIndependentBM() {
}

void NormalIndependentBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	for(size_t iP=0; iP < pInd.size(); ++iP){
		double val = Parallel::mpiMgr().getPRNG()->genNormal(mu, std);
		sample.setDoubleParameter(pInd[iP], val);
	}
	BlockModifier::defaultReflection(pInd, sample);
	sample.setEvaluated(false);
}

double NormalIndependentBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	double moveProb = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProb *= boost::math::pdf(distribution, toSample.getDoubleParameter(pInd[iP]));
	}

	return moveProb;
}

double NormalIndependentBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	// Forward move (theta to theta')
	double moveProbFW = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProbFW *= boost::math::pdf(distribution, toSample.getDoubleParameter(pInd[iP]));
	}

	// Backward move (theta' to theta)
	double moveProbBW = 1.0;
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		moveProbBW *= boost::math::pdf(distribution, fromSample.getDoubleParameter(pInd[iP]));
	}

	return moveProbBW/moveProbFW;
}

void NormalIndependentBM::setWindowSize(const vector<double> &aWinSize) {
	assert(aWinSize.size() == 2);
	mu = aWinSize[0];
	std = aWinSize[1];

	boost::math::normal_distribution<> newDist(mu, std);
	distribution = newDist;
}

std::vector<double> NormalIndependentBM::getWindowSize() const {
	std::vector<double> windSize(2);
	windSize[0] = mu;
	windSize[1] = std;
	return windSize;
}

} /* namespace ParameterBlock */
