//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HyperPriorDirichletIndepBM.cpp
 *
 * @date May 25, 2017
 * @author meyerx
 * @brief
 */
#include "HyperPriorDirichletIndepBM.h"

namespace ParameterBlock {

const HyperPriorDirichletIndepBM::dirichletHP_t HyperPriorDirichletIndepBM::DIRICHLET_HP_TYPE =
		Utils::Statistics::DirichletHyperPrior::EXP_DIRICHLET_HP_TYPE;

HyperPriorDirichletIndepBM::HyperPriorDirichletIndepBM(double aAlpha0, std::vector<double> aAlphas, Model &aModel) :
		BlockModifier(false, aModel), alpha0(aAlpha0), alphas(aAlphas),
		ptrDirichletHP(new Utils::Statistics::DirichletHyperPrior(alpha0, alphas, DIRICHLET_HP_TYPE)) {
}

HyperPriorDirichletIndepBM::~HyperPriorDirichletIndepBM() {
}

void HyperPriorDirichletIndepBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	assert(pInd.size() == alphas.size());

	std::vector<double> values(ptrDirichletHP->drawFromDistribution(Parallel::mpiMgr().getPRNG()));

	for(size_t iP=0; iP < pInd.size(); ++iP){
		double val = values[iP];
		sample.setDoubleParameter(pInd[iP], val);
	}

	BlockModifier::defaultReflection(pInd, sample);
	sample.setEvaluated(false);
}

double HyperPriorDirichletIndepBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	std::vector<double> values(alphas.size());
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] =  toSample.getDoubleParameter(pInd[iP]);
	}
	double moveProb = ptrDirichletHP->computePDF(values);
	return moveProb;
}

double HyperPriorDirichletIndepBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {

	// Forward move (theta to theta')
	std::vector<double> values(alphas.size());
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] = toSample.getDoubleParameter(pInd[iP]);
	}
	double moveProbFW = ptrDirichletHP->computePDF(values);

	// Backward move (theta' to theta)
	for(size_t iP=0; iP < pInd.size(); ++iP) {
		values[iP] = fromSample.getDoubleParameter(pInd[iP]);
	}
	double moveProbBW = ptrDirichletHP->computePDF(values);

	return moveProbBW/moveProbFW;
}

void HyperPriorDirichletIndepBM::setWindowSize(const vector<double> &aWinSize) {
	assert(aWinSize.size() > 1);
	alpha0 = aWinSize.front();
	alphas.assign(aWinSize.begin()+1, aWinSize.end());
	ptrDirichletHP.reset(new Utils::Statistics::DirichletHyperPrior(alpha0, alphas, DIRICHLET_HP_TYPE));
}

std::vector<double> HyperPriorDirichletIndepBM::getWindowSize() const {
	std::vector<double> tmp(1, alpha0);
	tmp.insert(tmp.end(), alphas.begin(), alphas.end());
	return tmp;
}

} /* namespace ParameterBlock */
