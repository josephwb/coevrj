//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockModifierState.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#ifndef BLOCKMODIFIERSTATE_H_
#define BLOCKMODIFIERSTATE_H_

#include <stddef.h>
#include <string>
#include <vector>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class BlockModifier;
}
namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace ParameterBlock {
namespace State {

class BlockModifierState {
	friend class ParameterBlock::BlockModifier;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	BlockModifierState();
	~BlockModifierState();

	void setBMType(size_t aBMType) { bmType = aBMType; }

	size_t getBMType() const { return bmType; }
	std::vector<double> getValues() const { return values; }

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t bmType;
	std::vector<double> values;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* BLOCKMODIFIERSTATE_H_ */
