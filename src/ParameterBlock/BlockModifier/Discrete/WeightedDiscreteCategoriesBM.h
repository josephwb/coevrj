//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WeightedDiscreteCategoriesBM.h
 *
 * @date May 10, 2017
 * @author meyerx
 * @brief
 */
#ifndef WEIGHTEDDISCRETECATEGORIESBM_H_
#define WEIGHTEDDISCRETECATEGORIESBM_H_

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"

namespace ParameterBlock {

class WeightedDiscreteCategoriesBM: public BlockModifier {
public:
	WeightedDiscreteCategoriesBM(const std::vector<double> &data, Model &aModel);
	WeightedDiscreteCategoriesBM(const std::vector<size_t> &aIdProfiles, const std::vector<double> &aWeight, Model &aModel);
	~WeightedDiscreteCategoriesBM();

	void updateParameters(const vector<size_t> &pInd, Sample &sample) const;
	double getUpdateProbability(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;
	double getUpdateProbabilityRatio(const vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const;

	void setWindowSize(const std::vector<double> &aWinSize);
	vector<double> getWindowSize() const;

	bm_name_t getName() const {return WEIGHTED_DISCRETE_CATEGORIES;}

private:
	std::vector<size_t> idProfiles;
	std::vector<double> weight;
};

} /* namespace ParameterBlock */

#endif /* WEIGHTEDDISCRETECATEGORIESBM_H_ */
