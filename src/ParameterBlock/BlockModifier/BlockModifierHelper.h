//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockModifierHelper.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#ifndef BLOCKMODIFIERHELPER_H_
#define BLOCKMODIFIERHELPER_H_

#include <stddef.h>
#include <vector>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Model.h"
#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "ParameterBlock/BlockModifier/BlockModifierState.h"
#include "ParameterBlock/BlockState.h"

namespace ParameterBlock { namespace State { class BlockModifierState; } }
namespace StatisticalModel { class Model; }

namespace ParameterBlock {
namespace Support {

BlockModifier::sharedPtr_t createBlockModifier ( size_t N, StatisticalModel::Model& model,
		State::BlockModifierState &state);

void saveBMState ( BlockModifier::sharedPtr_t aBM,
				   State::BlockModifierState &state);

} /* namespace Support */
} /* namespace ParameterBlock */

#endif /* BLOCKMODIFIERHELPER_H_ */

