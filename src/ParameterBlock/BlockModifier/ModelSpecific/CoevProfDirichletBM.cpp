//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CoevProfDirichletBM.cpp
 *
 * @date May 24, 2017
 * @author meyerx
 * @brief
 */
#include "ParameterBlock/BlockModifier/ModelSpecific/CoevProfDirichletBM.h"

namespace ParameterBlock {

CoevProfDirichletBM::CoevProfDirichletBM(const std::vector<double> &data, Model &aModel) :
		BlockModifier(false, aModel),
		dummyVec1(data.begin()+1, data.begin()+1+(size_t)data[0]),
		dummyVec2(data.begin()+1+(size_t)data[0], data.end()),
		profileBM(dummyVec1, aModel),
		paramsBM(dummyVec2, aModel) {

	dummyVec1.clear();
	dummyVec2.clear();
}

CoevProfDirichletBM::CoevProfDirichletBM(const std::vector<size_t> &aIdProfiles, const std::vector<double> &aWeight,
										 const std::vector<double> &aAlphas, Model &aModel) :
		BlockModifier(false, aModel), profileBM(aIdProfiles, aWeight, aModel), paramsBM(aAlphas, aModel) {

}

CoevProfDirichletBM::~CoevProfDirichletBM() {
}

void CoevProfDirichletBM::updateParameters(const std::vector<size_t> &pInd, Sample &sample) const {
	std::vector<size_t> profId(1, pInd.front());
	profileBM.updateParameters(profId, sample);

	std::vector<size_t> paramsId(pInd.begin()+1, pInd.end());
	paramsBM.updateParameters(paramsId, sample);
	sample.setEvaluated(false);
}


double CoevProfDirichletBM::getUpdateProbability(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	std::vector<size_t> profId(1, pInd.front());
	double moveProb = profileBM.getUpdateProbability(profId, fromSample, toSample);

	std::vector<size_t> paramsId(pInd.begin()+1, pInd.end());
	moveProb *= paramsBM.getUpdateProbability(paramsId, fromSample, toSample);

	return moveProb;
}

double CoevProfDirichletBM::getUpdateProbabilityRatio(const std::vector<size_t> &pInd, const Sample &fromSample, const Sample &toSample) const {
	std::vector<size_t> profId(1, pInd.front());
	double probRatio = profileBM.getUpdateProbabilityRatio(profId, fromSample, toSample);

	std::vector<size_t> paramsId(pInd.begin()+1, pInd.end());
	probRatio *= paramsBM.getUpdateProbabilityRatio(paramsId, fromSample, toSample);

	return probRatio;
}

void CoevProfDirichletBM::setWindowSize(const std::vector<double> &aWinSize) {
	size_t nbDataProfileBM = aWinSize[0];
	std::vector<double> dataProfileBM(aWinSize.begin()+1, aWinSize.begin()+1+nbDataProfileBM);
	std::vector<double> dataParamsBM(aWinSize.begin()+1+nbDataProfileBM, aWinSize.end());
	profileBM.setWindowSize(dataProfileBM);
	paramsBM.setWindowSize(dataParamsBM);
}

std::vector<double> CoevProfDirichletBM::getWindowSize() const {
	std::vector<double> dataProfileBM(profileBM.getWindowSize());
	std::vector<double> dataParamsBM(paramsBM.getWindowSize());

	std::vector<double> data(1+dataProfileBM.size()+dataParamsBM.size());

	data[0] = dataProfileBM.size();
	size_t start1 = 1;
	size_t end1 = start1 + dataProfileBM.size();
	assert(end1-start1 == dataProfileBM.size());
	for(size_t iD=start1; iD<end1; ++iD) data[iD] = dataProfileBM[iD];

	size_t start2 = end1;
	size_t end2 = start2+dataParamsBM.size();
	assert(end2-start2 == dataParamsBM.size() && end2 == data.size());
	for(size_t iD=start2; iD<end2; ++iD) data[iD] = dataParamsBM[iD];

	return data;
}


} /* namespace ParameterBlock */

