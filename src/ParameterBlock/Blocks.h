//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Blocks.h
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCKS_H_
#define BLOCKS_H_

#include <stddef.h>
#include <list>
#include <set>
#include <stdexcept>
#include <vector>

#include "ParameterBlock/Block.h"
#include "Utils/Code/IDManagement/IDManager.h"
#include "Utils/Uncopyable.h"

using namespace std;

namespace ParameterBlock {

class Blocks : public Uncopyable {
public:
	Blocks();
	~Blocks();

	void addBaseBlock(Block::sharedPtr_t aBlock);
	void removeBaseBlocks(std::vector<size_t> blockId);

	void addDynamicBlock(Block::sharedPtr_t aBlock);
	bool tryToRemoveDynamicBlock(size_t blockId);
	void removeDynamicBlocks(std::vector<size_t> blockId);

	Block::sharedPtr_t getBlock(size_t aInd);
	Block::sharedPtr_t getBlock(size_t aInd) const;

	Block::sharedPtr_t getNextBlock(size_t aCurrentBlockID);
	Block::sharedPtr_t getNextBlock(size_t aCurrentBlockID) const;

	Block::sharedPtr_t getPreviousBlock(size_t aCurrentBlockID);
	Block::sharedPtr_t getPreviousBlock(size_t aCurrentBlockID) const;

	Block::sharedPtr_t getFirstBlock();
	Block::sharedPtr_t getFirstBlock() const;

	Block::sharedPtr_t getLastBlock();
	Block::sharedPtr_t getLastBlock() const;

	std::vector<Block::sharedPtr_t> getBlocks() const;

	std::vector<size_t> getOptimisableBlocksIdx();
	std::vector<size_t> getOptimisableParametersIdx();

	size_t getNBaseBlocks() const;
	size_t getNDynamicBlocks() const;
	size_t getNTotalBlocks() const;
	size_t nbUniqueParam() const;

	double getSumFrequencies() const;

	size_t getVersion() const;

	void reset();
	void resetBaseBlocks();

private:

	static Utils::IDManagement::IDManager idManager;

	typedef std::vector<Block::sharedPtr_t> vecBlocks_t;
	typedef vecBlocks_t::iterator itVecBlocks_t;
	typedef vecBlocks_t::const_iterator constItVecBlocks_t;

	typedef std::list<Block::sharedPtr_t> listBlocks_t;
	typedef listBlocks_t::iterator itListBlocks_t;
	typedef listBlocks_t::const_iterator constItListBlocks_t;
	typedef listBlocks_t::const_reverse_iterator revItListBlocks_t;
	typedef listBlocks_t::const_reverse_iterator constRevItListBlocks_t;

	size_t version; // Should be "unique"

	mutable double freqSum;

	vecBlocks_t vecBlocks;
	listBlocks_t listDynBlocks;

};

} // namespace ParameterBlock

#endif /* BLOCKS_H_ */
