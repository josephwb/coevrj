//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockState.h
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef BLOCKSTATE_H_
#define BLOCKSTATE_H_

#include <stddef.h>
#include <string>
#include <vector>

#include "BlockModifier/BlockModifierState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class Block;
}
namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace ParameterBlock {
namespace State {

class BlockState {
	friend class ParameterBlock::Block;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	BlockState();
	~BlockState();

	BlockModifierState& getGlobalBMState() { return globalBMState; }
	std::vector<BlockModifierState>& getLocalBMState() { return localBMState; };

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t m_rej, m_acc, m_throw, id;
	double rollMeanPropTime, rollMeanCompTime;
	double avgPropTime, avgCompTime;
	size_t adaptiveType;
	size_t blockOpti;
	std::string m_name;
	std::vector<size_t> pIndices;
	double totalFreq;
	std::vector<double> pFreq;

	BlockModifierState globalBMState;
	std::vector<BlockModifierState> localBMState;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* BLOCKSTATE_H_ */
