//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BlockState.cpp
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#include "BlockState.h"

#include <boost/serialization/nvp.hpp>

namespace ParameterBlock {
namespace State {

BlockState::BlockState() {

	m_rej = 0;
	m_acc = 0;
	m_throw = 0;
	id = 0;
	rollMeanPropTime = 0.;
	rollMeanCompTime = 0.;
	avgPropTime = 0.;
	avgCompTime = 0.;
	adaptiveType = 0;
	blockOpti = 0;
	totalFreq = 0.;

}

BlockState::~BlockState() {
}

template<class Archive>
void BlockState::serialize(Archive & ar, const unsigned int version) {

	ar & BOOST_SERIALIZATION_NVP( m_acc );
	ar & BOOST_SERIALIZATION_NVP( m_rej );
	ar & BOOST_SERIALIZATION_NVP( m_throw );
	ar & BOOST_SERIALIZATION_NVP( m_name );
	ar & BOOST_SERIALIZATION_NVP( id );
	ar & BOOST_SERIALIZATION_NVP( avgPropTime );
	ar & BOOST_SERIALIZATION_NVP( avgCompTime );
	ar & BOOST_SERIALIZATION_NVP( rollMeanPropTime );
	ar & BOOST_SERIALIZATION_NVP( rollMeanCompTime );
	ar & BOOST_SERIALIZATION_NVP( adaptiveType );
	ar & BOOST_SERIALIZATION_NVP( blockOpti );
	ar & BOOST_SERIALIZATION_NVP( pIndices );
	ar & BOOST_SERIALIZATION_NVP( totalFreq );
	ar & BOOST_SERIALIZATION_NVP( pFreq );
	ar & BOOST_SERIALIZATION_NVP( globalBMState );
	ar & BOOST_SERIALIZATION_NVP( localBMState );

}

template void BlockState::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void BlockState::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void BlockState::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void BlockState::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);

} /* namespace State */
} /* namespace ParameterBlock */
