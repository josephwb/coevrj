//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RobinMonroConv.h
 *
 * @date Jun 10, 2014
 * @author meyerx
 * @brief
 */
#ifndef ROBINMONROCONV_H_
#define ROBINMONROCONV_H_

#include <stddef.h>
#include <algorithm>
#include <cmath>

#include "ParameterBlock/BlockStats/Config/Container/UpdaterCfg.h"
#include "RMCState.h"

namespace ParameterBlock { namespace State { class RMCState; } }

using namespace std;

namespace ParameterBlock {

using Config::UpdaterCfg;

class RobinMonroConv {
public:
	RobinMonroConv(const UpdaterCfg *aUpdCfg);
	virtual ~RobinMonroConv();

	void reset();

	size_t getT() const;

	virtual void saveState(State::RMCState &state) const;
	virtual void loadState(const State::RMCState &state);

protected:

	const UpdaterCfg *updCfg;

	size_t t;
	double exponent;

	double _computeGamma() const;
	void _incrementT();

};

} // namespace ParameterBlock


#endif /* ROBINMONROCONV_H_ */
