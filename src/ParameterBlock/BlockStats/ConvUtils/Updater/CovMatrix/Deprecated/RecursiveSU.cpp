//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RecursiveSU.cpp
 *
 * @date Jun 11, 2014
 * @author meyerx
 * @brief
 */
#include "RecursiveSU.h"

namespace ParameterBlock {

RecursiveSU::RecursiveSU(const unsigned int aN, const vector<MeanUpdater*> &means,
						 const UpdaterCfg *aUpdCfg, const ConvCheckerCfg *aConvCfg) :
		CovMatrixUpdater(aN, aUpdCfg, aConvCfg)
{
	// init matMu
	matMu.resize(N*N, 0.);
	setInitialMeans(means);
}

RecursiveSU::~RecursiveSU() {
}

void RecursiveSU::addVals(vector<double> X, const vector<MeanUpdater*> &means) {
	double dblT = t;
	vector<double> tmp(matMu), mu(N, 0.);

	Eigen::Map<Eigen::MatrixXd> mappedTmp(tmp.data(), N, N);
	Eigen::Map<Eigen::MatrixXd> mappedMatMu(matMu.data(), N, N);
	Eigen::Map<Eigen::MatrixXd> mappedSigma(sigma.data(), N, N);
	Eigen::Map<Eigen::VectorXd> mappedX(X.data(), N);
	Eigen::Map<Eigen::VectorXd> mappedMu(mu.data(), N);

	Eigen::VectorXd copiedX(mappedX);
	Eigen::VectorXd copiedMu(mappedMu);


	for(unsigned int i=0; i<N; ++i){
		mu[i] = means[i]->getMean();
	}

	// tmp = tmp*dblT
	mappedTmp = mappedTmp*dblT;

	// Add X*X^T to tmp
	mappedTmp = mappedTmp + copiedX * copiedX.adjoint();

	// Process matMu t
	mappedMatMu = copiedMu * copiedMu.adjoint();

	double c1 = dblT+1.;
	double c2 = (dblT-1.)/dblT;
	mappedTmp = mappedTmp - c1*mappedMatMu;
	mappedSigma *= c2;
	mappedSigma += mappedTmp/dblT;

	hasChanged = true;
	updateMCC();

	++t;
}

void RecursiveSU::reset(const bool keepSigma) {
	CovMatrixUpdater::reset(keepSigma);
	if(!keepSigma){
		for(unsigned int i=0; i<N*N; ++i){
			matMu[i] = 0.;
		}
	}
}

void RecursiveSU::setInitialMeans(const vector<MeanUpdater*> &means) {
	vector<double> mu(N, 0.);

	for(unsigned int i=0; i<N; ++i){
		mu[i] = means[i]->getMean();
	}

	Eigen::Map<Eigen::MatrixXd> mappedMatMu(matMu.data(), N, N);
	Eigen::Map<Eigen::VectorXd> mappedMu(mu.data(), N);

	Eigen::VectorXd copiedMu(mappedMu);

	mappedMatMu = copiedMu*copiedMu.adjoint();
}

} // namespace ParameterBlock

