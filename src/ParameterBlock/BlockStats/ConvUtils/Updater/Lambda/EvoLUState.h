//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoLUState.h
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef EVOLUSTATE_H_
#define EVOLUSTATE_H_

#include <stddef.h>

#include "../../CC/MCCState.h"
#include "../../RMC/RMCState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class BatchEvoLU;
	class BatchLightEvoLU;
	class LambdaUpdater;
	class LightLambdaUpdater;
}

namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}


namespace ParameterBlock {
namespace State {

class EvoLUState {
	friend class ParameterBlock::LambdaUpdater;
	friend class ParameterBlock::LightLambdaUpdater;
	friend class ParameterBlock::BatchLightEvoLU;
	friend class ParameterBlock::BatchEvoLU;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	EvoLUState();
	~EvoLUState();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:
	size_t t, N;
	double alpha, Sd, traceAlpha, rollingMean;
	RMCState rmcState;
	MCCState mccState;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* EVOLUSTATE_H_ */
