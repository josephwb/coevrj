//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EvoMUState.h
 *
 * @date Jan 23, 2016
 * @author meyerx
 * @brief
 */
#ifndef EVUMUSTATE_H_
#define EVUMUSTATE_H_

#include <stddef.h>

#include "../../CC/MCCState.h"
#include "../../RMC/RMCState.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

// Forward declaration
namespace ParameterBlock {
	class BatchEvoMU;
	class EvolutiveMU;
	class MeanUpdater;
	class RecursiveMU;
}
namespace Sampler {
namespace BlockOptimizer {
	class BlockUpdater;
}
}

namespace ParameterBlock {
namespace State {

class EvoMUState {
	friend class ParameterBlock::MeanUpdater;
	friend class ParameterBlock::EvolutiveMU;
	friend class ParameterBlock::BatchEvoMU;
	friend class ParameterBlock::RecursiveMU;
	friend class Sampler::BlockOptimizer::BlockUpdater;
public:
	EvoMUState();
	~EvoMUState();

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

private:

	size_t t;
	double mean;
	RMCState rmcState;
	MCCState mccState;

	friend class boost::serialization::access;

};

} /* namespace State */
} /* namespace ParameterBlock */

#endif /* EVUMUSTATE_H_ */
