//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DefaultFactory.h
 *
 * @date May 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef DEFAULTFACTORY_H_
#define DEFAULTFACTORY_H_

#include <ParameterBlock/BlockStats/Config/ConfigFactory.h>

namespace ParameterBlock {
namespace Config {

class DefaultFactory: public ConfigFactory {
public:
	DefaultFactory() : ConfigFactory() {}
	~DefaultFactory(){}

	BlockStatCfg::sharedPtr_t createConfig(const size_t N) const {
		return BlockStatCfg::getDefault(N);
	}
};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* DEFAULTFACTORY_H_ */
