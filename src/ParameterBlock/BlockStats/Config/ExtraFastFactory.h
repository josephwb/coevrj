//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ExtraFastFactory.h
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef EXTRAFASTFACTORY_H_
#define EXTRAFASTFACTORY_H_

#include <ParameterBlock/BlockStats/Config/ConfigFactory.h>

namespace ParameterBlock {
namespace Config {

class ExtraFastFactory: public ParameterBlock::Config::ConfigFactory {
public:
	ExtraFastFactory(){}
	~ExtraFastFactory(){}

	BlockStatCfg::sharedPtr_t createConfig(const size_t N) const {
		return BlockStatCfg::getExtraFast(N);
	}

};

} /* namespace Config */
} /* namespace ParameterBlock */

#endif /* EXTRAFASTFACTORY_H_ */
