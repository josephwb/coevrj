//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Blocks.cpp
 *
 *  Created on: 27 sept. 2013
 *  Modified on: 10 april 2017 (dynamic blocks)
 *      Author: meyerx
 */

#include "Blocks.h"

#include <assert.h>


namespace ParameterBlock {

Utils::IDManagement::IDManager Blocks::idManager(Block::NB_MAX_BASE_BLOCKS, std::numeric_limits<size_t>::max());

Blocks::Blocks() {
	freqSum = 0.0;
	version = 0;
}

Blocks::~Blocks() {
}

void Blocks::addBaseBlock(Block::sharedPtr_t aBlock){
	vecBlocks.push_back(aBlock);
	vecBlocks.back()->setId(vecBlocks.size()-1);
	assert(vecBlocks.size()-1 < Block::NB_MAX_BASE_BLOCKS);

	freqSum += aBlock->getTotalFreq();
	version++;
}

Block::sharedPtr_t Blocks::getBlock(size_t aInd) {
	return static_cast<const Blocks*>(this)->getBlock(aInd);
}

Block::sharedPtr_t Blocks::getBlock(size_t aInd) const {
	if(aInd < getNBaseBlocks()) {
		return vecBlocks[aInd];
	} else {
		constItListBlocks_t itB = std::find_if(listDynBlocks.begin(), listDynBlocks.end(), HasSameBlockID(aInd));
		if(itB != listDynBlocks.end()) {
			return *(itB);
		} else {
			return Block::sharedPtr_t();
		}
	}
}

std::vector<Block::sharedPtr_t> Blocks::getBlocks() const {
	std::vector<Block::sharedPtr_t> allBlocks(vecBlocks);
	allBlocks.insert(allBlocks.end(), listDynBlocks.begin(), listDynBlocks.end());
	return allBlocks;
}

void Blocks::removeBaseBlocks(std::vector<size_t> blockId) {
	std::sort(blockId.begin(), blockId.end());
	assert(blockId.back() < getNBaseBlocks()); // FIXME CHECK THAT
	assert(blockId.back() < Block::NB_MAX_BASE_BLOCKS);

	// For each block id to remove
	for(int iB=blockId.size()-1; iB >= 0; --iB) {
		// Block vector : 1) Get the position
		size_t pos = vecBlocks.size();
		for(size_t iV=vecBlocks.size(); iV > 0; --iV ) { // reverse loop
			if(vecBlocks[iV-1]->getId() == blockId[iB]) {
				pos = iV-1;
				break;
			}
		}
		assert(pos < vecBlocks.size());

		// Update freqSum cleanup
		freqSum -= (*(vecBlocks.begin()+pos))->getTotalFreq();

		// 2) cleanup
		vecBlocks.erase(vecBlocks.begin()+pos);
	}

	// Recompute the blocks id for base blocks (nasty.)
	for(size_t iB=0; iB<vecBlocks.size(); ++iB) {
		vecBlocks[iB]->setId(iB);
	}

	version++;
}


void Blocks::addDynamicBlock(Block::sharedPtr_t aBlock) {
	// Give the next ID
	if(aBlock->getId() == 0) { // New block
		aBlock->setId(idManager.allocateId());
	} else { // Loading a block -> serialize e.g.
		assert(aBlock->getId() >= Block::NB_MAX_BASE_BLOCKS);
		idManager.markAsUsed(aBlock->getId());
	}

	// Get insert point (sorted insert)
	itListBlocks_t it = std::find_if(listDynBlocks.begin(), listDynBlocks.end(), HasBiggerBlockID(aBlock->getId()));

	if(it != listDynBlocks.end()) {
		listDynBlocks.insert(it, aBlock);
	} else {
		listDynBlocks.push_back(aBlock);
	}

	freqSum += aBlock->getTotalFreq();

	version++;
}

bool Blocks::tryToRemoveDynamicBlock(size_t blockId) {

	assert(blockId >= Block::NB_MAX_BASE_BLOCKS);

	// For each block id to remove
	// Block vector : 1) Get the position
	itListBlocks_t it = std::find_if(listDynBlocks.begin(), listDynBlocks.end(), HasSameBlockID(blockId));
	if(it == listDynBlocks.end()) {
		return false;
	}
	// Update Frequency sum
	freqSum -= (*it)->getTotalFreq();

	// 2) cleanup
	idManager.freeId(blockId);
	listDynBlocks.erase(it);

	version++;

	return true;
}

void Blocks::removeDynamicBlocks(std::vector<size_t> blockId) {

	std::sort(blockId.begin(), blockId.end());
	assert(blockId.front() >= Block::NB_MAX_BASE_BLOCKS);

	// For each block id to remove
	for(size_t iB=0; iB < blockId.size(); ++iB) {
		// Block vector : 1) Get the position
		itListBlocks_t it = std::find_if(listDynBlocks.begin(), listDynBlocks.end(), HasSameBlockID(blockId[iB]));
		assert(it != listDynBlocks.end());
		// Update Frequency sum
		freqSum -= (*it)->getTotalFreq();

		// 2) cleanup
		idManager.freeId(blockId[iB]);
		listDynBlocks.erase(it);
	}

	version++;
}


std::vector<size_t> Blocks::getOptimisableBlocksIdx() {
	std::vector<size_t> blocksIdx;
	for(size_t iB=0; iB<getNBaseBlocks(); ++iB) {
		if(vecBlocks[iB]->isSupportingBlockOptimisation()) {
			blocksIdx.push_back(iB);
		}
	}
	return blocksIdx;
}

std::vector<size_t> Blocks::getOptimisableParametersIdx() {
	std::vector<size_t> blocksIdx = getOptimisableBlocksIdx();
	std::vector<size_t> paramIdx;
	for(size_t iB=0; iB<blocksIdx.size(); ++iB) {
		const std::vector<size_t> &pInd = vecBlocks[blocksIdx[iB]]->getPIndices();
		paramIdx.insert(paramIdx.end(), pInd.begin(), pInd.end());
	}

	std::sort( paramIdx.begin(), paramIdx.end() );
	paramIdx.erase( std::unique( paramIdx.begin(), paramIdx.end() ), paramIdx.end() );

	return paramIdx;
}

size_t Blocks::getNBaseBlocks() const {
	return vecBlocks.size();
}

size_t Blocks::getNDynamicBlocks() const {
	return listDynBlocks.size();
}

size_t Blocks::getNTotalBlocks() const {
	return getNBaseBlocks()+getNDynamicBlocks();
}

size_t Blocks::nbUniqueParam() const {
	std::set<int> tmp;
	for(constItVecBlocks_t itB=vecBlocks.begin(); itB != vecBlocks.end(); ++itB){
		tmp.insert((*itB)->getPIndices().begin(), (*itB)->getPIndices().end());
	}

	return tmp.size();
}

Block::sharedPtr_t Blocks::getNextBlock(size_t aCurrentBlockID) {
	return static_cast<const Blocks*>(this)->getNextBlock(aCurrentBlockID);
}

Block::sharedPtr_t Blocks::getNextBlock(size_t aCurrentBlockID) const {

	if(aCurrentBlockID >= getNBaseBlocks()) { // The block is a dynamic block
		if(listDynBlocks.empty() || aCurrentBlockID >= listDynBlocks.back()->getId()) {
			// Next block is outside range
			return Block::sharedPtr_t();
		} else {
			// We have to be careful since aCurrentBlockID may no longer exists!
			// -> Search the first block with id bigger than (aCurrentBlockID)
			constItListBlocks_t it = std::find_if(listDynBlocks.begin(), listDynBlocks.end(), HasBiggerBlockID(aCurrentBlockID));
			return (*it);
		}
	} else { // The block is a base block
		if(aCurrentBlockID == getNBaseBlocks()-1) { // last block
			if(listDynBlocks.empty()) { // No dyn blocks
				return Block::sharedPtr_t();
			} else { // Some dyn blocks
				return listDynBlocks.front();
			}
		} else { // Next base blocks
			return vecBlocks[aCurrentBlockID+1];
		}
	}

}

Block::sharedPtr_t Blocks::getPreviousBlock(size_t aCurrentBlockID) {
	return static_cast<const Blocks*>(this)->getPreviousBlock(aCurrentBlockID);
}

Block::sharedPtr_t Blocks::getPreviousBlock(size_t aCurrentBlockID) const {

	if(aCurrentBlockID == 0) { // There are no previous block
		return Block::sharedPtr_t();
	} else if(aCurrentBlockID < getNBaseBlocks()) { // Just decrease of 1
		return vecBlocks[aCurrentBlockID-1];
	} else { // The block is dynamic
		// We have to be careful since aCurrentBlockID may no longer exists!
		// -> Search the first block with id smaller than (aCurrentBlockID)
		constRevItListBlocks_t it = std::find_if(listDynBlocks.rbegin(), listDynBlocks.rend(), HasSmallerBlockID(aCurrentBlockID));
		if(it != listDynBlocks.rend()) {
			return *it;
		} else {
			return vecBlocks.back(); // Last of base blocks
		}
	}

}

Block::sharedPtr_t Blocks::getFirstBlock() {
	return static_cast<const Blocks*>(this)->getFirstBlock();
}

Block::sharedPtr_t Blocks::getFirstBlock() const {
	assert(!vecBlocks.empty() || !listDynBlocks.empty());

	if(!vecBlocks.empty()) {
		return vecBlocks.front();
	} else {
		return listDynBlocks.front();
	}
}

Block::sharedPtr_t Blocks::getLastBlock() {
	return static_cast<const Blocks*>(this)->getLastBlock();
}

Block::sharedPtr_t Blocks::getLastBlock() const {
	assert(!vecBlocks.empty() || !listDynBlocks.empty());

	if(!listDynBlocks.empty()) {
		return listDynBlocks.back();
	} else {
		return vecBlocks.back();
	}
}


double Blocks::getSumFrequencies() const {
	return freqSum;
}

size_t Blocks::getVersion() const {
	return version;
}


void Blocks::reset() {
	vecBlocks.clear();
	listDynBlocks.clear();
	freqSum = 0.;
	version = 0;
}

void Blocks::resetBaseBlocks() {
	std::vector<size_t> blocksId;
	for(size_t iB=0; iB<vecBlocks.size(); ++iB) {
		blocksId.push_back(vecBlocks[iB]->getId());
	}

	removeBaseBlocks(blocksId);
	//vecBlocks.clear();

	version = 0;
}

} // namespace ParameterBlock

