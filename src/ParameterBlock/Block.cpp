//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Block.cpp
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#include "Block.h"

#include <stdlib.h>

#include "ParameterBlock/BlockModifier/BlockModifierInterface.h"
#include "ParameterBlock/BlockState.h"
#include "mpi.h"

namespace Sampler { class Sample; }
namespace boost { namespace accumulators { namespace tag { struct variance; } } }

namespace ParameterBlock {

const size_t Block::ROLLING_MEAN_SIZE = 8;
const size_t Block::NB_MAX_BASE_BLOCKS = 1000000;

Block::Block(adaptiveType_t aAdaptiveType, blockOptimisation_t aBlockOpti) :
		m_rej(0), m_acc(0), m_throw(0), id(0), totalFreq(0.0),
		rollMPropTime(boostAcc::tag::rolling_mean::window_size = ROLLING_MEAN_SIZE),
		rollMCompTime(boostAcc::tag::rolling_mean::window_size = ROLLING_MEAN_SIZE) {

	adaptiveType = aAdaptiveType;
	blockOpti = aBlockOpti;
}

Block::~Block() {
}

void Block::addParameter(size_t aInd, double freq){
	pIndices.push_back(aInd);
	pFreq.push_back(freq);
	totalFreq += freq;
	bms.push_back(BlockModifier::sharedPtr_t());
}

void Block::addParameter(size_t aInd, BlockModifier::sharedPtr_t aBM, double freq){
	pIndices.push_back(aInd);
	pFreq.push_back(freq);
	totalFreq += freq;
	bms.push_back(aBM);
}

int Block::getPIndice(size_t aInd) const {
	return pIndices[aInd];
}

const vector<size_t>& Block::getPIndices() const {
	return pIndices;
}

const vector<double>& Block::getPFreqs() const {
	return pFreq;
}

double Block::getTotalFreq() const {
	return totalFreq;
}

bool Block::hasBlockModifier() const {
	return bm != NULL;
}

bool Block::hasBlockModifier(int ind) const {
	return (int)bms.size() > ind && bms[ind] != NULL;
}

BlockModifier::sharedPtr_t  Block::getBlockModifier() const {
	return bm;
}

BlockModifier::sharedPtr_t  Block::getBlockModifier(const int ind) const {
	return bms[ind];
}

void Block::setBlockModifier(BlockModifier::sharedPtr_t aBM) {
	bm = aBM;
}

void Block::setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM) {
	bms[ind] = aBM;
}

void Block::setBlockModifier(BlockModifier::sharedPtr_t aBM) const {
	bm = aBM;
}

void Block::setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM) const {
	bms[ind] = aBM;
}

size_t Block::size() const {
	return pIndices.size();
}

void Block::setId(size_t aId) {
	id = aId;
}

size_t Block::getId() const {
	return id;
}

void Block::setName(const string a_name) {
	m_name = a_name;
}

const string Block::getName() const {
	return m_name;
}

std::vector<double> Block::getTimes() const {
	std::vector<double> times(2, 0.);
	double mPT = boost::accumulators::mean(accPropTime);
	double mCT = boost::accumulators::mean(accCompTime);

	times[0] = mPT;
	times[1] = mCT;

	return times;
}

double Block::getTotalTime() const {
	double mPT = boost::accumulators::mean(accPropTime);
	double mCT = boost::accumulators::mean(accCompTime);
	return mPT + mCT;
}

std::vector<double> Block::getLastTimes() const {
	std::vector<double> times(2, 0.);
	double mPT = boost::accumulators::rolling_mean(rollMPropTime);
	double mCT = boost::accumulators::rolling_mean(rollMCompTime);

	times[0] = mPT;
	times[1] = mCT;

	return times;
}

void Block::proposeMove(Sampler::Sample &sample) const {
	if(hasBlockModifier()){
		getBlockModifier()->updateParameters(pIndices, sample);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			double start = MPI_Wtime();
			double sValue = sample.getDoubleParameter(pIndices[iP]);
			if(hasBlockModifier(iP)){
				getBlockModifier(iP)->updateParameters(vector<size_t>(1, pIndices[iP]), sample);
			} else {
				cerr << "Block::proposeMove(Sampler::Sample &sample)" << endl;
				cerr << "Error : no BlockModifier specified for parameter " << pIndices[iP] << endl;
				abort();
			}
			double end = MPI_Wtime();
			if(end-start > 0.5) {
				cout << "[WARNING] Block prop time :  " << end-start << " : " << getId() << " - Prop " << getPIndice(iP) ;
				cout << " : " << getBlockModifier(iP)->getWindowSize()[0] << " : " << sValue << std::endl;
			}
		}
	}
}

double Block::getMoveProbability(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const {
	double q = 1.0;

	if(hasBlockModifier()){
		q *= getBlockModifier()->getUpdateProbability(pIndices, fromSample, toSample);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			size_t indP = pIndices[iP];
			if(hasBlockModifier(iP)){
				q *= getBlockModifier(iP)->getUpdateProbability(vector<size_t>(1,indP), fromSample, toSample);
			} else {
				cerr << "double Block::getMoveProbability(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;" << endl;
				cerr << "Error : no BlockModifier specified for parameter " << pIndices[iP] << endl;
				abort();
			}
		}
	}
	return q;
}

double Block::getMoveProbabilityRatio(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const {
	double q = 1.0;

	if(hasBlockModifier()){
		q *= getBlockModifier()->getUpdateProbabilityRatio(pIndices, fromSample, toSample);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			size_t indP = pIndices[iP];
			if(hasBlockModifier(iP)){
				q *= getBlockModifier(iP)->getUpdateProbabilityRatio(vector<size_t>(1,indP), fromSample, toSample);
			} else {
				cerr << "double Block::getMoveProbability(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;" << endl;
				cerr << "Error : no BlockModifier specified for parameter " << pIndices[iP] << endl;
				abort();
			}
		}
	}
	return q;
}

void Block::updateMeanTimes(double aPropTime, double aCompTime) const {
	// Overall mean
	accPropTime(aPropTime);
	accCompTime(aCompTime);

	// Rolling mean
	rollMPropTime(aPropTime);
	rollMCompTime(aCompTime);
}

const std::string Block::toString() const {
	std::stringstream ss;
	ss << "[" << id << "] " << (isBaseBlock() ? " [BASE] " : " [DYN] ") << m_name << std::endl;

	double mPT = boost::accumulators::mean(accPropTime);
	double mCT = boost::accumulators::mean(accCompTime);
	ss.precision(5);
	ss << "Avg. proposal, computing, summed times = { " << std::scientific << mPT;
	ss << ", "  << std::scientific << mCT << ", " << std::scientific << mPT + mCT << " }";
	ss << std::endl;
	for(unsigned int i=0; i<pIndices.size(); ++i) {
		ss << pIndices[i] << ", ";
	}
	ss << endl;

	size_t m_total = m_rej+m_acc;
	if(m_total > 0) {
		ss << "Accepted/(Accepted+Rejected) = " << m_acc << " / " << m_total << " = " << (float)m_acc/(float)m_total << endl;
		if(m_throw > 0) {
			m_total += m_throw;
			ss << "Threw/Selected = " << m_throw << " / " << m_total << " = " << (float)m_throw/(float)m_total << endl;
		}
	}

	if(hasBlockModifier()){
		std::vector<double> winSize(bm->getWindowSize());
		if((double)size() == (double)sqrt((double)winSize.size())) {
			boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > accTmp;
			for(unsigned int i=0; i<size(); ++i) {
				accTmp(winSize[i+size()*i]);
			}
			ss << "[Correlated move] Avg element window size = " << boost::accumulators::mean(accTmp) << "\t||\t std = ";
			ss << sqrt(boost::accumulators::variance(accTmp));
			ss << std::endl;
		} else {
			ss << "[Correlated move] 1st element window size = " << bm->getWindowSize()[0] << endl;
		}
	} else {
		boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > accTmp;
		for(unsigned int i=0; i<pIndices.size(); ++i) {
			if(hasBlockModifier(i)){
				accTmp(bms[i]->getWindowSize()[0]);
			}
		}
		ss << "[Indep move] Avg window size = " << boost::accumulators::mean(accTmp) << "\t||\t std = ";
		ss << sqrt(boost::accumulators::variance(accTmp));
		ss << std::endl;
	}

	return ss.str();
}

const std::string Block::summaryString() const {
	std::stringstream ss;
	ss << "[" << id << "] " << m_name << std::endl;

	double mPT = boost::accumulators::mean(accPropTime);
	double mCT = boost::accumulators::mean(accCompTime);
	ss.precision(5);
	ss << "Avg. proposal, computing, summed times = { " << std::scientific << mPT;
	ss << ", "  << std::scientific << mCT << ", " << std::scientific << mPT + mCT << " }";
	ss << std::endl;
	/*for(unsigned int i=0; i<pIndices.size(); ++i) {
		ss << pIndices[i] << ", ";
	}
	ss << endl;*/

	size_t m_total = m_rej+m_acc;
	if(m_total > 0) {
		ss << "Accepted/(Accepted+Rejected) = " << m_acc << " / " << m_total << " = " << (float)m_acc/(float)m_total;
		if(m_throw > 0) {
			m_total += m_throw;
			ss << "\t||\tThrowed/Selected = " << m_throw << " / " << m_total << " = " << (float)m_throw/(float)m_total;
		}
		ss << std::endl;
	}

	if(hasBlockModifier()){
		if(size() == sqrt(bm->getWindowSize().size())) {
			boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > accTmp;
			for(unsigned int i=0; i<size(); ++i) {
				accTmp(bm->getWindowSize()[i*size()*i]);
			}
			ss << "[Correlated move] Avg element window size = " <<  boost::accumulators::mean(accTmp) << "\t||\t std = ";
			ss << sqrt(boost::accumulators::variance(accTmp));
			ss << std::endl;
		} else {
			ss << "[Correlated move] 1st element window size = " << bm->getWindowSize()[0] << endl;
		}
	} else {
		boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::variance> > accTmp;
		for(unsigned int i=0; i<pIndices.size(); ++i) {
			if(hasBlockModifier(i)){
				accTmp(bms[i]->getWindowSize()[0]);
			}
		}
		ss << "[Indep move] Avg window size = " << boost::accumulators::mean(accTmp) << "\t||\t std = ";
		ss << sqrt(boost::accumulators::variance(accTmp));
		ss << std::endl;
	}

	return ss.str();
}

void Block::accepted(const bool isLocalProposal) const {
	if(hasBlockModifier()){
		getBlockModifier()->moveAccepted(isLocalProposal);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			getBlockModifier(iP)->moveAccepted(isLocalProposal);
		}
	}
	++m_acc;
}

void Block::rejected(const bool isLocalProposal) const {
	if(hasBlockModifier()){
		getBlockModifier()->moveRejected(isLocalProposal);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			getBlockModifier(iP)->moveRejected(isLocalProposal);
		}
	}
	++m_rej;
}

void Block::thrown(const bool isLocalProposal) const {
	if(hasBlockModifier()){
		getBlockModifier()->moveThrown(isLocalProposal);
	} else {
		for(size_t iP=0; iP<pIndices.size(); ++iP){
			getBlockModifier(iP)->moveThrown(isLocalProposal);
		}
	}
	++m_throw;
}

size_t Block::getNAccepted() const {
	return m_acc;
}

size_t Block::getNRejected() const {
	return m_rej;
}

size_t Block::getNThrown() const {
	return m_throw;
}

size_t Block::getNSampled() const {
	return m_acc + m_rej;
}

void Block::setAdaptiveType(adaptiveType_t aAdaptiveType) {
	adaptiveType = aAdaptiveType;
}

Block::adaptiveType_t Block::getAdaptiveType() const {
	return adaptiveType;
}

bool Block::isNotAdaptive() const {
	return adaptiveType == NOT_ADAPTIVE;
}

bool Block::isSingleDouble() const {
	return adaptiveType == SINGLE_DOUBLE_VARIABLE;
}

bool Block::isMixedDouble() const {
	return adaptiveType == MIXED_DOUBLE_VARIABLES;
}

bool Block::isPCADouble() const {
	return adaptiveType == PCA_DOUBLE_VARIABLES;
}

bool Block::isLangevin() const {
	return adaptiveType == LANGEVIN;
}

bool Block::isSMALA() const {
	return adaptiveType == SMALA;
}

bool Block::isParallelLangevin() const {
	return adaptiveType == PARALLEL_LANGEVIN;
}

bool Block::isTreeLangevin() const {
	return adaptiveType == TREE_LANGEVIN;
}

bool Block::isParallelTreeLangevin() const {
	return adaptiveType == PARALLEL_TREE_LANGEVIN;
}

bool Block::isBaseBlock() const {
	return getId() < NB_MAX_BASE_BLOCKS;
}

bool Block::isDynamicBlock() const {
	return !isBaseBlock();
}

void Block::setBlockOptimisation(bool isActive) {
	if(isActive) {
		blockOpti = OPTI_ENABLED;
	} else {
		blockOpti = OPTI_DISABLED;
	}
}

bool Block::isSupportingBlockOptimisation() const {
	return blockOpti != OPTI_DISABLED;
}

void Block::saveState(State::BlockState &state) const {
	state.id = id;
	state.m_acc = m_acc;
	state.m_rej = m_rej;
	state.m_throw = m_throw;

	state.adaptiveType = static_cast<size_t>(adaptiveType);
	state.blockOpti = static_cast<size_t>(blockOpti);

	state.avgPropTime = boost::accumulators::mean(accPropTime);
	state.avgCompTime = boost::accumulators::mean(accCompTime);

	state.rollMeanPropTime = boost::accumulators::rolling_mean(rollMPropTime);
	state.rollMeanCompTime = boost::accumulators::rolling_mean(rollMCompTime);

	state.m_name = m_name;

	state.pIndices = pIndices;
	state.pFreq = pFreq;
	state.totalFreq = totalFreq;

	Support::saveBMState(bm, state.globalBMState);

	state.localBMState.resize(bms.size());
	for(size_t iBM=0; iBM<bms.size(); ++iBM) {
		Support::saveBMState(bms[iBM], state.localBMState[iBM]);
	}
}

void Block::loadState(const State::BlockState &state) {
	id = state.id;
	m_acc = state.m_acc;
	m_rej = state.m_rej;
	m_throw = state.m_throw;

	adaptiveType = static_cast<adaptiveType_t>(state.adaptiveType);
	blockOpti = static_cast<blockOptimisation_t>(state.blockOpti);

	for(size_t i=0; i<getNSampled(); ++i) {
		accPropTime(state.avgPropTime);
		accCompTime(state.avgCompTime);
	}

	for(size_t i=0; i<ROLLING_MEAN_SIZE; ++i) {
		rollMPropTime(state.rollMeanPropTime);
		rollMCompTime(state.rollMeanCompTime);
	}

	m_name = state.m_name;

	pIndices = state.pIndices;
	pFreq = state.pFreq;
	totalFreq = state.totalFreq;

	bms.resize(pIndices.size(), BlockModifier::sharedPtr_t());
}

} // namespace ParameterBlock

