//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Block.h
 *
 *  Created on: 27 sept. 2013
 *      Author: meyerx
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include <stddef.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include "BlockState.h"
#include "Utils/Uncopyable.h"
#include "Sampler/Samples/Sample.h"
#include "BlockModifier/BlockModifierHelper.h"
#include "BlockModifier/BlockModifierInterface.h"
#include "BlockModifier/BlockModifierState.h"


namespace Sampler { class Sample; }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }
namespace boost { namespace accumulators { namespace tag { struct rolling_mean; } } }

namespace ParameterBlock {

namespace State { class BlockState; }

namespace boostAcc = boost::accumulators ;

class Block {
public:
	typedef boost::shared_ptr<Block> sharedPtr_t;

	enum adaptiveType_t {NOT_ADAPTIVE, SINGLE_DOUBLE_VARIABLE, MIXED_DOUBLE_VARIABLES, PCA_DOUBLE_VARIABLES,
		LANGEVIN, SMALA, PARALLEL_LANGEVIN, TREE_LANGEVIN, PARALLEL_TREE_LANGEVIN};
	enum blockOptimisation_t {OPTI_ENABLED, OPTI_DISABLED};
	static const size_t ROLLING_MEAN_SIZE;
	static const size_t NB_MAX_BASE_BLOCKS;
public:
	Block(adaptiveType_t aAdaptiveType=PCA_DOUBLE_VARIABLES, blockOptimisation_t aBlockOpti=OPTI_DISABLED);
	~Block();

	void addParameter(size_t aInd, double freq=1.0);
	void addParameter(size_t aInd, BlockModifier::sharedPtr_t aBM, double freq=1.0);

	int getPIndice(size_t aInd) const;
	const std::vector<size_t>& getPIndices() const;
	const std::vector<double>& getPFreqs() const;
	double getTotalFreq() const;

	void setName(const std::string a_name);
	const std::string getName() const;

	std::vector<double> getTimes() const;
	std::vector<double> getLastTimes() const;
	double getTotalTime() const;

	void proposeMove(Sampler::Sample &sample) const;
	double getMoveProbability(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;
	double getMoveProbabilityRatio(const Sampler::Sample &fromSample, const Sampler::Sample &toSample) const;

	bool hasBlockModifier() const;
	bool hasBlockModifier(const int ind) const;
	void setBlockModifier(BlockModifier::sharedPtr_t aBM);
	void setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM);
	void setBlockModifier(BlockModifier::sharedPtr_t aBM) const;
	void setBlockModifier(const int ind, BlockModifier::sharedPtr_t aBM) const;
	BlockModifier::sharedPtr_t getBlockModifier() const;
	BlockModifier::sharedPtr_t getBlockModifier(const int ind) const;

	void updateMeanTimes(double aPropTime, double aCompTime) const;

	void setId(size_t aId);
	size_t getId() const;

	size_t size() const;

	const std::string toString() const;
	const std::string summaryString() const;

	void accepted(const bool isLocalProposal) const;
	void rejected(const bool isLocalProposal) const;
	void thrown(const bool isLocalProposal) const;

	size_t getNAccepted() const;
	size_t getNRejected() const;
	size_t getNThrown() const;
	size_t getNSampled() const;

	void setAdaptiveType(adaptiveType_t aAdaptiveType);
	adaptiveType_t getAdaptiveType() const;
	bool isNotAdaptive() const;
	bool isSingleDouble() const;
	bool isMixedDouble() const;
	bool isPCADouble() const;
	bool isLangevin() const;
	bool isSMALA() const;
	bool isParallelLangevin() const;
	bool isTreeLangevin() const;
	bool isParallelTreeLangevin() const;

	bool isBaseBlock() const;
	bool isDynamicBlock() const;

	void setBlockOptimisation(bool isActive);
	bool isSupportingBlockOptimisation() const;

	void saveState(State::BlockState &state) const;
	void loadState(const State::BlockState &state);

private:
	mutable size_t m_rej, m_acc, m_throw;
	adaptiveType_t adaptiveType;
	blockOptimisation_t blockOpti;
	size_t id;
	std::string m_name;
	double totalFreq;
	std::vector<size_t> pIndices;
	std::vector<double> pFreq;
	mutable boostAcc::accumulator_set<double, boostAcc::stats<boostAcc::tag::rolling_mean> > rollMPropTime, rollMCompTime;
	mutable boostAcc::accumulator_set<double, boostAcc::stats<boostAcc::tag::mean> > accPropTime, accCompTime;
	mutable BlockModifier::sharedPtr_t bm;
	mutable vector<BlockModifier::sharedPtr_t> bms;
};

struct HasSameBlockID {
	const std::size_t id;
	HasSameBlockID(const size_t aId) : id(aId) { }
	bool operator()(Block::sharedPtr_t const& lhs) const { return lhs->getId() == id; }
};

struct HasBiggerBlockID {
	const std::size_t id;
	HasBiggerBlockID(const size_t aId) : id(aId) { }
	bool operator()(Block::sharedPtr_t const& lhs) const { return lhs->getId() > id; }
};

struct HasSmallerBlockID {
	const std::size_t id;
	HasSmallerBlockID(const size_t aId) : id(aId) { }
	bool operator()(Block::sharedPtr_t const& lhs) const { return lhs->getId() < id; }
};


} // namespace ParameterBlock


#endif /* BLOCK_H_ */

