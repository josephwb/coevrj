# Analyzing datasets with CoevRJ
[Back to main page](../README.md)

## Command line

An analysis with CoevRJ can be directly run using the following command

	> mpirun -np <P> build/CoevRJ -r <nIterations> <myAlignment.fasta> <outputBaseName>

The parameters within `<...>` have the following meaning

* `<P>` defines the number of processors employed and thus the number of parallel chains you are using for MC3. While CoevRJ can well be run with only 1 chain, using more chains (e.g. 4) should help reaching convergence faster.
* `<nIterations>` defines the number of iterations required for this analysis. A sample will be saved each 100th iterations.
* `<myAlignment.fasta>` is the path to your alignment file in fasta format.
* `<outputBaseName>` is the path and basename of your result files. CoevRJ will write numerous files, we thus advise you to create a separate folder (e.g. `results/myRun1_`).

Using one of the simulated datasets provided with the code and using a single chain for 100000 iterations, the command would be

	> mpirun -np 1 build/CoevRJ -r 100000 data/simulations/alignment_50Pairs.fasta results/run_50Pairs_
	
## XML Files

Many more options are avaible such as to configure an analysis with CoevRJ. These options can be provided using [XML configuration files](configXML.md) provided to CoevRJ as argument.

	> mpirun -np <P> build/CoevRJ -x <myConfig.xml>

The parameters within `<...>` have the following meaning

* `<P>` defines the number of processors employed and thus the number of parallel chains you are using for MC3. While CoevRJ can well be run with only 1 chain, using more chains (e.g. 4) should help reaching convergence faster.
* `<myConfig.xml>` is the path to your XML configuration files.
