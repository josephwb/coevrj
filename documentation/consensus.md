# Summarizing the tree distribution
[Back to main page](../README.md)

Obtaining consensus a consensus tree or maximum clade credibility tree can be achieved once the post-processing has been done. For that, you will have to install [sumtrees](http://www.dendropy.org/programs/sumtrees.html?highlight=sumtrees): a python script summarizing tree distributions.
This can be easily done by installing the [dendropy](http://www.dendropy.org) python package on your computer.


Summarize your tree distribution by following these steps:

1. Randomly sample from the `<...>.trees` file that represent that contain the trees distribution. For instance, use the python script `scripts/consensus/drawTreesForConsensus.py`

	> scripts/consensus/drawTreesForConsensus.py <...>.trees sampledTrees.txt N_TREE

	* The first argument is the `<...>.trees` file.
	* The second argument is a file that will be created and will contains the samples.
	* The third argument is the number of samples that you want.
	
2. Use `sumtrees` to analyze the samples
	> sumtrees.py --burnin 0 sampledTrees.txt -o myConsensusTree.nwk
	
	* Discarding samples for the burnin is not required given that the samples are directly drawn from the posterior trees distribution.

