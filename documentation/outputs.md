# CoevRJ outputs ands post-processing
[Back to main page](../README.md)

## Files automatically written by CoevRJ

During its execution, CoevRJ is writing the result of an analysis in multiple files

* `<outputBaseName>_0.log` contains the samples for the base parameters (e.g. parameters of the independent evolutionnary model, branches length). The samples contained in this fiels can be summarized and visualized with [Tracer](http://beast.community/tracer) software.
* `<outputBaseName>_0.logCoev` contains the samples of the parameters related to the pairs of coevolving sites.
* `<outputBaseName>_0.logTrees` contains the samples of the tree topology.
* `<outputBaseName>*_CKP.xml` depending on the options you defined, CoevRJ will write checkpoint files that enables you to continue an analysis by relaunching the same command/XML configuration file.

At the end of an execution, CoevRJ will automatically summarize the `<...>.logCoev` and `<...>.logTrees` files by creating new files

* `<...>.CoevStat` summarizes the posterior probability of significant coevolving pairs with their most probable profile as well as the expected rate parameters of the dependent evolutionary process.
* `<...>.trees` summarizes the tree topologies. Each line represent a tree with first its ID, then its posterior probability and finally its topology in newick format with the expected branches length. 
* `<...>.logCorr` contains the same information as the `<...>.log` file. However the tree parameter in the log file are modified to correspond to the tree ID defined in the `<...>.trees` file.

## Manual post-processing

You can manually use CoevRJ at any point to summarize the `<...>.logCoev` and `<...>.logTrees`. This could become useful whenever you want to have a look at the summarized results while CoevRJ is still analyzing your dataset. 

* Summarizing coevolving pairs samples (`<...>.logCoev` file)

		> mpirun -np 1 ./CoevRJ -c  <burnin> <outputBaseName>

* Summarizing the trees topology samples (`<...>.logTrees` file)

		> mpirun -np 1 ./CoevRJ -t <burnin> <outputBaseName>

	* `<burnin>` is the number of samples to discard for the burnin phase.
	* `<outputBaseName>` is the path and basename of your result files.
* These summaries can also be obtained using [XML configuration files](configXML.md)

		> mpirun -np 1 ./CoevRJ -x <myConfig.xml>
		
## Advanced: bipartitions/splits frequency

If you specified in your [XML configuration file](configXML.md) that you wanted to have split frequencies (`withTreeSplitFreq` set to `true`), several new files will appear after the post-processing of the tree distribution.

* `<...>.split` provides you the biparations frequency (or splits frequency) that is the posterior probability of observing an ancestor in the phylogeny that splits the descendents in two distinct sets (arbitratily identified by `*` and `-` as in MrBayes).
* `<...>.taxa` gives you the mapping between the positions in the split partition string and the taxa name.
* `<...>_.splitFreq` and `<...>.splitCnt` contain matrices displaying the splits frequencies and count respectively as a function of the sample number. These matrices have the split ordered as in the `<...>.split` file for rows and the sample number as column. Plotting the content of these files with matplotlib or R provides a helpful diagnostic on the convergence of the tree topology.

