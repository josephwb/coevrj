# Restart files
[Back to main page](../README.md)

## What is a restart file
Restart files are a convenient way to initialize the first sample of a MCMC analysis with CoevRJ. This file contains the values for the base parameters (GTR+Gamma), the number of coevolving pairs (K) and the positions of these pairs.

Restart files can be used to either tailor by hand a first sample based on prior knowledge of potential coevolving pairs or to create a starting point from the last sample of an existing run.

## Creating restart files

### Extracting them from an existing analysis.
Obtaining a restart file from an existing `CoevRJ` analysis can be easily achieved by using the `scripts/restart/prepareRestart.py` python script.

> python scripts/restart/prepareRestart.py <analysisBaseName>

 This will create a `<analysisBaseName>.restart` in the same folder as the other `log` files.


### Creating them manually

We encourage you to first run `CoevRJ` on your dataset for several iterations and then create a restart file.
This restart file can then be modified to integrate your prior knowledge.

The first line of the file contains (1) the number of parameters and (2) the number of coevolving pairs.

The second line contains the value of the base parameters (see the `<...>.log` header to find the order of parameters). It is not recommanded to change these values manually

The next lines contain the information for each coevolving pairs. The number of lines here must match the number of coevolving pairs specified in the first line.

The parameters for each coevolving pairs are separated by tabulation. E.g.
> 52      53      [TT|GG]

* The two first values are the positions (0-index) of the alignement
* The string represent the profile of coevolution. Pairs of nucleotides are separated by `|` and are contained within square brackets.


## Using restart files
Restart files can be provided within an [XML configuration file](configXML.md) in the *likelihood* section.